/*
 * Copyright (C) 2009 Michael Niedermayer (michaelni@gmx.at)
 *
 * ffvotetov is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License.
 *
 * ffvotetov is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ffvotetov; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <inttypes.h>

#undef NDEBUG
#include <assert.h>


#define CANDIDATES_MAX 100
#define CANDIDATE_LEN 100

char canditates[CANDIDATES_MAX][CANDIDATE_LEN];
int canditate_count;

int sums[CANDIDATES_MAX], subs[CANDIDATES_MAX];
int pair_matrix[CANDIDATES_MAX][CANDIDATES_MAX];
int pair_ranking_matrix[CANDIDATES_MAX][CANDIDATES_MAX];

int64_t cssd_beats[CANDIDATES_MAX][CANDIDATES_MAX];

int (*irv)[CANDIDATES_MAX];
int vote_count;

int contains_loop(const int matrix[CANDIDATES_MAX][CANDIDATES_MAX]){
    int m[CANDIDATES_MAX][CANDIDATES_MAX];
    int s[CANDIDATES_MAX]={0};
    int i,j,changed,ret;

    for(j=0; j<canditate_count; j++){
        for(i=0; i<canditate_count; i++){
            m[i][j] = matrix[i][j] - matrix[j][i];
            s[i] += m[i][j] < 0;
        }
    }

    do{
        changed=0;
        ret=0;
        for(i=0; i<canditate_count; i++){
            if(!s[i]){
                s[i]--;
                changed=1;
                for(j=0; j<canditate_count; j++){
                    s[j] -= (m[i][j] < 0) + (m[j][i] < 0);
                    m[i][j]= m[j][i]=0;
                }
            }else if(s[i]>0)
                ret=1;
        }
    }while(changed);

    return ret;
}

void print_condorcet_winners(const int matrix[CANDIDATES_MAX][CANDIDATES_MAX], int limit){
    int j,k;

    for(j=0; j<canditate_count; j++){
        for(k=0; k<canditate_count; k++){
            if(matrix[j][k] < matrix[k][j] - limit)
                break;
        }
        if(k==canditate_count)
            printf("    %s\n", canditates[j]);
    }
}

void beatpath(int64_t strength[CANDIDATES_MAX], const int matrix[CANDIDATES_MAX][CANDIDATES_MAX], int a){
    int i, j, changed;

    for(i=0; i<canditate_count; i++)
        strength[i]= matrix[a][i] > matrix[i][a] ? (matrix[a][i]<<24) - matrix[i][a] : 0;

    do{
        changed=0;
        for(i=0; i<canditate_count; i++){
            for(j=0; j<canditate_count; j++){
                int64_t diff= matrix[i][j] > matrix[j][i] ? (matrix[i][j]<<24) - matrix[j][i]: 0;
                if(strength[i] < diff)
                    diff= strength[i];
                if(strength[j] < diff){
                    strength[j]= diff;
                    changed=1;
                }
            }
        }
    }while(changed);
}

void ranked_pairs(int pair_ranking_matrix[CANDIDATES_MAX][CANDIDATES_MAX]){
    int j, k;
    int64_t limit;

    for(limit=INT_MAX; limit>1; ){
        int64_t max=1; 
        for(j=0; j<canditate_count; j++){
            for(k=0; k<canditate_count; k++){
                int64_t diff= ((int64_t)pair_matrix[j][k]<<24) - pair_matrix[k][j];
                if(diff > max && diff < limit) max=diff;
            }
        }
        limit=max;
        for(j=0; j<canditate_count; j++){
            for(k=0; k<canditate_count; k++){
                int64_t diff= ((int64_t)pair_matrix[j][k]<<24) - pair_matrix[k][j];
                if(diff==max){
                    pair_ranking_matrix[j][k] = pair_matrix[j][k];
                    pair_ranking_matrix[k][j] = pair_matrix[k][j];
                }
            }
        }
        if(contains_loop(pair_ranking_matrix)){
            for(j=0; j<canditate_count; j++){
                for(k=0; k<canditate_count; k++){
                    int64_t diff= ((int64_t)pair_matrix[j][k]<<24) - pair_matrix[k][j];
                    if(diff==max){
                        fprintf(stderr, "  Cycle, ignoring pair %d %d with score of %lld\n", j, k, max);
                        pair_ranking_matrix[j][k] =
                        pair_ranking_matrix[k][j] = 0;
                    }
                }
            }
        }
    }
}

void do_condercets(void){
    int i,j,k;
    int64_t max;

    memset(pair_ranking_matrix, 0, sizeof(pair_ranking_matrix));

    printf("  Pair table:\n");
    for(j=0; j<canditate_count; j++){
        for(k=0; k<canditate_count; k++){
            printf("%4d", pair_matrix[j][k]);
        }
        printf("\n");
    }

    printf("  Condorcet winner(s):\n");
    print_condorcet_winners(pair_matrix, 0);

    printf("  minimax winner(s):\n");
    max= INT_MIN;
    for(j=0; j<canditate_count; j++){
        int min= INT_MAX;
        for(k=0; k<canditate_count; k++){
            int diff= pair_matrix[j][k] - pair_matrix[k][j];
            if(diff < min) min=diff;
        }
        if(min>max) max= min;
    }
    print_condorcet_winners(pair_matrix, -max);

    printf("  nameless? sum:\n");
    max= INT64_MIN;
    for(i=0; i<2; i++){
        for(j=0; j<canditate_count; j++){
            int64_t sum=0;
            for(k=0; k<canditate_count; k++){
                if(pair_matrix[j][k] < pair_matrix[k][j]){
                    sum-= (int64_t)pair_matrix[k][j]<<24;
                }else
                    sum+= pair_matrix[j][k];
            }
            if(sum>max) max= sum;
            if(sum == max && i)
                printf("    %s\n", canditates[j]);
        }
    }
    ranked_pairs(pair_ranking_matrix);
    printf("  Ranked pairs winner(s):\n");
    print_condorcet_winners(pair_ranking_matrix, 0);

    printf("  Cloneproof schwartz sequential droping / beatpath winner(s):\n");
    for(i=0; i<canditate_count; i++)
        beatpath(cssd_beats[i], pair_matrix, i);
    for(i=0; i<canditate_count; i++){
        for(j=0; j<canditate_count; j++){
            if(cssd_beats[i][j] < cssd_beats[j][i])
                break;
        }
        if(j==canditate_count)
            printf("    %s\n", canditates[i]);
    }
}

void print_irv(void){
    int droped_canditates[CANDIDATES_MAX]={0};
    int i, j;

    printf("Instant runoff winners:\n");
    for(;;){
        int votes[CANDIDATES_MAX]={0};
        int votes2[CANDIDATES_MAX]={0};
        int min= INT_MAX;
        int non_min= 0;

        for(i=0; i<vote_count; i++){
            for(j=0; j<canditate_count && irv[i][j] && droped_canditates[irv[i][j]-1]; j++);
            if(irv[i][j])
                votes[ irv[i][j]-1 ]++;
        }
        for(i=0; i<canditate_count; i++){
            fprintf(stderr, "%3d", votes[i]);
            if(!droped_canditates[i] && votes[i] < min)
                min=votes[i];
        }

        for(i=0; i<vote_count; i++){
            for(j=0; j<canditate_count && irv[i][j] && droped_canditates[irv[i][j]-1]; j++);
            if(j<canditate_count && irv[i][j] && votes[ irv[i][j]-1 ]== min)
                j++;
            for(; j<canditate_count && irv[i][j] && droped_canditates[irv[i][j]-1]; j++);
            if(irv[i][j])
                votes2[ irv[i][j]-1 ]++;
        }
        min= INT_MAX;
        for(i=0; i<canditate_count; i++){
            fprintf(stderr, "%3d", votes2[i]);
            if(!droped_canditates[i]){
                if(min != INT_MAX && votes2[i] != min)
                    non_min= 1;
                if(votes2[i] <= min)
                    min=votes2[i];
            }
        }
        fprintf(stderr, "\n");
        if(!non_min)
            break;
        for(i=0; i<canditate_count; i++){
            if(votes2[i] == min)
                droped_canditates[i]=1;
        }
    }
    for(i=0; i<canditate_count; i++)
        if(!droped_canditates[i])
            printf("  %s\n",  canditates[i]);
}

void add_vote(int vote[CANDIDATES_MAX]){
    int j,k;

    irv= realloc(irv, sizeof(*irv)*(vote_count+1));
    if(!irv){
        fprintf(stderr, "out of memory\n");
        exit(1);
    }
    for(j=0; j<CANDIDATES_MAX; j++)
        irv[vote_count][j]=0;

    for(j=0; j<CANDIDATES_MAX; j++){
        if(vote[j]>0 && vote[j] <=10){
            sums[j] -= vote[j];
            subs[j] ++;
        }
        for(k=j+1; k<CANDIDATES_MAX; k++){
            int score_j= vote[j] ? vote[j] : CANDIDATES_MAX;
            int score_k= vote[k] ? vote[k] : CANDIDATES_MAX;
            if(score_j < score_k)
                pair_matrix[j][k]++;
            if(score_j > score_k)
                pair_matrix[k][j]++;
        }
        if(vote[j]>0 && vote[j]<=CANDIDATES_MAX)
            irv[vote_count][ vote[j] - 1 ]= j+1;
    }
    vote_count++;

    for(j=0; j<CANDIDATES_MAX; j++)
        vote[j]=0;
}

int main(){
    int j, k, num, cand_idx;
    char line[1000], *p;
    int vote[CANDIDATES_MAX]={0};

    //very lame parser below, this is not robust nor tamper proof,only use with manually checked input!
    while(!feof(stdin)){
        scanf("%999[^\n]\n", line);
        num= strtol(line, &p, 0);
        if(num>0){
            p+= strspn(p, " .:,;()[]{}=-_#~|");
            for(cand_idx=0; cand_idx<canditate_count; cand_idx++)
                if(!strcasecmp(canditates[cand_idx], p))
                    break;
            if(cand_idx==canditate_count){
                if(canditate_count==CANDIDATES_MAX){
                    fprintf(stderr, "Remove cadidate (try russian roulette if you have no fanatasy) or increase CANDIDATES_MAX\n");
                    exit(1);
                }
                strncpy(canditates[canditate_count++], p, CANDIDATE_LEN-1);
            }
            vote[cand_idx]= num;
            fprintf(stderr, "Parsed as:%d %s\n", num, p);
        }else
            add_vote(vote);
    }
    add_vote(vote);

    printf("Candidates:\n");
    for(cand_idx=0; cand_idx<canditate_count; cand_idx++)
        printf(" Borda:%3d %3d\"%s\"\n", sums[cand_idx] + subs[cand_idx]*canditate_count, sums[cand_idx] + 10*subs[cand_idx], canditates[cand_idx]);

    print_irv();

    printf("Condorcet methods based on votes:\n");
    do_condercets();

    for(j=0; j<canditate_count; j++){
        for(k=j+1; k<canditate_count; k++){
            pair_matrix[j][k] -= pair_matrix[k][j];
            pair_matrix[k][j] = -pair_matrix[j][k];
        }
    }

    printf("Condorcet methods based on margins:\n");
    do_condercets();


    return 0;
}