/*
 * Copyright (c) 2007 Michael Niedermayer <michaelni@gmx.at>
 *
 * mnzip is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * mnzip is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with mnzip; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 * Note everything in this file except the state_table can be used under LGPL
 */

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <limits.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <errno.h>


#undef NDEBUG
#include <assert.h>

#define RADIX_PASSES 4


#define MAX(a,b) ((a) > (b) ? (a) : (b))
#define MIN(a,b) ((a) > (b) ? (b) : (a))
#define ABS(a) ((a) >= 0 ? (a) : -(a))
#define FFSWAP(type,a,b) do{type SWAP_tmp= b; b= a; a= SWAP_tmp;}while(0)

//---------------- Range coder taken from libavcodec copied here to avoid a nasty dependancy
typedef struct RangeCoder{
    int low;
    int range;
    int outstanding_count;
    int outstanding_byte;
    uint8_t *bytestream_start;
    uint8_t *bytestream;
    uint8_t *bytestream_end;
}RangeCoder;

static inline void renorm_encoder(RangeCoder *c){
    //FIXME optimize
    while(c->range < 0x100){
        if(c->outstanding_byte < 0){
            c->outstanding_byte= c->low>>8;
        }else if(c->low <= 0xFF00){
            *c->bytestream++ = c->outstanding_byte;
            for(;c->outstanding_count; c->outstanding_count--)
                *c->bytestream++ = 0xFF;
            c->outstanding_byte= c->low>>8;
        }else if(c->low >= 0x10000){
            *c->bytestream++ = c->outstanding_byte + 1;
            for(;c->outstanding_count; c->outstanding_count--)
                *c->bytestream++ = 0x00;
            c->outstanding_byte= (c->low>>8) & 0xFF;
        }else{
            c->outstanding_count++;
        }

        c->low = (c->low & 0xFF)<<8;
        c->range <<= 8;
    }
}

static inline void put_rac(RangeCoder *c, uint8_t prob, int bit){
    int range1= (c->range * prob) >> 8;

    assert(prob);
    assert(range1 < c->range);
    assert(range1 > 0);
    if(!bit){
        c->range -= range1;
    }else{
        c->low += c->range - range1;
        c->range = range1;
    }

    renorm_encoder(c);
}

static inline void refill(RangeCoder *c){
    if(c->range < 0x100){
        c->range <<= 8;
        c->low <<= 8;
        if(c->bytestream < c->bytestream_end)
            c->low+= c->bytestream[0];
        c->bytestream++;
    }
}

static inline int get_rac(RangeCoder *c, uint8_t prob){
    int range1= (c->range * prob) >> 8;

    c->range -= range1;

    if(c->low < c->range){
        refill(c);
        return 0;
    }else{
        c->low -= c->range;
        c->range = range1;
        refill(c);
        return 1;
    }
}

void ff_init_range_encoder(RangeCoder *c, uint8_t *buf, int buf_size){
    c->bytestream_start=
    c->bytestream= buf;
    c->bytestream_end= buf + buf_size;

    c->low= 0;
    c->range= 0xFF00;
    c->outstanding_count= 0;
    c->outstanding_byte= -1;
}

void ff_init_range_decoder(RangeCoder *c, const uint8_t *buf, int buf_size){
    /* cast to avoid compiler warning */
    ff_init_range_encoder(c, (uint8_t *) buf, buf_size);

    c->low = (c->bytestream[0]<<8) + c->bytestream[1];
    c->bytestream+=2;
}

/**
 *
 * @return the number of bytes written
 */
int ff_rac_terminate(RangeCoder *c){
    c->range=0xFF;
    c->low +=0xFF;
    renorm_encoder(c);
    c->range=0xFF;
    renorm_encoder(c);

    assert(c->low   == 0);
    assert(c->range >= 0x100);

    return c->bytestream - c->bytestream_start;
}

//--------------------------------------------------


static int pretransform(uint8_t *in, int len){
    int step, i, beststep;
    double bestsum= INT_MAX;

    for(step=0; step<=16; step++){
        int hist[256]={0};
        double sum=0;
        for(i=0; i<len; i++){
            uint8_t v= in[i];
            if(step && i>=step)
                v-= in[i-step];

            hist[v]++;
        }
        for(i=0; i<256; i++){
            if(hist[i])
                sum += hist[i] * log2(len / (double)hist[i]);
        }
//        fprintf(stderr, "step:%d sum:%f\n", step, sum);

        if(sum < bestsum){
            bestsum= sum;
            beststep= step;
        }
    }

    if(beststep)
        for(i=len-1; i>=beststep; i--)
            in[i] -= in[i-beststep];

    return beststep;
}

static void inv_pretransform(uint8_t *in, uint8_t *tmp, int len, int step){
    int i;

    for(i=0; i<step; i++)
        in[i] += tmp[i];
    for(i=step; i<len; i++)
        in[i] += in[i-step];
    for(i=0; i<step; i++)
        tmp[i] = in[len-step+i];
}

static void qsort2(uint8_t **ptr, int *idx, int len){
    int pivot;
    int i=0;
    int j=0;
    int k=len-1;

    assert(len>1);

    if(len<10){
        for(j=len-1; j>0; j--){
            for(i=0; i<j; i++){
                if(idx[i] > idx[i+1]){
                    FFSWAP(int     , idx[i], idx[i+1]);
                    FFSWAP(uint8_t*, ptr[i], ptr[i+1]);
                }
            }
        }
        return;
    }

    pivot= idx[len>>1];
    if(idx[0] < pivot){
        if     (idx[len-1] < idx[0]) pivot= idx[0];
        else if(idx[len-1] < pivot ) pivot= idx[len-1];
    }else{
        if     (idx[len-1] > idx[0]) pivot= idx[0];
        else if(idx[len-1] > pivot ) pivot= idx[len-1];
    }
    i=0;
    while(idx[k] > pivot)
        k--;
    for(;j<=k; j++){
        int v= idx[j];
        if(v<pivot){
            FFSWAP(int, idx[j], idx[i]);
            FFSWAP(uint8_t*, ptr[j], ptr[i]);
            i++;
        }else if(v>pivot){
            int v2= idx[k];
            uint8_t *p2= ptr[k];
            idx[k]= v;
            ptr[k]= ptr[j];
            if(v2 == pivot){
                idx[j]= v2;
                ptr[j]= p2;
            }else{
                idx[j]= idx[i];
                ptr[j]= ptr[i];
                idx[i]= v2;
                ptr[i]= p2;
                i++;
            }
            do{
                k--;
            }while(idx[k] > pivot);
        }
    }
    assert(i>=0);
    assert(i<=j);
    assert(j<=k+1);
    assert(k<=len-1);
    if(i>1)
        qsort2(ptr  ,idx  , i      );
    if(len-k>2)
        qsort2(ptr+j,idx+j, len-1-k);
}

#define read(p) ((p)>=end ? (p)[in - end] : *(p))

static unsigned int bwt(uint8_t *out, uint8_t *in, unsigned int len){
    unsigned int i, j, ret;
    uint8_t **ptr = malloc(len*sizeof(uint8_t*));
    uint8_t **ptr2= malloc(len*sizeof(uint8_t*));
    int histogram[257];
    int last= len-1;
    int last2= len;
    int sorted;
    int *idx= ptr2;
    int *idx2;

    if(!ptr || !ptr2 || len >= UINT_MAX / sizeof(uint8_t*)) //FIXME memleak
        return -1;

    memset(histogram, 0, sizeof(histogram));
    for(i=0; i<len; i++)
        histogram[ in[i] + 1 ]++;

    for(i=1; i<257; i++)
        histogram[i] += histogram[i-1];

    for(i=0; i<RADIX_PASSES; i++)
        in[len+i]= in[i];

    for(i=0; i<len; i++){
        ptr2[ histogram[in[i+4]]++ ]= in+i;
    }

    memmove(histogram+1, histogram, sizeof(int)*256); //FIXME simplify
    histogram[0]= 0;
    for(i=0; i<len; i++){
        ptr[ histogram[ptr2[i][3]]++ ]= ptr2[i];
    }

    memmove(histogram+1, histogram, sizeof(int)*256); //FIXME simplify
    histogram[0]= 0;
    for(i=0; i<len; i++){
        ptr2[ histogram[ptr[i][2]]++ ]= ptr[i];
    }

    memmove(histogram+1, histogram, sizeof(int)*256); //FIXME simplify
    histogram[0]= 0;
    for(i=0; i<len; i++){
        ptr[ histogram[ptr2[i][1]]++ ]= ptr2[i];
    }
fprintf(stderr, "radix sort done\n");

    idx2= malloc(len*sizeof(int));
    for(i=len-1; (int)i>=0; i--){
        if(ptr[last][1] != ptr[i][1] || ptr[last][2] != ptr[i][2] ||
           ptr[last][3] != ptr[i][3] || ptr[last][4] != ptr[i][4]){

            if(last - i > 1)
                last2= i+1;
            else{
                ptr[last]= in-last2;
                assert(in - ptr[last] > last);
                assert(last2 <= len);
            }

            last= i;
        }
        idx[ptr[i]-in]= last;
    }

    if(last - i == 1){
        ptr[last]= in-last2;
        assert(in - ptr[last] > last);
    }

fprintf(stderr, "idx init done\n");
    for(sorted=RADIX_PASSES; sorted<len; sorted<<=1){
//fprintf(stderr, "pass %d\n", sorted);

        i=0;
        for(;;){
            int right;
//             assert(i < len+1);
//             assert(i>=0);

            if(i >= len)
                break;

            if(ptr[i] - in < 0){
                int openlink= i;
                do{
                    i = in - ptr[i];
                }while(i < len && ptr[i] - in < 0);
                ptr[openlink]= in - i;
                if(i >= len)
                    break;
            }

            right= idx[ptr[i]-in];
#define limit(i) ((i)>=len ? (i) - len : (i))
            assert(right-(int)i+1 > 1);
            for(j=i; j<=right; j++){
                idx2[j-i]= idx[limit(ptr[j] - in + sorted)];
//                assert(idx[ptr[j] - in] == idx[ptr[i] - in]);
            }
            qsort2(ptr + i, idx2, right-i+1);

            last= right;
            assert(idx[ptr[right]-in] == right);
            last2= right+1;
            for(j=right-1; (int)j>=(int)i; j--){
                if(idx2[j-i] < idx2[j-i+1]){
                    assert(last - j >= 1);
                    if(last - j > 1){
                        last2= j+1;
                    }else{
                        ptr[last]= in - last2;
                    }
                    last=j;
                }
//                assert(ptr[j]-in >= 0);
                assert(idx[ptr[j]-in] >= last);
                idx[ptr[j]-in]= last;
            }
            if(last - j == 1)
                ptr[last]= in - last2;

            i= right + 1;
        }
    }
    for(i=0; i<len; i++){
        out[idx[i]]= in[i];
    }
    ret= idx[0];

    free(ptr);
    free(ptr2);
    return ret;
}

/*
                     3,0 (48) ->  35
    2,2 (40) ->  26
    2,3 (36) ->  31
    2,4 (34) ->  37
                     2,5 (33) ->  44
    3,1 (32) ->  42
    3,2 (24) ->  51  4,1 (24) ->  76
    3,3 (20) ->  62  5,1 (20) -> 166
    3,4 (18) ->  75
    3,5 (17) ->  90
    4,2 (16) ->  93
    4,3 (12) -> 115  5,2 (12) -> 199
    4,4 (10) -> 141
    4,5 ( 9) -> 172
    5,3 ( 8) -> 239
    5,4 ( 6) -> 289
    5,5 ( 5) -> 348
    6,4 ( 4) -> 580
    6,5 ( 3) -> 689
*/

static int ibwt(uint8_t *in, FILE *fo, unsigned int len, unsigned int start, unsigned int histogram[256], int low_mem, int pretype){
    int shift= low_mem*3;
    int shift2= shift + 8;
    int mask= (1<<shift)-1;
    unsigned int i, j;
    unsigned int int_histogram[257], int_histogram2[257];
    uint8_t buffer[1024*32];
    unsigned idx_len = len>>shift;
    unsigned idx2_len= len>>shift2;
    uint32_t *idx= malloc(idx_len*sizeof(uint32_t));
    uint8_t *idx2=malloc(idx2_len*256*sizeof(uint8_t));
    int packed= idx_len <= 256*256*256 && !shift;
    uint8_t pret_buf[16]={0};

    if(!idx || idx_len >= UINT32_MAX / sizeof(uint32_t)) //FIXME memleak
        return -1;

    if(start >= len)
        return -1;

    int_histogram[0]= 0;
    for(i=1; i<257; i++)
        int_histogram[i]= int_histogram[i-1]+histogram[i-1];
    memcpy(int_histogram2, int_histogram, sizeof(int_histogram)); //FIXME remove

    for(i=0; i<len; i++){
        int j= int_histogram2[ in[i] ]++;
        if(!(j&mask)){
            idx[j>>shift]= i;
            if(packed)
                idx[j>>shift]|= in[j]<<24;
            idx2[ idx2_len*in[i] + (i>>shift2)]= 1;
        }else
            idx2[ idx2_len*in[i] + (i>>shift2)]++;
    }

    for(i=0; i<len; i+=sizeof(buffer)){
        unsigned int len2= MIN(sizeof(buffer), len-i);
        for(j=0; j<len2; j++){
            int skip= start&mask;
            unsigned int target= start;
            if(packed){
                buffer[j]= idx[start>>shift]>>24;
                start= idx[start>>shift] & 0xFFFFFF;
            }else{
                buffer[j]= in[start];
                start= idx[start>>shift];
            }
            if(skip){
                int byte= in[start];
                unsigned int next= idx[(target + mask)>>shift];

//                assert(int_histogram2[byte] > target - skip);
                while(int_histogram2[byte] <= target){
                    start=0;
                    skip= target - int_histogram2[byte++];
//                    assert(target - int_histogram2[byte-1] <= mask);
//                    assert(skip >= 0);
                }

                while((target + mask)>>shift < idx_len && next > ((start>>shift2)+1)<<shift2 && 0 ){ //FIXME buggy, works with one but not the second file
                    unsigned int i2= idx2[ idx2_len*byte + (start>>shift2)];

                    if(i2 > skip)
                        break;
                    if(start==0 && i2) //FIXME figure out why this is needed
                        break;

                    start=((start>>shift2)+1)<<shift2;
                    skip-= i2;
                }

                for(; start<len; start++){
                    if(in[start] == byte && !skip--)
                        break;
                }
            }
        }
        if(pretype)
            inv_pretransform(buffer, pret_buf, len2, pretype);

        fwrite(buffer, len2, 1, fo);
    }

    free(idx);
    return 0;
}

//from paq8l.cpp GPL
static const uint8_t state_table[256][4]={
  {  1,  2, 0, 0},{  3,  5, 1, 0},{  4,  6, 0, 1},{  7, 10, 2, 0}, // 0-3
  {  8, 12, 1, 1},{  9, 13, 1, 1},{ 11, 14, 0, 2},{ 15, 19, 3, 0}, // 4-7
  { 16, 23, 2, 1},{ 17, 24, 2, 1},{ 18, 25, 2, 1},{ 20, 27, 1, 2}, // 8-11
  { 21, 28, 1, 2},{ 22, 29, 1, 2},{ 26, 30, 0, 3},{ 31, 33, 4, 0}, // 12-15
  { 32, 35, 3, 1},{ 32, 35, 3, 1},{ 32, 35, 3, 1},{ 32, 35, 3, 1}, // 16-19
  { 34, 37, 2, 2},{ 34, 37, 2, 2},{ 34, 37, 2, 2},{ 34, 37, 2, 2}, // 20-23
  { 34, 37, 2, 2},{ 34, 37, 2, 2},{ 36, 39, 1, 3},{ 36, 39, 1, 3}, // 24-27
  { 36, 39, 1, 3},{ 36, 39, 1, 3},{ 38, 40, 0, 4},{ 41, 43, 5, 0}, // 28-31
  { 42, 45, 4, 1},{ 42, 45, 4, 1},{ 44, 47, 3, 2},{ 44, 47, 3, 2}, // 32-35
  { 46, 49, 2, 3},{ 46, 49, 2, 3},{ 48, 51, 1, 4},{ 48, 51, 1, 4}, // 36-39
  { 50, 52, 0, 5},{ 53, 43, 6, 0},{ 54, 57, 5, 1},{ 54, 57, 5, 1}, // 40-43
  { 56, 59, 4, 2},{ 56, 59, 4, 2},{ 58, 61, 3, 3},{ 58, 61, 3, 3}, // 44-47
  { 60, 63, 2, 4},{ 60, 63, 2, 4},{ 62, 65, 1, 5},{ 62, 65, 1, 5}, // 48-51
  { 50, 66, 0, 6},{ 67, 55, 7, 0},{ 68, 57, 6, 1},{ 68, 57, 6, 1}, // 52-55
  { 70, 73, 5, 2},{ 70, 73, 5, 2},{ 72, 75, 4, 3},{ 72, 75, 4, 3}, // 56-59
  { 74, 77, 3, 4},{ 74, 77, 3, 4},{ 76, 79, 2, 5},{ 76, 79, 2, 5}, // 60-63
  { 62, 81, 1, 6},{ 62, 81, 1, 6},{ 64, 82, 0, 7},{ 83, 69, 8, 0}, // 64-67
  { 84, 71, 7, 1},{ 84, 71, 7, 1},{ 86, 73, 6, 2},{ 86, 73, 6, 2}, // 68-71
  { 44, 59, 5, 3},{ 44, 59, 5, 3},{ 58, 61, 4, 4},{ 58, 61, 4, 4}, // 72-75
  { 60, 49, 3, 5},{ 60, 49, 3, 5},{ 76, 89, 2, 6},{ 76, 89, 2, 6}, // 76-79
  { 78, 91, 1, 7},{ 78, 91, 1, 7},{ 80, 92, 0, 8},{ 93, 69, 9, 0}, // 80-83
  { 94, 87, 8, 1},{ 94, 87, 8, 1},{ 96, 45, 7, 2},{ 96, 45, 7, 2}, // 84-87
  { 48, 99, 2, 7},{ 48, 99, 2, 7},{ 88,101, 1, 8},{ 88,101, 1, 8}, // 88-91
  { 80,102, 0, 9},{103, 69,10, 0},{104, 87, 9, 1},{104, 87, 9, 1}, // 92-95
  {106, 57, 8, 2},{106, 57, 8, 2},{ 62,109, 2, 8},{ 62,109, 2, 8}, // 96-99
  { 88,111, 1, 9},{ 88,111, 1, 9},{ 80,112, 0,10},{113, 85,11, 0}, // 100-103
  {114, 87,10, 1},{114, 87,10, 1},{116, 57, 9, 2},{116, 57, 9, 2}, // 104-107
  { 62,119, 2, 9},{ 62,119, 2, 9},{ 88,121, 1,10},{ 88,121, 1,10}, // 108-111
  { 90,122, 0,11},{123, 85,12, 0},{124, 97,11, 1},{124, 97,11, 1}, // 112-115
  {126, 57,10, 2},{126, 57,10, 2},{ 62,129, 2,10},{ 62,129, 2,10}, // 116-119
  { 98,131, 1,11},{ 98,131, 1,11},{ 90,132, 0,12},{133, 85,13, 0}, // 120-123
  {134, 97,12, 1},{134, 97,12, 1},{136, 57,11, 2},{136, 57,11, 2}, // 124-127
  { 62,139, 2,11},{ 62,139, 2,11},{ 98,141, 1,12},{ 98,141, 1,12}, // 128-131
  { 90,142, 0,13},{143, 95,14, 0},{144, 97,13, 1},{144, 97,13, 1}, // 132-135
  { 68, 57,12, 2},{ 68, 57,12, 2},{ 62, 81, 2,12},{ 62, 81, 2,12}, // 136-139
  { 98,147, 1,13},{ 98,147, 1,13},{100,148, 0,14},{149, 95,15, 0}, // 140-143
  {150,107,14, 1},{150,107,14, 1},{108,151, 1,14},{108,151, 1,14}, // 144-147
  {100,152, 0,15},{153, 95,16, 0},{154,107,15, 1},{108,155, 1,15}, // 148-151
  {100,156, 0,16},{157, 95,17, 0},{158,107,16, 1},{108,159, 1,16}, // 152-155
  {100,160, 0,17},{161,105,18, 0},{162,107,17, 1},{108,163, 1,17}, // 156-159
  {110,164, 0,18},{165,105,19, 0},{166,117,18, 1},{118,167, 1,18}, // 160-163
  {110,168, 0,19},{169,105,20, 0},{170,117,19, 1},{118,171, 1,19}, // 164-167
  {110,172, 0,20},{173,105,21, 0},{174,117,20, 1},{118,175, 1,20}, // 168-171
  {110,176, 0,21},{177,105,22, 0},{178,117,21, 1},{118,179, 1,21}, // 172-175
  {110,180, 0,22},{181,115,23, 0},{182,117,22, 1},{118,183, 1,22}, // 176-179
  {120,184, 0,23},{185,115,24, 0},{186,127,23, 1},{128,187, 1,23}, // 180-183
  {120,188, 0,24},{189,115,25, 0},{190,127,24, 1},{128,191, 1,24}, // 184-187
  {120,192, 0,25},{193,115,26, 0},{194,127,25, 1},{128,195, 1,25}, // 188-191
  {120,196, 0,26},{197,115,27, 0},{198,127,26, 1},{128,199, 1,26}, // 192-195
  {120,200, 0,27},{201,115,28, 0},{202,127,27, 1},{128,203, 1,27}, // 196-199
  {120,204, 0,28},{205,115,29, 0},{206,127,28, 1},{128,207, 1,28}, // 200-203
  {120,208, 0,29},{209,125,30, 0},{210,127,29, 1},{128,211, 1,29}, // 204-207
  {130,212, 0,30},{213,125,31, 0},{214,137,30, 1},{138,215, 1,30}, // 208-211
  {130,216, 0,31},{217,125,32, 0},{218,137,31, 1},{138,219, 1,31}, // 212-215
  {130,220, 0,32},{221,125,33, 0},{222,137,32, 1},{138,223, 1,32}, // 216-219
  {130,224, 0,33},{225,125,34, 0},{226,137,33, 1},{138,227, 1,33}, // 220-223
  {130,228, 0,34},{229,125,35, 0},{230,137,34, 1},{138,231, 1,34}, // 224-227
  {130,232, 0,35},{233,125,36, 0},{234,137,35, 1},{138,235, 1,35}, // 228-231
  {130,236, 0,36},{237,125,37, 0},{238,137,36, 1},{138,239, 1,36}, // 232-235
  {130,240, 0,37},{241,125,38, 0},{242,137,37, 1},{138,243, 1,37}, // 236-239
  {130,244, 0,38},{245,135,39, 0},{246,137,38, 1},{138,247, 1,38}, // 240-243
  {140,248, 0,39},{249,135,40, 0},{250, 69,39, 1},{ 80,251, 1,39}, // 244-247
  {140,252, 0,40},{249,135,41, 0},{250, 69,40, 1},{ 80,251, 1,40}, // 248-251
  {140,252, 0,41}};  // 252, 253-255 are reserved

#if 1
static inline int clip(int a, int amin, int amax){
    if (a < amin)      return amin;
    else if (a > amax) return amax;
    else               return a;
}
#endif

static unsigned short prob[16][256];

static void init_prob(unsigned short *prob){
    int i;
    for(i=0;i<256*16; i++){
        int ctx= i&0xFF;
        prob[i]= 256*256*(state_table[ctx][3]+1) / (state_table[ctx][2] + state_table[ctx][3]+2);
    }
}

static inline void putbit(RangeCoder *c, uint8_t *state, int b, unsigned short *prob){
    int ctx= *state;
    int p8= (prob[ctx] + 128)>>8;

    put_rac(c, clip(p8, 1, 255), b);
    prob[ctx]+= b*256 - p8;

    *state= state_table[ctx][b];
}

static inline int getbit(RangeCoder *c, uint8_t *state, unsigned short *prob){
    int ctx= *state;
    int p8= (prob[ctx] + 128)>>8;

    int b= get_rac(c, clip(p8, 1, 255));
    prob[ctx]+= b*256 - p8;

    *state= state_table[ctx][b];

    return b;
}

static inline void put_symbol_255(RangeCoder *c, uint8_t *state, int v){
    const static uint8_t log2_tab[16]={0,0,1,1,2,2,2,2,3,3,3,3,3,3,3,3};
    int e, i;

    putbit(c, state+14,  v==0, prob[14]);  //14
    if(!v)
        return;

    if(v<16) e=   log2_tab[v   ];
    else     e= 4+log2_tab[v>>4];

    putbit(c, state         ,  e>3    , prob[0]);  //0
    putbit(c, state+1+(e>3) , (e&2)>>1, prob[1+(e>3)]);  //1..2
    putbit(c, state+3+(e>>1), (e&1)   , prob[3+(e>>1)]);  //3..6

    for(i=e-1; i>=0; i--)
        putbit(c, state+7+i, (v>>i)&1 , prob[7+i]); //7..13
}

static inline int get_symbol_255(RangeCoder *c, uint8_t *state){
    int e,a,i;

    if(getbit(c, state+14, prob[14]))
        return 0;

    e =4*getbit(c, state         , prob[0]);  //0
    e+=2*getbit(c, state+1+(e>3) , prob[1+(e>3)]);  //1..2
    e+=  getbit(c, state+3+(e>>1), prob[3+(e>>1)]);  //3..6

    a= 1;
    for(i=e-1; i>=0; i--)
        a += a + getbit(c, state+7 + i, prob[7+i]); //7..13

    return a;
}

static inline int rb32(uint8_t *p){
    return (p[0]<<24) + (p[1]<<16) + (p[2]<<8) + p[3];
}

static inline void wb32(uint8_t *p, int v){
    p[0]= v>>24;
    p[1]= v>>16;
    p[2]= v>>8;
    p[3]= v;
}

static unsigned int compress(uint8_t *out, uint8_t *in, unsigned int len){
    uint8_t *tmp= malloc(len);
    unsigned int start,j, out_size, pretype;
    int i;
    RangeCoder c;
    uint8_t state[16*512*32+1];
    int mtf[256];

fprintf(stderr, "pretransform\n");
    pretype= pretransform(in, len);

fprintf(stderr, "bwt\n");
    start= bwt(tmp, in, len);
fprintf(stderr," done\n");

fprintf(stderr,"range coding (%d %d)\n", len, start);
    wb32(out, pretype); out+=4;
    wb32(out, len  ); out+=4;
    wb32(out, start); out+=4;
    wb32(out, 0    ); out+=4;

    memset(state, 0, sizeof(state));
//memset(state, 3, sizeof(state));

    for(i=0; i<256; i++)
        mtf[i]= -1;

    ff_init_range_encoder(&c, out, len);

    for(i=len-1; i>=0; i--){
        int v= tmp[i];
        for(j=0; mtf[j]>=0 && mtf[j] != v; j++);
        assert(j<256);
        tmp[i]= j;
        for(;j>0; j--)
            mtf[j]=mtf[j-1];
        mtf[0]= v;
    }

    for(i=0; i<256; i++){
        if(mtf[i]<0){
            put_symbol_255(&c, &state[256*16], mtf[0]);
            break;
        }
        put_symbol_255(&c, &state[256*16], mtf[i]);
    }

    for(i=0; i<len; i++){
        int v, ndx;

        ndx= tmp[i];
        v= mtf[0];
        for(j=0; j<ndx; j++)
            mtf[j]= mtf[j+1];
        mtf[ndx]= v;
        put_symbol_255(&c, &state[v*16], ndx);
    }
//FIXME right order mtf optim

    free(tmp);
    out_size= ff_rac_terminate(&c);
    wb32(out-4, out_size);
fprintf(stderr," done (%d %d %d)\n", len, start, out_size);
    return out_size+16;
}

static int decompress(FILE *fi, FILE *fo, int low_mem){
    uint8_t *in;
    unsigned int in_len;
    unsigned int out_len,start,i, j, pretype;
    int ret;
    uint8_t *tmp; //FIXME reduce memory needs
    RangeCoder c;
    uint8_t state[257*16];
    unsigned int histogram[256]={0};
    int mtf[256];
    uint8_t tmpX[16];

    ret=fread(tmpX, 1, 16, fi);
    if(ret==0)
        return 0;
    else if(ret != 16)
        return -1;

    pretype= rb32(tmpX  );
    out_len= rb32(tmpX+4);
    start  = rb32(tmpX+8);
    in_len = rb32(tmpX+12);
fprintf(stderr," block (%d %d %d)\n", out_len, start, in_len);
    if(out_len<=0 || start<0 || in_len<=0 || start >= out_len)
        return -1;

    in = malloc(in_len);
    tmp= malloc(out_len);
    if(!in || !tmp)
        return -1;

    if(fread(in, in_len, 1, fi)!=1)
        return -1; //FIXME check all return memleaks

 //FIXME check all fread

    memset(state, 0, sizeof(state));

    ff_init_range_decoder(&c, in, in_len);

    for(i=0; i<256; i++){
        int test[256]={0};
        mtf[i]= get_symbol_255(&c, &state[256*16]);
        if(i && mtf[i] == mtf[0])
            break;
        if(test[mtf[i]]++){
            fprintf(stderr, "fatal error mtf invalid\n");
            return -1;
        }
    }

    for(i=0; i<out_len; ){
        int v= mtf[0];
        int ndx= get_symbol_255(&c, &state[v*16]);

        for(j=0; j<ndx; j++)
            mtf[j]= mtf[j+1];
        mtf[ndx]= v;

        tmp[i++]= v;
        histogram[v]++;
    }

    free(in);
    if(c.bytestream_end - c.bytestream != -1){
        fprintf(stderr, "error (%d)\n", c.bytestream - c.bytestream_end);
        return -1;
    }
    ibwt(tmp, fo, out_len, start, histogram, low_mem, pretype);

    return out_len;
}

static void help(void){
    fprintf(stderr, 
        "Usage: mnzip [OPTIONS] [INPUT_FILE|-] [OUTPUT_FILE|-]\n"
        "options:\n"
        "   c   compress\n"
        "   d   decompress\n"
        "   l   slow/low memory mode\n"
        "   0-9 block size in 2^x megabyte (4mb is default)\n"
        "\n"
        "examples:\n"
        "   Compress one file to another file:\n"
        "       mnzip c input_file output_file\n"
        "   Compress from stdin to stdout:\n"
        "       mnzip c\n"
        "       mnzip c - -\n"
        "   Decompress from stdin to a file:\n"
        "       mnzip d - output_file\n"
    );
    exit(1);
}

int main(int argc, char **argv){
    FILE *fi, *fo;
    uint8_t *out, *in;
    int comp= !strcmp(argv[0], "mnzip");
    int low_mem= 0;
    int block_size= 1<<22;
    int outsize, i;
    static const uint8_t header[7]={'M','N','Z','I','P',0,0};

    if (argc > 1) {
        for(i=0; argv[1][i]; i++){
            switch(argv[1][i]){
            case 'c'        : comp=1                            ; break;
            case 'd'        : comp=0                            ; break;
            case 'l'        : low_mem =1                        ; break;
            case '0' ... '9': block_size= 1<<(argv[1][i]-'0'+20); break;
            case '-'        :                                     break;
            default         : help()                            ;
            }
        }
    }

    if (argc < 3 || !strcmp(argv[2], "-"))
        fi = stdin;
    else if (! (fi = fopen(argv[2], "rb"))) {
        perror("Error opening input file");
        exit(1);
    }

    if (argc < 4 || !strcmp(argv[3], "-"))
        fo = stdout;
    else if (! (fo = fopen(argv[3], "w+b"))) {
        perror("Error opening output file");
        exit(1);
    }

    init_prob(prob);

    if(comp){
        in = malloc(block_size+RADIX_PASSES);
        out= malloc(2*block_size + 5000);//FIXME int and buffer overfl

        fwrite(header, 7, 1, fo);

        while(!feof(fi)){
            block_size= fread(in, 1, block_size, fi);
fprintf(stderr, "bwt in %p %p %d\n", out, in, block_size); 
            if(!block_size)
                break;
            outsize= compress(out, in, block_size);
fprintf(stderr, "bwt out %d\n", outsize); 
            fwrite(out, outsize, 1, fo);
        }
    }else{
        uint8_t tmp[256];

        fread(tmp, sizeof(header), 1, fi);
        if(memcmp(tmp, header, sizeof(header))){
            fprintf(stderr, "input header missmatch\n");
            exit(1);
        }
        while(!feof(fi)){
            if(decompress(fi, fo, low_mem)<0)
                exit(1);
        }
    }
    fclose(fi);

    return 0;
}
