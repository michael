;VGA600 ver 0.6 Beta Copyright (C) Michael Niedermayer 1997

;history:
; 0.1 initial ver                                      (never released)
; 0.2 added 240-line modes and ET4000-clock            (never released)
; 0.3 added 350/400-line modes and S3-clock(untested)  (never released)
; 0.4 added more inteligent activation                 (never released)
; 0.5 added clock probeing         
;           et4000 clock divide /2 /4
;           mode recorrection  WINDOZE 95 SUCKS
;           Loader rewriten
ideal
P286

cr            equ 0ah
lf            equ 0dh
paras         equ (end10h-int10h)/16+1
paras_all     equ (end_all-start+100h)/16+1
words         equ (end10h-int10h)/2+5
SC_INDEX      equ 03c4h   
CRTC_INDEX    equ 03d4h   
MISC_OUTPUT   equ 03c2h   
MISC_INPUT    equ 03cch   
Attrib_INDEX  equ 03c0h
Grafix_INDEX  equ 03ceh
PatchCodeSize equ offset(PatchClockCode_end)-offset(PatchClockCode)
PatchArgsSize equ offset(PatchArgs_end)-offset(PatchArgs)
delay         equ 6
delay2        equ 12
FPx           equ 12
FPx_1         equ 1 shl FPx
ProbeTimes    equ 8

segment code
org 100h
assume cs:code, ds:code, es:code, ss:code

start:
jmp install                           ;jmp to install code

mcb_name db 'VGA..600'                ;ends up in mcb if it works
int10h:
 cmp ax,020FFh                        ;check for allredy installed call
  jne exit_10
  mov ax,00600h
  mov bx,cs                           ;bx=cs of TSR
 iret                                 ;get out
exit_10:
 push ax
 cmp ax,04F02h                        ;check for VESA ModeSet
  je mode_set
 test ah,ah                           ;check for Standart ModeSet
  jnz no_mode_set                     ;only mode set
  and al,7Fh
  cmp al,013h                         ;check for 320x200x256
   je x_mode_set                      ;only 320x200 (x-modes)
mode_set:
  mov [BYTE cs:temp-int10h],1         ;set call_CheckSet_flag
x_mode_set:
  mov [BYTE cs:counter-int10h],delay or 11000000b ;set delay but disable int 8
no_mode_set:
 pop ax
 pushf                                ;call orginal int 10h
 db 09Ah
old_int10h dd 0
 pushf
 shr [BYTE cs:temp-int10h],1          ;check call_CheckSet_flag
  jnc no_CheckSet
 call CheckSet
no_CheckSet:
 and [BYTE cs:counter-int10h],00111111b ;reaktivate int 8 
 popf
retf 2

int8h:
 dec [BYTE cs:counter-int10h]           ;dec counter
  jns decr                              
 inc [BYTE cs:counter-int10h]           ;inc counter if negative
decr:                        ;if(X>0) X1=X-1 if(X<1) X1=X if(X=8000h) X1=7FFFh 
  jnz exit_8                            ;get out if not zero
 mov [BYTE cs:counter-int10h],delay2    ;set new counter

 call CheckSet

exit_8:                                 ;jmp to old int 8
db 0eah
old_int8h dd 0
 
CheckSet:
pusha
push ds
push es
 push cs
 pop ds
 push cs
 pop es
 cld

 mov si,offset(where)-offset(int10h)        ;Read and convert Vga-Regs
 mov di,offset(what)-offset(int10h)
 mov bx,offset(VSE2Real_code)-offset(int10h)

 mov dx,SC_INDEX
 mov al,1
 out dx,al
 inc dx
 in al,dx                                  ;get SCM
 stosb                                     ;save SCM
 mov cx,9
 test al,1
  jz chr_9
 dec cx
chr_9:
 xchg ax,cx
 stosw                                     ;save chr_clock

 mov dl,low(Grafix_INDEX)                  ;dh=3!
 mov al,6
 out dx,al
 inc dx
 in al,dx                                  ;get GM
 stosb                                     ;save Grafix Misc(text/grafix)

 mov dl,low(MISC_INPUT)                    ;dh=3!
 in al,dx
 stosb                                     ;save Misc-reg

int 3
get_regs:
 mov dl,low(CRTC_INDEX)                     ;!dh=3
 lodsb                                      ;read index from table1
 test al,al                                 ;check for sign bit
  js end_get_regs                           ;get out is set
 out dx,al                                  ;set index
 inc dx                                     ;dx= data_port
 in al,dx                                   ;get data
 push ax                                    ;save data
 lodsw                                      ;get jmp_table_entry and inc si
 cbw                                        ;convert it to word
 add bx,ax                                  ;and add it to the last one
 pop ax                                     ;restore data
 mov cl,1                                   ;cl=1 (needed for some codes)
 mov ah,0                                   
 call bx                                    ;call it
 stosw                                      ;!ah=0 /write data to table2
jmp get_regs
end_get_regs:

 mov ax,[HT-int10h]                         ;check horizontal coherenzy
 cmp ax,[FP8_1-int10h]                      ;check HT<256 (causes div error)
  jnb Not2_End_CheckSet
End2_Checkset:
 jmp End_CheckSet
Not2_End_CheckSet:
 mov bx,[HBS-int10h]
 cmp ax,bx
  jb End2_Checkset
 mov bx,[HSS-int10h]
 cmp ax,bx
  jb End2_CheckSet
 mov ax,[HDE-int10h]
 cmp bx,ax
  jb End2_CheckSet
                                          
 mov ax,[VT-int10h]                         ;check vertical coherenzy
 mov bx,[VBS-int10h]
 cmp ax,bx
  jb End2_CheckSet
 mov bx,[VSS-int10h]
 cmp ax,bx
  jb End2_CheckSet
 mov ax,[VDE-int10h]
 cmp bx,ax
  jb End2_CheckSet

 cmp ax,[MaxLines-int10h]                   ;check num_lines (300-1000)
  ja End2_CheckSet
 cmp ax,[MinLines-int10h]
  jb End2_CheckSet

 int 3
 mov al,[MSL-int10h]
 and al,10011111b
 test [BYTE gm-int10h],1                     ;test if text
  jnz no_text
  and al,10000000b
no_text:
 test al,al
  jns no_cgax2                               ;test if cga like mode
 cmp al,10000001b
  jae End2_CheckSet                           ;get out if Real-CGA
 mov al,1
no_cgax2:
 and al,00011111b
 inc ax                                      ;al=mult (msl=mult-1)
 cbw
 stosb                                       ;save lm

 xchg ax,bx                                  ;bx=line_mult
 mov ax,[VDE-int10h]                         ;ax=phys-lines
 cwd
 div bx                                      ;find logical_lines
 stosw                                       ;save ll
 xchg ax,di                                  ;di=logi-line / bx=line_mult
 mov cx,-1                                   ;cx=-1 (best_clock_index)

;int 3
FindBest:
 inc bx                                      ; inc line_mult
  js End_FindBest                            ; just in case VDE is very strange
 cmp bl,3                                    ;check line_mult<3
  jb n3
 test [BYTE gm-int10h],1                     ;test if text
  jz End_FindBest                            ;get out if more then *2 + TEXT
n3:
 mov ax,bx
 call FindSet_V                              ;find and set Vertical nums
 cmp si,[MaxLines-int10h]
  ja End_FindBest                            ; get out if toooo much

 xor si,si                                   ;si=clock
 xor bp,bp                                   ;bp=best_clock (=0)

FindBestClock:
 mov ax,[WORD si+FP8_ClockTable-int10h]      ;get clock
 test [BYTE SCM-int10h],1000b                ;check if dotclk=mstclk/2
 int 3
  jz clk_ok                                  ;skip if not
 shr ax,1                                    ;"double" ht if yes
clk_ok:
 mul [WORD K_1-int10h]                       ;convert to kHz
 div [WORD HT-int10h]                        ;find line_freq  HT MUST >256
 cmp ax,[FP8_MinLineFreq-int10h]             ;check if < MinLineFreq
  jb End_FindBestClock
 cmp ax,[FP8_MaxLineFreq-int10h]             ;check if > MaxLineFreq
  ja End_FindBestClock
 mul [WORD K_1-int10h]                       ;convert to Hz
 div [WORD VT-int10h]                        ;find refresh_freq VT>256
 cmp ax,[FP8_MinRefresh-int10h]              ;check if < MinRefresh
  jb End_FindBestClock
 cmp ax,[FP8_MaxRefresh-int10h]              ;check if > MaxRefresh
  ja End_FindBestClock
 cmp ax,bp                                   ;check if better than best
  jbe End_FindBestClock
 mov bp,ax                                   ;if yes save as best
 mov cx,si                                   ;cx=si
 shr cx,1                                    ;get WORD FP8_ClockTable[cx]
 or ch,bl                                    ;ch=line_mult   cl=Clock_index
End_FindBestClock:
 inc si
 inc si                                      ;inc clock_pointer
 cmp si,offset(FP8_ClockTable_end-FP8_ClockTable);check if all clocks where tested
  jb FindBestClock                           ;error ?jbe
 jmp FindBest
End_FindBest:

 test cx,cx                                  ;get out if nothing found
  jns Not_End_CheckSet
End_CheckSet:
 pop es
 pop ds
 popa
ret
Not_End_CheckSet:
; int 3
 mov al,ch                                   ;set ax= best line_mult
 cbw
 call FindSet_V                              ;set Vertical Nums (in mem)

 dec ch                                      ;ERROR should be rewritten ch=Mult-1 (for MSL)
 mov bx,  0000000001011111b                  ;set and/or start masks(+clear VBS)
 test [BYTE gm-int10h],1                     ;test if text
  jnz no_text2
  cmp ch,1                                   ;check if *2 & text
   jne end_SetMSL
   mov bh,10000000b                          ;set cga like doubling
 jmp end_SetMSL                              ;and get out
no_text2:
  mov bl,ch                                  ;set and-mask
  mov bh,ch                                  ;set or-mask
  or  bl,01000000b                           ;correct and-mask(+clear VBS)
  and bh,00011111b                           ;correct or-mask (xor bx,xxx...!!! limit line_mult)
end_SetMSL:
 and [MSL-int10h],bl                         
 or  [MSL-int10h],bh                         ;set MSL
 
 mov ch,0                                    ;ch=0 (we no longer need line_mult)
 push cx                                     ;save Clock_index

 mov dx,CRTC_INDEX                           
 mov al,11h                    
 out dx,al                                   
 inc dx                        
 in al,dx                                    ;read VSE
 and al,7fh                                  ;remove write protect bit
 out dx,al                                   ;write VSE
 dec dx                        

 and [BYTE OF-int10h],00010000b           ;clear everithing witch will be set 

 mov si,offset(where_backward)-offset(int10h)  ;Read and convert Vga-Regs
 mov di,offset(what_backward)-offset(int10h)   ;backwards (hehe)
 mov bx,offset(Real2VBE_code)-offset(int10h)
 std

set_regs:
 lodsb                                      ;read delta
 cbw                                        ;convert it to 16-bit
 add bx,ax                                  ;add it to bx
 dec si                                     ;skip foreward info
 lodsb                                      ;get index
 test al,al                                 ;check for sign bit
  js end_set_regs                           ;get out is set
 out dx,al                                  ;set index
 inc dx                                     ;dx= data_port
 in al,dx                                   ;read (only VSE needs that now)
 mov ch,al                                  ;put into ch
 mov ax,[di]                                ;get data
 dec di                                     ;sub di,2
 dec di
 mov cl,1                                   ;cl=1 (needed for some codes)
 call bx                                    ;call it
 out dx,al                                  ;set data
 dec dx                                     ;dx=data_port
jmp set_regs
end_set_regs:
 cld

 pop ax
 push offset(End_CheckSet-int10h) ; for return later
PatchClockCode2:
 xchg ax,bx
 mov ah,bl                     ;ah=bl=clock
 shl ah,2
 mov dl,LOW(MISC_INPUT)       ;dh=3!    dont read from mem (clock_probing)
 in al,dx                      ;al=MiscOutput
 mov dl,LOW(MISC_OUTPUT)       ;dh=3!
PatchClockCode:
et4000_clock:
 and ax,0000110011110011b      
 or al,ah
 out dx,al                     ;0-1

 mov dl,0bfh                   ;!dh=3
 mov al,3
 out dx,al
 mov dl,0d8h                   ;!dh=3
 mov al,0a0h
 out dx,al                     ;enable ET4000 ext.

 mov dl,LOW(CRTC_INDEX)                  ;!dh=3
 mov al,34h
 out dx,al
 inc dx
 in al,dx
 mov ah,bl
 shr ah,1
 and ax,0000001011111101b
 or al,ah
 out dx,al                               ;2

 dec dx
 mov al,31h
 out dx,al
 inc dx
 in al,dx
 mov ah,bl
 shl ah,3
 and ax,1100000000111111b
 or al,ah
 out dx,al                     ;3-4

 mov dl,LOW(SC_INDEX)
 mov al,7h
 out dx,al
 inc dx
 in al,dx
 mov bh,bl
 and bx,0100000000100000b
 shl bl,1
 shr bh,6
 or bl,bh
 and al,10111110b
 or al,bl
 out dx,al                     ;5/6 *double/quad*

 dec dx
 mov al,6h
 out dx,al
 inc dx
 in al,dx
 out dx,al                     ;ET4000 SUCKs

ret
PatchClockCode_end:
    db 0FFh                     ;end-marker  \
    db 0FFh                     ;end-marker   > (for upwards thingy)
    db 0FFh                     ;end-marker  /
where:
    db 011h                     ;VSE / Protect 
    db offset(VSE2Real_code)-offset(VSE2Real_code)
    db offset(Real2VSE_code)-offset(Real2OF_code)
    db 007h                     ;OF
    db offset(OF2Real_code)-offset(VSE2Real_code)
    db offset(Real2OF_code)-offset(Real2MSL_code)
    db 009h                     ;MSL
    db offset(MSL2Real_code)-offset(OF2Real_code)
    db offset(Real2MSL_code)-offset(Real2HT_code)
    db 000h                     ;HT
    db offset(HT2Real_code)-offset(MSL2Real_code)
    db offset(Real2HT_code)-offset(Real2HDE_code)
    db 001h                     ;HDE
    db offset(HDE2Real_code)-offset(HT2Real_code)
    db offset(Real2HDE_code)-offset(Real2HBS_code)
    db 002h                     ;HBS
    db offset(HBS2Real_code)-offset(HDE2Real_code)
    db offset(Real2HBS_code)-offset(Real2HBE_code)
    db 003h                     ;HBE
    db offset(HBE2Real_code)-offset(HBS2Real_code)
    db offset(Real2HBE_code)-offset(Real2HSS_code)
    db 004h                     ;HSS
    db offset(HSS2Real_code)-offset(HBE2Real_code)
    db offset(Real2HSS_code)-offset(Real2HSE_code)
    db 005h                     ;HSE 
    db offset(HSE2Real_code)-offset(HSS2Real_code)
    db offset(Real2HSE_code)-offset(Real2VT_code)
    db 006h                     ;VT
    db offset(VT2Real_code)-offset(HSE2Real_code)
    db offset(Real2VT_code)-offset(Real2VSS_code)
    db 010h                     ;VSS
    db offset(VSS2Real_code)-offset(VT2Real_code)
    db offset(Real2VSS_code)-offset(Real2VDE_code)
    db 012h                     ;VDE
    db offset(VDE2Real_code)-offset(VSS2Real_code)
    db offset(Real2VDE_code)-offset(Real2VBS_code)
    db 015h                     ;VBS
    db offset(VBS2Real_code)-offset(VDE2Real_code)
    db offset(Real2VBS_code)-offset(Real2VBE_code)
    db 016h                     ;VBE
    db offset(VBE2Real_code)-offset(VBS2Real_code)
where_backward:
    db offset(Real2VBE_code)-offset(Real2VBE_code)
    db 0FFh                     ;end-marker
what:
SCM db ?
CHR_Clock dw ?
GM  db ?
MR  db ?
VSE dw ?
OF  dw ?
MSL dw ?
HT  dw ?
HDE dw ?
HBS dw ?
HBE dw ?
HSS dw ?
HSE dw ?
VT  dw ?
VSS dw ?
VDE dw ?
VBS dw ?
what_backward:
VBE dw ?
LM  db ?
LL  dw ?

HT2Real_code:
 add ax,4
HDE2Real_code:
 inc ax
HBS2Real_code:
HSS2Real_code:
 mul [BYTE chr_clock-int10h]
HBE2Real_code:                           ;IGNORE FOR NOW
HSE2Real_code:                           ;IGNORE FOR NOW
OF2Real_code:                            ;OK
MSL2Real_code:                           ;OK
ret

VSS2Real_code:
 inc cx
VDE2Real_code:
 inc cx
VT2Real_code:                            ;cl must be 1
 mov ah,[BYTE OF-int10h]
 shr ah,cl                               
 pushf                                   
 shr ah,4                                
 popf                                    
 rcl ah,1                                
 and ah,00000011b                        
VSE2Real_code:                           ;OK/NEVER USED
VBE2Real_code:                           ;OK/NEVER USED (?REAL OK)
 inc ax
ret

VBS2Real_code:                          
 mov ah,[BYTE OF-int10h]
 mov ch,[BYTE MSL-int10h]
 shr ah,3      
 shr ch,4      
 and ah,00000001b
 and ch,00000010b
 or ah,ch
 inc ax
ret

Real2VSS_code:
 inc cx
Real2VDE_code:
 inc cx
Real2VT_code:                            ;cl must be 1
 dec ax
 shr ah,1
 pushf                   
 shl ah,4                     
 popf                    
 rcl ah,cl                  
 or [BYTE of-int10h],ah
ret

Real2VBS_code:
 dec ax
 shl ah,3
 mov ch,ah
 and ah,00001000b        
 or [BYTE OF-int10h],ah
 shl ch,1                
 and ch,00100000b        
 or [BYTE MSL-int10h],ch
ret

Real2HT_code:
 sub ax,32
Real2HDE_code:
 sub ax,8
Real2HBS_code:
Real2HSS_code:
 mov cl,[chr_clock-int10h]
 div cl
Real2HBE_code:                           ;IGNORE FOR NOW
Real2HSE_code:                           ;IGNORE FOR NOW
 pop ax
 inc ax
 push ax                                 ;increase ret ip (skip OUT DX,AL)
Real2OF_code:                            ;OK/NEVER_Called
Real2MSL_code:                           ;OK
ret

Real2VBE_code:                           ;OK
 dec ax
ret

Real2VSE_code:
 dec ax
 and al,00001111b                         ;only 0-3 are used
 and ch,01110000b                         ;preserve 4-6
 or ch,10000000b                          ;set write-protect-bit
 or al,ch
ret

FindSet_V:
 mul di                                      ; find new phys_lines
 push di                                     ; save di
 mov [VDE-int10h],ax                         ; save(phys_lines) as new
 mov si,ax                                   ; save(phys_lines) in si
 mov di,ax                                   ; save(phys_lines) in di
 mul [WORD FPx_bor-int10h]                   ; find right-border-lenght
 div [WORD FPx_1_-int10h]                    ;could be replaced by shrd
 cmp ax,di                                   ; check if no round-off error
  jne FPx_bor_Ok
 inc ax
FPx_bor_Ok:
 mov bp,ax                                   ; save in bp
 add si,ax                                   ; add to old phys
 mov [VBS-int10h],si                         ; save in VBS
 inc si                                      ; inc it
 mov [VSS-int10h],si                         ; save in VSS
 mov ax,di                                   ;get old phys from di
 mul [WORD FPx_VS-int10h]                    ; find VS-lenght
 div [WORD FPx_1_-int10h]                    ;could be replaced by shrd
 cmp ax,di                                   ; check if no round-off error
  jne FPx_VS_Ok
 inc ax
FPx_VS_Ok:
 add si,ax                                   ;add it
 mov [VSE-int10h],si                         ; save in VSE
 xchg di,ax                                  ;get old phys from di
 mul [WORD FPx_VB-int10h]                    ; find VB-lenght
 div [WORD FPx_1_-int10h]                    ;could be replaced by shrd
 cmp ax,di                                   ; check if no round-off error
  jne FPx_VB_Ok
 inc ax
FPx_VB_Ok:
 add si,ax                                   ;add it
 mov [VBE-int10h],si                         ; save in VBE
 add si,bp                                   ;add left-border-lenght
 mov [VT-int10h],si                          ; save in VT
 pop di                                      ; restore di
ret

K_1             dw 1000

PatchArgs:
FP8_1           dw 256
FP8_MinRefresh  dw 60 shl 8
FP8_MaxRefresh  dw 90 shl 8            
FP8_MinLineFreq dw 28 shl 8            
FP8_MaxLineFreq dw 72 shl 8            
                                        
FPx_1_  dw FPx_1
FPx_bor dw FPx_1/67              ;1/.015=67
FPx_VS  dw FPx_1/200             ;1/.005=200
FPx_VB  dw FPx_1*7/100           ;1/.070=(100/7)

MaxLines        dw 1023
MinLines        dw 300
PatchArgs_End:


FP8_ClockTable   dw 128 dup (?)
FP8_ClockTable_end:

clock_d  db 0

counter  db delay2
temp db 0

end10h:



install:
 cld                              ;clear dir_falg
 mov dx,offset(c_r)               ;dx=offset(copy-right)
 call wstring                     ;write it to screen

 call chk_600                    ;check if installed (copy info from tsr)
  jne NOT_Read
  mov ds,bx                      ;es=cs of TSR
  mov si,offset(PatchArgs)-offset(int10h)
  mov di,offset(PatchArgs)
  mov cx,PatchArgsSize
  rep movsb                      ;Copy TSR Variables
  mov al,[offset(CheckSet)-int10h];get En/Disable state
  mov [cs:offset(CheckSet)],al   ;set En/Disable state
NOT_Read:

                                  ;Find corr path to executable
 mov ds,[cs:02Ch]                 ;ds=EnvSeg
 mov si,1                         
 mov di,offset(FP8_ClockTable)    ;si=Temp Buffer
GetPath_Loop:
 dec si
 lodsw
 test ax,ax                       ;find 0,0
  jnz GetPath_Loop
 lodsw                            ;skip 2
GetPath_Copy:
 lodsb                            ;copy until 0
 stosb
 cmp al,'.'
  jnz GetPath_NDot
  mov bx,di                       ;bx=last .
GetPath_NDot:
 test al,al
  jnz GetPath_Copy
 push cs
 pop ds                           ;restore ds
 mov [WORD bx+1],'KL'           



 mov di,80h                       ;di=offset(Num_ComLineArgs)
 mov ch,0
 mov cl,[di]                      ;cx=Num_ComLineArgs
 inc di                           ;di=offset(ComLineArgsBuffer)
 or cx,cx                         ;check if nothing there
  jz MSG_UnkCommand
 add cx,di                        ;cx=ComLineEndPointer
 xor bp,bp                        ;set nothing_Specified


CommLine:
 call SkipSpace
 mov al,[di]                      ;fetch byte from CommLine
 inc di                           ;and inc pointer

 cmp al,'/'                       ;check if CommLineArg starts with /
  jz Ok_Comm                      ;go on if so
 cmp al,'-'                       ;check if CommLineArg starts with -
  jnz MSG_UnkCommand              ;get out if not
Ok_Comm:

 mov si,offset(CommLineOptionsTable)
 mov bx,offset(CLO_Enable)            
Next_CommLineArg:
 cmp [BYTE si],0                       ;check if end of table reached
  je MSG_UnkCommand
 push di                          ;Save di
Next_CommLineArgByte:
 lodsb                            ;fetch byte from table
 or [BYTE di],00100000b           ;conv to lower-case
 scasb                            ;compare if same at CommLine
  je Next_CommLineArgByte         ;if yes check next
 test al,al                       ;check if CommLineEqual
  jz CommLineArgId                
SkipTillZeroP1:
 lodsb                            ;fetch next from Table
 test al,al                       ;check if zero
  jnz SkipTillZeroP1              
 lodsb                            ;get call address from table
 cbw                              ;convert to word
 add bx,ax                        ;add to last
 pop di                           ;di=old di
jmp Next_CommLineArg
CommLineArgId:

 dec di                           ; for /V1 instaed of /V 1
 lodsb                            ;get call address from table
 cbw                              ;convert to word
 add bx,ax                        ;add to last
 call bx                          ;call it
 cbw                              ;convert to word
 or bp,ax                         ;set bit in bp
 pop ax                           ;corr stack
jmp CommLine                      ;get next CommLineArg from CommLine

EndCommLine:
 test bp,bp                       ;check if no_VgaCardSpecified
  jnz ComLineOK
MSG_UnkCommand:                   ;print error-msg and exit
  mov dx,offset(u_c)
 jmp estring

ComLineOk:

 push bp                          ;save bp
 shr bp,2                         ;remove OptionFlag and VgaCardSpecifiedFlag
 test bp,bp                       ;check if zero (CRASH) 
  jz ZERO_BP                      ;don't shr if yes
CheckLine:
 shr bp,1                         ;remove rightmost flag
  jnc CheckLine                   ;if flag was 0 try next
ZERO_BP:
 test bp,bp                       ;if not zero then error
  jnz MSG_UnkCommand
 pop bp                           ;restore bp

 mov ax,bp                       ;ax=bp
 mov ah,al
 not ah                          ;ah=~al
 cmp  al,00010000b               ;Check for UnInstall
  je UnInstall
 cmp  al,00100001b               ;Check for Probe(and vga_card)
  je Probe2
 test ax,0000000111110000b       ;Check for VgaCard/Options/E/D w/o other stuff
  jz GoTSR
 test al,11110001b               ;Check for Enable/Disable/options w/o other stuff
  jnz MSG_UnkCommand
 jmp PassToTSR
Probe2:
jmp Probe

UnInstall:
 call chk_600                     ;check if installed
  je nMSG_NotInstalled
MSG_NotInstalled:                 ;print "Not Installed"
  mov dx,offset(n_i)
 jmp estring
nMSG_NotInstalled:
push bx
 mov ax,3510h
 int 21h                          ;get int 10
 pop bx
 push bx                          ;bx=cs of TSR
 mov ax,es                        ;ax=cs of Int10 Handler
 cmp ax,bx                        ;check if equal
  je nMSG_CantUnload              ;get out if not
MSG_CantUnload:                   ;print "Not Installed"
  mov dx,offset(n_u)
 jmp estring
nMSG_CantUnload:

 mov ax,3508h
 int 21h                          ;get int 8
 pop bx                           ;bx=cs of TSR
 mov ax,es                        ;ax=cs of Int8 Handler
 cmp ax,bx                        ;check if equal
  jne MSG_CantUnload              ;get out if not

 mov dx,[es:offset(old_int10h)-int10h]
 mov ds,[es:offset(old_int10h)-int10h+2]
 mov ax,2510h
 int 21h            ;set old int 10
 mov dx,[es:offset(old_int8h)-int10h]
 mov ds,[es:offset(old_int8h)-int10h+2]
 mov ax,2508h
 int 21h            ;set old int 8
 mov ah,49h
 int 21h            ;free mem
int 20h

GoTSR:
 call chk_600                    ;check if installed
  jne n2MSG_AlreadyInstalled
  jmp MSG_AlreadyInstalled
n2MSG_AlreadyInstalled:

 mov ax,3D00h                    ;open file for read
 mov dx,offset(FP8_ClockTable)   ;CorrPath\vga600.clk
 int 21h
  jnc nMSG_FileError2
MSG_FileError2:
  mov dx,offset(f_e)
 jmp estring
nMSG_FileError2:
 xchg ax,bx                      ;bx=file_handle

 mov ah,3Fh                      ;read from file
 mov dx,offset(FP8_ClockTable)   ;into ClockTable
 mov cx,128*2                    ;128 Words
 int 21h
  jc MSG_FileError2

 mov ah,3Fh
 int 21h                         ;and close it

 mov ax,3510h
 int 21h                       ;get old int 10
 mov [offset(old_int10h)],bx
 mov [offset(old_int10h)+2],es

 mov ax,3508h
 int 21h                       ;get old int 8
 mov [offset(old_int8h)],bx
 mov [offset(old_int8h)+2],es

 mov sp,offset end_all+0FEh    ;shrink stack (less mem needed)

 push cs
 pop es                        ;es=cs
 mov ah,04Ah
 mov bx,paras_all+010h
 int 21h                       ;shrink us
  jc mem_error 

 mov ah,048h                   ;!BX=paras_all+10h
 int 21h                       ;allocate mem for copy
  jc mem_error                  

 mov es,ax                     ;es=cs of copy
 mov cx,offset end_all+0100h
 xor di,di
 xor si,si
 rep movsb                    ;copy IT

 push es
 push offset cool_jmp
 retf                         ;mov cs,es 

cool_jmp:                     ;Now We are in the Copy
 push cs
 pop ss                       ;and the stack Too

 mov ax,ds
 cmp [016h],ax                ;check if parent process seg addr old=oldcs
  jne ok2
 mov [es:016h],es             ;if yes set parent process seg addr new=newcs
ok2:
 cmp [034h+2],ax              ;check if handle array pointer old=oldcs
  jne ok3
 mov [es:034h+2],es            ;if yes set handle array pointer new=newcs
ok3:                          

 mov ah,50h
 mov bx,cs                     ;bx=cs of copy
 int 21h                       ;set addr of PSP

 mov ax,cs
 dec ax
 mov es,ax                     ;es=cs-1 (MCB-Seg)
 mov [es:1],cs                 ;set seg to itself

 push ds
 pop es
 mov ah,49h
 int 21h                       ;get rid of the old shit

 push cs
 pop ds                        ;ds=cs

 mov es,[2ch]                  ;es=env-seg
 mov ah,49h                    ;free memory
 int 21h                       ;free env-seg
 mov bx,paras
 mov ah,48h                    ;alloc memory
 int 21h                       ;alloc new seg
 mov es,ax                     ;es=seg of new seg
  jnc nmem_error
mem_error:
  mov dx,offset(m_e)
  call wstring
 jmp exit_load
nmem_error:

 mov ax,es
 dec ax
 mov es,ax                     ;mcb of env-seg
 inc ax
 mov [es:1],ax                 ;set seg to itself
 mov di,8
 mov si,offset(mcb_name)
 mov cx,words
 rep movsw                     ;copy name & prog to mcb & mem
 mov ds,ax
 xor dx,dx
 mov ax,2510h
 int 21h                       ;set new int 10
 mov dx,int8h-int10h     
 mov ax,2508h
 int 21h                       ;set new int 8

exit_load:
 push cs
 pop es                        ;es=cs
 mov ah,49h                    ;free mem
 int 21h                       ;free us (hope that doesnt kill us)
mov ah,04ch
int 21h                        ;get out (cool TSR)


PassToTSR:
 call chk_600                    ;check if installed
  je n2MSG_NotInstalled
  jmp MSG_NotInstalled
n2MSG_NotInstalled:
  mov dx,offset(p_a)             ;print "passing arguments to TSR"
  call wstring
  mov es,bx                      ;es=cs of TSR
  cli                            ;better 
  mov di,offset(PatchArgs)-offset(int10h)
  mov si,offset(PatchArgs)
  mov cx,PatchArgsSize
  rep movsb                      ;Update TSR Variables
  mov al,[offset(CheckSet)]      ;get En/Disable state
  mov [es:CheckSet-int10h],al    ;set En/Disable state
 int 20h                         ;get OUT

probe:
 call chk_600
MSG_AlreadyInstalled:
MSG_ProbeError:
  jne nMSG_ProbeError
  mov dx,offset(p_e)
 jmp estring
nMSG_ProbeError:

 mov ax,0013h
 int 10h                                ;set 320x200 (must be 25MHz)

 call GetVSyncTime                      ;wait for end
 call GetVSyncTime                      ;get Time
 mov bp,1
Probe_FindShift:                        ;BitScan
 shl bp,1                               ;Double Divider
 shr dx,1                               ;32bit shift
 rcr ax,1
 test dx,dx                             ;max 16bit
  jnz Probe_FindShift
 cmp ax,01000h                          ;max 25*16Mhz
  jae Probe_FindShift



;MSG_InterError:                         ;print error-msg and exit
;  mov dx,offset(d_e)
; jmp estring

 xchg bx,ax                             ;bx=lenght of refresh at 25Mhz

 mov dx,offset(FP8_ClockTable)
 xor cx,cx
 mov ah,3Ch                            ;create file
 int 21h
  jnc nMSG_FileError
MSG_FileError:                         ;print error-msg and exit
  mov dx,offset(f_e)
  jmp estring
nMSG_FileError:
 push ax                               ;save handle

 mov si,dx
 xor di,di

probe_loop:
 pusha                                  ;save regs
 xchg ax,di                             ;ax=clock
 mov dh,3                               
 call PatchClockCode2                   ;set clock
 popa                                   ;restore regs

 call GetVSyncTime                      ;wait for end
 call GetVSyncTime                      ;get Time for refresh at 25.175Mhz
 div bp                                 

 xchg cx,ax                             ;cx=lenght of refresh
 mov ax,bx                              ;ax=lenght of refresh at 25Mhz
 mov dx,6445                            ;25.175*256=6444.8
 mul dx
 div cx                                 ;ax=FP8_Clock

 mov [si],ax                            ;save it
 inc si                                 ;inc pointer
 inc si
 inc di                                 ;inc clock num
 cmp di,128                              ;check if end
  jb probe_loop

 mov ax,0003h                           ;set text mode 3
 int 10h   

 pop bx                                  ;bx=FileHandle

 mov ah,40h                              ;write to file
 mov cx,128*2                            ;how many bytes
 mov dx,offset FP8_ClockTable            ;from where
 int 21h
  jc MSG_FileError

 mov ah,3Fh
 int 21h
int 20h


GetVSyncTime:                      ;dx:ax=ticks for V-Refresh
 push cx
 mov cx,ProbeTimes
 xor ax,ax
 xor dx,dx
WaitVSync:
   add ax,1                         ;inc di:si
   adc dx,0
   cmp dx,ProbeTimes*64              ;check if to big
    ja GetVSyncTimeEnd0
   push dx
   push ax
   mov dx,03dah
   in al,dx                         ;get data
   test al,1000b                     ;mask v-sync-bit
   pop ax
   pop dx
  jz WaitVSync                      
WaitVEndSync:
   add ax,1                         ;inc di:si
   adc dx,0
   cmp dx,ProbeTimes*64             ;check if too big
    ja GetVSyncTimeEnd0
   push dx
   push ax
   mov dx,03dah
   in al,dx                         ;get data
   test al,1000b                    ;mask v-sync-bit
   pop ax
   pop dx
  jnz WaitVEndSync
 loop WaitVSync 
GetVSyncTimeEnd0:
 pop cx
ret

estring:
 call wstring
int 20h

wstring:
 mov ah,09h
 int 21h                                 ;print string
ret

;wchar:
; mov ah,02h
; int 21h                                 ;print string
;ret

;SetNumHex:
; ror ax,12                             ;4 MSB of ax ->al
; mov cx,4
;SetNumHex_Next:
; push ax                               ;save it
; and al,0Fh                            
; add al,030h                           ; conv to ascii
; cmp al,03Ah                           ; check if A-F
;  jl SetNumHex_NoChar
; add al,7                              ; conv to ascii (A-F)
;SetNumHex_NoChar:
; xchg ax,dx                            ;put in dx
; call wchar                            ; write it
; pop ax                                ;restore old
; rol ax,4                              ; rol to next
;  loop SetNumHex_Next                  ; loop if not all written
;ret

GetNum:                                  ;ds:di !!
 call SkipSpace
 push dx
 push bx                                 ;save bx/dx
 mov bh,0
 xor ax,ax                               ;bh/ax=0
 mov dh,0FFh                             ;check if num exist (mul resets dx=0)
GetNum_NextByte:
 mov bl,[di]                             ;bl=ASC(num)
 cmp bl,' '                              ;check for space
  je GetNum_End
 cmp di,cx
  jae GetNum_End
 inc di
 sub bl,30h                              ;bl=num /check for <30h
  jb xMSG_UnkCommand
 cmp bl,9                                ;check for >39h
  ja xMSG_UnkCommand                      
 mul [n10]                               ;ax=OldNum*10
 add ax,bx                               ;ax=OldNum*10+Num
jmp GetNum_NextByte
GetNum_End:
 test dh,dh                              ;check if anything there
  jz n2MSG_UnkCommand
xMSG_UnkCommand:
  jmp MSG_UnkCommand
n2MSG_UnkCommand:
 pop bx                                  ;restore bx/dx
 pop dx                                  ;end
ret

GetNumFP8:                                ;ds:di !!
 call SkipSpace
 push dx
 push bx
 push bp                                  ;save bx/dx/bp/si
 push si
 mov bh,0
 xor ax,ax                                ;ch/ax=0
 mov bp,1
 mov si,bp                                ;si=bp=1
 mov dh,0FFh                             ;check if num exist (mul resets dx=0)
GetNumFP8_NextByte:
 mov bl,[di]                             ;bl=ASC(num)
 cmp bl,' '                              ;check for space
  je GetNumFP8_End
 cmp di,cx
  jae GetNumFP8_End
 cmp bl,'.'
  jne GetNumFP8_NotDot
 mov si,10
 inc di
 jmp GetNumFP8_NextByte
GetNumFP8_NotDot:
 inc di
 sub bl,30h                              ;bl=num /check for <30h
  jb xMSG_UnkCommand
 cmp bl,9                                ;check for >39h
  ja xMSG_UnkCommand                      
 xchg ax,bp
 mul si
 xchg ax,bp                              ;mul bp,si
 mul [n10]                               ;ax=OldNum*10
 add ax,bx                               ;ax=OldNum*10+Num
jmp GetNumFP8_NextByte      
GetNumFP8_End:
 test dh,dh                              ;check if anything there
  jnz xMSG_UnkCommand
 mul [FP8_1]
 div bp
 pop si
 pop bp
 pop bx                                  ;restore bx/dx/bp/si
 pop dx                                  ;end
ret


chk_600:
 mov ax,020FFh
 int 10h
 cmp ax,00600h                            ;if equal vga600 already installed
ret

vga_clock:
 and ax,0000110011110011b      
 or al,ah
 out dx,al                     ;0-1
ret

s3_clock:
 or al,00001100b                         ;set std_clock to 3
 out dx,al

 mov dl,LOW(CRTC_INDEX)                   ;!dh=3
 mov al,38h                               ;unlock registers
 out dx,al
 inc dx
 mov al,48h
 out dx,al
 dec dx
 mov al,39h
 out dx,al
 inc dx
 mov al,0A5h
 out dx,al
 dec dx

 mov al,42h
 out dx,al
 inc dx
 in al,dx
 and al,11110000b
 and bl,00001111b
 or al,bl
 out dx,al
ret

CLO_Enable:
 mov [BYTE CheckSet],060h              ;set first oppcode to pusha
 mov al,00000100b                      ;set Enable-Flag
ret

CLO_Disable:
 mov [BYTE CheckSet],0C3h              ;set first oppcode to ret
 mov al,00001000b                      ;set Disable-Flag
ret

CLO_VgaCard:
 call GetNum                           ;Get Number
 pusha
 cmp ax,2                              ;check if >2
  jna OK_VgaCardNum
  jmp MSG_UnkCommand                   ;get out if yes
OK_VgaCardNum:
 shl ax,1                              ;*2
 xchg ax,si                            ;si=num*2 
 mov si,[si+offset(ClockCodeTable)]    ;si=ClockCodeTable[num*2]
 mov di,offset(PatchClockCode)         
 mov cx,PatchCodeSize
 rep movsb                             ;copy NewClockCode to PatchClockCode
 popa
 mov al,00000001b                      ;set Vga_card_specified-flag
ret

CLO_UnInstall:
 mov al,00010000b                      ;set UnInstall-Flag
ret

CLO_Probe:
 mov al,00100000b                       ;set Probe-Flag
ret

CLO_MaxLineFreq:
 call GetNumFP8
 mov [FP8_MaxLineFreq],ax
 mov al,00000010b                       ;set Options-Flag
ret

CLO_MinLineFreq:
 call GetNumFP8
 mov [FP8_MinLineFreq],ax
 mov al,00000010b                       ;set Options-Flag
ret

CLO_MaxRefresh:
 call GetNumFP8
 mov [FP8_MaxRefresh],ax
 mov al,00000010b                       ;set Options-Flag
ret

CLO_MinRefresh:
 call GetNumFP8
 mov [FP8_MinRefresh],ax
 mov al,00000010b                       ;set Options-Flag
ret

CLO_MaxLines:
 call GetNum
 mov [MaxLines],ax
 mov al,00000010b                       ;set Options-Flag
ret

CLO_MinLines:
 call GetNum
 mov [MinLines],ax
 mov al,00000010b                       ;set Options-Flag
ret

CLO_Border:
 call GetNumFP8
 pusha
 mov bx, (FPx_1/256)
 mul bx
 mov bx, 100
 div bx
 mov [FPx_bor],ax
 popa
 mov al,00000010b                       ;set Options-Flag
ret

CLO_VSync:
 call GetNumFP8
 pusha
 mov bx, (FPx_1/256)
 mul bx
 mov bx, 100
 div bx
 mov [FPx_VS],ax
 popa
 mov al,00000010b                       ;set Options-Flag
ret

CLO_VBlank:
 call GetNumFP8
 pusha
 mov bx, (FPx_1/256)
 mul bx
 mov bx, 100
 div bx
 mov [FPx_VB],ax
 popa
 mov al,00000010b                       ;set Options-Flag
ret

SkipSpace:                        ;!DS:DI
 push ax
 mov al,' '
Check_Space:
 scasb                            ;skip spaces
  je Check_Space
 dec di
 pop ax
 cmp di,cx
  jae PopEndCommLine
ret
PopEndCommLine:
pop ax
jmp EndCommLine

int8_counter dw 0

n10 dw 10

ClockCodeTable:
 dw offset(vga_clock)
 dw offset(et4000_clock)
 dw offset(s3_clock)

CommLineOptionsTable:
 db 'e',0,offset(CLO_Enable)       -offset(CLO_Enable)
 db 'd',0,offset(CLO_Disable)      -offset(CLO_Enable)
 db 'v',0,offset(CLO_VgaCard)      -offset(CLO_Disable)
 db 'u',0,offset(CLO_UnInstall)    -offset(CLO_VgaCard)
 db 'p',0,offset(CLO_Probe)        -offset(CLO_UnInstall)
 db 'alf',0,offset(CLO_MaxLineFreq)-offset(CLO_Probe)
 db 'ilf',0,offset(CLO_MinLineFreq)-offset(CLO_MaxLineFreq)
 db 'ar',0,offset(CLO_MaxRefresh)  -offset(CLO_MinLineFreq)
 db 'ir',0,offset(CLO_MinRefresh)  -offset(CLO_MaxRefresh)
 db 'al',0,offset(CLO_MaxLines)    -offset(CLO_MinRefresh)
 db 'il',0,offset(CLO_MinLines)    -offset(CLO_MaxLines)
 db 'bo',0,offset(CLO_Border)      -offset(CLO_MinLines)
 db 'ys',0,offset(CLO_VSync)       -offset(CLO_Border)
 db 'yb',0,offset(CLO_VBlank)      -offset(CLO_VSync)
 db 0

c_r db 'VGA600 ver 0.6 Beta Copyright (C) Michael Niedermayer 1997',cr,lf,'$'
u_c db 'VGA600 <options>|/U|/E|/D|/P',cr,lf
    db ' options: /V n   (VgaCard)     0=VGA 1=ET4000 2=S3(untested)',cr,lf
    db '          /ALF n (MaxLineFreq)    default=0.0',cr,lf
    db '          /ILF n (MinLineFreq)    default=28.0',cr,lf
    db '          /AR n  (MaxRefresh)     default=70.0',cr,lf
    db '          /IR n  (MinRefresh)     default=60.0',cr,lf
    db '          /AL n  (MaxLines)       default=480',cr,lf
    db '          /IL n  (MinLines)       default=300',cr,lf
    db '          /BO n  (Border)         default=1.5',cr,lf
    db '          /YS n  (Vertical-Sync)  default=0.5',cr,lf
    db '          /YB n  (Vertical-Blank) default=7.0',cr,lf
    db '          /E     (Enable)',cr,lf
    db '          /D     (Disable)',cr,lf
    db '          /P     (Probe for PixelClocks)',cr,lf
    db 'ex: VGA600 /V1 /ALF72 /AL1023 /AR90',cr,lf,'$'
n_i db 'VGA600 is not installed!',cr,lf,'$'
n_u db 'VGA600 is not unloadable!',cr,lf,'$'
m_e db 'Mem allocating error!',cr,lf,'$'
p_a db 'Passing arguments to TSR!',cr,lf,'$'
p_e db 'Uninstall VGA600 first!',cr,lf,'$'
d_e db 'Internal Error!',cr,lf,'$'
f_e db 'Clock Data File Access Error!',cr,lf,'$'
end_all:
ends
end start
