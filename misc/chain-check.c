#include <stdlib.h>
#include <inttypes.h>

#define MAX_LENGTH 256*256*256

main(){
    int length, start, i, len;
    uint8_t *array= malloc(MAX_LENGTH+10);
    uint8_t *subchain= malloc(MAX_LENGTH+10);
    
    srand(time(0));

    for(length=16; length<MAX_LENGTH; length+=length){
        printf("length: %d :", length);
        
        for(i=0; i<length; i++)
            array[i]= rand()/19;
        memset(subchain, 0, length);
        
        for(start=0; start<length; start++){
            int packets=0;
            
            if(subchain[start]) continue;

            for(i=start; i<length; ){
                int v= array[i];
                int len0= array[i+1];
                int len1= array[i+2];
                int len2= array[i+3];
                
                if     (!(len0&128)) len= len0 + 2;
                else if(!(len1&128)) len= len0 - 128 + len1*128 + 3;
                else                 len= len0 - 128 + len1*128 - 128*128 + len2*128*128 + 4;
                len= 16*len + (v&15);
                
                packets++;

                subchain[i]=1;
                                
                if(v == 'N') break;
                if(len + i >= length){
                    if(len + i == length)
                        printf("%d(%d) ", packets, length-start);
                    break;
                }
                i += len;
            }
        }
            
        printf("\n");
    }
}
