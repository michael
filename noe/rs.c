/*
 *   Copyright (C) 2002-2008 Michael Niedermayer <michaelni@gmx.at>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <inttypes.h>
#include <assert.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>

#include "noe_internal.h"
#include "galois_internal.h"
#include "rs.h"
#include "gfft.h"


#undef NDEBUG
#include <assert.h>

void EXT(getSyndrom)(GFF4Element *syn, GFF4Element *src, int order, int codeBits){
    *syn++= order;
    if(!M){
        EXT(gfft)(syn, src, codeBits);
        EXT(permute)(syn, syn, order+1, codeBits);
    }else{
        int i;
        assert(codeBits == SHIFT);
        for(i=1; i<=order; i++){
            int bak= src[-1];
            src[-1]= SIZE-2;
            syn[i]= EXT(evalPoly)(src-1, EXT(exp)[i]);
            src[-1]= bak;
        }
    }
    syn[0]= 1;
}

/**
 *
 */
void EXT(rsEncode)(GFF4Element *data, GFF4Element *parityLocator, int parityCount, int codeBits){
    int i;
    const int decimate= SHIFT - codeBits;
    const int dataCount= ((SIZE - 1)>>decimate) - parityCount;
    int parityLocations[SIZE]; //[parityCount]

    for(i=0; i<parityCount; i++)
        parityLocations[i]= (dataCount + i)<<decimate;

    EXT(rsDecode)(data, parityLocations, parityLocator, parityCount, parityCount, codeBits);
}

#if M==0
//FIXME avoid the noe_RsEncoder dependancy
int EXT(rsTransform)(GFF4Element *data, GFF4Element *parityLocator, int parityCount, GFF4Element *tPoly, int tLocation, int encode, int codeBits){
    int i, j;
    const int decimate= SHIFT - codeBits;
    const int dataCount= ((SIZE - 1)>>decimate) - parityCount;
    GFF4Element temp[SIZE-1];
    char *stats= (char*)temp;
    GFF4Element t= data[ tLocation ];
    int statsSize= encode ? parityCount + t + 1 : t;

    if(tLocation<0 || tLocation>=dataCount || statsSize>SIZE)
        return -1;

    if(tPoly[0]<=0){
        memset(temp, 0, sizeof(temp));
        temp[tLocation]= 1;
        EXT(rsEncode)(temp, parityLocator, parityCount, codeBits);
        memcpy(tPoly, temp + dataCount, parityCount*sizeof(GFF4Element));
    }

    memset(stats, 0, statsSize);

    for(i=0; i<parityCount; i++){
        GFF4Element p= diff(data[i+dataCount], prod(tPoly[i], t));

        j= prod(MINUS1 - p, inv(tPoly[i]));  //FIXME get rid of the neg
        if(j<statsSize)
            stats[j]=1;
    }

    if(encode){
        for(j=i=0; i<SIZE && j<=t; i++)
            j+= !stats[i];
        i--;
        assert(i<SIZE);
    }else{
        for(j=i=0; j<t; j++)
            i+= stats[j]^1;
    }
    data[ tLocation ]= i;
    t= diff(i, t);

    for(i=0; i<parityCount; i++)
        data[i+dataCount]= sum(data[i+dataCount], prod(tPoly[i], t));

    return 0;
}
#endif

#ifdef ALTERNATIVE_SOLVERS
#define XCHG(a,b) {void *tmp= a; a= b; b= tmp;}

static void hgcd(GFF4Element *ma[2], GFF4Element *mb[2], GFF4Element *a, GFF4Element *b, int len){
    int j;
    int end= a[0] + b[0] - len;
    assert(a[0] >= b[0]);

//    assert(len);
//printf("In %d %d %d len=%d\n", level, a[0], b[0], len);
    if(len > 256){
        int max;
        int len2= len>>1;
        GFF4Element a_tmp[4*len];
        GFF4Element b_tmp[4*len];
        GFF4Element m2[4][2*len];
        GFF4Element *ma2[2]= {m2[0], m2[1]};
        GFF4Element *mb2[2]= {m2[2], m2[3]};
        assert(len-1 <= a[0]);
        assert(len-1 <= b[0]);
        a_tmp[0]= a[0] - b[0] + len2-1;
        b_tmp[0]=               len2-1;
        max= a[0] - a_tmp[0];
        memcpy(a_tmp+1, a+1+a[0]-a_tmp[0], (a_tmp[0]+1)*sizeof(GFF4Element));
        memcpy(b_tmp+1, b+1+b[0]-b_tmp[0], (b_tmp[0]+1)*sizeof(GFF4Element));
        hgcd(ma, mb, a_tmp, b_tmp, len2);

        max+= NOE_MAX(a_tmp[0], b_tmp[0]);

        EXT(partialProdPoly)(a_tmp, b, ma[1], max);
        EXT(partialProdPoly)(b_tmp, a, mb[0], max);
        EXT(partialProdPoly)(a    , a, ma[0], max);
        EXT(partialProdPoly)(b    , b, mb[1], max);
        EXT(sumPoly)(a, a, a_tmp);
        EXT(sumPoly)(b, b, b_tmp);
        len2= a[0] + b[0] - end;
//printf("Mid %d %d %d len2=%d\n", level, a[0], b[0], len2);

        if(len2 == 0 || a[ a[0] + 1 ]==0)
            return;
            
        assert(len2>0);
        assert(len2 <= a[0]+1);
        assert(len2 <= b[0]+1);

        if(a[0] < b[0]){
            XCHG(a,b);
            XCHG(ma,mb);
        }
        j= b[0] + 1 - len2;
        a[j]= a[0] - j;
        b[j]= b[0] - j;
        hgcd(ma2, mb2, a + j, b + j, len2);

//printf("End %d %d %d\n", level, a[0], b[0]);
        EXT(prodMatrix)(ma, mb, ma2, mb2);
    }else{
        SET_POLY0(ma[0], 1);
        SET_POLY0(ma[1], 0);
        SET_POLY0(mb[0], 0);
        SET_POLY0(mb[1], 1);

        for(;;){
            GFF4Element q= neg(prod(a[ a[0]+1 ], inv(b[ b[0]+1 ])));
            int s= a[0] - b[0];

            assert(s >= 0);
            assert(a[ a[0]+1 ]);
            assert(b[ b[0]+1 ]);

            for(j=end - b[0] + 2; j<=a[0]; j++){
                a[ j ] = sum(a[ j ], prod(q, b[ j-s ]));
            }
            a[0]--;

            EXT(scaledSumPoly)(ma[0], ma[0], mb[0], q, s);
            EXT(scaledSumPoly)(ma[1], ma[1], mb[1], q, s);

            EXT(getOrder)(a);
            if(a[0] + b[0] <= end || a[a[0] + 1]==0)
                return;

            if(a[0] < b[0]){
                XCHG(a,b)
                XCHG(ma,mb)
            }
        }
    }
}

static int rsSchoenhage(GFF4Element *locator[2], GFF4Element *omega[2], int parityCount, int erasureCount){
    int i;
    GFF4Element quot[parityCount+2]; //FIXME size ok?

    omega[0][0]= parityCount+1;
    memset(omega[0]+1, 0, (parityCount+1)*sizeof(GFF4Element));
    omega[0][parityCount+1+1]= 1;
    SET_POLY0(locator[0], 0);
    SET_POLY0(locator[1], 1);
    
    for(i=0; 1; i++){
        GFF4Element a_tmp[SIZE];
        GFF4Element b_tmp[SIZE];
        GFF4Element m[4][SIZE];
        GFF4Element *ma[2]= {m[0], m[1]};
        GFF4Element *mb[2]= {m[2], m[3]};
        int max;

        const int di= i&1;
        const int si= 1-di;
        int len= 2*omega[si][0] - parityCount - erasureCount;
//printf("%d %d\n", omega[si][0], omega[di][0]);
        if(2*omega[si][0] <= parityCount + erasureCount)
            return si;

        assert(len <= omega[si][0]+1);
        if(len>128 && omega[si][0]/2 < len) len= omega[si][0]/2;

        assert(omega[si][ omega[si][0]+1 ]);

        a_tmp[0]= omega[di][0] - omega[si][0] + len-1;
        b_tmp[0]=                               len-1;
        memcpy(a_tmp+1, omega[di]+1+omega[di][0]-a_tmp[0], (a_tmp[0]+1)*sizeof(GFF4Element));
        memcpy(b_tmp+1, omega[si]+1+omega[si][0]-b_tmp[0], (b_tmp[0]+1)*sizeof(GFF4Element));
        assert(b_tmp[ b_tmp[0]+1 ]);
//printf("In 0 %d %d\n", omega[di][0], omega[si][0]);
        hgcd(ma, mb, a_tmp, b_tmp, len);
        max= omega[si][0] - (len-1) + NOE_MAX(a_tmp[0], b_tmp[0]);

        EXT(partialProdPoly)(a_tmp, omega[si], ma[1], max);
        EXT(partialProdPoly)(b_tmp, omega[di], mb[0], max);
        EXT(partialProdPoly)(omega[di], omega[di], ma[0], max);
        EXT(partialProdPoly)(omega[si], omega[si], mb[1], max);
        EXT(sumPoly)(omega[di], omega[di], a_tmp);
        EXT(sumPoly)(omega[si], omega[si], b_tmp);

        EXT(prodPoly)(a_tmp, locator[si], ma[1]);
        EXT(prodPoly)(b_tmp, locator[di], mb[0]);
        EXT(prodPoly)(locator[di], locator[di], ma[0]);
        EXT(prodPoly)(locator[si], locator[si], mb[1]);
        EXT(sumPoly)(locator[di], locator[di], a_tmp);
        EXT(sumPoly)(locator[si], locator[si], b_tmp);

        if(omega[si][0] < omega[di][0])
            i++;
    }
}

/**
 * ...
 * @param omega must be parityCount+2 big
 */
static int rsEuclid(GFF4Element *locator[2], GFF4Element *omega[2], int parityCount, int erasureCount){
    int i;
    GFF4Element quot[parityCount+2]; //FIXME size ok?

    omega[0][0]= parityCount+1;
    memset(omega[0]+1, 0, (parityCount+1)*sizeof(GFF4Element));
    omega[0][parityCount+1+1]= 1;
    SET_POLY0(locator[0], 0);
    SET_POLY0(locator[1], 1);

    EXT(getOrder)(omega[1]); //get syndrom isnt guranteed to set this correctly divPoly asserts on it though

    for(i=0; 1; i++){
        const int di= i&1;
        const int si= 1-di;

        if(2*omega[si][0] <= parityCount + erasureCount)
            return si;

        EXT(divPoly)(quot, omega[di], omega[di], omega[si]);

        EXT(prodPoly)(quot, quot, locator[si]);
        EXT(diffPoly)(locator[di], locator[di], quot);
    }
}
#endif

/**
 * ...
 * @param omega must be parityCount+2 big
 */
static int rsBerlekampMassey(GFF4Element *locator[2], GFF4Element *omega[2], int parityCount, int erasureCount){
    GFF4Element *syn= omega[1]+1; //yeah maybe we should clean this ...
    int k, j;
    GFF4Element oldDelta=MINUS1;
    GFF4Element *Tix= locator[0]+1;
    GFF4Element *Lix= locator[1]+1;
    GFF4Element *temp;
    int Tshift= 1;

    Tix[-1]=1;
    Tix[ 0]=1;
    SET_POLY0(Lix-1, 1);

    for(k=1+erasureCount; k<=parityCount; k++){
        const int k2= k - erasureCount;
        GFF4Element delta= syn[k];
        for(j=1; j<=Lix[-1]; j++)
            delta ADD_OP prod(Lix[j], syn[k-j]);

        delta= reduce(delta);

        if(delta){
            GFF4Element factor= prod(delta, oldDelta);
            if(Lix[-1] >= k2 - Lix[-1]){
                for(j=Tshift; j<=Tix[-1]; j++)
                    Lix[j]= sum(Lix[j], prod(factor, Tix[j-Tshift]));
            }else{
                //Tshift == 2 true in >99% of the cases
                if(Tshift == 2 && Lix[-1]>0){
                    for(j=Tix[-1]; j>Lix[-1]; j--)
                        Tix[j]=             prod(factor, Tix[j-2]);
                    for(; j>=2; j--)
                        Tix[j]= sum(Lix[j], prod(factor, Tix[j-2]));
                }else if(Lix[-1] >= Tshift-1){
                    for(j=Tix[-1]; j>Lix[-1]; j--)
                        Tix[j]=             prod(factor, Tix[j-Tshift]);
                    for(; j>=Tshift; j--)
                        Tix[j]= sum(Lix[j], prod(factor, Tix[j-Tshift]));
                }else{
                    for(j=Tix[-1]; j>=Tshift; j--)
                        Tix[j]=             prod(factor, Tix[j-Tshift]);
                    for(; j>Lix[-1]; j--)
                        Tix[j]= 0;
                }
                for(; j>0; j--)
                    Tix[j]= Lix[j];
                Tshift=0;
                temp= Tix; Tix= Lix; Lix= temp;
                oldDelta= neg(inv(delta));
                Lix[-1]= k2 - Tix[-1];
            }
        }
        Tix[-1]++;
        Tshift++;
    }

    assert(Lix[0]==1);

    k= Tix-1 == locator[0];
    EXT(partialProdPoly)(omega[k], Lix-1, syn-1, syn[-1]);

    return k;
}

int EXT(rsDecode)(GFF4Element *data, int *erasure, GFF4Element *erasureLocator, int erasureCount, int parityCount, int codeBits){
    int i, j, k;
    int errorCount, elfft, gfftEval;
    const int decimate= SHIFT - codeBits;
    const int codeCount= (SIZE - 1)>>decimate;
    GFF4Element locator0[SIZE]; //[parityCount + 1 + 1];
    GFF4Element locator1[SIZE]; //[parityCount + 1 + 1];
    GFF4Element *locators[2]={locator0, locator1};
    GFF4Element omega0[SIZE - 1 + 1];
    GFF4Element omega1[SIZE - 1 + 1];
    GFF4Element *omegas[2]={omega0, omega1}; //FIXME clean this shit up
    GFF4Element *errorLocator, *omega, *syn, *psi;
    int error[SIZE>>1]; //[parityCount>>1]

    if(erasureCount > parityCount)
        return -1;

    syn= omegas[1];
    
    /* kill erased symbols */
    for(i=0; i<erasureCount; i++){
        data[ erasure[i]>>decimate ]=0;
    }

    EXT(getSyndrom)(syn, data, parityCount, codeBits);

    for(i=1; i<=parityCount; i++){
        if(syn[i+1]) break;
    }
    if(i>parityCount)
        return 0;

    //FIXME check truncated symbols syndrom

    if(erasureCount){
        if(erasureLocator[0]<=0)
            EXT(synthPoly)(erasureLocator, erasure, erasureCount);
        EXT(partialProdPoly)(syn, erasureLocator, syn, syn[0]);
    }
#if 0
for(i=0; i<erasureCount; i++){
    int eval= EXT(evalPoly)(erasureLocator, inv(EXT(exp)[erasure[i]]));
    assert( eval==0);
}
#endif
//START_TIMER
    i= rsBerlekampMassey(locators, omegas, parityCount, erasureCount);
//    i= rsEuclid(locators, omegas, parityCount, erasureCount);
//    i= rsSchoenhage(locators, omegas, parityCount, erasureCount);
//STOP_TIMER
//printf("errorCount %d \n", errorCount);

    if(i<0) return -1;
    errorCount= locators[i][0];

    omega= omegas[i];
    errorLocator= locators[i];
    psi= locators[1-i]; //reuse some unused space

    elfft = !M && errorCount>20; //FIXME finetune threshold

    if(M)                                   gfftEval= 0;
    else if(errorCount + erasureCount > codeCount/64) gfftEval= 2;
    else                                    gfftEval= errorCount>20;

    if(erasureCount && errorCount){
        EXT(prodPoly)(psi, errorLocator, erasureLocator);
        EXT(getDerivative)(psi, psi);
    }else{
        EXT(getDerivative)(psi, erasureCount ? erasureLocator : errorLocator);
    }

    if(elfft){
        errorLocator++;
        EXT(gfft_0padded)(errorLocator, errorLocator, codeBits, errorLocator[-1]+1);

        for(j=0,i=0; j<errorCount; i++){
            if(i >= codeCount)
                return -1;
            if(!errorLocator[i])
                error[j++]= i ? MINUS1 -  bitReverse(i, SHIFT) : 0;
        }
    }else{
        for(j=0,i=0; j<errorCount; i += 1<<decimate){
            int i2= EXT(exp)[MINUS1 - i];
            if(i >= SIZE)
                return -1;
            if(!EXT(evalPoly)(errorLocator, i2)){
                GFF4Element rem=errorLocator[ errorLocator[0]+1 ];
                for(k=errorLocator[0]; k>0; k--){
                    GFF4Element temp= sum(errorLocator[k], prod(rem, i2));
                    errorLocator[k]= rem;
                    rem= temp;
                }
                assert(rem == 0);
                errorLocator[0]--;

                error[j++]= i;
                assert(!decimate || !(error[j-1]&1));
            }
        }
    }

    if(gfftEval>1){
        omega++;
        psi++;
        EXT(gfft_0padded)(omega, omega, codeBits, omega[-1]+1);
        EXT(gfft_0padded)(psi  , psi  , codeBits, psi  [-1]+1);
    }

    for(i=0; i<erasureCount + errorCount; i++){
        const int r= i<erasureCount ? erasure[i] : error[i-erasureCount];
        int e;

        assert(r>=0 && r<MINUS1);
        
        if(gfftEval>1){
            int i2= bitReverse(MINUS1 - r, SHIFT);
            e= r - EXT(log)[  psi[i2]];
            if(e<0) e+= MINUS1;
            e+=    EXT(log)[omega[i2]];
        }else{
            GFF4Element invX= EXT(exp)[MINUS1 - r];
            e= r - EXT(log)[EXT(evalPoly)(  psi, invX)];
            if(e<0) e+= MINUS1;
            e+=    EXT(log)[EXT(evalPoly)(omega, invX)];
        }

        data[r>>decimate]= sum(data[r>>decimate], EXT(exp)[e]);
    }

    return erasureCount + errorCount;
}

