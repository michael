/*
 *   Copyright (C) 2008 Michael Niedermayer <michaelni@gmx.at>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <inttypes.h>
#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

#include "galois_internal.h"
#include "random_internal.h"
#include "ldpc.h"

#undef NDEBUG
#include <assert.h>

int main(){
    int data_len, parity_len, i, x, reliable;
    KISSState kiss;
    int failsum=0;

    init_random(&kiss, 0);
    EXT(init)();

    for(data_len=100; data_len < 2000; data_len<<=1){
        for(parity_len=10; parity_len < data_len; parity_len<<=1){
          for(reliable=0; reliable<=1; reliable++){
            const int code_len= data_len + parity_len;
            int seed, erasure_count, try;
            struct LDPCContext *enc= NULL;
            struct LDPCContext *dec= NULL;
            int parity_pos[parity_len];

            for(i=0; i<parity_len; i++)
                parity_pos[i]= data_len + i;

            fprintf(stderr, "I");
            for(seed=0; 1; seed++){
                enc= EXT(initLDPC)(data_len, parity_len, seed, reliable ? 0 : 3);
                if(EXT(init_matrixLDPC)(enc, parity_len, parity_pos) >= 0)
                    break;
                EXT(freeLDPC)(enc);
            }

            dec= EXT(initLDPC)(data_len, parity_len, seed, reliable ? 0 : 3);

            for(erasure_count= reliable ? parity_len-(SIZE==2 ? 4 : 1) : parity_len*3/4; ; erasure_count--){
                int fail=0;
                for(try=0; try<10; try++){
                    LDPC_ELEM code[code_len], code_bak[code_len];
                    int erasure_pos[erasure_count];

                    fprintf(stderr, "E");
                    for(i=0; i<data_len; i++)
                        code[i]= code_bak[i]= get_random(&kiss);
                    EXT(decodeLDPC)(enc, code, parity_len, parity_pos);
                    memcpy(code_bak+data_len, code+data_len, parity_len * sizeof(*code));
                    for(i=0; i<code_len; i++)
                        if(code[i] != code_bak[i])
                            fprintf(stderr, "FATALXX error %X!= %X at %d\n", code[i], code_bak[i], i);

                    fprintf(stderr, "F");
                    for(i=0; i<erasure_count; i++){
                        do{
                            x= get_random(&kiss) % code_len;
                        }while(code[x] != code_bak[x]);
                        code[x]+= get_random(&kiss)%(8*sizeof(LDPC_ELEM)-1)+1;
                        erasure_pos[i]= x;
                    }

                    if(EXT(init_matrixLDPC)(dec, erasure_count, erasure_pos) < 0){
                        fail++;
                        continue;
                    }
                    fprintf(stderr, "D");
                    for(i=0; i<100; i++)
                        EXT(decodeLDPC)(dec, code, erasure_count, erasure_pos);
                    for(i=0; i<code_len; i++){
                        if(code[i] != code_bak[i])
                            fprintf(stderr, "FATAL error %X!= %X at %d\n", code[i], code_bak[i], i);
                    }
                }
                printf("data:%5d parity:%5d extra:%3d fail:%d seed:%d\n", data_len, parity_len, parity_len-erasure_count, fail, seed);
                failsum+= fail;
                if(!fail)
                    break;
            }
            EXT(freeLDPC)(dec);
            EXT(freeLDPC)(enc);
          }
        }
    }
    printf("failsum:%d\n", failsum);
    return 0;
}
