/*
 *   Copyright (C) 2002-2008 Michael Niedermayer <michaelni@gmx.at>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <inttypes.h>
#include <assert.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

#include "noe_internal.h"
#include "galois_internal.h"
#include "random_internal.h"
#include "gfft.h"
#include "rs.h"
#include "bytestream.h"

static const unsigned int gfftChecksums[20]={
0xF379D8D9,
0xCB7A8F92,
0xE7401691,
0x12340E66,
0x8CE989B1,
0x058D8DF6,
0xB81094BA,
0xE7CF85E8,
0x80A5F7C1,
0x11FFDAF8,
0x6972A0A4,
0x78CE49AC,
0x95F6132F,
0xE74A6870,
0x860EB18B,
0x7A5A1B63,
0xFE3A9EAC,
0xD6DC97A1,
0x27822BAE,
};

int main(){
    int i, j, decimate, codeBits;
    GFF4Element bp=1;
    KISSState rnd;

    EXT(init)();

    printf("Testing GF(%d)\n", SIZE);
    for(i=0; i<SIZE-1; i++){
        if(bp==0 || (bp==1 && i>0) || bp >= SIZE){
            printf("FAIL %d %d\n", i, bp);
            break;
        }
        bp= prod(bp, PRIMITIVE_ELEMENT);
    }
    if(bp!=1) printf("FAILED to reach 1\n");
    
    printf("Testing inverse\n");
    bp=1;
    for(i=1; i<SIZE; i++){
        if(prod(bp, inv(bp)) != 1){
            printf("FAIL %X %X\n", (int)bp, (int)inv(bp));
            break;
        }
        bp= prod(bp, PRIMITIVE_ELEMENT);
    }
#if 0
    printf("Experiment");
    for(i=0; i<(1<<30); i++){
        unsigned int a= reduce(i*0xFF01);
        unsigned int b= reduce(reduce(i)*0xFF01);
        
        if(a!=b) printf("%X %X %X %X\n", i, reduce(i), i*0xFF01, reduce(i)*0xFF01);
    }
#endif
    init_random(&rnd, 31415);
  if(!M){
    printf("Testing GFFT");
    for(i=1; i<20; i++){
        int n= SIZE - 1;
        GFF4Element syn [SIZE - 1];
        GFF4Element code[SIZE - 1];
        int acc=0;
        
        for(j=0; j<n; j++)
            code[j]= get_random(&rnd) % SIZE;

        EXT(gfft)(syn, code, SHIFT);
        
        for(j=0; j<n; j++){
            acc+= syn[j];
            acc*=2*j+1;
        }
        if(gfftChecksums[i-1] != acc && SHIFT == 16) 
            printf("FAIL: 0x%08X != 0x%08X\n", gfftChecksums[i-1], acc);
        
        EXT(igfft)(syn, syn, SHIFT);
        for(j=0; j<n; j++){
            syn[j]= prod(syn[j], inv(MINUS1));
        }
        
        for(j=0; j<n; j++){
            if(syn[j] != code[j]) printf("IGFFT missmatch at %d %X!=%X\n", j, syn[j], code[j]);
        }
        
//      EXT(printPoly)(gen, i);
//      EXT(printPoly)(syn, i-1);
        
        printf("."); fflush(stdout);
    }
  }
#if 1
    srand (31415);

    for(codeBits=8; codeBits<=SHIFT; codeBits++){
        printf("\n\nCodeSize=%d\n", 1<<codeBits);
        decimate= SHIFT - codeBits;
#if 0 // only func using getRsGenerator so droped with it
    printf("\nTesting generator polynoms");
    for(i=1; i<100; i+=5){
        int n= (SIZE - 1)>>decimate;
        int k= n - i;
        GFF4Element syn[n+1];
        GFF4Element gen[i+1+1];
        GFF4Element data[k+1];
        GFF4Element code[n+1];

        data[0]= k-1;
        for(j=0; j<k; j++)
            data[j+1]= get_random(&rnd) & MASK;
        
        EXT(getRsGenerator)(gen, 1, i);
        EXT(prodPoly)(code, gen, data);
        assert(code[0]==n-1);
        
        EXT(getSyndrom)(syn, code+1, i, codeBits);
        
//      EXT(printPoly)(gen, i);
//      EXT(printPoly)(syn, i-1);
        
        for(j=0; j<i; j++){
            if(syn[j+1+1]) printf("FAIL generator:%d coefficient:%d\n", i, j);
        }
        printf("."); fflush(stdout);
    }
#endif
#if 1
    printf("\nTesting encoder");
    for(i=1; i<(SIZE/2)>>decimate; i+=i+5){
        int n= (SIZE - 1)>>decimate;
        int k= n - i;
        GFF4Element syn[SIZE];
//      GFF4Element gen[i+1];
        GFF4Element data[SIZE - 1];
        GFF4Element code[SIZE - 1];
        int pass=5;
        int tLoc= get_random(&rnd) % k;
        GFF4Element parityLocator[SIZE/2+2];
        GFF4Element tPoly[SIZE/2];

        parityLocator[0]=0;
        tPoly[0]= 0;

        for(pass=5; pass; pass--){
            for(j=0; j<k; j++)
                data[j]= get_random(&rnd) & MASK;
            data[tLoc] %= k;

            memcpy(code, data, k*sizeof(GFF4Element));
{START_TIMER
            EXT(rsEncode)(code, parityLocator, i, codeBits);
STOP_TIMER}
            EXT(getSyndrom)(syn, code, i, codeBits);
            //FIXME check that code contains data, but its not touched so ...

    //      EXT(printPoly)(gen, i);
    //      EXT(printPoly)(syn, i-1);

            for(j=0; j<i; j++){
                if(syn[j+1+1]) printf("FAIL generator:%d coefficient:%d is %X\n", i, j, syn[j+1]);
            }
#if M==0
            if(EXT(rsTransform)(code, parityLocator, i, tPoly, tLoc, 1, codeBits) < 0)
                printf("transform failure\n");
            EXT(getSyndrom)(syn, code, i, codeBits);
            for(j=0; j<i; j++)
                if(syn[j+1+1]) printf("FAILT generator:%d coefficient:%d is %X\n", i, j, syn[j+1]);
            for(j=0; j<n; j++){
                if(code[j] == MINUS1)
                    printf("illegal symbol detected\n");
            }

            if(EXT(rsTransform)(code, parityLocator, i, tPoly, tLoc, 0, codeBits) < 0)
                printf("inv-transform failure\n");
            EXT(getSyndrom)(syn, code, i, codeBits);
            for(j=0; j<i; j++)
                if(syn[j+1+1]) printf("FAILTT generator:%d coefficient:%d is %X\n", i, j, syn[j+1]);
            for(j=0; j<k; j++){
                if(data[j] != code[j])
                    printf("data changed\n");
            }
#endif
            printf("."); fflush(stdout);
        }
    }
#endif
#endif
    printf("\nTesting error and erasure decoding\n");
    for(i=1; i<(SIZE/2)>>decimate; i+=i+1){
        int n= (SIZE - 1)>>decimate;
        int k= n - i;
        int erased[SIZE/2];
        GFF4Element erasureLocator[SIZE/2+2];
        GFF4Element data[SIZE - 1];
        GFF4Element code[SIZE - 1];
        GFF4Element parityLocator [SIZE/2+2];
        int pass=5;
        uint64_t end, start= rdtsc();
        int usedParityCount;

        parityLocator[0]= 0;

        for(pass=5; pass; pass--){
            int erasedCount, errorCount;
            for(j=0; j<k; j++)
                data[j]= get_random(&rnd) & MASK;

            memcpy(code, data, n*sizeof(GFF4Element));
            erasureLocator[0]= 0;

            EXT(rsEncode)(code, parityLocator, i, codeBits);
            
            erasedCount=0;
            if(get_random(&rnd)&0x40){
                erasedCount= (get_random(&rnd)%i)+1;
                for(j=0; j<erasedCount; j++){
                    int e=-1;
                    
                    while(e==-1 || code[e]==MINUS1) e=get_random(&rnd)%n;

                    erased[j]= e<<decimate;
                    code[ e ]= MINUS1;
                }
                for(j=0; j<erasedCount; j++){
                    int e= erased[j]>>decimate;

                    code[ e ]= get_random(&rnd)&MASK;
                }
            }
            errorCount=0;
            if(erasedCount==0 || (get_random(&rnd)&0x40)){
                int max= (i - erasedCount)/2;
                if(max){
                    errorCount= (get_random(&rnd) % max)+1;
                    for(j=0; j<errorCount; j++){
                        int pos= get_random(&rnd)%n;
                        code[pos] = get_random(&rnd)&MASK;
//                      printf("P%6d\n", pos);
                    }
                }
            }
            if(get_random(&rnd)&0x40){
                usedParityCount= erasedCount + 2*errorCount;
            }else
                usedParityCount= i;

//printf("erased:%d errors:%d parity:%d\n", erasedCount, errorCount, i);
            EXT(rsDecode)(code, erased, erasureLocator, erasedCount, usedParityCount, codeBits);

    //      EXT(printPoly)(gen, i);
    //      EXT(printPoly)(syn, i-1);

            for(j=0; j<k; j++){
                if(data[j]!=code[j]){
                    printf("FAIL at:%d is %X %X\n", j, data[j], code[j]);
                    break;
                }
            }
        }

        end= rdtsc();
        printf("%Ld cpu cycles for (%d, %d) code, %f bytes/cycle\n", (end-start)/5, n, k, 10*n/(float)(end-start)); fflush(stdout);
    }

    //Encoding is implemented as erasure decoding thus we dont need to
    //benchmark them seperately
    printf("\nencoding / erasure decoding benchmarking\n"); fflush(stdout);
        for(i=1; i<(SIZE/2)>>decimate; i+=i+1){
            int n= (SIZE - 1)>>decimate;
            int k= n - i;
            GFF4Element code[SIZE - 1];
            GFF4Element parityLocator[SIZE/2+2];
            int pass;
            uint64_t end, start= rdtsc();

            parityLocator[0]= 0;
            for(pass=10; pass; pass--){
                for(j=0; j<k; j++)
                    code[j]= (pass*j+123) & MASK;

                EXT(rsEncode)(code, parityLocator, i, codeBits);
            }
            end= rdtsc();
            printf("%Ld cpu cycles for (%d, %d) code, %f bytes/cycle\n", (end-start)/10, n, k, 20*n/(float)(end-start)); fflush(stdout);
        }
    }
#if 0
    {
        uint8_t buffer[20000];
        uint32_t val[2000];
        ByteStream bs;

        printf("\nTesting v coder");
        
        initByteStream(&bs, buffer, 20000);
        for(i=0; i<2000; i++){
            int len= get_random(&rnd)%32;
            val[i] = get_random(&rnd) % (1<<len);
            put_v(&bs, val[i]);
            if(i%100 == 0) printf(".");
        }

        initByteStream(&bs, buffer, 20000);
        for(i=0; i<2000; i++){
            if(val[i] != get_v(&bs)){
                printf("FAIL\n");
                break;
            }
            if(i%100 == 0) printf(".");
        }
    }
#endif
    printf("\n");

    return 0;
}
