/*
 *   Copyright (C) 2002-2008 Michael Niedermayer <michaelni@gmx.at>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/**
 * Public header for reed solomon decoding.
 * @Note, noe_init_*() has to be called before using any functions from this file.
 */

/**
 * gets the syndroms.
 * @param src a 2^16 entry long polynom
 * @param syn the syndrom polynom will be stored here
 * @param codeBits log2(codeSize)
 */
void noe_getSyndrom_100(GFF4Element *syn, GFF4Element *src, int order, int codeBits);
void noe_getSyndrom_101(GFF4Element *syn, GFF4Element *src, int order, int codeBits);
void noe_getSyndrom_10001(GFF4Element *syn, GFF4Element *src, int order, int codeBits);

/**
 * Appends parity symbols to data to make the whole a Reed Solomon codeword.
 * @param data The input data to encode and the output codeword
 *        The length of the input data is (1<<codeBits) - parityCount for GF(fermat prime)
 *        and (1<<codeBits) - parityCount - 1 for GF(2^n) based codes.
 * @param parityCount The number of parity symbols
 * @param parityLocator Some temporary space used by this function,
 *        it should be at least parityCount+2 symbols long and its first symbol
 *        should be set to 0 whenever parityCount changes.
 * @param codeBits log2(codeSize), must be equal to n for GF(2^n) based codes currently,
 *        this may change though.
 */
void noe_rsEncode_100(GFF4Element *data, GFF4Element *parityLocator, int parityCount, int codeBits);
void noe_rsEncode_101(GFF4Element *data, GFF4Element *parityLocator, int parityCount, int codeBits);
void noe_rsEncode_10001(GFF4Element *data, GFF4Element *parityLocator, int parityCount, int codeBits);

/**
 * Corrects some data using a Reed Solomon code.
 * @param data The input codeword to correct and the corrected output codeword.
 * @param parityCount The number of parity symbols.
 *        You can pass a smaller value here than what was used during noe_rsEncode*()
 *        Using a smaller value here is faster but decoding is only guranteed to succeed
 *        as long as parityCount >= erasedCount + 2*errorCount
 * @param erasedCount The number of erased symbols, that is symbols for which we know they
 *        are damaged / damaged symbols with known locations.
 * @param erased the indexes in the input codeword of erased symbols.
 * @param erassureLocator Some temporary space used by this function,
 *        it should be at least parityCount+2 symbols long and its first symbol
 *        should be set to 0 whenever erased or erasedCount changes.
 * @param codeBits log2(codeSize), must be equal to n for GF(2^n) based codes currently,
 *        this may change though.
 * @returns the number of corrected symbols or a negative value in case of a uncorrectable error.
 */
int noe_rsDecode_100(GFF4Element *data, int *erased, GFF4Element *erassureLocator, int erasedCount, int parityCount, int codeBits);
int noe_rsDecode_101(GFF4Element *data, int *erased, GFF4Element *erassureLocator, int erasedCount, int parityCount, int codeBits);
int noe_rsDecode_10001(GFF4Element *data, int *erased, GFF4Element *erassureLocator, int erasedCount, int parityCount, int codeBits);

/**
 * Transforms (or inverse transforms) a Reed Solomon code based on GF(fermat prime) so that
 * none of its parity symbols equals the largest symbol. Both the original and the transformed
 * codes are normal RS codes. This transform is usefull as the largest symbol of GF(fermat prime)
 * does not fit in 16 (or 8 bit).
 * (Possibly) damaged codes should always be corrected through rsDecode() before using this
 * function here!
 * This function/transform needs a data symbol (at tLocation) that is not too large, that
 * is for 50% or less parity than code the most significant bit at tLocation must be 0.
 * @param data The input and output codeword.
 * @param parityCount The number of parity symbols
 * @param parityLocator Some temporary space used by this function,
 *        it should be at least parityCount+2 symbols long and its first symbol
 *        should be set to 0 whenever parityCount changes.
 * @param tPoly Some temporary space used by this function,
 *        it should be at least parityCount symbols long and its first symbol
 *        should be set to 0 whenever parityCount or tLocation changes.
 * @param tLocation
 * @param codeBits log2(codeSize), must be equal to n for GF(2^n) based codes currently,
 *        this may change though.
 * @param encode 1 for the transform to avoid the largest symbols, 0 to reverse it.
 * @returns 0 if success or a negative value in case of an error.
 */
int noe_rsTransform_10001(GFF4Element *data, GFF4Element *parityLocator, int parityCount, GFF4Element *tPoly, int tLocation, int encode, int codeBits);
