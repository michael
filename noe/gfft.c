/*
 *   Copyright (C) 2002-2008 Michael Niedermayer <michaelni@gmx.at>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */
 
#include <inttypes.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "noe_internal.h"
#include "galois_internal.h"
#include "gfft.h"

uint8_t noe_revTable[257];

void noe_gfft_init(){
    int i;
    
    for(i=0; i<257; i++){
        int x= i;
        x = ((x & 0xaa) >> 1) | ((x & 0x55) << 1);
        x = ((x & 0xcc) >> 2) | ((x & 0x33) << 2);
        x = ( x         >> 4) | ( x         << 4);
        
        noe_revTable[i]= x;
    }
}

void EXT(permute)(GFF4Element *dst, GFF4Element *src, int outCount, int codeBits){
    int i;

    if(dst==src){
        for(i=1; i<outCount; i++){
            const int r= bitReverse(i, codeBits);
            unsigned int t;
            if(r<=i) continue;
            t= dst[i];
            dst[i]= dst[r];
            dst[r]= t;
        }
    }else{
        for(i=0; i<outCount; i++){
            dst[i]= src[bitReverse(i, codeBits)];
        }
    }
}

static inline void fft2(GFF4Element *p){
    const unsigned int a= p[0];
    const unsigned int b= p[1];
    
    p[0]= sum(a, b);
    p[1]= sum(a, SIZE - b);
}

static inline void fft4(GFF4Element *p){
    unsigned int a,b,c,d;

    d= p[0] + SIZE*(SIZE/2ULL);
    
    a= d   +p[2];
    b= p[1]+p[3];
    c= d   -p[2];
    d= (p[3] - p[1])<<(SHIFT/2) /*(p[1] - p[3])*noe_exp[16384]*/;

    p[0]= reduce(a+b);
    p[1]= reduce(a-b);
    p[2]= reduce(c+d);
    p[3]= reduce(c-d);
}

/*
aWi + bWiWn
(aWi - bWiWn)W2i

(a + bWn)Wi
(a - bWn)WiW2i
*/
static inline void fft8(GFF4Element *p){
    unsigned int a,b;

    a= p[0];
    b= p[4];
    p[0]= a+b;
    p[4]= a-b;

    a= p[1];
    b= p[5];
    p[1]= a+b;
    p[5]= hsreduce(((a-b)<<(SHIFT*3/4)))/*(a-b)*noe_exp[8192]*/;

    a= p[2];
    b= p[6];
    p[2]= a+b;
    p[6]= (b-a)<<(SHIFT/2) /*(a-b)*noe_exp[16384]*/;

    a= p[3];
    b= p[7];
    p[3]= a+b;
    p[7]= (a-b)<<(SHIFT/4)/*(a-b)*noe_exp[3*8192]*/;

    fft4(p);
    fft4(p+4);
}

static void fft16(GFF4Element *p){
    int n;

    for(n=0; n<4; n++){
        const unsigned int w2= EXT(exp)[(  n+4)<<(SHIFT-4) ];
        const unsigned int wx= EXT(exp)[(3*n+4)<<(SHIFT-4) ];
              unsigned int a= p[n      ];
              unsigned int b= p[n +   4];
        const unsigned int c= p[n + 2*4];
        const unsigned int d= p[n + 3*4];

        p[n      ]= a + c;
        a         = SIZE + (SIZE<<(SHIFT/2)) + ((a - c)<<(SHIFT/2));
        p[n +   4]= b + d;
        b         = b - d;

        p[n + 2*4]= hureduce(reduce(a + b)*w2);
        p[n + 3*4]= hureduce(reduce(a - b)*wx);
    }

    fft8(p);
    fft4(p+8);
    fft4(p+12);
}

static void fftn(GFF4Element *p, int logSize){
    int n;
    const int size= 1<<(logSize-2);

    for(n=0; n<size; n++){
        const unsigned int w2= EXT(exp)[(  n+size)<<(SHIFT-logSize ) ];
        const unsigned int wx= EXT(exp)[(3*n+size)<<(SHIFT-logSize ) ];
              unsigned int a= p[n         ];
              unsigned int b= p[n +   size];
        const unsigned int c= p[n + 2*size];
        const unsigned int d= p[n + 3*size];

        p[n         ]= sum (a, c);
        a            = SIZE + (SIZE<<(SHIFT/2)) + ((a - c)<<(SHIFT/2));
        p[n +   size]= sum (b, d);
        b            = b - d;

        p[n + 2*size]= reduce(reduce(a + b)*w2);
        p[n + 3*size]= reduce(reduce(a - b)*wx);
    }

    if(logSize==6){
        fftn(p, logSize-1);
        fft16(p+32);
        fft16(p+48);
    }else if(logSize==5){
        fft16(p);
        fft8 (p+16);
        fft8 (p+24);
    }else{
        fftn(p       , logSize-1);
        fftn(p+2*size, logSize-2);
        fftn(p+3*size, logSize-2);
    }
}

static void fftn2(GFF4Element *p, GFF4Element *src, int logSize){
    int n;
    const int size= 1<<(logSize-2);

    for(n=0; n<size; n++){
        const unsigned int w2= EXT(exp)[(  n+size)<<(SHIFT-logSize ) ];
        const unsigned int wx= EXT(exp)[(3*n+size)<<(SHIFT-logSize ) ];
              unsigned int a= src[n         ];
              unsigned int b= src[n +   size];
        const unsigned int c= src[n + 2*size];
        const unsigned int d= src[n + 3*size];

        p[n         ]= sum (a, c);
        a            = SIZE + (SIZE<<(SHIFT/2)) + ((a - c)<<(SHIFT/2));
        p[n +   size]= sum (b, d);
        b            = b - d;

        p[n + 2*size]= reduce(reduce(a + b)*w2);
        p[n + 3*size]= reduce(reduce(a - b)*wx);
    }

    if(logSize==6){
        fftn(p, logSize-1);
        fft16(p+32);
        fft16(p+48);
    }else if(logSize==5){
        fft16(p);
        fft8 (p+16);
        fft8 (p+24);
    }else{
        fftn(p       , logSize-1);
        fftn(p+2*size, logSize-2);
        fftn(p+3*size, logSize-2);
    }
}

static inline void ifft2(GFF4Element *p){
    const unsigned int a= p[0];
    const unsigned int b= p[1];
    
    p[0]= sum(a, b);
    p[1]= sum(a, SIZE - b);
}

static inline void ifft4(GFF4Element *p, int step){
    unsigned int a,b,c,d;

    d= p[0] + SIZE*(SIZE/2ULL);

    a= d   +p[step];
    c= d   -p[step];
    b=  p[2*step] + p[3*step];
    d= (p[2*step] - p[3*step])<<(SHIFT/2) /*(p[1] - p[3])*noe_exp[16384]*/;

    p[0     ]= reduce(a+b);
    p[2*step]= reduce(a-b);
    p[1*step]= reduce(c+d);
    p[3*step]= reduce(c-d);
}

static inline void ifft8(GFF4Element *p){
    unsigned int a,b;

    a= p[0];
    b= p[1];
    p[0]= a+b;
    p[1]= a-b;

    a= p[6];
    b= p[7];
    p[6]= a+b;
    p[7]= hsreduce(((b-a)<<(SHIFT*3/4)))/*(a-b)*noe_exp[8192]*/;

    a= p[2];
    b= p[3];
    p[2]= a+b;
    p[3]= (a-b)<<(SHIFT/2) /*(a-b)*noe_exp[16384]*/;

    a= p[4];
    b= p[5];
    p[4]= a+b;
    p[5]= (b-a)<<(SHIFT/4)/*(a-b)*noe_exp[3*8192]*/;

    ifft4(p  ,2);
    ifft4(p+1,2);
}

static void ifft16(GFF4Element *p){
    int n;
    const int size= 1<<(4-2);

    ifft8(p);
    ifft4(p+8 ,1);
    ifft4(p+12,1);

    for(n=0; n<size; n++){
        const unsigned int w1= EXT(exp)[ MINUS1 - ( n       <<(SHIFT-4)) ];
        const unsigned int wx= EXT(exp)[ MINUS1 - ((3*n    )<<(SHIFT-4)) ];
        const unsigned int a=        p[n         ];
        const unsigned int b=        p[n +   size] + SIZE + (SIZE<<(SHIFT/2));
              unsigned int c= reduce(p[n + 2*size]*w1);
        const unsigned int t= reduce(p[n + 3*size]*wx);
        unsigned int d= (c - t)<<(SHIFT/2);
        c= sum (c, t);

        p[n         ]= sum(a, c);
        p[n + 2*size]= sum(a, SIZE - c);
        p[n + 1*size]= reduce(b + d);
        p[n + 3*size]= reduce(b - d);
    }
}

static void ifftn(GFF4Element *p, int logSize){
    int n;
    const int size= 1<<(logSize-2);

    if(logSize==5){
        ifft16(p);
        ifft8(p+16);
        ifft8(p+24);
    } else if(logSize==6){
        ifftn(p       , logSize-1);
        ifft16(p+32);
        ifft16(p+48);
    }else{
        ifftn(p       , logSize-1);
        ifftn(p+2*size, logSize-2);
        ifftn(p+3*size, logSize-2);
    }
    
    for(n=0; n<size; n++){
        const unsigned int w1= EXT(exp)[ MINUS1 - ( n       <<(SHIFT-logSize)) ];
        const unsigned int wx= EXT(exp)[ MINUS1 - ((3*n    )<<(SHIFT-logSize)) ];
        const unsigned int a=        p[n         ];
        const unsigned int b=        p[n +   size] + SIZE + (SIZE<<(SHIFT/2));
              unsigned int c= reduce(p[n + 2*size]*w1);
        const unsigned int t= reduce(p[n + 3*size]*wx);
        unsigned int d= (c - t)<<(SHIFT/2);
        c= sum (c, t);

        p[n         ]= sum(a, c);
        p[n + 2*size]= sum(a, SIZE - c);
        p[n + 1*size]= reduce(b + d);
        p[n + 3*size]= reduce(b - d);
    }
    
}

void EXT(gfft)(GFF4Element *dst, GFF4Element *src, int logSize){
//  int i, j, pass;
//printf("%X %X\n", noe_exp[4096], noe_exp[3*4096]);

    if(logSize<5){
        memcpy(dst, src, sizeof(*dst)<<logSize);
             if(logSize==1) fft2(dst);
        else if(logSize==2) fft4(dst);
        else if(logSize==3) fft8(dst);
        else if(logSize==4) fft16(dst);
        return;
    }

    assert(logSize>=5);

    if(src==dst)
        fftn(dst, logSize);
    else
        fftn2(dst, src, logSize);
#if 0
    memcpy(dst, src, sizeof(GFF4Element)<<logSize);
    for(pass=0; pass<12; pass++){
        int block;
        const int np= 1<<(16-pass-1);
        const int blockCount= 1<<pass;
        
        for(block=0; block<blockCount; block++){
            int n;
            const int top= block*np<<1;
            const int bot= top + np;
            
            for(n=0; n<np; n++){
                const unsigned int w= EXT(exp)[ n<<pass ];
                const unsigned int a= dst[top + n];
                const unsigned int b= dst[bot + n];
                
                dst[top + n]= sum(a, b);
                dst[bot + n]= prod(sum(a, SIZE - b), w);
            }
        }
    }

    {
        int block;
        for(block=0; block<4096; block++){
{START_TIMER
            fft16(dst + block*16);
STOP_TIMER}
        }
    }
#endif
}

void EXT(gfft_0padded)(GFF4Element *dst, GFF4Element *src, int logSize, int nzCoeffs){
    int lognz= noe_log2(nzCoeffs-1)+1;
    int i, j;

    for(i=1; i<1<<(logSize-lognz); i++){
        int idx= i<<lognz;
        int m= bitReverse(i, SHIFT)>>lognz;
        for(j=0; j<nzCoeffs; j++){
            dst[idx+j]= prod(src[j], EXT(exp)[ j*m ]);
        }
        memset(dst+nzCoeffs+idx, 0, sizeof(*dst)*((1<<lognz)-nzCoeffs));
        EXT(gfft)(dst+idx, dst+idx, lognz);
    }

    memcpy(dst, src, sizeof(*dst)*nzCoeffs);
    memset(dst+nzCoeffs, 0, sizeof(*dst)*((1<<lognz)-nzCoeffs));
    EXT(gfft)(dst, dst, lognz);
}

void EXT(igfft)(GFF4Element *dst, GFF4Element *src, int logSize){
    assert(logSize>=5);

    if(src!=dst)
        memcpy(dst, src, sizeof(GFF4Element)<<logSize);

    ifftn(dst, logSize);
}
