/*
 *   Copyright (C) 2008 Michael Niedermayer <michaelni@gmx.at>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <inttypes.h>
#include <assert.h>
#include <math.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include <limits.h>

#include "noe_internal.h"
#include "galois_internal.h"
#include "random_internal.h"
#include "ldpc.h"

#undef NDEBUG
#include <assert.h>

#define MAX_NZC 32

typedef struct LDPCContext{
    int (*parity_matrix)[2][MAX_NZC];
    int (*inv_matrix)[2];
    int parity_len;
    int data_len;
    int nzc;
    int (*op_table)[2];
    int *temp;
}LDPCContext;

typedef uint8_t ELEM;

static int factor(ELEM *matrix, int width, int height, unsigned int row_weight[], unsigned int col_weight[], int erasure_pos[], int (*op_table)[2]){
    int i, j, k, m, p;
    int (*logline)[2]= malloc((width+1)*sizeof(*logline));
    int ops=0;

    for(p=i=0; i<width; i++){
        int bestweight= height+1;

        for(k=i; k<width; k++){
            if(col_weight[k] < bestweight){
                bestweight=col_weight[k];
                j= k;
            }
        }
        if(j != i){
            for(k=0; k<height; k++)
                SWAP(int, matrix[j + k*width ], matrix[i + k*width])

            SWAP(int, erasure_pos[j], erasure_pos[i])
            SWAP(int, col_weight [j], col_weight [i])
        }

        bestweight= width+1;
        j=-1;
        for(k=p; k<height; k++){
            if(matrix[i + k*width]){
                if(row_weight[k] < bestweight){
                    bestweight=row_weight[k];
                    j= k;
                }
            }
        }
        if(j>=0){
            unsigned int inve= EXT(log)[inv(matrix[i + j*width])];

            SWAP(int, row_weight[j], row_weight[p])
            k=0;
            for(m=i; m<width; m++){
                ELEM t= matrix[m + j*width];
                matrix[m + j*width]= matrix[m + p*width];
                matrix[m + p*width]= EXT(exp)[EXT(log)[t] + inve];
                if(matrix[m + p*width]){
                    logline[k  ][0]= m;
                    logline[k++][1]= EXT(log)[matrix[m + p*width]];
                    col_weight[m]--;
                }
            }
            op_table[ops  ][0]= inve;
            op_table[ops++][1]= -j;

            logline[k][0]= -1;

            for(k=p+1; k<height; k++){ // eliminate lower left
                if(matrix[i + k*width]){
                    unsigned int factor= EXT(log)[neg(matrix[i + k*width])];
                    int idx= logline[1][0];
                    m=1;
                    matrix[i + k*width]= 0;
                    row_weight[k]--;
                    col_weight[i]--;
                    while(idx>=0){
                        if(!matrix[idx + k*width]){
                            row_weight[k]++;
                            col_weight[idx]++;
                        }
                        matrix[idx + k*width] = sum(matrix[idx + k*width], EXT(exp)[logline[m++][1] + factor]);
                        if(!matrix[idx + k*width]){
                            row_weight[k]--;
                            col_weight[idx]--;
                        }
                        idx= logline[m][0];
                    }

                    op_table[ops  ][0]= factor;
                    op_table[ops++][1]= k;
                }
            }
            p++;
        }
    }
    free(logline);
    op_table[ops++][1]= INT_MIN;

    return p;
}

LDPCContext *EXT(initLDPC)(int data_len, int parity_len, uint32_t seed, int nzc){
    LDPCContext *c= malloc(sizeof(LDPCContext));
    int *row_weight;
    KISSState kiss;
    int i, j, y, min_weight;
    int code_len= data_len + parity_len;
    uint8_t *tab;
    int weight_hist[16];

    c->temp= malloc(sizeof(*c->temp)*parity_len*2);
    row_weight= c->temp;
    tab       = c->temp + parity_len;

    if(nzc==0){
#if SIZE==2
        if     (parity_len < 16) nzc=3;
        else                     nzc=5;
#else
        if     (parity_len < 16) nzc=4;
        else if(parity_len < 32) nzc=5;
        else if(parity_len < 64) nzc=5;
        else if(parity_len <256) nzc=6;
        else                     nzc=6;
#endif
    }

    if(nzc > MAX_NZC || nzc > parity_len)
        goto fail;

    init_random(&kiss, seed);

    c->parity_matrix= malloc(code_len * sizeof(*c->parity_matrix));
    c->   inv_matrix= malloc(parity_len*(parity_len+1) * sizeof(*c->inv_matrix));
    c->op_table     = malloc(parity_len*(parity_len+1)/2 * sizeof(*c->op_table));
//FIXME could we in theory need more space?

    memset(row_weight, 0, sizeof(*row_weight)*parity_len);
    memset(tab, 0, parity_len);
    memset(weight_hist, 0, sizeof(weight_hist));
    weight_hist[0]= parity_len;
    min_weight    = 0;

    for(i=0; i<code_len; i++){
        for(j=0; j<nzc; j++){
            if(i<data_len || seed>0){
                while(!weight_hist[min_weight&15])
                    min_weight++;
                do{
                    y= get_random(&kiss)%parity_len;
                }while(tab[y] || (row_weight[y] > min_weight + (SIZE == 2)));
            }else{
                y= i + j - data_len;
                if(y >= parity_len){
                    c->parity_matrix[i][0][j] = -1;
                    continue;
                }
            }
            tab[y]=1;
            c->parity_matrix[i][0][j] = y;
            c->parity_matrix[i][1][j] = EXT(log)[get_random(&kiss)%(SIZE-1)+1];
            weight_hist[ row_weight[y]&15 ]--;
            row_weight[y]++;
            weight_hist[ row_weight[y]&15 ]++;
        }
        for(j=0; j<nzc; j++)
            tab[ c->parity_matrix[i][0][j] ]= 0;
    }
#if 0
    for(;;){
        int maxrow=0;
        int minrow=999999;
        int maxrowx=0;
        uint8_t tab[parity_len];
        memset(tab, 0, parity_len);

        for(j=0; j<parity_len; j++){
            if(row[j] > maxrow){
                maxrow= row[j];
                maxrowx= j;
            }
            if(row[j] < minrow)
                minrow= row[j];
        }
        do{
            j= get_random(&kiss)%nzc;
            i= get_random(&kiss)%code_len;
        }while(c->parity_matrix[i][0][j] != maxrowx);
        for(y=0; y<nzc; y++)
            tab[ c->parity_matrix[i][0][y] ]=1;
        do{
            y= get_random(&kiss)%parity_len;
        }while(tab[y]);
        c->parity_matrix[i][0][j] = y;
        row[y] ++;
        row[maxrowx]--;
//        fprintf(stderr, "%d %d\n", maxrow, minrow);
        if(maxrow - 3 <= minrow)
            break;
    }
#endif

    c->  data_len=   data_len;
    c->parity_len= parity_len;
    c->nzc       = nzc;

    return c;
fail:
    free(c);
    return NULL;
}

int EXT(init_matrixLDPC)(LDPCContext *c, int erasure_count, int erasure_pos[]){
    int i, j, rank, x, y;
    int nzc= c->nzc;
    int parity_len= c->parity_len;
    ELEM *inv_matrix= calloc(parity_len*erasure_count, sizeof(ELEM));
    unsigned int *row_weight= c->temp;
    unsigned int *col_weight= c->temp + parity_len;

    memset(row_weight, 0, sizeof(*row_weight)*parity_len);

    for(i=0; i<erasure_count; i++){
        x= erasure_pos[i];
        for(j=0; j<nzc; j++){
            y= c->parity_matrix[x][0][j];
            if(y>=0){
                inv_matrix[i + y*erasure_count]= EXT(exp)[c->parity_matrix[x][1][j]];
                row_weight[y]++;
            }
        }
        col_weight[i]= nzc;
    }

    rank= factor(inv_matrix, erasure_count, parity_len, row_weight, col_weight, erasure_pos, c->op_table);
    x=0;
    for(j=erasure_count-1; j>=0; j--){
        for(i=0; i<erasure_count; i++){
            if(i != j && inv_matrix[i + j*erasure_count]){
                c->inv_matrix[x  ][0]= i;
#if SIZE == 2
                c->inv_matrix[x++][1]= -inv_matrix[i + j*erasure_count];
#else
                c->inv_matrix[x++][1]= EXT(log)[inv_matrix[i + j*erasure_count]];
#endif
            }
        }
        c->inv_matrix[x++][0]= parity_len;
    }

    free(inv_matrix);
    return rank - erasure_count;
}

int EXT(decodeLDPC)(LDPCContext *c, LDPC_ELEM *code, int erasure_count, int erasure_pos[]){
    int i,j;
    int parity_len= c->parity_len;
    int   code_len= c->data_len + parity_len;
    unsigned int *syndrom= c->temp;
    unsigned int *sp= &syndrom[c->parity_len];
    memset(syndrom, 0, sizeof(*syndrom)*parity_len*2);

    for(i=0; i<erasure_count; i++)
        code[ erasure_pos[i] ]= 0;

#if SIZE == 2
    for(i=0; i<code_len; i++){
        unsigned int v= code[i];
        if(v)
            for(j=0; j<c->nzc; j++){
                int y= c->parity_matrix[i][0][j];
                if(y>=0){
                    sp[y]^= v;
                }
            }
    }


    j=0;
    for(i=0; c->op_table[i][1]>INT_MIN; i++){
        unsigned t= sp[ -c->op_table[i][1] ];
        sp[ -c->op_table[i][1] ]= sp[j];
        sp[j]= t;

        for(i++; c->op_table[i][1]>=0; i++){
            sp[ c->op_table[i][1] ]^= t;
        }
        i--;
        j++;
    }

    j=0;
    for(i=erasure_count-1; i>=0; i--){
        unsigned int v= sp[i];

        int idx= c->inv_matrix[j][0];
        while(idx<parity_len){
            v^= syndrom[idx];
            idx= c->inv_matrix[++j][0];
        }
        j++;
        syndrom[i] = v;
        code[ erasure_pos[i] ]= v;
    }
#else
    for(i=0; i<code_len; i++){
        unsigned int v= code[i];
        if(v){
            v= EXT(log)[v];
            for(j=0; j<c->nzc; j++){
                int y= c->parity_matrix[i][0][j];
                if(y>=0){
                    int p= c->parity_matrix[i][1][j];
                    sp[y]= sum(sp[y], EXT(exp)[p + v]);
                }
            }
        }
    }

    j=0;
    for(i=0; c->op_table[i][1]>INT_MIN; i++){
        unsigned t= EXT(log)[sp[ -c->op_table[i][1]] ] + c->op_table[i][0];
        sp[ -c->op_table[i][1] ]= sp[j];
        sp[j]= EXT(exp)[t];
        t= EXT(log)[sp[j]];

        for(i++; c->op_table[i][1]>=0; i++){
            unsigned t2= t + c->op_table[i][0];
            sp[ c->op_table[i][1] ]= sum(sp[ c->op_table[i][1] ], EXT(exp)[t2]);
        }
        i--;
        j++;
    }

    j=0;
    for(i=erasure_count-1; i>=0; i--){
        unsigned int v= sp[i];
        int idx= c->inv_matrix[j][0];
        while(idx<parity_len){
            v= sum(v, EXT(exp)[syndrom[idx] + c->inv_matrix[j++][1]]);
            idx= c->inv_matrix[j][0];
        }
        j++;
        syndrom[i] = EXT(log)[ v ];
        code[ erasure_pos[i] ]= v;
    }
#endif

    return 0;
}

void EXT(freeLDPC)(LDPCContext *c){
    free(c->parity_matrix);
    free(c->inv_matrix);
    free(c->op_table);
    free(c->temp);
    free(c);
}
