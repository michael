/*
 *   Copyright (C) 2008 Michael Niedermayer <michaelni@gmx.at>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

struct LDPCContext;

#if SIZE==2
    typedef uint32_t LDPC_ELEM;
#else
    typedef uint8_t  LDPC_ELEM;
#endif

/**
 *
 * @param seed if 0 then a matrix will be generated that can be encoded quicker
 *             at the expense of worse error recovery capabilities.
 * @param nzc number of non zero elements per column, 0 means choose automatically.
 */
struct LDPCContext *EXT(initLDPC)(int data_len, int parity_len, uint32_t seed, int nzc);

/**
 * Initialize decoder, this only needs to be called once per erasure_pos array, and can then
 * be used to decode many vectors with decodeLDPC().
 * @return 0 if sucessfull, <0 if not (will only partially decode)
 *         for encoding you should try another seed when this returns <0
 */
int EXT(init_matrixLDPC)(struct LDPCContext *c, int erasure_count, int erasure_pos[]);

/**
 *
 * @param code the code word to encode/decode.
 * @param erasure_count must match what has been passed to init_matrixLDPC
 * @param erasure_pos must match what has been passed to init_matrixLDPC
 */
int EXT(decodeLDPC)(struct LDPCContext *c, LDPC_ELEM *code, int erasure_count, int erasure_pos[]);

void EXT(freeLDPC)(struct LDPCContext *c);
