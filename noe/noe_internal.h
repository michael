/*
 *   Copyright (C) 2002 Michael Niedermayer <michaelni@gmx.at>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#define NOE_MAX(a,b) ((a) > (b) ? (a) : (b))
#define NOE_MIN(a,b) ((a) > (b) ? (b) : (a))
#define SWAP(type,a,b) {type NOE_temp=b; b=a; a=NOE_temp;}

static inline uint64_t rdtsc(void)
{
    uint32_t a, d;
    __asm__ volatile("rdtsc\n\t"
                 : "=a" (a), "=d" (d));
    return ((uint64_t)d << 32) + a;
}

#define START_TIMER \
static uint64_t tsum=0;\
static int tcount=0;\
static int tskip_count=0;\
uint64_t tend;\
uint64_t tstart= rdtsc();\


#define STOP_TIMER \
tend= rdtsc();\
if(tcount<2 || tend - tstart < 16*tsum/tcount){\
    tsum+= tend - tstart;\
    tcount++;\
}else\
    tskip_count++;\
if(256*256*256*64%(tskip_count+tcount)==0){\
    fprintf(stderr, "%Ld dezicycles, %d runs, %d skips\n", tsum*10/tcount, tcount, tskip_count);\
    fflush(stderr);\
}


