/*
 *   Copyright (C) 2002 Michael Niedermayer <michaelni@gmx.at>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include "galois.h"

#if SIZE == 0x100
#define PRIMITIVE_ELEMENT 2
#define SHIFT 8
#define M 0x11D
#define EXT(name) noe_ ## name ## _100
#define ADD_OP ^=
#elif SIZE == 0x10001
#define PRIMITIVE_ELEMENT 3
#define SHIFT 16
#define M 0
#define EXT(name) noe_ ## name ## _10001
#define ADD_OP +=
#elif SIZE == 0x101
#define PRIMITIVE_ELEMENT 3
#define SHIFT 8
#define M 0
#define EXT(name) noe_ ## name ## _101
#define ADD_OP +=
#elif SIZE == 2
#define PRIMITIVE_ELEMENT 1
#define SHIFT 8
#define M 0x100
#define EXT(name) noe_ ## name ## _2
#define ADD_OP ^=
#else
#error wrong SIZE
#endif

#define MINUS1 (SIZE-1)
#define MASK ((1<<SHIFT)-1)

extern GFF4Element EXT(exp)[4*SIZE];
extern GFF4Element EXT(log)[SIZE];

void EXT(prodPoly)(GFF4Element *dst, GFF4Element *src1, GFF4Element *src2);
void EXT(partialProdPoly)(GFF4Element *dst, GFF4Element *src1, GFF4Element *src2, int order);
void EXT(printPoly)(GFF4Element *src);
void EXT(getDerivative)(GFF4Element *dst, GFF4Element *src);
GFF4Element EXT(evalPoly)(GFF4Element *src, GFF4Element x);
void EXT(synthPoly)(GFF4Element *dst, int *src, int count);
#ifdef ALTERNATIVE_SOLVERS
void EXT(divPoly)(GFF4Element *quot, GFF4Element *rem, GFF4Element *nom, GFF4Element *denom);
void EXT(diffPoly)(GFF4Element *dst, GFF4Element *src1, GFF4Element *src2);
void EXT(sumPoly)(GFF4Element *dst, GFF4Element *src1, GFF4Element *src2);
#endif
void EXT(getOrder)(GFF4Element *src);
#ifdef ALTERNATIVE_SOLVERS
void EXT(scaledSumPoly)(GFF4Element *dst, GFF4Element *src1, GFF4Element *src2, GFF4Element f, int s);
void EXT(prodMatrix)(GFF4Element *d1[2], GFF4Element *d2[2], GFF4Element *s1[2], GFF4Element *s2[2]);
#endif
int noe_log2(unsigned int v);

static inline GFF4Element reduce(GFF4Element a){
#if M==0
    a = (a&MASK) - (a>>SHIFT);
    a += (((int)a)>>31)&SIZE;
#endif
    return a;
}

static inline GFF4Element hsreduce(GFF4Element a){
#if M==0
    a = (a&MASK) - ((int)a>>SHIFT);
#endif
    return a;
}

static inline GFF4Element hureduce(GFF4Element a){
#if M==0
    a = (a&MASK) - (a>>SHIFT);
#endif
    return a;
}

static inline GFF4Element prod(GFF4Element a, GFF4Element b){
#if SIZE == 2
    return a&b;
#elif M!=0 || SIZE < 0x500
    return EXT(exp)[EXT(log)[a] + EXT(log)[b]];
#else
    if(a==MINUS1)  return -b + ((((int)-b)>>31)&SIZE);
    else           return reduce(a*b);
#endif
}

static inline GFF4Element diff(GFF4Element a, GFF4Element b){
#if M!=0
    return a^b;
#else
    a -= b;
    return a + ((((int)a)>>31)&SIZE);
#endif
}

#if M!=0
#define neg(a) (a)
#else
#define neg(a) (SIZE - (a))
#endif

#define sum(a,b) diff(a,neg(b))

static inline GFF4Element inv(GFF4Element a){
    assert(a!=0);

#if SIZE == 2
    return a;
#else
    return (EXT(exp)+MINUS1)[- (signed)EXT(log)[a]];
#endif
}

#define SET_POLY0(dst, coeff0) (dst)[0]=0; (dst)[1]=coeff0;
#define SET_POLY1(dst, coeff0, coeff1) (dst)[0]=1; (dst)[1]=coeff0; (dst)[2]=coeff1;
#define SET_POLY2(dst, coeff0, coeff1, coeff2) (dst)[0]=2; (dst)[1]=coeff0; (dst)[2]=coeff1; (dst)[3]=coeff2;
