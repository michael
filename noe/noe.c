/*
 *   Copyright (C) 2003-2005 Michael Niedermayer <michaelni@gmx.at>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <inttypes.h>
#include <string.h>
#include <assert.h>
#include <limits.h>
#include "galois.h"
#include "rs.h"
#include "rw.h"
#include "bytestream.h"

#define SIZE 0x10001
#define SHIFT 16


static const uint8_t startcode[8]={0xAB, 0x2C, 0xE2, 0x47, 0x15, 'N', 'O', 'E'};
//#define STARTCODE (0xAB2CE24715000000ULL + ('N'<<16) + ('O'<<8) + 'E')
#define STARTCODE_THRESHOLD 6
#define HEADER_PARITY_LENGTH 16
#define HEADER_DATA_LENGTH 34
#define HEADER_LENGTH (HEADER_DATA_LENGTH + HEADER_PARITY_LENGTH)
#define HEADER_THRESHOLD (HEADER_LENGTH/2) //50
#define BUFFER_LENGTH 262144
#define HASH_BITS 32

typedef struct NoeContext{
    int debug;
    int fileChecksum;
    int seqNum;
    int headerCount;
    int parityCount;
    int fingerprintCount;
    int flags;
    int dataSize;
    int fileIdThreshold;
    int version;
    int interleave;
    int parity_per_header;              ///< number of parity symbols (16bit) per header
    int cw_per_pass;
//    uint8_t header[HEADER_LENGTH];
    int header[HEADER_LENGTH][256];
    uint32_t *fingerprint;
    uint8_t *fingerprintOk;
    int fingerprintOk_count;
}NoeContext;

const static uint8_t parity[256]={
0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0,
1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,
1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,
0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0,
1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,
0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0,
0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0,
1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,
1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,
0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0,
0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0,
1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,
0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0,
1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,
1,0,0,1,0,1,1,0,0,1,1,0,1,0,0,1,
0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0,
};

/**
 * searches for a header.
 * @return length if not found
 */
static int find_header(NoeContext *n, uint8_t *buffer, int length){
    int i;

    for(i=0; i + HEADER_LENGTH < length; i++){
        int j;
        int score=8;

        for(j=0; j<8; j++){
            if(buffer[i + j] != startcode[j])
                score--;
        }
        if(score < STARTCODE_THRESHOLD) continue;

        score+=4;
        for(j=0; j<4; j++){
            if(buffer[i + 12 + j] != ((n->fileChecksum >> (24 - 8*j)) & 0xFF))
                score--;
        }
/*        for(j=0; j<4; j++){
            if(buffer[i + 16+ j] != ((n->fileId >> (24 - 8*j)) & 0xFF))
                score--;
        }*/
        if(score < n->fileIdThreshold) continue;

        return i;
    }

    return length;
}

static int decode_header(NoeContext *n, uint8_t *buffer){
    uint8_t *bs;
    int erasedCount=0; //FIXME
    const int nn= SIZE - 1;
    const int k= SIZE - 1 - HEADER_PARITY_LENGTH;
    GFF4Element erased[HEADER_PARITY_LENGTH];
    GFF4Element code[nn];
    int i,j,dataword_size, file_parity_size;

    for(j=0; j<8; j++){
        buffer[j] = startcode[j];
    }

    for(i=12; i<HEADER_DATA_LENGTH; i++){
        int best=0;
        int bestScore=0;

        for(j=0; j<256; j++){
            const int score= n->header[i][j];
            if(score > bestScore){
                bestScore= score;
                best= j;
            }
        }
        buffer[i]= best; //FIXME dont modify it / load into code directly
    }

    bs= buffer;

    memset(code, 0, sizeof(GFF4Element)*nn);
    for(i=0; 2*i<HEADER_DATA_LENGTH; i++){
        code[i  ]= get_c(&bs, 2); //FIXME change to 1
    }
    for(i=0; 2*i<HEADER_PARITY_LENGTH; i++){
        code[k+i]= get_c(&bs, 2);
    }

    if(noe_rsDecode_10001(code, erased, erasedCount, HEADER_PARITY_LENGTH) < 0)
        return -1;

    for(i=HEADER_DATA_LENGTH; i<k; i++){
        if(code[i] != 0)
            return -2;
    }
    for(i=0; i<8; i++){
        if(buffer[i] != startcode[i])
            return -2;
    }

    //      EXT(printPoly)(gen, i);
    //      EXT(printPoly)(syn, i-1);

    for(i=0; 2*i<HEADER_DATA_LENGTH; i++){
        buffer[2*i+0]= code[  i] >> 8;
        buffer[2*i+1]= code[  i];
    }
    for(i=0; 2*i<HEADER_PARITY_LENGTH; i++){
        const int j= i + HEADER_DATA_LENGTH/2;
        buffer[2*j+0]= code[k+i] >> 8;
        buffer[2*j+1]= code[k+i];
    }

    bs= buffer;

    get_c(&bs, 8);
    n->seqNum= get_c(&bs, 4);
    n->fileChecksum= get_c(&bs, 4);
    n->version= get_c(&bs, 1);
    if(n->version > 0)
        return -3;

    n->flags= get_c(&bs, 1);
    n->headerCount= get_c(&bs, 4);
    if(n->seqNum >= n->headerCount)
        return -2;

    n->parityCount= get_c(&bs, 2);
    n->fingerprintCount= get_c(&bs, 2);
    n->dataSize= get_c(&bs, 8);

    dataword_size= 2*(65535 - n->parityCount);
    n->interleave= (n->dataSize + dataword_size - 1)/dataword_size;
    file_parity_size= n->interleave * n->parityCount;
    n->parity_per_header= (file_parity_size + n->headerCount - 1)/n->headerCount;

    for(i=12; i<HEADER_DATA_LENGTH; i++){
        n->header[i][ buffer[i] ]= INT_MAX/2;
    }

    return n->seqNum; 
}


/**
 *
 * @return -1 if not found
 */
static int find_and_decode_global_header(NoeContext *n, noe_RwContext *rw){
    uint64_t i;
    uint8_t buffer[BUFFER_LENGTH];

    for(i=rw->size; i>=0; i-= BUFFER_LENGTH - HEADER_LENGTH){
        int length= MIN(BUFFER_LENGTH, i);
        int pos;

        rw->io(rw, buffer, i - length, length, 0);

        for(pos=0; ; pos++){
            int j;
            pos += find_header(n, buffer + pos, length - pos);
            if(pos >= length) break;

            for(j=0; j<HEADER_LENGTH; j++){ //FIXME bitwise histogram?
                const int c= buffer[pos + j];
                n->header[j][c]++;
            }

            if( decode_header(n, buffer + pos) >= 0)
                return 0;
        }
    }

    return -1;
}

static inline int getChecksum(unsigned int fingerprint, unsigned int pos){
    return (fingerprint ^ pos) % 251;
}

static int decode_fingerprints(NoeContext *n, uint8_t *buffer, int length){
    int i, count;
    uint8_t *bs;;

    count= n->fingerprintCount;
    if(count > length/5)
        count= length/5;

    bs= buffer;
    for(i=0; i<count; i++){
        const unsigned int c= get_c(&bs, 4);
        const int checksum= get_c(&bs, 1);
        const int index= n->seqNum + i*n->headerCount;
        const int expectedChecksum= getChecksum(c, index);

        if(expectedChecksum == checksum && !n->fingerprintOk[index]){
            n->fingerprintOk[index]= 1;
            n->fingerprint[index]= c;
            n->fingerprintOk_count++;
        }//FIXME resync
    }
    return 0;
}

/**
 *
 * @return -1 if not found
 */
static int find_and_decode_packets(NoeContext *n, noe_RwContext *rw){
    const int packetLength= HEADER_LENGTH + 5*n->fingerprintCount;
    uint64_t i;
    uint8_t buffer[BUFFER_LENGTH];

    n->fingerprint  = malloc(n->fingerprintCount*n->headerCount * sizeof(int));
    n->fingerprintOk= malloc(n->fingerprintCount*n->headerCount);
    memset(n->fingerprintOk, 0, n->fingerprintCount*n->headerCount);

    for(i=rw->size; i>=0; i-= BUFFER_LENGTH - packetLength){
        int length= MIN(BUFFER_LENGTH, i);
        int pos;

        rw->io(rw, buffer, i - length, length, 0);

        for(pos=0; ; pos++){
            int j;
            pos += find_header(n, buffer + pos, length - pos);
            if(pos >= length) break;

            if( decode_header(n, buffer + pos) < 0 )
                continue;
            pos += HEADER_LENGTH;

            decode_fingerprints(n, buffer + pos, length - pos);
        }

        if(n->fingerprintOk_count >= n->fingerprintCount*n->headerCount*15/16) //FIXME
            break;
    }

    return 0;
}

static int match_fingerprints(NoeContext *n, noe_RwContext *rw){
    int length= BUFFER_LENGTH;
//    const int packetLength= HEADER_LENGTH + 5*n->fingerprintCount;
    uint64_t i;
    uint8_t buffer[BUFFER_LENGTH];

    for(i=0; i<n->fingerprintCount*n->headerCount; i++){
        if(!n->fingerprintOk[i])
            continue;
        
    }

    for(i=0; i<rw->size; i+= length - HASH_BITS/8){
        int pos;

        rw->io(rw, buffer, i, length, 0);

        for(pos=0; ; pos++){
        }
    }

//    remove certainly false matches (where better match exists)

//    setup remap table
}

//reorder data based on fingerprint matches

static int correct(NoeContext *n, noe_RwContext *out, noe_RwContext *in){
    uint8_t buffer[BUFFER_LENGTH];
    int passes, pass, i;
    const int nn= SIZE - 1;
//    const int k= SIZE - 1 - HEADER_PARITY_LENGTH;
    GFF4Element erased[/*HEADER_PARITY_LENGTH*/nn];
    const int erasedCount=0;
    GFF4Element (*code)[nn];
    const int packet_size= HEADER_LENGTH + 5*n->fingerprintCount + 2*n->parity_per_header;
    int uncorrectable=0;

    assert(2*packet_size <= BUFFER_LENGTH);

    n->cw_per_pass= 32; //4mb storege

    code= malloc(n->cw_per_pass*nn*sizeof(GFF4Element));
    passes= (n->interleave + n->cw_per_pass - 1) / n->cw_per_pass;

    for(pass=0; pass<passes; pass++){
        int first_cw= n->interleave* pass    / passes;
        int  next_cw= n->interleave*(pass+1) / passes;

        memset(code, 0, n->cw_per_pass*nn*sizeof(GFF4Element));

        for(i=0; i<in->size; i+= BUFFER_LENGTH - packet_size){
            int length= MIN(BUFFER_LENGTH, in->size - i);
            int pos;

            in->io(in, buffer, i, length, 0);

            for(pos=0; ; pos++){
                int j;
                pos += find_header(n, buffer + pos, length - pos);
                if(pos + packet_size > length) break;

                if( decode_header(n, buffer + pos) < 0 )
                    continue;
                pos += HEADER_LENGTH + 5*n->fingerprintCount;

                for(j=0; j<n->parity_per_header; pos+=2, j++){
                    int j2= j + n->seqNum*n->parity_per_header;
                    int cw_id = j2 % n->interleave;//FIXME optimize
                    int cw_pos= j2 / n->interleave;
                    if(cw_id < first_cw || cw_id >= next_cw)
                        continue;
                    code[cw_id - first_cw][cw_pos]= 256*buffer[pos] + buffer[pos+1];
                }
            }

            assert(i%2==0);
            for(pos=0; pos<length; pos+=2){
                int cw_id = ((i+pos)/2) % n->interleave; //FIXME optimize
                int cw_pos= ((i+pos)/2) / n->interleave; //FIXME optimize
                if(i+pos >= n->dataSize)
                    break;
                if(cw_id < first_cw || cw_id >= next_cw)
                    continue;
                code[cw_id - first_cw][cw_pos]= 256*buffer[pos] + buffer[pos+1];
            }
        }
        if(noe_rsDecode_10001(code, NULL/*erased*/, erasedCount, n->parityCount) < 0)
            uncorrectable++;

//        write data
    }

    av_free(code);
    return uncorrectable;
}

//main

//encode mainheader
//encode fingerprints
//encoder parity

