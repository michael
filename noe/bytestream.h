/*
 *   Copyright (C) 2003-2005 Michael Niedermayer <michaelni@gmx.at>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

/**
 *
 */
static inline uint64_t get_c(uint8_t **buf_ptr, int length){
    uint64_t val= 0;
    int i;

    for(i=length-1; i>=0; i--){
        val |= (*((*buf_ptr)++)) << (8*i) ;
    }
    return val;
}

/**
 *
 */
static inline void put_c(uint8_t **buf_ptr, uint64_t val, int length){
    int i;

    for(i=length-1; i>=0; i--){
        *((*buf_ptr)++)= val >> (i*8);
    }
}
