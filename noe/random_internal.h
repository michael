/*
 *   Copyright (C) 2008 Michael Niedermayer <michaelni@gmx.at>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 *
 *   This code implements the KISS99 PRNG algorithm from George Marsaglia
 */

typedef struct KISSState{
    uint32_t z,w,jsr,jcong;
}KISSState;

static uint32_t get_random(KISSState *s){
    s->z    = (s->z>>16) + 36969*(s->z&0xFFFF);
    s->w    = (s->w>>16) + 18000*(s->w&0xFFFF);
    s->jsr ^= s->jsr<<17;
    s->jsr ^= s->jsr>>13;
    s->jsr ^= s->jsr<< 5;
    s->jcong= 1234567 + 69069*s->jcong; 
    return ((s->w + (s->z<<16)) ^ s->jcong) + s->jsr;
}

static void init_random(KISSState *s, uint32_t seed){
    s->z    = 362436069 ^   seed;
    s->w    = 521288629 ^ (987654321*seed);
    s->jsr  = 123456789 +   seed;
    s->jcong= 380116160 -  314159265*seed;
}
