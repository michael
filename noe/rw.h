/*
 *   Copyright (C) 2003 Michael Niedermayer <michaelni@gmx.at>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#define RW_SIZE (1<<18)

typedef struct noe_RwContext{
    uint64_t size;

    /**
     * reads or writes some data.
     * @returns <0 if an error occured
     */
    int (*io )(struct noe_RwContext *context, uint8_t *data, uint64_t pos, int len, int write);

    void (*uninit)(struct noe_RwContext *context);
    struct noe_RwContext *parent;
}noe_RwContext;

typedef struct noe_Map{
    uint64_t src;
    uint64_t dst;
    uint64_t len;
}noe_Map;

