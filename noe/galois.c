/*
 *   Copyright (C) 2002 Michael Niedermayer <michaelni@gmx.at>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */
 
//FIXME try to use 16bit table with check in noe_log/exp()
 
#include <inttypes.h>
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "noe_internal.h"
#include "galois_internal.h"
#include "gfft.h"

GFF4Element EXT(exp)[4*SIZE];
GFF4Element EXT(log)[SIZE];

void EXT(init)(){
    GFF4Element ge= 1;
    int i;
    
    for(i=0; i<=(SIZE-2)*2; i++){
        EXT(exp)[i]= ge;
        EXT(log)[ge]= i%(SIZE-1);
#if M!=0
        ge+=ge;
        if(ge&SIZE) ge ^= M;
#else
        ge= reduce(ge * PRIMITIVE_ELEMENT);
#endif
    }
    EXT(log)[0]= (SIZE-2)*2 + 1;

#if M==0
    noe_gfft_init();
#endif
}

int noe_log2(unsigned int v) // from ffmpeg
{
    int n= 0;
    if (v & 0xff00) {
        v >>= 8;
        n += 8;
    }
    if (v & 0xf0) {
        v >>= 4;
        n += 4;
    }
    if (v & 0xc) {
        v >>= 2;
        n += 2;
    }
    if (v & 0x2) {
        n++;
    }
    return n;
}

/**
 * ...
 */
void EXT(prodPoly)(GFF4Element *dst, GFF4Element *src1, GFF4Element *src2){
    const int order1= *src1++;
    const int order2= *src2++;
    int order= order1 + order2;
    int i;

    *dst++= order;

    if(order1*order2 <= (order1+order2)*64 || M){
        dst[order]= prod(src1[order1], src2[order2]);
        if(!dst[order]) dst[-1]=0; //special case for zero polynoms
        for(i=order-1; i>=0; i--){
            unsigned int acc=0;
            int j, end;

            j  = NOE_MAX(0, i - order2);
            end= NOE_MIN(i, order1);

            for(; j<=end; j++)
                acc ADD_OP prod(src1[j], src2[i-j]);
            dst[i]= reduce(acc);
        }
    }else{
        const int logSize= noe_log2(NOE_MIN(order-1, 1<<(SHIFT-1)))+1;
        const int size= 1<<logSize;
        GFF4Element temp[2][SIZE]; //[size]
        const GFF4Element scale= inv(size);

        EXT(gfft_0padded)(temp[0], src1, logSize, order1+1);
        EXT(gfft_0padded)(temp[1], src2, logSize, order2+1);
        
        for(i=0; i<size; i++){
            temp[0][i]= prod(prod(temp[0][i], temp[1][i]), scale);
        }

        EXT(igfft)(temp[0], temp[0], logSize);
        if(size == order){
            temp[0][order]= diff(temp[0][0], prod(src1[0], src2[0]));
            temp[0][    0]= diff(temp[0][0], temp[0][order]);
        }

        memcpy(dst, temp[0], sizeof(GFF4Element)*(order+1));
    }
}

#ifdef ALTERNATIVE_SOLVERS
void EXT(prodMatrix)(GFF4Element *d1[2], GFF4Element *d2[2], GFF4Element *s1[2], GFF4Element *s2[2]){
//FIXME improve threshold decission
    if((s1[0][0] + s1[1][0] + s2[0][0] + s2[1][0]) < 64 || M/*|| 
        d1[0][0]==0 || d1[1][0]==0 || d2[0][0]==0 || d2[1][0]==0 ||
        s1[0][0]==0 || s1[1][0]==0 || s2[0][0]==0 || s2[1][0]==0*/){
        GFF4Element t1[SIZE]; //FIXME size
        GFF4Element t2[SIZE]; //FIXME size

        EXT(prodPoly)(t1   , d1[0], s2[0]);
        EXT(prodPoly)(d1[0], d1[0], s1[0]);
        EXT(prodPoly)(t2   , d2[0], s1[1]);
        EXT(prodPoly)(d2[0], d2[0], s2[1]);
        EXT(sumPoly)(d1[0], d1[0], t2);
        EXT(sumPoly)(d2[0], d2[0], t1);

        EXT(prodPoly)(t1   , d1[1], s2[0]);
        EXT(prodPoly)(d1[1], d1[1], s1[0]);
        EXT(prodPoly)(t2   , d2[1], s1[1]);
        EXT(prodPoly)(d2[1], d2[1], s2[1]);
        EXT(sumPoly)(d1[1], d1[1], t2);
        EXT(sumPoly)(d2[1], d2[1], t1);
    }else{
        int i;
        int max_order= NOE_MAX( NOE_MAX( NOE_MAX(d1[0][0] + s2[0][0], d1[0][0] + s1[0][0]),
                                         NOE_MAX(d2[0][0] + s2[1][0], d2[0][0] + s1[1][0]) ),
                                NOE_MAX( NOE_MAX(d1[1][0] + s2[0][0], d1[1][0] + s1[0][0]),
                                         NOE_MAX(d2[1][0] + s2[1][0], d2[1][0] + s1[1][0]) ));

        const int logSize= noe_log2(max_order)+1;
        const int size= 1<<logSize;
        const GFF4Element scale= inv(size);

        s1[0]++; s1[1]++; s2[0]++; s2[1]++;
        d1[0]++; d1[1]++; d2[0]++; d2[1]++;

        for(i=0; i<2; i++){
            memset(s1[i] + s1[i][-1] + 1, 0,  sizeof(GFF4Element)*(size - s1[i][-1] - 1));
            memset(s2[i] + s2[i][-1] + 1, 0,  sizeof(GFF4Element)*(size - s2[i][-1] - 1));
            memset(d1[i] + d1[i][-1] + 1, 0,  sizeof(GFF4Element)*(size - d1[i][-1] - 1));
            memset(d2[i] + d2[i][-1] + 1, 0,  sizeof(GFF4Element)*(size - d2[i][-1] - 1));
            EXT(gfft)(s1[i], s1[i], logSize);
            EXT(gfft)(s2[i], s2[i], logSize);
            EXT(gfft)(d1[i], d1[i], logSize);
            EXT(gfft)(d2[i], d2[i], logSize);
        }

        for(i=0; i<size; i++){
            int d1t= d1[0][i];
            d1[0][i]= prod(sum(prod(d1t, s1[0][i]), prod(d2[0][i], s1[1][i])), scale);
            d2[0][i]= prod(sum(prod(d1t, s2[0][i]), prod(d2[0][i], s2[1][i])), scale);
        }
        for(i=0; i<size; i++){
            int d1t= d1[1][i];
            d1[1][i]= prod(sum(prod(d1t, s1[0][i]), prod(d2[1][i], s1[1][i])), scale);
            d2[1][i]= prod(sum(prod(d1t, s2[0][i]), prod(d2[1][i], s2[1][i])), scale);
        }

        for(i=0; i<2; i++){
            EXT(igfft)(d1[i], d1[i], logSize);
            EXT(igfft)(d2[i], d2[i], logSize);
            *(--d1[i])= max_order;
            *(--d2[i])= max_order;
            EXT(getOrder)(d1[i]);
            EXT(getOrder)(d2[i]);
        }
    }
}
#endif

/**
 * returns the first order2 coefficients of the product of the 2 polynoms.
 * Note dst must be at least 2^log2(order1+order2) big FIXME benchmark if its worth it
 */
void EXT(partialProdPoly)(GFF4Element *dst, GFF4Element *src1, GFF4Element *src2, int order){
    int order1= *src1++;
    int order2= *src2++;
    int i;

    *dst++= order;

    if(order < order1) order1= order;
    if(order < order2) order2= order;

    if(order1*order2 <= (order1+order2)*64 || M){
        for(i=order; i>=0; i--){
            unsigned int acc=0;
            int j, end;

            j  = NOE_MAX(0, i - order2);
            end= NOE_MIN(i, order1);

            for(; j<=end; j++)
                acc ADD_OP prod(src1[j], src2[i-j]);
            dst[i]= reduce(acc);
        }
    }else{
        const int logSize= noe_log2(NOE_MIN(order2 + order1, 1<<(SHIFT-1)))+1;
        const int size= 1<<logSize;
        GFF4Element temp[SIZE]; //[size]
        const GFF4Element scale= inv(size);

        EXT(gfft_0padded)(temp, src2, logSize, order2+1);
        EXT(gfft_0padded)(dst , src1, logSize, order1+1);
        
        for(i=0; i<size; i++){
            dst[i]= prod(prod(dst[i], temp[i]), scale);
        }

        EXT(igfft)(dst, dst, logSize);
    }

    EXT(getOrder)(dst-1);
}

void EXT(printPoly)(GFF4Element *src){
    int i;
    
    for(i=src[0]+1; i>0; i--){
        printf(" + %X*x^%d", src[i], i-1);
    }
    printf("\n");
}

/**
 * Evaluates the src polynom at x.
 */
GFF4Element EXT(evalPoly)(GFF4Element *src, GFF4Element x){
    unsigned int acc=src[ src[0] + 1 ];
    int j;

    if(M || x==MINUS1){
        for(j=src[0]; j>0; j--)
            acc = sum(prod(acc, x), src[j]);
    }else{
        for(j=src[0]; j>0; j--)
            acc = reduce(acc*x + src[j]);
    }

    return acc;
}

void EXT(getDerivative)(GFF4Element *dst, GFF4Element *src){
    int i;

#if M!=0
    dst[1]= src[2];
    for(i=3; i<=src[0]; i+=2){
        dst[i-1]= 0;
        dst[i  ]= src[i+1];
    }
    dst[0]= i-3;
    EXT(getOrder)(dst);
#else
    for(i=1; i<=src[0]; i++){
        dst[i  ]= prod(src[i+1], i);
    }
    dst[0]= src[0]-1;
#endif
}

/**
 * ...
 */
void EXT(synthPoly)(GFF4Element *dst, int *src, int count){
        int pass, i;
        GFF4Element temp[2][2*SIZE]; //[(4<<noe_log2(count-1))+2]
        GFF4Element *temp1= count<=2 ? dst : temp[0];

        for(i=0; i+1<count; i+=2){
            GFF4Element a= neg(EXT(exp)[src[i+0]]);
            GFF4Element b= neg(EXT(exp)[src[i+1]]);
            SET_POLY2(temp1+2*i, 1, sum(a,b), prod(a,b));
        }
        if(i<count){
            SET_POLY1(temp1+2*i, 1, neg(EXT(exp)[src[i+0]]));
        }
        count= (count+1)>>1;

        for(pass=1; count>1; pass++){
            const int step= 2<<pass;
            GFF4Element *temp1= count<=2 ? dst : temp[ pass&1   ];
            GFF4Element *temp2= temp[(pass&1)^1];

            for(i=0; i+1<count; i+=2){
                EXT(prodPoly)(temp1, temp2, temp2+step);
                temp1 += 2*step;
                temp2 += 2*step;
            }

            if(i<count){
                memcpy(temp1, temp2, step*sizeof(GFF4Element));
            }
            count= (count+1)>>1;
        }
}

void EXT(getOrder)(GFF4Element *src){
    while(src[0] && src[ src[0] + 1 ]==0) 
        src[0]--;
}

#ifdef ALTERNATIVE_SOLVERS
/**
 * ...
 * Note: rem can be identical to nom
 */
void EXT(divPoly)(GFF4Element *quot, GFF4Element *rem, GFF4Element *nom, GFF4Element *denom){
    const int nomOrder= *nom++;
    const int denomOrder= *denom++;
    int i;
    int remOrder= nomOrder;

    *rem++= nomOrder;
    *quot++= remOrder - denomOrder;

    if(rem != nom)
        memcpy(rem, nom, (nomOrder+1)*sizeof(GFF4Element));

    assert(  nom[  nomOrder]);
    assert(denom[denomOrder]);

    quot[0]=0;

    for(; remOrder >= denomOrder; remOrder--){
        GFF4Element scale;

        if(rem[remOrder]==0) continue;

        scale= prod(rem[remOrder], inv(denom[denomOrder]));

        for(i=0; i<=denomOrder; i++){ //FIXME optimize
            const int remIndex= i + remOrder - denomOrder;
            rem[remIndex]= diff(rem[remIndex], prod(scale, denom[i])); //FIXME skip reduce
        }

        quot[remOrder - denomOrder]= scale;
    }

    EXT(getOrder)(rem-1);
}

void EXT(diffPoly)(GFF4Element *dst, GFF4Element *src1, GFF4Element *src2){
    const int order1= *src1++;
    const int order2= *src2++;
    int i;
    int minOrder= NOE_MIN(order1, order2);

    *dst++= order1;

    for(i=0; i<=minOrder; i++){
        dst[i]= diff(src1[i], src2[i]);
    }
    
    if(order1==order2){
        EXT(getOrder)(dst-1);
    }else{
        if(dst!=src1)
            memcpy(dst+i, src1+i, (i-1-order1)*sizeof(GFF4Element));

        for(; i<=order2; i++){
            dst[i]= neg(src2[i]);
        }

        dst[-1]= NOE_MAX(order1, order2);
    }
}

void EXT(sumPoly)(GFF4Element *dst, GFF4Element *src1, GFF4Element *src2){
    const int order1= *src1++;
    const int order2= *src2++;
    int i;
    int minOrder= NOE_MIN(order1, order2);

    *dst++= order1;

    for(i=0; i<=minOrder; i++){
        dst[i]= sum(src1[i], src2[i]);
    }
    
    if(order1==order2){
        EXT(getOrder)(dst-1);
    }else if(order1 < order2){
        dst[-1]= order2;
        if(dst!=src2)
            memcpy(dst+i, src2+i, (order2 - order1)*sizeof(GFF4Element));
    }else{
        if(dst!=src1)
            memcpy(dst+i, src1+i, (order1 - order2)*sizeof(GFF4Element));
    }
}

void EXT(scaledSumPoly)(GFF4Element *dst, GFF4Element *src1, GFF4Element *src2, GFF4Element f, int s){
    const int order1= *src1++;
    const int order2= *src2++; //FIXME +s
    int i;
    int minOrder= NOE_MIN(order1, order2+s);

    *dst++= order1;

    for(i=0; i<s; i++)
        dst[i]= src1[i];
    for(   ; i<=minOrder; i++)
        dst[i]= sum(src1[i], prod(src2[i-s], f));

    if(order1==order2 + s){
        EXT(getOrder)(dst-1);
    }else{
        if(dst!=src1)
            memcpy(dst+i, src1+i, (order1+1-i)*sizeof(GFF4Element));

        for(; i<=order2+s; i++){
            dst[i]= prod(src2[i-s], f);
        }

        dst[-1]= NOE_MAX(order1, order2+s);
        EXT(getOrder)(dst-1); //FIXME we need this due to src2= 0
    }
}
#endif
