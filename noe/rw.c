/*
 *   Copyright (C) 2003 Michael Niedermayer <michaelni@gmx.at>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <inttypes.h>
#include <stdlib.h>
#include <string.h>

#include "rw.h"

typedef struct noe_RemapRwContext{
    noe_RwContext rw;
    noe_Map *map;
    int count;
}noe_RemapRwContext;


static int remapIO(noe_RwContext *c, uint8_t *data, uint64_t pos, int len, int write){
    int i;
    noe_RemapRwContext * const p= (noe_RemapRwContext *)c;
    noe_RwContext *parent= c->parent;

    //FIXME do binary search or cache last 

    for(i=0; i<p->count; i++){
        if(pos < p->map[i].src + p->map[i].len){
            const uint64_t diff= p->map[i].dst - p->map[i].src;
            uint64_t currentLen= p->map[i].src + p->map[i].len - pos;
            if(currentLen > len) currentLen= len;

            parent->io(parent, data, pos + diff, currentLen, write);
            
            data+= currentLen;
            pos+= currentLen;
            len-= currentLen;
            if(len == 0) return 0;
        }
    }

    return -1;
}

static void remapUninit(noe_RwContext *c){
    noe_RemapRwContext * const p= (noe_RemapRwContext *)c;

    if(c==NULL) return;

    free(p->map);
    memset(p, 0, sizeof(noe_RemapRwContext));
    free(p);
}

noe_RwContext *noe_getRemapRwContext(noe_RwContext *parent, noe_Map *map, int count){
    noe_RemapRwContext *p= malloc(sizeof(noe_RemapRwContext));
    noe_RwContext *c= (noe_RwContext*)p;

    c->parent= parent;

    p->map= malloc(sizeof(noe_Map)*count);
    memcpy(p->map, map, sizeof(noe_Map)*count);
    p->count= count;
    
    //FIXME sort map

    c->io= remapIO;
    c->uninit= remapUninit;

    return c;
}
