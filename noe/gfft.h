/*
 *   Copyright (C) 2002-2008 Michael Niedermayer <michaelni@gmx.at>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

void noe_gfft_init();
void EXT(gfft)(GFF4Element *dst, GFF4Element *src, int size);
void EXT(gfft_0padded)(GFF4Element *dst, GFF4Element *src, int logSize, int nzCoeffs);
void EXT(igfft)(GFF4Element *dst, GFF4Element *src, int size);
void EXT(permute)(GFF4Element *dst, GFF4Element *src, int outCount, int codeBits);

extern uint8_t noe_revTable[257];

static inline unsigned int bitReverse(unsigned int x, int codeBits){
#if SHIFT==16
    return ((noe_revTable[x&255]<<8) | (noe_revTable[x>>8]))>>(SHIFT-codeBits);
#else
    assert(codeBits == 8);
    return noe_revTable[x];
#endif
}

