//2010 0.1 Copyright (C) Michael Niedermayer 1998
#ifndef n2_all_h
#define n2_all_h

#define mabs(a) ((a)<0 ? -(a) : (a))
#define sign(a)   ((a)<0 ? -1 :  1)
#define is_neg(a) ((a)<0 ?  0 : -1)
#define is_pos(a) ((a)<0 ? -1 :  0)
#define round(a) ( (a)<0.0 ? (a)-.5 : (a)+.5 )

#define max(a,b)   ((a)>(b) ? (a) : (b))
#define min(a,b)   ((a)<(b) ? (a) : (b))

#define minmax(a,b,c)   ((a)>(b) ? (a) : ( (b)<(c) ? (b) : (c) ))

#define limit(a,b,c) if( (a)<(b) || (a)>(c) ) error(-201)

#define ISTATES 3


#define u_char unsigned char
#define u_long unsigned long
#define u_short unsigned short
#define byte unsigned char
#define word unsigned short
#define dword unsigned long
#define ll long long


struct COL{
  long yuv;
  short rgb16;
  byte r, g, b;
};

/*
struct s_yuv{
  byte u, y1, v, y2;
};

struct s_rgb{
  byte b, g, r, x;
};

struct s_cl{
  byte chrom, lum;
};

union s_col{
  s_rgb rgb;
  s_yuv yuv;
  u_long a;
};
  */

enum VID2MEMBUFSTATE{
  Empty,
  Working,
  Grabbing,
  Grabbed,
};

struct VID2MEMBUF{
  byte *b;
  int phys;
  VID2MEMBUFSTATE state;
  static int num;
};

class color{
 public:
   COL col;
   color(void) { };
   color(int y, int u, int v, byte yuv_mode) {init(y,u,v,yuv_mode);};
   void init(int y, int u, int v, bool isRGB){ 
     if(isRGB){
       col.r=y;
       col.g=u;
       col.b=v;
       y= minmax(0   , int( (+ .299120)*col.r + (+ .584954)*col.g + (+ 0.115935)*col.b), 255);
       u= minmax(-128, int( (- .172465)*col.r + (- .337271)*col.g + (+ 0.509731)*col.b), 127);
       v= minmax(-128, int( (+ .509731)*col.r + (- .536992)*col.g + (- 0.084317)*col.b), 127);
     }
     else{
       col.r=minmax(0, int( y + 1.375   *v             ), 255);
       col.g=minmax(0, int( y - 0.703125*v - 0.34375 *u), 255);
       col.b=minmax(0, int( y              + 1.734375*u), 255);
     }

     const int r5= ((col.r>>3) & 0x1F);
     const int g6= ((col.g>>2) & 0x3F); 
     const int b5= ((col.b>>3) & 0x1F);
     col.rgb16= b5 | (g6<<5) | (r5<<11);
     col.yuv=   u | (y<<8) | (v<<16) | (y<<24);

/*
     r=y + (+ 1.375   )*v             
     g=y + (- 0.703125)*v + (- 0.34375 )*u
     b=y                  + (+ 1.734375)*u

         r    =y + (+ 1.375   )*v
     (-1)r + g=  + (- 2.078125)*v + (- 0.34375 )*u 
     (-1)r + b=  + (- 1.375   )*v + (+ 1.734375)*u 

                r               =y + (+ 1.375   )*v
            (-1)r +            g=  + (- 2.078125)*v + (- 0.34375 )*u 
            (-1)r +            b=  + (- 1.375   )*v + (+ 1.734375)*u 
     (+ .481203)r + (- .481203)g=  +              v + (+ 0.165414)*u

                r               =y + (+ 1.375   )*v
            (-1)r +            g=  + (- 2.078125)*v + (- 0.34375 )*u 
            (-1)r +            b=  + (- 1.375   )*v + (+ 1.734375)*u 
     (+ .481203)r + (- .481203)g=  +              v + (+ 0.165414)*u
     (+ .661654)r + (- .661664)g=  +  (+ 1.375   )v + (+ 0.227444)*u
     (- .661654)r + (+ .661664)g=  +  (- 1.375   )v + (- 0.227444)*u

     (+ .338346)r + (+ .661664)g +          =y +   + (- 0.227444)*u
     (+ .481203)r + (- .481203)g +          =  + v + (+ 0.165414)*u
     (- .338346)r + (- .661664)g +         b=  +   + (+ 1.961819)*u

     (+ .338346)r + (+ .661664)g +              =y +   + (- 0.227444)*u
     (+ .481203)r + (- .481203)g +              =  + v + (+ 0.165414)*u
     (- .172465)r + (- .337271)g + (+ 0.509731)b=  +   +              u
     (+ .039226)r + (+ .076710)g + (- 0.115935)b=  +   +              u(- 0.227444)
     (- .028528)r + (- .055789)g + (+ 0.084317)b=  +   +              u(+ 0.165414)

     (+ .299120)r + (+ .584954)g + (+ 0.115935)b=y 
     (+ .509731)r + (- .536992)g + (- 0.084317)b=  v 
     (- .172465)r + (- .337271)g + (+ 0.509731)b=    u
  */

   };
} ;

#endif
