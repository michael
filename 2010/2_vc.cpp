//2010 0.1 Copyright (C) Michael Niedermayer 1999

#include <stdio.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include "2_all.h"
#include "2_crypt.h"
#include "2_vc.h"
#include "2_71x6.h"
#include "2_gfunc.h"
#include "2_vc_a.h"
#include "2_hw_mem.h"
#include "2010.h"

//#define DEBUG_CUTP
#define SHOW_EDGE_AREA
//#define DEBUG
#define SHOW_FINALS
//#define NO_CACHE

#define FREQ_SMP       2.1e7
#define POINT_LAND     765.0                         
#define NO_POINT_LAND1 (148)  // GUESSED 148/153
#define NO_POINT_LAND2 (152)  // GUESSED 152/147
 // ? POINT_LAND + NO_POINT_LAND(1|2) = 910   (FIFO SIZE)
 // ? NO_POINT_LAND + POINT_LAND = 1065, (1060) 
 // ? NO_POINT_LAND1 not 150 ?
 // PHASE_SEG 268-306 Grad

#define NO_POINT_LAND  (NO_POINT_LAND1 + NO_POINT_LAND2)
#define PHASE_POINT    (fmod(FREQ_CHROM / (FREQ_SMP/3.0), 1.0) * PI * 2.0)
#define PHASE_SMP      (fmod(FREQ_CHROM / (FREQ_SMP    ), 1.0) * PI * 2.0)
#define PHASE_LINE     (fmod(FREQ_CHROM / FREQ_HS       , 1.0) * PI * 2.0) // + some/180.0*PI)
#define PHASE_SEG      (fmod(FREQ_CHROM / FREQ_SMP \
                          * (POINT_LAND + NO_POINT_LAND), 1.0) * PI * 2.0)// + some/180.0*PI)



#define MIN_LINES_FOR_EDGE_AREA 50
#define EDGE_CHANGE_THRESHOLD 0.2
#define PHASE_CORRECT_THRESHOLD (0.5/128*PI)
#define MAX_PHASE_CORRECT ( 22.0/180.0*PI)
#define MIN_PHASE_CORRECT (-22.0/180.0*PI)
#define FIELDS_PER_PHASE_CHECK 50
//#define EDGE_AREA_TRUNC_DIV 6
#define EDGE_AREA_START -4
#define EDGE_AREA_END 4
#define DEFINAL_WAIT 30
#define NOT_VC_THRESHOLD 20   // below 10 and above 90 bad
#define HEADER_EMPTY 0
#define HEADER_FINAL 15
#define GOOD_CACHE_THRESHOLD 3
#define FINAL_CACHE_DIFF_THRESHOLD 4            
#define SCORE_THRESHOLD 3
#define MAX_EQ_SETS 14
#define nTEST_LINES 10
#define MAX_BAD_CACHES 50000
#define MAX_GOOD_CACHES 25000

#define MAGIC_ID "VCC0"
#define STEP_START 16
#define STEP_END 2
#define DWN_SMP 1
#define MAX_DRIFT 2
#define DRIFT_TAB {1200, 400, 0, 400, 1200}
#define PHASE_CMP_STEP 3
#define BAD_MEAN_THRESHOLD 17
#define BAD_THRESHOLD1 0.8
#define BAD_THRESHOLD2 200
#define EDGE_LIMIT 70
#define MAX_EDGE 500                // ? 2000 ?
#define PHASE_EXP 4
#define PHASE_LIMIT 40
#define MAX_PHASE_PENALTY 1000
#define AMP_THRESHOLD1 250
#define AMP_THRESHOLD2 600

#define PHASE_COEFF (MAX_PHASE_PENALTY/pow(PHASE_LIMIT, PHASE_EXP))
#define EDGE_COEFF (MAX_EDGE/log(EDGE_LIMIT))

#define NUM_PHASE_SMP (vcDiff>>PHASE_CMP_STEP)

extern int vgax, vgay, wndx, wndy, x_field;
extern VID2MEMBUF *actVid2MemBufp;
extern volatile int scales_x, scalee_x;
extern volatile int scales_y, scalee_y;
extern bool showPoints;
extern int iState;
extern int yuvMode;
extern int some;
extern bool mmx;
extern int infoPosX;
extern int infoPosY;
extern long uclockWaste;
extern bool drop;

bool isVCPhaseTrick=false;

static void loadVCCache(void);


static inline float atan3(const float f1, const float f2){
  float out;
                     // ?! atan2 crashes 
  asm(//"int $3\n\t"
      "fpatan \n\t"
      : "=t"(out)
      : "0"(f2), "u"(f1)
      : "st(1)"
                          );

  return out;
}

static pTestLine[nTEST_LINES]= {43, 62, 84, 109, 122, 147, 160, 181, 205, 238};
static CutPLut *ppCutPLut[nTEST_LINES];
static int nGoodCaches=0;
static int nBadCaches=0;
static int nFieldsCached=0;
static int nFieldsUnCached=0;
static int nFields=0;
static int iCurrentLUTScore=0;
static int iCurrentUsed=0;
static CutPCache *pGoodCutPCache=NULL;
static CutPCache *pBadCutPCache=NULL;
static int badFinalsLeft=0;

static void delFromCache(CutPCache *pOld){

    if(pOld->nUsed<GOOD_CACHE_THRESHOLD) nBadCaches--;
    else                                 nGoodCaches--;

    if(pOld==pBadCutPCache)
        pBadCutPCache=pOld->pNext;
    if(pOld==pGoodCutPCache)
        pGoodCutPCache=pOld->pNext;

    pOld->pNext->pPrev= pOld->pPrev;
    pOld->pPrev->pNext= pOld->pNext;

    if(pOld->pNext == pOld)
        {
        if( pBadCutPCache == pOld )
            pBadCutPCache= NULL;
        else if( pGoodCutPCache == pOld )
            pGoodCutPCache= NULL;
        else error(error_code(-76));               
        }
}

static void addCutPsToCache(CutPCache *pCutPCache, int *piCutP){

    static byte *pbCScratchPad= new byte[ (MAX_EQ_SETS+1)*wndy+10 ];

    int xIn=0, xOut=0;
    int nInBytes=0;
    int nInHeaders=0;
    int xLastOutHeader=-1;
    int nOutHeaders=-1;
    int iLastOutHeader=-1;
    for(int line=0; line<wndy; line++)
        {
        if(nInHeaders==0 && pCutPCache->pbCCutP!=NULL)
            {
            nInHeaders= (pCutPCache->pbCCutP[xIn]>>4) + 1;
            nInBytes= pCutPCache->pbCCutP[xIn] & 15;
            xIn++;
            }

        bool isOldFinal=true;
        bool isFinal=true;
		int bestCutP= piCutP[line];
		int bestCutPs=1;
		if(bestCutP==-1) bestCutPs=0;

		int nextBestCutP=-1;
		int nextBestCutPs=0;
        if(nInBytes!=HEADER_FINAL)
            {
            isOldFinal=false;
            isFinal=false;
			int currentCutP=-1;
			int currentCutPs=0;
            for(int i=0; i<nInBytes; i++)
                {
				if(int(pCutPCache->pbCCutP[xIn+i]) != currentCutP)
					{
					currentCutP= pCutPCache->pbCCutP[xIn+i];
					currentCutPs=0;
					if(currentCutP == piCutP[line]) currentCutPs++;
					}
				currentCutPs++;

				if(currentCutPs >= bestCutPs)
					{
					if(bestCutP != currentCutP)
						{
						nextBestCutP=  bestCutP;
						nextBestCutPs= bestCutPs;
						}
					bestCutP=  currentCutP;
					bestCutPs= currentCutPs;
					}
				}
			
			if(bestCutPs-nextBestCutPs >= FINAL_CACHE_DIFF_THRESHOLD) isFinal=true; 
            }

	    int nOutBytes=nInBytes;
        if(!isFinal && piCutP[line]!=-1) nOutBytes++;
        int xKill=-1;
        if(!isFinal && nOutBytes>MAX_EQ_SETS)
            {
            nOutBytes=MAX_EQ_SETS;
            xKill=random() % MAX_EQ_SETS;
            }
        if(isFinal) nOutBytes=1;

        int iOutHeader;
        if(isFinal) iOutHeader= HEADER_FINAL;
        else        iOutHeader= nOutBytes;

        if(iLastOutHeader == iOutHeader && nOutHeaders<16)
            nOutHeaders++;
        else
            {
            if(xLastOutHeader>=0)
                pbCScratchPad[xLastOutHeader]=
                    ((nOutHeaders-1)<<4) + iLastOutHeader;
            iLastOutHeader= iOutHeader;
            nOutHeaders=1;
            xLastOutHeader=xOut;
            xOut++;
            }


        if(isOldFinal)
            {
            pbCScratchPad[xOut]= pCutPCache->pbCCutP[xIn];
            xOut++;
            }
        else if(isFinal)
            {
            pbCScratchPad[xOut]= bestCutP;
            xOut++;
            }
        else
            {
            bool bOut;
            if(piCutP[line]==-1) bOut=true;
            else                 bOut=false;

            for(int i=0; i<nInBytes; i++)
                {
                if(i==xKill) continue;
                if(piCutP[line] < pCutPCache->pbCCutP[xIn+i] && !bOut)
                    {
                    pbCScratchPad[xOut]= piCutP[line];
                    xOut++;
                    bOut=true;
                    }
                pbCScratchPad[xOut]= pCutPCache->pbCCutP[xIn+i];
                xOut++;
                }
            if(!bOut)
                {
                pbCScratchPad[xOut]= piCutP[line];
                xOut++;
                }
            }


        if(nInBytes!=HEADER_FINAL) xIn+=nInBytes;
        else                       xIn++;

        nInHeaders--;
        }
    pbCScratchPad[xLastOutHeader]= ((nOutHeaders-1)<<4) + iLastOutHeader;

    delete [] pCutPCache->pbCCutP;
    pCutPCache->pbCCutP= new byte[xOut];
    pCutPCache->nbCCutP= xOut;
    memcpy(pCutPCache->pbCCutP, pbCScratchPad, xOut);

}

static int *getBestCutPs(CutPCache *pCutPCache){
    static int piCutP[600];
    int x=0;
    int nBytes=-1;
    int nHeaders=0;
    bool isFinal=false;
    for(int line=0; line<wndy; line++)
        {
        if(nHeaders==0)
            {
            isFinal=false;
            nHeaders= (pCutPCache->pbCCutP[x]>>4) + 1;
            nBytes= pCutPCache->pbCCutP[x] & 15;
            if(nBytes==HEADER_FINAL)
                {
                nBytes=1;
                isFinal= true;
                }
            x++;
            }

#ifdef SHOW_FINALS
        if(isFinal)
            {
            actVid2MemBufp->b[((line) * vgax<<1) +1]=255;
            actVid2MemBufp->b[((line) * vgax<<1) +3]=0;
            }
#endif

//        printf("x %d %d %d\n", nHeaders, nBytes, pCutPCache->nbCCutP);


        int iCurrentCutP=0;     
        int nCurrentCutPs=0;
        int iBestCutP=0;
        int nBestCutPs=0;
        for(int i=0; i<nBytes; i++)
            {
//            printf("y %d %d\n", x, pCutPCache->pbCCutP[x]);
            if(pCutPCache->pbCCutP[x] == iCurrentCutP)
                {
                nCurrentCutPs++;
                }
            else
                {
                nCurrentCutPs=1;
                iCurrentCutP=pCutPCache->pbCCutP[x];
                }

            if(nCurrentCutPs>nBestCutPs)
                {
                nBestCutPs= nCurrentCutPs;
                iBestCutP=  iCurrentCutP;
                }
            x++;
            }
//        printf("z %d %d\n", x, bBestCutP);
        piCutP[line] =iBestCutP;

        nHeaders--;
        }

    return piCutP;
}

static void deFinalize(CutPCache *pCutPCache){
    static byte *pbCScratchPad= new byte[ (MAX_EQ_SETS+1)*wndy+10 ];

    badFinalsLeft--;

	int xIn=0;
    int xOut=0;
    int nInBytes=-1;
    int nInHeaders=0;
    bool isFinal=false;
    do
        {
        if(nInHeaders==0)
            {
            isFinal=false;
            nInHeaders= (pCutPCache->pbCCutP[xIn]>>4) + 1;
            nInBytes= pCutPCache->pbCCutP[xIn] & 15;
            if(nInBytes==HEADER_FINAL)
                {
                nInBytes=1;
                isFinal= true;
				pbCScratchPad[xOut]= ((nInHeaders-1)<<4) | 2;
                }
			else
				{
				pbCScratchPad[xOut]= pCutPCache->pbCCutP[xIn];
				}
			xOut++;
            xIn++;
            }

		for(int i=0; i<nInBytes; i++)
			{
			if(isFinal)
				{
				pbCScratchPad[xOut]= pCutPCache->pbCCutP[xIn];
				xOut++;
				}
			pbCScratchPad[xOut]= pCutPCache->pbCCutP[xIn];
			xOut++;
			xIn++;
			}

        nInHeaders--;
        }while(xIn < pCutPCache->nbCCutP);

    delete [] pCutPCache->pbCCutP;
    pCutPCache->pbCCutP= new byte[xOut];
    pCutPCache->nbCCutP= xOut;
    memcpy(pCutPCache->pbCCutP, pbCScratchPad, xOut);

   	pCutPCache->isDeFinal= 1;
}

static int addToLut(CutPCache *pNewCutPCache){
    int *piCutP= getBestCutPs(pNewCutPCache);
    int nTimesAdded=0;

    for(int xTestLine=0; xTestLine<nTEST_LINES; xTestLine++)
        {
        int iTestLine= pTestLine[ xTestLine ];
        int iDiff1= (byte)(piCutP[ iTestLine   ] - piCutP[ iTestLine-1 ]);
        int iDiff2= (byte)(piCutP[ iTestLine+1 ] - piCutP[ iTestLine   ]);
        if(iDiff1==0 && iDiff2==0) continue;  

        nTimesAdded++;

        int xDiff= (iDiff1<<8) + iDiff2;
        CutPLut *pCutPLut= &ppCutPLut[ xTestLine ][ xDiff ];
        while( pCutPLut->pNext!=NULL ) pCutPLut= pCutPLut->pNext;

        if(pCutPLut->pCutPCache!=NULL)
            {
            pCutPLut->pNext= new CutPLut;
            pCutPLut= pCutPLut->pNext;
            }
        pCutPLut->pNext=NULL;
        pCutPLut->pCutPCache= pNewCutPCache;
        }
    return nTimesAdded;
}

static void delFromLut(CutPCache *pOldCutPCache){
    int *piCutP= getBestCutPs(pOldCutPCache);
    for(int xTestLine=0; xTestLine<nTEST_LINES; xTestLine++)
        {
        int iTestLine= pTestLine[ xTestLine ];
        int iDiff1= (byte)(piCutP[ iTestLine   ] - piCutP[ iTestLine-1 ]);
        int iDiff2= (byte)(piCutP[ iTestLine+1 ] - piCutP[ iTestLine   ]);
//        if(iDiff1==0 && iDiff2==0) continue;  

        int xDiff= (iDiff1<<8) + iDiff2;
        CutPLut *pCutPLut= &ppCutPLut[ xTestLine ][ xDiff ];
        CutPLut *pPrev=NULL;
        while( pCutPLut!=NULL )
            {
            if(pCutPLut->pCutPCache==pOldCutPCache)
                {
                if(pPrev!=NULL)
                    {
                    pPrev->pNext=pCutPLut->pNext;
                    delete pCutPLut;
                    }
                else
                    {
                    pCutPLut->pCutPCache=NULL;
                    }
                break;
                }
            pPrev=pCutPLut;
            pCutPLut= pCutPLut->pNext;
            }
        }
}

static CutPCache *findInLutX(int *piCutP, CutPCache *pMergeCutPCache, bool bInc){
    int iBestScore=0;
    CutPCache *pBestCutPCache=NULL;
    for(int xTestLine=0; xTestLine<nTEST_LINES; xTestLine++)
        {
        int iTestLine= pTestLine[ xTestLine ];

        if(  piCutP[ iTestLine-1 ]==-1 || piCutP[ iTestLine  ]==-1
           ||piCutP[ iTestLine+1 ]==-1                            ) continue;

        int iDiff1= (byte)(piCutP[ iTestLine   ] - piCutP[ iTestLine-1 ]);
        int iDiff2= (byte)(piCutP[ iTestLine+1 ] - piCutP[ iTestLine   ]);
        for(int iDrift1=-1; iDrift1<2; iDrift1++)
            {
            int iTDiff1= (byte)(iDiff1 + iDrift1);
            for(int iDrift2=-1; iDrift2<2; iDrift2++)
                {
                int iTDiff2= (byte)(iDiff2 + iDrift2);

                int xDiff= (int(iTDiff1)<<8) + int(iTDiff2);
                CutPLut *pCutPLut= &ppCutPLut[ xTestLine ][ xDiff ];
                while( pCutPLut!=NULL && pCutPLut->pCutPCache!=NULL)
                    {
                    CutPLut *pNextCutPLut= pCutPLut->pNext;
                    CutPCache *pCutPCache= pCutPLut->pCutPCache;

                    if(bInc)
                        {
                        pCutPCache->score++;
                        if(pCutPCache->score > iBestScore)
                            {
                            iBestScore=pCutPCache->score;
                            pBestCutPCache=pCutPCache;
                            }
                        }
                    else
                        {
                        if(   pCutPCache->score>=SCORE_THRESHOLD
                           && pCutPCache!=pMergeCutPCache && 0) //FIX ?! 
                            {

                            addCutPsToCache(
                                pMergeCutPCache,
                                getBestCutPs(pCutPCache));

                            delFromLut(pCutPCache);
                            delFromCache(pCutPCache);
                            delete [] pCutPCache->pbCCutP;
                            delete pCutPCache;
                            }
                        else
                            {
                            pCutPCache->score=0;
                            }
                        }
                    pCutPLut= pNextCutPLut;
                    }
                }
            }
        }
    return pBestCutPCache;
}

static void mergeAndClean(int *piCutP, CutPCache *pBestCutPCache){
    findInLutX(piCutP, pBestCutPCache, false);
}

static CutPCache *findInLut(int *piCutP){
    return findInLutX(piCutP, NULL, true);
}

static void addToCache(CutPCache *pNew){
                                 // ? FIX too many pointers
    CutPCache **ppPlace;
    if(pNew->nUsed < GOOD_CACHE_THRESHOLD)
        {
        if(nBadCaches>MAX_BAD_CACHES)
            {
            CutPCache *pDel= pBadCutPCache->pPrev;
            delFromLut(pDel);
            delFromCache(pDel);
            delete [] pDel->pbCCutP;
            delete pDel;
            }
        ppPlace= &pBadCutPCache;
        nBadCaches++;
        }
    else
        {
        if(nGoodCaches>MAX_GOOD_CACHES)
            {
            CutPCache *pDel= pGoodCutPCache->pPrev;
            delFromLut(pDel);
            delFromCache(pDel);
            delete [] pDel->pbCCutP;
            delete pDel;
            }
        ppPlace= &pGoodCutPCache;
        nGoodCaches++;
        }

    if(*ppPlace==NULL)
        {
        *ppPlace=          pNew;
        (*ppPlace)->pNext= pNew;
        (*ppPlace)->pPrev= pNew;
        }

    pNew->pNext= *ppPlace;
    pNew->pPrev= (*ppPlace)->pPrev;

    pNew->pNext->pPrev=pNew;
    pNew->pPrev->pNext=pNew;
    *ppPlace= pNew;
}

static void cache(int *piCurrentCutP){

    static bool bInit=true;
    if(bInit)
        {
        for(int i=0; i<nTEST_LINES; i++)
            {
            ppCutPLut[i]=new CutPLut[1<<16];
            memset( ppCutPLut[i], 0, sizeof(CutPLut)*(1<<16) );
            }
        bInit=false;
        loadVCCache();
        }

    CutPCache *pBestCutPCache= findInLut(piCurrentCutP);


    int iBestScore;
    if(pBestCutPCache!=NULL) iBestScore=pBestCutPCache->score;
    else                     iBestScore=0;

	static int deFinalWait= DEFINAL_WAIT;
    if(iBestScore>=SCORE_THRESHOLD && deFinalWait<=0 && pBestCutPCache->isDeFinal==0 )
		{
		deFinalWait+= DEFINAL_WAIT;
		deFinalize(pBestCutPCache);
		}
	if(iBestScore>=SCORE_THRESHOLD) deFinalWait--;

	mergeAndClean(piCurrentCutP, pBestCutPCache);

    if(pBestCutPCache!=NULL && iBestScore>=SCORE_THRESHOLD)
        {

         /* LRU Stuff */
        delFromCache(pBestCutPCache);
        pBestCutPCache->nUsed++;
        addToCache(pBestCutPCache);


        delFromLut(pBestCutPCache);

        addCutPsToCache(pBestCutPCache, piCurrentCutP);

        addToLut(pBestCutPCache);

        }
    else if(iBestScore<SCORE_THRESHOLD)
        {
        CutPCache *pNewCutPCache= new CutPCache;

        pNewCutPCache->nUsed=0;
        pNewCutPCache->score=0;
		pNewCutPCache->isDeFinal=1;
        pNewCutPCache->pbCCutP=NULL;
        addCutPsToCache(pNewCutPCache, piCurrentCutP);

        addToCache(pNewCutPCache);

        // add and check if we could ever use it, if not thrash it
        if( addToLut(pNewCutPCache)<SCORE_THRESHOLD )
            {
            delFromLut(pNewCutPCache);
            delFromCache(pNewCutPCache);
            delete [] pNewCutPCache->pbCCutP;
            delete pNewCutPCache;
            }
        }

    drop=true;
    nFieldsUnCached++;
    iCurrentLUTScore=iBestScore;
    iCurrentUsed=0;

#ifdef NO_CACHE
	return;
#endif
    if(iBestScore<SCORE_THRESHOLD) return;

    iCurrentUsed=pBestCutPCache->nUsed;
    nFieldsUnCached--;
    nFieldsCached++;

#ifdef DEBUG
    printf("Field Cached %d\n", nFields);
#endif

    if(pBestCutPCache->nUsed<2) return;

    drop=false;

    int *piCutP= getBestCutPs(pBestCutPCache);
    memcpy(piCurrentCutP, piCutP, sizeof(int)*wndy);
}

static bool bWasLoaded=false;
void saveVCCache(void){
    if(!bWasLoaded) return;

    static FILE *pFCache=fopen("vc_cache.dat", "wb");
    if(pFCache==NULL) error(error_code(-91));

   	char magicId[5]=MAGIC_ID;
    fwrite(&magicId, 4*sizeof(char), 1, pFCache);
	int reserved=0;
    fwrite(&reserved, sizeof(int), 1, pFCache);
    CutPCache *tpCutPCache= pGoodCutPCache->pPrev;

    for(int i=0; i<nGoodCaches; i++)
        {
        fwrite(tpCutPCache, sizeof(CutPCache), 1, pFCache);
        fwrite(tpCutPCache->pbCCutP, sizeof(byte),
               tpCutPCache->nbCCutP, pFCache);

        tpCutPCache= tpCutPCache->pPrev;
        }

    tpCutPCache=pBadCutPCache->pPrev;
    for(int i=0; i<nBadCaches; i++)
        {
        fwrite(tpCutPCache, sizeof(CutPCache), 1, pFCache);
        fwrite(tpCutPCache->pbCCutP, sizeof(byte),
               tpCutPCache->nbCCutP, pFCache);

        tpCutPCache= tpCutPCache->pPrev;
        }

    fclose(pFCache);
}

static void loadVCCache(void){

    bWasLoaded=true;
    static FILE *pFCache=fopen("vc_cache.dat", "rb");
    if(pFCache==NULL) return;
    int reserved;
    fread(&reserved, sizeof(int), 1, pFCache);       
    fread(&reserved, sizeof(int), 1, pFCache);        
    CutPCache *tpCutPCache;

    while( !feof(pFCache) )
        {
        tpCutPCache= new CutPCache;

        fread(tpCutPCache, sizeof(CutPCache), 1, pFCache);
		if( feof(pFCache) ) break;						    // FIX LOOKS UGLY

//        printf("test %d %d\n", tpCutPCache->iScore, tpCutPCache->nUsed);


        tpCutPCache->pbCCutP= new byte[tpCutPCache->nbCCutP];
		if(tpCutPCache->nbCCutP > 600) printf("%d\n", tpCutPCache->nbCCutP);
        fread(tpCutPCache->pbCCutP, sizeof(byte),
              tpCutPCache->nbCCutP, pFCache);

        if(tpCutPCache->isDeFinal==0) badFinalsLeft++;
        addToCache(tpCutPCache);
//        printf("B\n");
        addToLut(tpCutPCache);
//        printf("A\n");
        }

    fclose(pFCache);
}

void vc_decrypt(void){
 int cutPointX[max_y];
 cutPointX[0]=
 cutPointX[1]=0;
 int addapBuff[max_x];
 char textbuf[9][128];
 long T1=0, T2;
 color c;
 
 int pChromAmp[max_x];

 static int edgePos=1;
 static int edgeAlgo=0;

// const int lowResSize= mmx ? DWN_SMP : (DWN_SMP-1);

#ifdef DEBUG2
 printf("xx%f %f %f\n", PHASE_POINT/PI*180, PHASE_SEG/PI*180, PHASE_SMP/PI*180);
#endif

 int vScore=0;
 nFields++;

// showPoints=true;

 const double freqPix2= double(wndx) / double(x_field) * FREQ_PIX; //FIX CHECK (xfield...)
#ifdef DEBUG2
 printf("xy%d %d\n", wndx, x_field);
#endif
                           //  18
// const int vcStartX=int(double(16 -scales_x+3)/x_field*wndx + .5) + some - 10;
 const int vcStartX=int(double(16 -scales_x+3)/x_field*wndx + .5);    //FIX CHECK

 static FILE *pF= fopen("out.dat", "wb");

 static int cutP2X[256];
 static int x2CutP[max_x];

 static int wndxBak= -1;
 if(wndx != wndxBak){
   for(int x=0; x<max_x; x++){       

     const int cutP=int( round(
        double(x) / ( 3.0 * freqPix2) * FREQ_SMP - (NO_POINT_LAND1)/3.0 ) );
     x2CutP[x]= cutP;
   }
   for(int cutP=0; cutP<256; cutP++){
     const int x=int( round(
        double(cutP*3 + NO_POINT_LAND1) * freqPix2 / FREQ_SMP ) );
     cutP2X[cutP]= x;
   }
   wndxBak=wndx;
 }
 const int vcEndX= vcStartX + int( double(POINT_LAND + NO_POINT_LAND)
                                               * freqPix2 / FREQ_SMP + .5);

#ifdef DEBUG2
 printf("vcEndX %d\n",vcEndX);
#endif
 if(vcEndX>wndx) error(error_code(-123));
 const int vcDiff=vcEndX - vcStartX;

// const int minDist= cutP2X[0];

 char *lowRes[2];

 lowRes[0]= (char*)newAlign(vcDiff, 8);
 lowRes[1]= (char*)newAlign(vcDiff, 8);

 int *convVec= new int[ (vcDiff>>(PHASE_CMP_STEP-1)) + 1 ];
 memset(convVec, 0, ( (vcDiff>>(PHASE_CMP_STEP-1)) + 1 )*sizeof(int) );

 long TCorr=0, TReSmp=0, TUVDetect=0, TPhaseDiff1=0, TPhaseCorrect=0,
      TPhaseDiff2_Drift=0, TEdgeDetect=0 ,TEdgeArea=0, TCache=0;

 if(iState==2){
   T1=uclock();
 }

 static int edgeLut[2048];
 static int phaseLut[256];
 static bool first= true;

 static int phasePerCutP[256];
 static char sin1PerCutP[256];
 static char cos1PerCutP[256];
 static char sin2PerCutP[256];
 static char cos2PerCutP[256];

// static int cutPs=0;
 static int fields=0;
 fields++;

 static bool findPhases=true;
 static double phaseCorrect=0;
 static double lastPhaseCorrect=0;
 if(isVCPhaseTrick)
 	{
	phaseCorrect+=12.0/180.0*PI;
	if(phaseCorrect > MAX_PHASE_CORRECT) phaseCorrect= MIN_PHASE_CORRECT;
	isVCPhaseTrick=false;
	}
 if(mabs(phaseCorrect-lastPhaseCorrect) > PHASE_CORRECT_THRESHOLD && some==0)
 	{
	lastPhaseCorrect= phaseCorrect;
	findPhases=true;
	}

 if(findPhases)
 	{
   	for(int cutP=0; cutP<256; cutP++)
		{
     	const double phase=   PHASE_SMP*NO_POINT_LAND1 - PHASE_LINE 
	 	 					+ PHASE_POINT*cutP - phaseCorrect;
     	phasePerCutP[cutP]= (byte)( round( phase * 128 / PI) );
     	sin1PerCutP[cutP]= char( round( sin( -phase + PHASE_SEG )*127 ) );
     	cos1PerCutP[cutP]= char( round( cos( -phase + PHASE_SEG )*127 ) );
     	sin2PerCutP[cutP]= char( round( sin( -phase )*127 ) );
     	cos2PerCutP[cutP]= char( round( cos( -phase )*127 ) );
   		}
	findPhases=false;
	}

 if(first){
//   if(first) some=10;

  for(int i=0; i<2048; i++){
     const int j= min(i+1, EDGE_LIMIT);
     edgeLut[i]=int( log(j)*EDGE_COEFF +.5 );
   }

   for(int i=0; i<256; i++){                           
     const int j= max(min(mabs(i-128), PHASE_LIMIT), 1);
     phaseLut[i]=int( pow(j, PHASE_EXP)*PHASE_COEFF +.5);
   }

   first=false;

 }

 const float sinSegPhase= sin(PHASE_SEG);
 const float cosSegPhase= cos(PHASE_SEG);

 const float sinSegPhase2= sin(PHASE_SEG*2);
 const float cosSegPhase2= cos(PHASE_SEG*2);

 bool *black= new bool[wndy];
 black[0]= true;

 byte * llinep=&actVid2MemBufp->b[ -(vgax<<1) ];
 byte * linep= &actVid2MemBufp->b[ 0 ];
 for(int line=1; line<wndy-1; line++){
   llinep+=vgax<<1;
   linep+=vgax<<1;

   char *dTemp = lowRes[0];
   lowRes[0] = lowRes[1];
   lowRes[1] = dTemp;

   black[line]=false; //fix this

   if(mmx && DWN_SMP==1)
        {
        int iMean;
        asm(//"int $3 \n\t"
         "xorl %%ecx, %%ecx                   \n\t"
         ".align 3                            \n\t"
         "1:                                  \n\t"
         "movzbl 0(%%esi, %%edx, 4), %%eax    \n\t"  
         "movzbl 2(%%esi, %%edx, 4), %%ebx    \n\t"  
         "addl %%eax, %%ebx                   \n\t"  
         "shrl $1, %%ebx                      \n\t"
         "addl %%ebx, %%ecx                   \n\t"
         "movb %%bl, (%%edi, %%edx)           \n\t"  
         "incl %%edx                          \n\t"  
         " jnz 1b                             \n\t"  
  
          : "=c" (iMean)
          : "S" ( linep + 1 + (vcStartX<<1) + (vcDiff>>1)*4 ) ,
            "d" ( -(vcDiff>>1) ) ,
            "D" ( lowRes[1] + (vcDiff>>1) ) 
          : "%eax", "%ebx", "%edx", "%esi", "%edi");

        if(double(iMean) / double(vcDiff>>1) < BAD_MEAN_THRESHOLD)
            {
            black[    line           ]=true;
            }
        }
   else if(mmx){
     asm(//"int $3 \n\t"
         "pushl %%ebp          \n\t"

         "1:                   \n\t"
         "xorl %%ebx, %%ebx    \n\t"   
         "movl %1, %%ebp       \n\t"   // U
         "xorl %%eax, %%eax    \n\t"   //  V 1
         "2:                   \n\t"
         "addl %%eax, %%ebx    \n\t"   // U
         "movb (%%esi), %%al   \n\t"   // U
         "addl $2, %%esi       \n\t"   //  V 1
         "decl %%ebp           \n\t"   // U
         " jnz 2b              \n\t"   //  V 1
         "incl %%edi           \n\t"   // U
         "addl %%eax, %%ebx    \n\t"   //  V 1
         "shrl %0, %%ebx       \n\t"   // U         
         "decl %%edx           \n\t"   //  V 1
         "movb %%bl, -1(%%edi) \n\t"   // U
         " jnz 1b              \n\t"   //  V 1
  
         "popl %%ebp            \n\t"
          : 
          : "I" (DWN_SMP), "i" (1<<DWN_SMP),
            "S" (linep + 1 + (vcStartX<<1)) ,
            "d" (vcDiff>>DWN_SMP) ,
            "D" (lowRes[1]) 
          : "%eax", "%ebx", "%edx", "%esi", "%edi");

   }
   else{
     asm(//"int $3 \n\t"
         "pushl %%ebp          \n\t"

         "1:                   \n\t"
         "movw (%%edi), %%ax   \n\t"   // U        load 32 byte in L1-cache
         "xorl %%ebx, %%ebx    \n\t"   //  V 1
         "movl %1, %%ebp       \n\t"   // U
         "xorl %%eax, %%eax    \n\t"   //  V 1
         "2:                   \n\t"
         "addl %%eax, %%ebx    \n\t"   // U
         "xorl %%eax, %%eax    \n\t"   //  V 1
         "movb (%%esi), %%al   \n\t"   // U
         "addl $2, %%esi       \n\t"   //  V 1
         "decl %%ebp           \n\t"   // U
         " jnz 2b              \n\t"   //  V 1
         "addl $2, %%edi       \n\t"   // U
         "addl %%eax, %%ebx    \n\t"   //  V 1
         "shrl %0, %%ebx       \n\t"   // U         
         "decl %%edx           \n\t"   //  V 1
         "movw %%bx, -2(%%edi) \n\t"   // U
         " jnz 1b              \n\t"   //  V 1
  
         "popl %%ebp            \n\t"
          : 
          : "I" (DWN_SMP), "i" (1<<DWN_SMP),
            "S" (linep + 1 + (vcStartX<<1)) ,
            "d" (vcDiff>>DWN_SMP) ,
            "D" (lowRes[1]) 
          : "%eax", "%ebx", "%edx", "%esi", "%edi");
   }



   if(iState==2){
     T2=uclock();
     TReSmp+=T2-T1-uclockWaste;
     T1=T2;
   }

   if(line<2) continue;

   int bestDiff=0x1FFFFFFF;
   int avgDiff=0;             // for detecting bad lines
   int avgDiffNum=1;          // for detecting bad lines
   int bestCutPX=0;

   if(!black[line])
        {
        bool firstPass=true;
        for(int step=STEP_START; step>STEP_END; step>>=1){

            const int cutPStart= firstPass ? 0 : step>>1;

            for(int cutP=cutPStart; cutP<vcDiff; cutP+=step){
                if(    firstPass || cutP+step>=vcDiff || cutP==step>>1 
                    || bestDiff+(bestDiff>>1) > addapBuff[ cutP - (step>>1) ]
                    || bestDiff+(bestDiff>>1) > addapBuff[ cutP + (step>>1) ]){

                    int diff;
                    if(mmx){
                        diff = vc_corr_mmx(
                            int(lowRes[1]                                ),
                            int(lowRes[0] + (( vcDiff - cutP) >> DWN_SMP)),
                                cutP >> DWN_SMP ,
                            int(lowRes[1] + (( cutP         ) >> DWN_SMP)),
                            int(lowRes[0]                                ),
                                (vcDiff-cutP) >> DWN_SMP                  );
                    }
                    else{
                        diff = vc_corr(
                            int(lowRes[1] + (((        + cutP) >> (DWN_SMP-1))&~1)),
                            int(lowRes[0] + ((( vcDiff       ) >> (DWN_SMP-1))&~1)),
                                -(( cutP >> (DWN_SMP-1) ) & ~1),
                            int(lowRes[1] + ((( vcDiff       ) >> (DWN_SMP-1))&~1)),
                            int(lowRes[0] + ((( vcDiff - cutP) >> (DWN_SMP-1))&~1)),
                                -(( (vcDiff-cutP) >> (DWN_SMP-1) ) & ~1)             );
              
                    }

                    if(bestDiff>diff){
                        bestDiff=diff;
                        bestCutPX=cutP;
                    }

                    avgDiff+=diff;
                    avgDiffNum++;

                    addapBuff[cutP]=diff;
                }
                else addapBuff[cutP]=0x1FFFFFFF;
            }
            if(firstPass) step<<=1;
            firstPass=false;
        }
    }

   asm("emms\n\t");
/*
   bestCutPX= bestCutPX+some-10;
   if(bestCutPX<0) bestCutPX+=vcDiff;
   if(bestCutPX>=vcDiff) bestCutPX-=vcDiff;
  */
   if( int( avgDiff*BAD_THRESHOLD1/float(avgDiffNum) ) - BAD_THRESHOLD2 < bestDiff 
   		&& !black[line-1] && !black[line])
        {
        black[line-1]=true;
        black[line  ]=true;
        }

   if(iState==2){
     T2=uclock();
     TCorr+=T2-T1-uclockWaste;
     T1=T2;
   }

//   bestCutPX+=some-10;
   int sumI=0;
   for(int x=-2; x<2; x+=2){
       const int nx= x + (vcDiff & ~1);
       const int ul= char(linep[ (((vcStartX & ~1) +  x)<<1) + 0]);
       const int vl= char(linep[ (((vcStartX & ~1) +  x)<<1) + 2]);
       const int un= char(linep[ (((vcStartX & ~1) + nx)<<1) + 0]);
       const int vn= char(linep[ (((vcStartX & ~1) + nx)<<1) + 2]);
       sumI+= ul*vn - vl*un;
//       linep[ (((vcStartX & ~1) - x)<<1) + 1]=255;
//       linep[ (((vcStartX & ~1) - nx)<<1) + 1]=255;
   }
  
   if(line>10 && line<250){
   	   if(line & 1){
         vScore+= sumI;
       }else{
         vScore-= sumI;
       }
   }
              

   bestCutPX+= cutPointX[line-1];
   if(bestCutPX>=vcDiff) bestCutPX-=vcDiff;
   cutPointX[line]=bestCutPX;

   if(iState==2){
     T2=uclock();
     TUVDetect+=T2-T1-uclockWaste;
     T1=T2;
   }
 }

 deleteAlign(lowRes[0]);
 deleteAlign(lowRes[1]);

 int iSumOfAbsDiff=0;
 int iCount=1;
 for(int xLine=2; xLine<wndy-1; xLine++)
    {
    if( black[xLine] || black[xLine-1]) continue;

    iCount++;

    int iCutP1= x2CutP[ cutPointX[xLine-1] ];
    int iCutP2= x2CutP[ cutPointX[xLine  ] ];

    iSumOfAbsDiff+= mabs(iCutP2 - iCutP1);
    }
 bool bVC=true;
 if(iSumOfAbsDiff/iCount < NOT_VC_THRESHOLD) bVC=false;

 byte *dir=new byte[(wndy + 1)<<8];

 int *bestDir=new int[wndy + 1];
 for(int i=0; i<wndy+1; i++) bestDir[i]=-1;

 const int driftPenalty[MAX_DRIFT*2+1]= DRIFT_TAB;

 byte *pPhaseErr= new byte[256*wndy];

 llinep=&actVid2MemBufp->b[ -(vgax<<2) ];
 linep= &actVid2MemBufp->b[ -(vgax<<1) ];
 if(bVC){
    int *newVal=new int[257], *lastVal=new int[257];
    memset(lastVal, 0, 256<<2);
	lastVal[256]=1; //for edge detect (faster at skiping zeros)

//    byte *newPhaseErr=new byte[256], *lastPhaseErr=new byte[256];
//    memset(lastPhaseErr, 0, 256);

    for(int line=0; line<wndy-1; line++){
        llinep+=vgax<<1;
        linep+=vgax<<1;

        memset(newVal, 0, 256<<2);
		newVal[256]=1; //for edge detect (faster at skiping zeros)
//        memset(newPhaseErr, 0, 256);
//	    memset(pPhaseErr+line*256, 0, 256);
        bool noLeft=true;

        bestDir[line]=-1;

        if(line<1) continue;

        int relCutPX= cutPointX[line] - cutPointX[line-1];
        if(relCutPX<0) relCutPX+=vcDiff;

        int chromAmp=1;
        for(int x=8; x<vcDiff-8; x+= 1<<PHASE_CMP_STEP){
            int nx= x + relCutPX;
            if(nx>=vcDiff) nx-= vcDiff;
            nx&= ~1;              //Changed
            const int ul= char(llinep[ (((vcStartX & ~1) +  x)<<1) + 0]);
            const int vl= char(llinep[ (((vcStartX & ~1) +  x)<<1) + 2]);
            const int un= char( linep[ (((vcStartX & ~1) + nx)<<1) + 0]); //HOTSPOT 2.7%
            const int vn= char( linep[ (((vcStartX & ~1) + nx)<<1) + 2]);
            const int p= x>>(PHASE_CMP_STEP-1);
            convVec[p  ]= ul*un + vl*vn;
            convVec[p+1]= ul*vn - vl*un;
            chromAmp+= mabs(un) + mabs(vn);
        }
        if(line<2) chromAmp=1;
		pChromAmp[line]= chromAmp;
   
//   linep[ ((vcStartX +  0)<<1) + 1]=255;
 //  linep[ ((vcStartX +  relCutPX)<<1) + 1]=200;

        if(iState==2){
            T2=uclock();
            TPhaseDiff1+=T2-T1-uclockWaste;
            T1=T2;
        }

        int rrR=0, rrI=0;
        int rlR=0, rlI=0;

        {                 
            int x=0;
            for(; x<vcDiff-relCutPX; x+= 1<<PHASE_CMP_STEP){
                const int p= x>>(PHASE_CMP_STEP-1);
                rrR+= convVec[p  ];
                rrI+= convVec[p+1];
            }
            for(; x<vcDiff; x+= 1<<PHASE_CMP_STEP){
                const int p= x>>(PHASE_CMP_STEP-1);
                rlR+= convVec[p  ];
                rlI+= convVec[p+1];
            }
        }

        const float s= ( (line & 1) ^ ( vScore>0 ) ) ? -1 : 1;

		int llR=0, llI=0;
		int lrR=0, lrI=0;
        int rlRC= int( round( rlR*cosSegPhase   + rlI*sinSegPhase*s) );
        int rlIC= int( round(-rlR*sinSegPhase*s + rlI*cosSegPhase  ) ); 
		int lrRC=0;
		int lrIC=0;

        int lastPhasePos=0;
            
        byte ang=0;
        if(!black[line] && !black[line-1]){
            for(int lastCutP = 0; lastCutP < 256; lastCutP++){
				while(lastVal[lastCutP]==0) lastCutP++;
				if(lastCutP>=256) break;

                const int newPhasePos=
                    min(
                        (cutP2X[ lastCutP ]+(1 << (PHASE_CMP_STEP-1)) )
                            & ((-1) << PHASE_CMP_STEP),
                        (vcDiff-1) & ((-1) << PHASE_CMP_STEP)
                       );

	            if( newPhasePos >= lastPhasePos )
					{

                    for(; lastPhasePos <= newPhasePos; lastPhasePos += (1<<PHASE_CMP_STEP))
						{
                        const bool isLast= lastPhasePos == newPhasePos;

                        const int p= (lastPhasePos>>(PHASE_CMP_STEP-1)) ;

						if(   lastPhasePos+relCutPX >= vcDiff 
						   && lastPhasePos+relCutPX < vcDiff+(1<<PHASE_CMP_STEP) )
							{
							rrR=rlR, rrI=rlI;
							lrR=llR, lrI=llI;
							llR=0  , llI=0;
							rlR=0  , rlI=0;
					        lrRC= int( round( lrR*cosSegPhase   + lrI*sinSegPhase*s) );
     					    lrIC= int( round(-lrR*sinSegPhase*s + lrI*cosSegPhase  ) ); 
							rlRC=0;
							rlIC=0;
							}

                      	rrR-=convVec[p  ];
                        rrI-=convVec[p+1];
         
                        if(isLast)
							{

                        	const float r= rrR + rlRC + lrRC
                                    + llR*cosSegPhase2
                                    + llI*sinSegPhase2*s;
                            const float i= rrI + rlIC + lrIC
                                    - llR*sinSegPhase2*s
                                    + llI*cosSegPhase2;                                    
            
                            ang= (byte)round( atan3(i, r)/PI*128 );
                            }

                        llR+=convVec[p  ];
                        llI+=convVec[p+1];
	                    }
	
                    }

                int newXTemp= cutPointX[line] - cutPointX[line-1] + cutP2X[lastCutP];
                if(newXTemp <  0     ) newXTemp+= vcDiff;
                if(newXTemp >= vcDiff) newXTemp-= vcDiff;
                if(newXTemp >= vcDiff) newXTemp-= vcDiff;

                const int newCutPStart= max(x2CutP[ newXTemp ] - MAX_DRIFT, 0);
                const int newCutPEnd=   min(x2CutP[ newXTemp ] + MAX_DRIFT, 256);

//       const int drift= newCutPStart - (x2CutP[ newXTemp ] - MAX_DRIFT);
                const int *piDrift=
                    &driftPenalty[newCutPStart - (x2CutP[ newXTemp ] - MAX_DRIFT)-1];

                byte a= (line & 1) ^ ( vScore>0 ) ? -ang: ang;
                a+=256/2;

                const int phase1= (a + phasePerCutP[ lastCutP ]) & 0xFF;
                const int lastPhaseErrLastCutP= pPhaseErr[(line-1)*256 + lastCutP] + 128;
          // FIX (ASM_OPTIMIZE)
                if(chromAmp>AMP_THRESHOLD1){
                    for(int newCutP= newCutPStart; newCutP <= newCutPEnd; newCutP++){
                        const int phaseErr= (phase1 + phasePerCutP[ newCutP ]) & 0xFF;
                        const int val=  lastVal[lastCutP] - *(++piDrift)
                         - phaseLut[ phaseErr ]
                         - phaseLut[ (phaseErr - lastPhaseErrLastCutP) & 0xFF];// useless without phase errors from decomb-filter
     
                        if(newVal[newCutP] < val){
                            newVal[newCutP]= val;
                            dir[(line<<8) + newCutP]= lastCutP;
                            pPhaseErr[line*256 + newCutP]= phaseErr;
        
                            noLeft=false;
                        }
                    }
                }
                else{
                    linep[ ((vcStartX -  1)<<1) + 1]=255;
                    for(int newCutP= newCutPStart; newCutP <= newCutPEnd; newCutP++){
//                        const int phaseErr= (phase1 + phasePerCutP[ newCutP ]) & 0xFF;
                        const int val=  lastVal[lastCutP] - *(++piDrift);
     
                        if(newVal[newCutP] < val){
                            newVal[newCutP]= val;
                            dir[(line<<8) + newCutP]= lastCutP;
                            pPhaseErr[line*256 + newCutP]= 0; //phaseErr;
        
                            noLeft=false;
                        }
                    }
                }
            }
        }

        if(iState==2){
            T2=uclock();
            TPhaseDiff2_Drift+=T2-T1-uclockWaste;
            T1=T2;
        }

        if(!black[line])
			{
            int iBestVal=0;

            if(edgeAlgo==0)
				{
	            for(int newCutP = 0; newCutP < 256; newCutP++)
					{
					if(!noLeft)
						{
						while(newVal[newCutP]==0) newCutP++;
						}
					if(newCutP>=256) break;

					
    	            const byte * const p=
        	            &linep[ ((cutP2X[ newCutP ] + vcStartX + edgePos)<<1) + 1];  // (-0) - (-4) 

            	    const int diff=edgeLut[ mabs( + int(* p   ) + int(*(p+2))
    	                                          - int(*(p+4)) - int(*(p+6)) ) ];
  
	                if(newVal[newCutP]==0) newVal[newCutP] =  diff + 1000000000;
    	            else                   newVal[newCutP] += diff;
			
            	    if(iBestVal < newVal[newCutP])
						{
                    	iBestVal= newVal[newCutP];
	                    bestDir[line]= newCutP;
    	            	}
			
            		}
        		}
            else 
				{
	            for(int newCutP = 0; newCutP < 256; newCutP++)
					{
					if(!noLeft)
						{
						while(newVal[newCutP]==0) newCutP++;
						}
					if(newCutP>=256) break;

					
    	            const byte * const p=
        	            &linep[ ((cutP2X[ newCutP ] + vcStartX + edgePos)<<1) + 1];  

            	    const int diff=edgeLut[ mabs( + int(* p   ) + int(*(p+2))
	                                              - int(*(p+6)) - int(*(p+8)) ) ]; //no converting factor!
  
	                if(newVal[newCutP]==0) newVal[newCutP] =  diff + 1000000000;
    	            else                   newVal[newCutP] += diff;
			
            	    if(iBestVal < newVal[newCutP])
						{
                    	iBestVal= newVal[newCutP];
	                    bestDir[line]= newCutP;
    	            	}
			
            		}
       			}
			}
   
        if(iState==2){
            T2=uclock();
            TEdgeDetect+=T2-T1-uclockWaste;
            T1=T2;
        }

        int *tempVal=lastVal;
        lastVal=newVal;
        newVal=tempVal;

    }


    delete [] newVal;
    delete [] lastVal;
 }
 delete [] convVec;


 int piBestCutP[600];
 piBestCutP[0     ]=
 piBestCutP[wndy-1]=-1;

 int iBestCutP=-1;
 for(int line=wndy-2; line>0; line--)
    {
    piBestCutP[line]=-1;

    if(bestDir[line]==-1)
        {
        iBestCutP=-1;
        continue;
        }

    if(iBestCutP==-1) iBestCutP= bestDir[line];
    if(iBestCutP<0 || iBestCutP>=256)
        {
        printf("one%d\n", iBestCutP);
        iBestCutP=-1;
        continue;
        }
    piBestCutP[line]= iBestCutP;

    iBestCutP= dir[(line<<8) + iBestCutP];
    }

#ifdef DEBUG_CUTP
 for(int line=1; line<wndy-1; line++)
    {
    fprintf(pF, "%d\n", piBestCutP[line]);
    }
#endif

 if(bVC) cache(piBestCutP);
	

 if(iState==2)
    {
    T2=uclock();
    TCache+=T2-T1-uclockWaste;
    T1=T2;
    }							 
 
 int phaseErrSum=0;
 for(int line=10; line<wndy-10; line++)
 	{
	if(black[line] || black[line-1] || pChromAmp[line]<AMP_THRESHOLD2) continue;
	int phaseErr= pPhaseErr[ line*256 + piBestCutP[line] ];
	if(phaseErr>128) phaseErr-=256;
	phaseErrSum+= phaseErr;
	}

 if(bVC) phaseCorrect-= double(phaseErrSum) / FIELDS_PER_PHASE_CHECK / wndy / 2 /128.0*PI;

 if(phaseCorrect>MAX_PHASE_CORRECT) phaseCorrect= MAX_PHASE_CORRECT;
 if(phaseCorrect<MIN_PHASE_CORRECT) phaseCorrect= MIN_PHASE_CORRECT;

 	
 delete [] pPhaseErr;

 if(iState==2)
    {
    T2=uclock();
    TPhaseCorrect+=T2-T1-uclockWaste;
    T1=T2;
    }							 
	

 int ppEdgeAreaScore[2][ (EDGE_AREA_END - EDGE_AREA_START) ];
 memset( ppEdgeAreaScore[0], 0, (EDGE_AREA_END - EDGE_AREA_START)*sizeof(int));
 memset( ppEdgeAreaScore[1], 0, (EDGE_AREA_END - EDGE_AREA_START)*sizeof(int));
 for(int line=10; line<wndy-10; line+=3)
 	{   
	if(!black[line] && piBestCutP[line]!=-1)
		{
        for(int x = EDGE_AREA_START; x<EDGE_AREA_END; x++)
			{
            const byte * const p=
			 	&actVid2MemBufp->b
					[
					+(line * (vgax<<1))
			 		+((cutP2X[ piBestCutP[line] ] + vcStartX + x)<<1) + 1
					];

            int pDiff[2];
			pDiff[0]= edgeLut[ mabs( + int(* p   ) + int(*(p+2))
                                     - int(*(p+4)) - int(*(p+6)) )     ];
            pDiff[1]= edgeLut[ mabs( + int(* p   ) + int(*(p+2))
                                     - int(*(p+6)) - int(*(p+8)) )*7/8 ];

  		    ppEdgeAreaScore[0][x-EDGE_AREA_START]+= pDiff[0];
  		    ppEdgeAreaScore[1][x-EDGE_AREA_START]+= pDiff[1];
            }
        }
	}

 int pBestEdgeScore[2]={0,0};
 int pBestEdgePos[2]={0,0};
 for(int a=0; a<2; a++)
 	{
 	for(int x = EDGE_AREA_START; x<EDGE_AREA_END; x++)
		{
		if(ppEdgeAreaScore[a][x-EDGE_AREA_START] > pBestEdgeScore[a])
			{
			pBestEdgeScore[a]= ppEdgeAreaScore[a][x-EDGE_AREA_START];
			pBestEdgePos[a]= x-EDGE_AREA_START;
			}
		}
	}

 double bestFit=0;
 int bestEdgeAlgo=0;
 int bestEdgePos=0;
 for(int a=0; a<2; a++)
 	{
	int larger= max( 
		ppEdgeAreaScore[a][ (pBestEdgePos[a]-1<0) ? (pBestEdgePos[a]+1) : (pBestEdgePos[a]-1) ],
		ppEdgeAreaScore[a][ (pBestEdgePos[a]+1>=(EDGE_AREA_END - EDGE_AREA_START)) ? 
  					 							   (pBestEdgePos[a]-1) : (pBestEdgePos[a]+1) ]
		);

	double fit= double(pBestEdgeScore[a]) / double(larger+.0001);
	if(fit>bestFit)
		{
		bestFit=fit;
		bestEdgeAlgo=a;
		bestEdgePos= pBestEdgePos[a] + EDGE_AREA_START;
		}
	}

 int linesOkEdge=0;
 int lines=0;
 for(int line=10; line<wndy-10; line+=3)
 	{   
	if(!black[line] && piBestCutP[line]!=-1)
		{
		int goodPos= 0;
		int goodScore= 0;
		lines++;

        for(int x = EDGE_AREA_START; x<EDGE_AREA_END; x++)
			{
            const byte * const p=
			 	&actVid2MemBufp->b
					[
					+(line * (vgax<<1))
			 		+((cutP2X[ piBestCutP[line] ] + vcStartX + x)<<1) + 1
					];

            int pDiff[2];
			pDiff[0]= edgeLut[ mabs( + int(* p   ) + int(*(p+2))
                                     - int(*(p+4)) - int(*(p+6)) )     ];
            pDiff[1]= edgeLut[ mabs( + int(* p   ) + int(*(p+2))
                                     - int(*(p+6)) - int(*(p+8)) )*7/8 ];

			if(pDiff[bestEdgeAlgo] > goodScore)
				{
				goodScore= pDiff[bestEdgeAlgo];
				goodPos= x;
				}
			}
		if(goodPos == bestEdgePos)
			linesOkEdge++;
        }
	}

if(lines>MIN_LINES_FOR_EDGE_AREA && lines*EDGE_CHANGE_THRESHOLD<linesOkEdge)
	{
	edgePos= bestEdgePos;
	edgeAlgo= bestEdgeAlgo;
	}


#ifdef SHOW_EDGE_AREA
	for(int line=10; line<wndy-10; line+=3)
		{
		actVid2MemBufp->b
			[
			+(line * (vgax<<1))
 			+((cutP2X[ piBestCutP[line] ] + vcStartX + edgePos)<<1) + 1
 			]= 255;
		actVid2MemBufp->b
			[
			+(line * (vgax<<1))
 			+((cutP2X[ piBestCutP[line] ] + vcStartX + edgePos)<<1) + 7
 			]= 255;
		}
#endif
 if(iState==2)
    {
    T2=uclock();
    TEdgeArea+=T2-T1-uclockWaste;
    T1=T2;
    }

 if(showPoints)
    {
    for(int line=wndy-2; line>0; line--)
        {
        int bestCutP=piBestCutP[line];
        if(bestCutP==-1) continue;

//     const int bestX= cutP2X[ bestCutP ];
         int bestX=cutP2X[ bestCutP ] + cutPointX[line-1] - cutPointX[line];
         if(bestX<0) bestX+= vcDiff;
        if(bestX>=vcDiff) bestX-= vcDiff;
        actVid2MemBufp->b[((line-1) * vgax<<1) + ((vcStartX + bestX)<<1)+1]=255;
        actVid2MemBufp->b[((line-1) * vgax<<1) + ((vcStartX + bestX)<<1)+3]=0;

        }
    }

 if(bVC){
    byte copy_buff[max_x<<1];

    linep= &actVid2MemBufp->b[ (wndy-1) * (vgax<<1) ];
    llinep=&actVid2MemBufp->b[  wndy    * (vgax<<1) ];
    for(int line=wndy-2; line>0; line--){
        linep-= vgax<<1;
        llinep-= vgax<<1;

        if(black[line]){
            linep[ ((vcStartX - 4)<<1) + 1 ]=255;
            linep[ ((vcStartX - 4)<<1) + 3 ]=255;
        }

        if(showPoints){
            linep[ (vcStartX<<1)+1 ]=255;
            linep[ (vcStartX<<1)+3 ]=0;
        }       
        int bestCutP=piBestCutP[line];
        if(bestCutP==-1) continue;

        const int bestX= cutP2X[ bestCutP ];

        int cutX= ((showPoints && 0) ? cutPointX[line] : bestX );

        const int cutP= max(x2CutP[cutX], 0);          // fix ? ness, min?
 
        const char s1= sin1PerCutP[ cutP ];
        const char c1= cos1PerCutP[ cutP ];
        const char s2= sin2PerCutP[ cutP ];
        const char c2= cos2PerCutP[ cutP ];

        if( (line & 1) ^ ( vScore>0 ) ){
            char *p= (char*)( &linep[ ((vcStartX & ~1)<<1) ] );
            int x=0;
            for(; x<cutX; x+=4){
                const char u= * p;
                const char v= *(p+2);

//                const char u1=(char)(( + v*c1 + u*s1)>>7);
//                const char v1=(char)(( - v*s1 + u*c1)>>7);

                const char u1=(char)(( + u*c1 - v*s1)>>7);
                const char v1=(char)(( - u*s1 - v*c1)>>7);


                if( (vcDiff - cutX) & 1 ){
                    * p   = v1;
                    *(p+2)= u1;
                    *(p+4)= v1;
                    *(p+6)= u1;

                }
                else{
                    * p   = u1;
                    *(p+2)= v1;
                    *(p+4)= u1;
                    *(p+6)= v1;
                }
                p+=8;
            }

            for(; x<vcDiff; x+=4){
                const char u= * p;
                const char v= *(p+2);

//                const char u1=(char)(( + v*c2 + u*s2)>>7);
//                const char v1=(char)(( - v*s2 + u*c2)>>7);

                const char u1=(char)(( + u*c2 - v*s2)>>7);
                const char v1=(char)(( - u*s2 - v*c2)>>7);

       
                if( cutX & 1 ){
                    * p   = v1;
                    *(p+2)= u1;
                    *(p+4)= v1;
                    *(p+6)= u1;
                }
                else{
                    * p   = u1;
                    *(p+2)= v1;
                    *(p+4)= u1;
                    *(p+6)= v1;
                }
                p+=8;
            }
        }

 
//   linep[ ((vcStartX)<<1)+1 ]= 255;
   
        memcpy(copy_buff, linep + (vcStartX<<1), vcDiff<<1);

        memcpy(linep + (vcStartX<<1), copy_buff + (cutX<<1), (vcDiff-cutX)<<1);
        memcpy(linep + (vcStartX<<1) + ((vcDiff-cutX)<<1), copy_buff, cutX<<1);

        if( !( (line & 1) ^ ( vScore>0 ) ) ){
            char *pl= (char*)( &llinep[ ((vcStartX<<1) & ~3) ] );
            char *pn= (char*)( &linep[  ((vcStartX<<1) & ~3) ] );
/*     for(int x=0; x<vcDiff; x+=4){
       * pn   = *(pn+4)= * pl;
       *(pn+2)= *(pn+6)= *(pl+2);
       pn+=8;
       pl+=8;
     }*/
     asm("1:                           \n\t"
         "movb  (%%esi, %%ecx), %%al   \n\t"
         "movb 2(%%esi, %%ecx), %%bl   \n\t"
         "movb %%al,  (%%edi, %%ecx)   \n\t"
         "movb %%bl, 2(%%edi, %%ecx)   \n\t"
//         "movb 4(%%esi, %%ecx), %%al   \n\t"
//         "movb 6(%%esi, %%ecx), %%bl   \n\t"
         "movb %%al, 4(%%edi, %%ecx)   \n\t"
         "movb %%bl, 6(%%edi, %%ecx)   \n\t"
//         "movb 8(%%edi, %%ecx), %%al   \n\t"
         "addl $8, %%ecx               \n\t"
         "  jnc 1b                     \n\t"
          :
          : "D" (int(pn) + (vcDiff<<1)),
            "S" (int(pl) + (vcDiff<<1)) ,
            "c" (-vcDiff<<1) 
          : "%eax", "%ebx", "%ecx");
        }

    }
 }

/*
 float avgCutP=0;
 for(int line=50; line<200; line++){
   avgCutP+=float(cutpp[l  ine]);
 }

 avgCutP/=(200.0-50.0);
 printf("avg%f\n", avgCutP-float(vcDiff)/2.0);
  */
  
#ifdef DEBUG2
 printf("vScore %d\n", vScore);
#endif

    if(iState==2)
        {
        T2=uclock();
        sprintf(textbuf[0],"%f ReSample", (float)(TReSmp)/UCLOCKS_PER_SEC);
        sprintf(textbuf[1],"%f Correlate", (float)(TCorr)/UCLOCKS_PER_SEC);
        sprintf(textbuf[2],"%f UVDetect", (float)(TUVDetect)/UCLOCKS_PER_SEC);
        sprintf(textbuf[3],"%f FindPhaseDiff1", (float)(TPhaseDiff1)/UCLOCKS_PER_SEC);
        sprintf(textbuf[4],"%f FindPhaseDiff2 / Drift", (float)(TPhaseDiff2_Drift)/UCLOCKS_PER_SEC);
        sprintf(textbuf[5],"%f EdgeDetect", (float)(TEdgeDetect)/UCLOCKS_PER_SEC);
        sprintf(textbuf[6],"%f Cache", (float)(TCache)/UCLOCKS_PER_SEC);
        sprintf(textbuf[7],"%f FindPhaseCorrect", (float)(TPhaseCorrect)/UCLOCKS_PER_SEC);
        sprintf(textbuf[7],"%f EdgeArea", (float)(TEdgeArea)/UCLOCKS_PER_SEC);
        sprintf(textbuf[8],"%f Rotate / ColorFix", (float)(T2-T1-uclockWaste)/UCLOCKS_PER_SEC);
        c.init(255, 0, 0, false);
        gprint(infoPosX, infoPosY+=10, c.col, textbuf[0]);
        gprint(infoPosX, infoPosY+=10, c.col, textbuf[1]);
        gprint(infoPosX, infoPosY+=10, c.col, textbuf[2]);
        gprint(infoPosX, infoPosY+=10, c.col, textbuf[3]);
        gprint(infoPosX, infoPosY+=10, c.col, textbuf[4]);
        gprint(infoPosX, infoPosY+=10, c.col, textbuf[5]);
        gprint(infoPosX, infoPosY+=10, c.col, textbuf[6]);
        gprint(infoPosX, infoPosY+=10, c.col, textbuf[7]);
        gprint(infoPosX, infoPosY+=10, c.col, textbuf[8]);
        infoPosY+=5;
        T1=T2;                           
    }
    if(iState>=1)
        {
        sprintf(textbuf[0],"%d/%d Caches, %d/%d Cached",
            nBadCaches,
            nGoodCaches,
            nFieldsUnCached,
            nFieldsCached);

        c.init(255, 0, 0, false);
        gprint(infoPosX, infoPosY+=10, c.col, textbuf[0]);

        byte pbScore[100];
        for(int i=0; i<iCurrentLUTScore; i++)
            pbScore[i]='*';
        for(int i=iCurrentLUTScore; i<nTEST_LINES; i++)
            pbScore[i]='.';
        pbScore[ nTEST_LINES ]=0;

        sprintf(textbuf[1],"LUT Score %s",
            pbScore);

        gprint(infoPosX, infoPosY+=10, c.col, textbuf[1]);


        byte pbUsed[100];
        iCurrentUsed= min(iCurrentUsed, 20);
        for(int i=0; i<iCurrentUsed; i++)
            pbUsed[i]='*';
        for(int i=iCurrentUsed; i<20; i++)
            pbUsed[i]='.';
        pbUsed[ 20 ]=0;

        sprintf(textbuf[1],"Used %s",
            pbUsed);

        gprint(infoPosX, infoPosY+=10, c.col, textbuf[1]);


        sprintf(textbuf[1],"Bad Final Left %d", badFinalsLeft);
        gprint(infoPosX, infoPosY+=10, c.col, textbuf[1]);


        sprintf(textbuf[1],"phaseCorrect %3.1f", phaseCorrect/PI*180);
        gprint(infoPosX, infoPosY+=10, c.col, textbuf[1]);

        }

  delete [] bestDir;
  delete [] dir;
  delete [] black;
}


