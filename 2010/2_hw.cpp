//2010 0.1 Copyright (C) Michael Niedermayer 1998

#include <stdlib.h>
#include <stdio.h>
#include <pc.h>
#include <go32.h>
#include <dpmi.h>
#include <sys/nearptr.h>
#include <signal.h>
#include <errno.h>
#include <time.h>
#include "2_all.h"
#include "2_hw.h"
#include "2_hw_a.h"
#include "2_hw_asm.h"
#include "2_hw_mem.h"
#include "2_71x6.h"
#include "2_mmx.h"
#include "2_gfunc.h"
#include "2010.h"

 extern byte lock_start, lock_end;
 extern int bpp;

 extern volatile int page_flip;
 extern volatile int yuvMode;
 extern volatile AllocList *alloclist;
 extern volatile VID2MEMBUF *vid2MemBuf, *actVid2MemBufp;
 extern volatile int grabVid2MemBuf, actVid2MemBuf;
 extern volatile bool oneField;
 extern volatile int active_meteor;
 extern volatile meteor meteors[8];
 extern volatile int fields;
 extern byte *vidbuf;
 extern int vgax, vgay, xresc, xresvb, vb, mc, wndy, wndx, outy;
 extern unsigned short *yuv2RgbLut;
 extern int single;
 extern int in_int;
 extern volatile int frames;
 extern bool strangeRgb16;
 extern bool iState;
 extern int infoPosX, infoPosY;
 extern TVSTD TVStd;
 extern int iVSmooth;
 extern int vgaBytesPerScanLine;

 bool drop=false;
 bool allowDrop=true;
 int g_mode=0;
 extern u_short my_cs, my_ds;
 asm("__esp: .long 0\n\t");

 asm("__temp1: .long 0\n\t");

 int VID2MEMBUF::num;

void mem2vid(byte *to, byte *from, int num, int bpp, int y){

 if(yuvMode!=0){
   if((y&1) && !oneField && wndy==outy)
     CopyYuv2Rgb2(int(from), int(to), num<<1, int(yuv2RgbLut));
   else
     CopyYuv2Rgb1(int(from), int(to), num<<1, int(yuv2RgbLut));
 }else{
   if(strangeRgb16){
     asm("addl %%ecx, %%esi             \n\t"
         "addl %%ecx, %%edi             \n\t"
         "negl %%ecx                    \n\t"
     
         "movl (%%esi, %%ecx), %%eax    \n\t"
         "1:                            \n\t"
         "  movl %%eax, %%ebx           \n\t"
         "  andl $0xFFE0FFE0, %%eax     \n\t"

         "  shll $1, %%eax              \n\t"
         "  andl $0x001F001F, %%ebx     \n\t"

         "  orl %%eax, %%ebx            \n\t"
         "  movl 4(%%esi, %%ecx), %%eax \n\t"

         "  movl %%ebx, (%%edi, %%ecx)  \n\t"
         "  addl $4, %%ecx              \n\t"

         "jnc 1b                        \n\t"
           :
           : "d" (vgax<<1), "S" (from), "D" (to), "c" (num<<1)
           : "%eax", "%ebx", "%ecx", "%edx", "%esi", "%edi");
   }
   else{
     memcpy(to, from, num<<1);
   }
 }
}

void close_hw(void){
 __dpmi_regs r;

 close_meteor();
 unlockall();
 unmapall();

 if(g_mode==1){
   r.x.ax = 0x4F02;
   r.x.bx = 0x0003;
   __dpmi_int(0x10, &r);
   r.x.ax = 0x0003;
   __dpmi_int(0x10, &r);
 }

 g_mode=0;

 dpmi_freeall(&alloclist);
 vds_freeall();
}

void sig_handler(int i){
 close_hw();
 signal(i, SIG_DFL); 
 raise(i);
}

void end_func(void){}

void init_hw(void){

 my_cs=_go32_my_cs();
 my_ds=_go32_my_ds();

 save_selectors();

 lock(&lock_start, &lock_end);

 lock(meteor_int_handler, meteor_int_handler_end);

}

VesaInfoBlock *get_vesa_info(void)
{
        static VesaInfoBlock info;
	__dpmi_regs r;

        strcpy((char *)&info, "VBE2");  //request vbe2-info
        dosmemput(&info, 4, __tb); // store in buffer

	/* Use the transfer buffer to store the results of VBE call */
        r.x.ax = 0x4F00;
	r.x.es = __tb / 16;
	r.x.di = 0;
	__dpmi_int(0x10, &r);
        if(r.x.ax!=0x004F) return 0;
        dosmemget(__tb, 512, &info);
        return &info;
}

ModeInfoBlock *get_mode_info(int mode)
{
         static ModeInfoBlock info;
	__dpmi_regs r;

	/* Use the transfer buffer to store the results of VBE call */
	r.x.ax = 0x4F01;
	r.x.cx = mode;
	r.x.es = __tb / 16;
	r.x.di = 0;
	__dpmi_int(0x10, &r);
        if(r.x.ax!=0x004F) return 0;
	dosmemget(__tb, sizeof(ModeInfoBlock), &info);
	return &info;
}

void set_start_disp(int x, int y){
 __dpmi_regs r; 

  r.x.ax=0x4F07;            //set/get start of display "window"
  r.x.cx=x;
  r.x.dx=y;
  r.x.bx=0;
  __dpmi_int(0x10, &r);
  if(r.x.ax!=0x4F) error(VESAFlip);
}

void copy_vidbuffer(void){

 static long T1=0, T2=0;
 if(iState){
   char textBuf[128];
   sprintf(textBuf, "%f vid2Mem", (float)(T2-T1)/UCLOCKS_PER_SEC);
   color c;
   c.init(255, 0, 0, false);
   gprint(infoPosX, infoPosY+=10,  c.col, textBuf);
   T1=uclock();
   infoPosY+=5;
 }

 if(!drop || !allowDrop){
   const int goodx= TVStd!=TXTPAL ? wndx : vgax;
   const int goody= TVStd!=TXTPAL ? wndy : vgay;

   if(page_flip==0){
     int pVga=0;
     int pBuf=0;
     for(int y=0; y<goody; y++){
       mem2vid(vidbuf+pVga,              actVid2MemBufp->b+pBuf, goodx, bpp, y);
       pVga+= vgaBytesPerScanLine;
       pBuf+= vgax<<1;
     }
   }else if(page_flip==2){
     int pVga=0;
     int pBuf=0;
     for(int y=0; y<goody; y++){
       mem2vid(vidbuf+vgay*vgaBytesPerScanLine+pVga,  actVid2MemBufp->b+pBuf, goodx, bpp, y);
       pVga+= vgaBytesPerScanLine;
       pBuf+= vgax<<1;
     }
     page_flip=1;
     set_start_disp(0, vgay);
   }else{
     int pVga=0;
     int pBuf=0;
     for(int y=0; y<goody; y++){
       mem2vid(vidbuf+pVga,              actVid2MemBufp->b+pBuf, goodx, bpp, y);
       pVga+= vgaBytesPerScanLine;
       pBuf+= vgax<<1;
     }
     page_flip=2;
     set_start_disp(0, 0);
   }
 }
 drop=false;

 asm("emms\n\t");

 if(iState){
   T2=uclock();
 }

 if( vid2MemBuf[actVid2MemBuf].state==Working ){
   vid2MemBuf[actVid2MemBuf].state=Empty;
 }
 actVid2MemBuf++;
 if(actVid2MemBuf >= vid2MemBuf[0].num) actVid2MemBuf=0;

 while( vid2MemBuf[ actVid2MemBuf ].state!= Grabbed );

 vid2MemBuf[ actVid2MemBuf ].state= Working;


 actVid2MemBufp= &vid2MemBuf[ actVid2MemBuf ];

/*
   printf("%X, %X\n", meteors[active_meteor].saa7116->capt_ctl_a,
                      meteors[active_meteor].saa7116->ints_ctl_a );

   printf("%X, %X\n", doubleBuff[0].state,
                      doubleBuff[1].state );

 printf("X\nDMA E0 START %X\n",  meteors[active_meteor].saa7116->dma.even[0]);
 printf("DMA E1 START %X\n",  meteors[active_meteor].saa7116->dma.even[1]);
 printf("DMA E2 START %X\n",  meteors[active_meteor].saa7116->dma.even[2]);
 printf("DMA O0 START %X\n",  meteors[active_meteor].saa7116->dma.odd[0]);
 printf("DMA O1 START %X\n",  meteors[active_meteor].saa7116->dma.odd[1]);
 printf("DMA O2 START %X\n",  meteors[active_meteor].saa7116->dma.odd[2]);

 printf("STRIDE E0 %X\n",  meteors[active_meteor].saa7116->stride.even[0]);
 printf("STRIDE E1 %X\n",  meteors[active_meteor].saa7116->stride.even[1]);
 printf("STRIDE E2 %X\n",  meteors[active_meteor].saa7116->stride.even[2]);
 printf("STRIDE O0 %X\n",  meteors[active_meteor].saa7116->stride.odd[0]);
 printf("STRIDE O1 %X\n",  meteors[active_meteor].saa7116->stride.odd[1]);
 printf("STRIDE O2 %X\n",  meteors[active_meteor].saa7116->stride.odd[2]);

 printf("DMA E END %X\n",  meteors[active_meteor].saa7116->dma_end.even);
 printf("DMA O END %X\n",  meteors[active_meteor].saa7116->dma_end.odd);
*/

}

u_long read_pci(int bus, int dev, int func, int reg, int size){
 confadd ca;
 u_long u;

 ca.a=0;
 ca.f.enable=1;
 ca.f.bus=bus;
 ca.f.dev=dev;
 ca.f.func=func;
 ca.f.reg=reg>>2;

 outportl(confadd_port, ca.a);             
 u=inportl(confdata_port);                 
 ca.f.enable=0;                            
 outportl(confadd_port, ca.a);             

 u >>= (reg & 3);
 u &=0xFFFFFFFF >> (32-(size<<3));

 return u;
}

void write_pci(int bus, int dev, int func, int reg, int size, u_long dat){
 confadd ca;
 u_long u;

 ca.a=0;
 ca.f.enable=1;
 ca.f.bus=bus;
 ca.f.dev=dev;
 ca.f.func=func;
 ca.f.reg=reg>>2;

 u=read_pci(bus, dev, func, reg, size);

 outportl(confadd_port, ca.a);

 if(size==4)      outportl(confdata_port             , dat);
 else if(size==2) outportw(confdata_port + (reg & 3) , dat);
 else             outportb(confdata_port + (reg & 3) , dat);

 ca.f.enable=0;
 outportl(confadd_port, ca.a);
}

int check_mmx(void){
 int ret;
 asm("movl $1, %%eax          \n\t"
     "cpuid                   \n\t" 
     "andl $0x00800000, %%edx \n\t"
     "shrl $23, %%edx         \n\t"
     : "=d"(ret)
     :
     : "%eax", "%ebx", "%ecx");

 return ret;
}
