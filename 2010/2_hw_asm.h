//2010 0.1 Copyright (C) Michael Niedermayer 1998
#ifndef n2_hw_asm_h
#define n2_hw_asm_h

void save_selectors(void);
void real_int(short interrupt, __dpmi_regs *);

#endif
