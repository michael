;2010 0.1 Copyright (C) Michael Niedermayer 1998

%include "2_all_a.asm"

segment .data

extern _mmx
extern esp_save   
extern _asm_temp  
extern _asm_temp2 

global _nagraCorr__Fiiiii

%define buf1    par2
%define buf2    par3
%define bufSize par4
%define best    par5
%define line    par6


_nagraCorr__Fiiiii:
 push eax
 push ebx

 push ecx
 push edx

 push esi
 push edi

 push ebp
 mov esi, [buf1]

 mov edx, [bufSize]
 mov edi, [buf2]

 and edx, 0xFFFFFFF8

 sub edx, byte 8

 add esi, edx
 add edi, edx

 xor edx, byte -1
 mov eax, [_mmx]

 inc edx
 mov ebx, [best]

 mov [_asm_temp], ebx
 mov ebx, [line]

 mov [_asm_temp2], ebx

 mov [esp_save], esp
 mov esp, edx

 test eax, eax
  jz NEAR NoMMX

 pxor mm3, mm3

 movq mm0, [esp + esi]
 movq mm5, mm3

 movq mm1, [esp + edi]
 movq mm2, mm0

 movq mm6, [mmx_clip]

align16
MMXloop:
;int3
 psubusb mm2, mm1            ; 3p01wMM2rMM1MM2 FD0  
 psubusb mm1, mm0            ; 3p01wMM1rMM0MM1 FD1  
 por mm2, mm1                ; 3p01wMM2rMM1MM2 FD2  
 movq mm0, [esp + esi + 8]   ; 5p2 wMM0rESPESI F D0  
 paddusb mm2, mm6            ; 3p01wMM2rMM6MM2 fF D0 
 movq mm4, mm2               ; 3p01wMM4rMM2     F D1
 punpcklbw mm2, mm3          ; 3p1 wMM2rMM3MM2  F D2
 punpckhbw mm4, mm3          ; 3p1 wMM4rMM3MM4  F  D0
 paddusw mm5, mm2            ; 3p01wMM5rMM2MM5  F  D1
 paddusw mm5, mm4            ; 3p01wMM5rMM4MM5  fF  D0
 movq mm2, mm0               ; 3p01wMM2rMM0      F  D1
 movq mm1, [esp + edi + 8]   ; 5p2 wMM1rESPEDI   F  D2
 add esp, byte 8             ; 3p01wESPrESP      F   D0
  jnc MMXloop                ; 2p1     rFLAG     F   D1
  ; 6 cyc deco
  ; 5- cyc rat
  ; 6 cyc exec (0/1 satur) 9 0 3 2 0 0
  ; 5 cyc ret
  ; -> 6 cyc

 psubusb mm2, mm1            
 psubusb mm1, mm0            

 por mm2, mm1

 paddusb mm2, mm6

 movq mm4, mm2

 punpcklbw mm2, mm3
 punpckhbw mm4, mm3          

 paddusw mm5, mm2
 paddusw mm5, mm4

 movq mm0, mm5
 psrlq mm5, 32

 paddusw mm5, mm0

 movq mm0, mm5
 psrlq mm5, 16

 paddusw mm5, mm0

 movd ebp, mm5

 mov ebx, [_asm_temp]
 and ebp, 0xFFFF

 jmp EndMMX       
NoMMX:

 xor ebp, ebp           
 xor edx, edx           
 xor ecx, ecx           

 mov eax, [esp + esi]
 mov ebx, [esp + edi]
 or eax, 0x80008000        

NoMMXloop:            
 sub edx, ecx                ; U
 sub eax, ebx                ;  V 1 
 and edx, 0x00FF00FF         ; U
 add ebp, edx                ;  V 1 
 mov ebx, eax                ; U
 and eax, 0xFF00FF00         ;  V 1
 shr eax, 8                  ; U
 mov ecx, [esp + esi + 4]    ;  V 1
 xor eax, 0x00800080         ; U
 or  ecx, 0x80008000         ;  V 1
 xor ebx, eax                ; U
 mov edx, [esp + edi + 4]    ;  V 1
 sub ebx, eax                ; U
 sub ecx, edx                ;  V 1
 and ebx, 0x00FF00FF         ; U
 mov edx, ecx                ;  V 1
 add ebp, ebx                ; U 
 and ecx, 0xFF00FF00         ;  V 1
 shr ecx, 8                  ; U
 mov eax, [esp + esi + 8]    ;  V 1
 xor ecx, 0x00800080         ; U
 or  eax, 0x80008000         ;  V 1
 xor edx, ecx                ; U
 mov ebx, [esp + edi + 8]    ;  V 1
 add esp, 8                  ; U
  jnc NoMMXloop              ;  V 1


 mov ebx, [_asm_temp]        ; U    line_inf[line]
 mov eax, ebp                ;  V 1
 shr eax, 16                 ; U
 and ebp, 0x0000FFFF         ;  V 1
 add ebp, eax                ; U

EndMMX: 

 mov eax, [ebx + 12]         ; U | V 1
 cmp ebp, eax                ; U
  jg End                     ;  V 1

 mov eax, [ebx + 8 ]         ; U
 mov edx, [_asm_temp2]       ;  V 1
 cmp ebp, eax                ; U
  jg JustLast                ;  V 1

 mov ecx, [ebx     ]         ; U
 mov [ebx + 12], eax         ;  V 1
 mov [ebx + 4 ], ecx         ; U
 mov [ebx     ], edx         ;  V 1
 mov [ebx + 8 ], ebp         ; U  1

 jmp End                     ; U  1
JustLast:
 
 mov [ebx + 4 ], edx         ; U
 mov [ebx + 12], ebp         ;  V 1

End:  
 
 mov esp, [esp_save]

 pop ebp
 pop edi
 pop esi
 pop edx
 pop ecx
 pop ebx
 pop eax

ret

xyz times 8 dd 0

align8
mmx_clip times 8 db 0

align16
