//2010 0.1 Copyright (C) Michael Niedermayer 1998

#include <stdio.h>
#include <dos.h>
#include <time.h>
#include "2_all.h"
#include "2010.h"
#include "2_file.h"
#include "2_hw.h"
#include "2_hw_mem.h"
#include "2_gfunc.h"
#include <jpeglib.h>

 extern int vgax, vgay, wndx, wndy, outy;
 extern int fysize;
 extern byte font[4096];
 extern VID2MEMBUF *actVid2MemBufp;
 extern int g_mode;
 extern int yuvMode;
 extern char *yuv2rgb_lut;
extern int infoPosX, infoPosY;

 static byte *ppbRawDat[64];
 static int nRawDats=0;

void GrabBmp(void){
 int i, j;
 FILE *fg;
 char grab_name[16];
 struct time ti;
 bmp bmpi;
 byte fwbuf[8];

 gettime(&ti);
 sprintf(grab_name, "G%02d%02d%02d.BMP", ti.ti_hour, ti.ti_min, ti.ti_sec);

 if((fg=fopen(grab_name, "ab"))==NULL && g_mode==0) error(FileWrite);

 bmpi.magic='B'+('M'<<8);
 bmpi.lenght=wndx * wndy * 3 + 54;
 bmpi.zero1=0;
 bmpi.datstart=54;
 bmpi.palstart=40; //??
 bmpi.xres=wndx;
 bmpi.yres=wndy;
 bmpi.planes=1;
 bmpi.bits=24;
 bmpi.comp=0;
 bmpi.size=wndx * wndy * 3;
 bmpi.xpel=0;
 bmpi.ypel=0;
 bmpi.clruse=0;
 bmpi.clrimp=0;

 fwrite(&bmpi, sizeof(bmpi), 1, fg);
 for(i=wndy-1; i>=0; i--){
   if(yuvMode==0){
     for(j=0; j<wndx; j++){
/*       const int b=  (actVid2MemBufp->b[ (j + i*vgax)<<1   ] & 0x1F) << 3;
       const int g= ((actVid2MemBufp->b[ (j + i*vgax)<<1   ] & 0xE0) >> 3)
                   +((actVid2MemBufp->b[((j + i*vgax)<<1)+1] & 0x07) << 5);
       const int r=   actVid2MemBufp->b[((j + i*vgax)<<1)+1] & 0xF8;
*/
       const int b=  (actVid2MemBufp->b[ (j + i*vgax)<<1   ] & 0x1F) << 3;
       const int g= ((actVid2MemBufp->b[ (j + i*vgax)<<1   ] & 0xE0) >> 2)
                   +((actVid2MemBufp->b[((j + i*vgax)<<1)+1] & 0x03) << 6);
       const int r=  (actVid2MemBufp->b[((j + i*vgax)<<1)+1] & 0x7C) << 1;

       const int bgr= b + (g<<8) + (r<<16);

       fwrite(&bgr, 1, 3, fg);
     }
   }
   else{
     for(j=0; j<wndx; j+=2){
       const int u = char(actVid2MemBufp->b[ ((j + i*vgax)<<1)     ]);
       const int y1=      actVid2MemBufp->b[ ((j + i*vgax)<<1) + 1 ];
       const int v = char(actVid2MemBufp->b[ ((j + i*vgax)<<1) + 2 ]);
       const int y2=      actVid2MemBufp->b[ ((j + i*vgax)<<1) + 3 ];
       color c[2];
       c[0].init(y1, u, v, 0);
       c[1].init(y2, u, v, 0);
       byte bgr2[6]={ c[0].col.b, c[0].col.g, c[0].col.r,
                      c[1].col.b, c[1].col.g, c[1].col.r };
       fwrite(bgr2, 1, 6, fg);
     }
   } 
 }

 fclose(fg);

}

void GrabRaw(void){
 FILE *fg;
 char grab_name[16];
 struct time ti;

 static int iCounter= ti.ti_hour;

 gettime(&ti);
 sprintf(grab_name, "G%02d%02d%02d.RAW", iCounter%100, ti.ti_min, ti.ti_sec);
 iCounter++;

 if((fg=fopen(grab_name, "ab"))==NULL && g_mode==0) error(FileWrite);

 fwrite(actVid2MemBufp->b, 1, vgax*outy*2, fg);

 fclose(fg);
}


void LoadRaws(int nRaws, char **ppcName){

    nRawDats= nRaws;

    FILE *pFRaw;

    for(int i=0; i<nRaws; i++)
        {
        if((pFRaw=fopen(ppcName[i], "rb"))==NULL) error(FileRead);
        const int iFileSize= fsize(pFRaw);
        ppbRawDat[i]= new byte[iFileSize];
        fread(ppbRawDat[i], 1, iFileSize, pFRaw);

        fclose(pFRaw);
        }
}

void ShowRaw(void){

    if(!nRawDats) return;

    static int i=0;

    i++;
    i%=nRawDats;
    memcpy(actVid2MemBufp->b, ppbRawDat[i], vgax*outy*2);
}
/*
static byte *jpegStorage= null;
static int nJpegs=0;
static byte **apJpeg= null;
static int *aJpegSizes= null;
static int jpegStorageSize=0;
*/
void RecordVideo(void)
{
	if(yuvMode==0) return;
	long T0= uclock();
	
	static FILE * outFile= NULL;		
 	if(outFile == NULL)
	{
		char fileName[16];
 		struct time ti;

 		gettime(&ti);
 		sprintf(fileName, "V%02d%02d%02d.jpg", ti.ti_hour, ti.ti_min, ti.ti_sec);
		
		outFile = fopen(fileName, "wb");
		if (outFile == NULL) 
		{
    		fprintf(stderr, "can't open %s\n", fileName);
    		exit(1);
  		}
		
	}
	
	int image_width= wndx/2;
	int image_height= wndy/2;
	int quality= 90;
	
	static byte * image_buffer= new byte[wndx*wndy*3];
	byte * pYUV= &actVid2MemBufp->b[ 0 ];
	byte * pYCbCr= &image_buffer[0];
	
	for(int y=0; y<(wndy>>1); y++)
	{
		for(int x=0; x<(wndx>>1); x++)
		{
			pYCbCr[0] =        (pYUV[1] + pYUV[3] + pYUV[(vgax<<1) + 1] + pYUV[(vgax<<1)+3] ) >> 2;
			pYCbCr[1] = ((char)(pYUV[0]           + pYUV[(vgax<<1)    ]                     ) >> 1) + 127;
			pYCbCr[2] = ((char)(pYUV[2]           + pYUV[(vgax<<1) + 2]                     ) >> 1) + 127;
			pYCbCr+=3;
			pYUV+=4;
		}
		pYUV+=(vgax*2 - wndx)*2;
	}			

	static struct jpeg_compress_struct cinfo;
	
	static struct jpeg_error_mgr jerr;

	JSAMPROW row_pointer[1];	/* pointer to JSAMPLE row[s] */
  	const int row_stride= image_width * 3;;		/* physical row width in image buffer */

	static bool isInit=true;
	if(isInit)
	{
		isInit=false;
		cinfo.err = jpeg_std_error(&jerr);

		/* Now we can initialize the JPEG compression object. */
  		jpeg_create_compress(&cinfo);
		
	  	jpeg_stdio_dest(&cinfo, outFile);
	}
	
   	cinfo.image_width = image_width; 	/* image width and height, in pixels */
  	cinfo.image_height = image_height;
  	cinfo.input_components = 3;		/* # of color components per pixel */
  	cinfo.in_color_space = JCS_YCbCr; 	/* colorspace of input image */
  	jpeg_set_defaults(&cinfo);
	
	cinfo.dct_method= JDCT_ISLOW; // JDCT_IFAST is 30% faster
	cinfo.optimize_coding= TRUE; // almost no differenc in speed
	
  	jpeg_set_quality(&cinfo, quality, TRUE /* limit to baseline-JPEG values */);

   	jpeg_start_compress(&cinfo, TRUE);

  	while (cinfo.next_scanline < cinfo.image_height) {
    	row_pointer[0] = & image_buffer[cinfo.next_scanline * row_stride];
    	(void) jpeg_write_scanlines(&cinfo, row_pointer, 1);
  	}

 
  	jpeg_finish_compress(&cinfo);

//   	jpeg_destroy_compress(&cinfo);

	long T1= uclock();
	char textBuffer[20];
	sprintf(textBuffer, "Recording %f", float(T1-T0)/float(UCLOCKS_PER_SEC));
	
	color c;
	c.init(255, 0, 0, false);
   	
	gprint(infoPosX, infoPosY+=10,  c.col, textBuffer);
	
}



long fsize(FILE *f){
 long i,j;
 i=ftell(f);                                  //get pos
 fseek(f,0,SEEK_END);                  //find end
 j=ftell(f);
 fseek(f,i,SEEK_SET);                         //restore pos
 return j;
}
