;2010 0.1 Copyright (C) Michael Niedermayer 1999

%include "2_all_a.asm"

segment .data

extern esp_save

global _CopyYuv2Rgb1__Fiiii
global _CopyYuv2Rgb2__Fiiii

%define pYuv     par2
%define pRgb     par3
%define nOp      par4
%define pLut     par5
%define nbX      par6

_CopyYuv2Rgb1__Fiiii:
 push eax
 push ebx
 push ecx
 push edx
 push esi
 push edi
 push ebp

 mov [esp_save], esp

 mov esi, [pYuv]
 mov edi, [pRgb]
 mov ecx, [nOp]
 mov esp, [pLut]
 add esi, ecx
 add edi, ecx
 xor ecx, byte -1
 inc ecx

 movq mm6, [uMask]
 movq mm7, [vMask]
align16
CopyLoop1:
 movq mm0, [esi+ecx]               ;4p2 wMM0rESIECX  1    0 3
; movq mm1, [esi+ecx]               ;4p2 wMM1rESIECX  10   1 4 // Hotspot ?2%
; movq mm2, [esi+ecx]               ;4p2 wMM2rESIECX  10   2 5
movq mm1, mm0
movq mm2, mm0

 psrlw mm0, 11                     ;4p1 wMM0rMM0     0    1
 ;iFetch
 pand mm1, mm6                     ;3p01wMM1rMM6MM1  1    2
 pand mm2, mm7                     ;3p01wMM2rMM7MM2  1    3
 pslld mm1, 2                      ;4p1 wMM1rMM1     0    3

 psrld mm2, 9                      ;4p1 wMM2rMM2     0    4
 ;iFetch
 por mm1, mm2                      ;3p01wMM1rMM2MM1  0    5
 movq mm2, mm1                     ;3p01wMM2rMM1     0
 pslld mm1, 16                     ;4p1 wMM1rMM1     0

 por mm0, mm2                      ;3p01wMM0rMM2MM0  0
 por mm0, mm1                      ;3p01wMM0rMM1MM0 ?0
 ;iFetch
 movd eax, mm0                     ;3p01wEAXrMM0     0
 movzx ebx, ax                     ;3p01wEBXrEAX     0
 shr eax, 16                       ;3p0 wEAXrEAX     0

 movzx eax, word [esp + eax*2]     ;4p2 wEAXrESPEAX  10
 shl eax, 16                       ;3p0 wEAXrEAX     0
 ;iFetch
 movzx ebx, word [esp + ebx*2]     ;4p2 wEBXrESPEBX  1
 psrlq mm0, 32                     ;4p1 wMM0rMM0     0
 or eax, ebx                       ;2p01wEAXrEBXEAX  0

 mov [edi+ecx], eax                ;3p3     rEDIECX  2    0   p4     rEAX 0
 movd eax, mm0                     ;3p01wMM0rMM1MM0 ?00
 ;iFetch
 movzx ebx, ax                     ;3p01wEBXrEAX     0
 shr eax, 16                       ;3p0 wEAXrEAX     0
 movzx ebx, word [esp + ebx*2]     ;4p2 wEBXrESPEBX  1

 movzx eax, word [esp + eax*2]     ;4p2 wEAXrESPEAX  10
 ;iFetch
 shl eax, 16                       ;3p0 wEAXrEAX     0
 or eax, ebx                       ;2p01wEAXrEBXEAX  0

 mov [edi+ecx+4], eax              ;3p3     rEDIECX  2    1   p4     rEAX 0
 add ecx, byte 8                   ;3p01wECXrECX     110  0
  jnc CopyLoop1                    ;2p1     rFLAG         0
 ; 13 4 6 7 2 2  12+ cyc 
 ; fetch/decode 14 cyc




; movzx eax, byte [esi+ecx  ]       ;p2 wEAXrESIECX
; movzx ebp, byte [esi+ecx+1]       ;p2 wEBPrESIECX
; movzx edx, byte [esi+ecx+2]       ;p2 wEDXrESIECX
; movzx ebx, byte [esi+ecx+3]       ;p2 wEBXrESIECX
; and edx, 0xF8                     ;p01wEDXrEDX
; and eax, 0xF8                     ;p01wEAXrEAX
; shr ebp, 3                        ;p0 wEBPrEBP
; shr ebx, 3                        ;p0 wEBXrEBX
; shl edx, 7                        ;p0 wEDXrEDX
; lea eax, [edx + eax*4]            ;p0 wEAXrEDXEAX
; or ebp, eax                       ;p01wEBPrEAXEBP
; or ebx, eax                       ;p01wEBXrEAXEBX 
; movzx ebp, word [esp + ebp*2]     ;p2 wEBPrESPEBP
; movzx ebx, word [esp + ebx*2]     ;p2 wEBXrESPEBX
; shl ebx, 16                       ;p0 wEBXrEBX    
; or ebp, ebx                       ;p01wEBPrEBXEBP
; mov [edi+ecx], ebp                ;p3     rEDIECX     p4     rEBP
; add ecx, byte 4                   ;p01wECXrECX
;  jnc NEAR CopyLoop1               ;p1     rFLAG
 ; 6 5 1 6 1 1    7+ cyc asy

 mov esp, [esp_save]

 pop ebp
 pop edi
 pop esi
 pop edx
 pop ecx
 pop ebx
 pop eax
ret

_CopyYuv2Rgb2__Fiiii:
 push eax
 push ebx
 push ecx
 push edx
 push esi
 push edi
 push ebp

 mov [esp_save], esp

 mov esi, [pYuv]
 mov edi, [pRgb]
 mov ecx, [nOp]
 mov esp, [pLut]
 add esi, ecx
 add edi, ecx
 xor ecx, byte -1
 inc ecx

 movq mm7, [antiSigned]
 movq mm6, [bit0Mask]
 movq mm5, [bit20Mask]

align16
CopyLoop2:
 movq mm0, [esi+ecx-1600]
 movq mm1, [esi+ecx+1600]
 paddb mm0, mm7
 paddb mm1, mm7
 pand mm0, mm6
 pand mm1, mm6
 psrlw mm0, 1
 psrlw mm1, 1
 paddb mm0, mm1
 psubb mm0, mm7
 pand mm0, mm5
 movd ebx, mm0
 psrlq mm0, 32

 movzx eax, bl
 movzx ebp, bh
 shr ebx, 16
 movzx edx, bl
 movzx ebx, bh

 shr ebp, 3
 shr ebx, 3
 shl edx, 7
 lea eax, [edx + eax*4]
 or ebp, eax                           
 or ebx, eax                           

 movzx ebp, word [esp + ebp*2]
 movzx ebx, word [esp + ebx*2]       
 shl ebx, 16                           
 or ebp, ebx

 mov [edi+ecx], ebp


 movd ebx, mm0

 movzx eax, bl
 movzx ebp, bh
 shr ebx, 16
 movzx edx, bl
 movzx ebx, bh

 shr ebp, 3
 shr ebx, 3
 shl edx, 7
 lea eax, [edx + eax*4]
 or ebp, eax                           
 or ebx, eax                           

 movzx ebp, word [esp + ebp*2]
 movzx ebx, word [esp + ebx*2]       
 shl ebx, 16                           
 or ebp, ebx

 mov [edi+ecx+4], ebp

 add ecx, byte 8
  jnc NEAR CopyLoop2                       

 mov esp, [esp_save]

 pop ebp
 pop edi
 pop esi
 pop edx
 pop ecx
 pop ebx
 pop eax
ret

xyz times 8 dd 0

align8
antiSigned times 2 dd 0x00800080
bit0Mask times 2 dd 0xFEFEFEFE
bit20Mask times 2 dd 0xF8F8F8F8
uMask times 2 dd 0x000000F8
vMask times 2 dd 0x00F80000

align16
