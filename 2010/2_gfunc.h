//2010 0.1 Copyright (C) Michael Niedermayer 1998
#ifndef n2_gfunc_h
#define n2_gfunc_h

void rect(int xs, int ys, int xe, int ye, const COL c);
void gprint(int xp, int yp, const COL c, char *text);
void xcliped_gprint(int xp, int yp, const COL c, int text);

#endif
