;2010 0.1 Copyright (C) Michael Niedermayer 1999

%include "2_all_a.asm"

segment .data 

global _VSmooth1RGB1555__Fiii
global _VSmooth2RGB1555__Fiii
global _Decomb1__Fiii
global _Decomb2__Fiii
global _SetDecombInc__Fi

%define pBuf1    par1
%define iVgaX21  par2
%define nOp1     par3

%define pBuf2    par2
%define iVgaX22  par3
%define nOp2     par4

%define pYuv3    par1
%define nOp3     par2
%define pBuf3    par3

_SetDecombInc__Fi:
 push eax

 mov eax, [par1 - 5*4]
 mov [decombInc], eax
 mov [decombInc+4], eax
 pop eax

ret

_Decomb1__Fiii:
 push eax
 push ebx

 push ecx
 push edx

 push esi
 push edi

 mov esi, [pYuv3]
 mov edi, [pBuf3]
 mov ecx, [nOp3]
 add esi, ecx
 add edi, ecx
 xor ecx, byte -1
 inc ecx

 movq mm7, [decombInc]
align16
Decomb1Loop:
 movq mm0, [esi + ecx]
 psllw mm0, 8
 movq mm2, [edi + ecx]
 psubsw mm0, mm2
 pcmpgtw mm2, mm0
 pand mm2, mm7
 paddsw mm0, mm2
 movq [edi + ecx], mm0
 add ecx, byte 8
  jnc Decomb1Loop

 pop edi
 pop esi

 pop edx
 pop ecx

 pop ebx
 pop eax

ret

_Decomb2__Fiii:
 push eax
 push ebx

 push ecx
 push edx

 push esi
 push edi

 mov esi, [pYuv3]
 mov edi, [pBuf3]
 mov ecx, [nOp3]
 add esi, ecx
 add edi, ecx
 xor ecx, byte -1
 inc ecx

; int3
 movq mm6, [decombYMask]
 movq mm7, [decombInc]
align16
Decomb2Loop:
 movq mm0, [esi + ecx]           ;4 p2  MM0 ESIECX   F 0
 movq mm1, mm0                   ;3 p01 MM1 MM0      F 1        
 pand mm1, mm6                   ;3 p01 MM1 MM6MM1   F 2
 psllw mm0, 8                    ;4 p1  MM0 MM0      F  0
 movq mm2, [edi + ecx]           ;4 p2  MM2 EDIECX   f 0
 psubsw mm0, mm2                 ;3 p01 MM0 MM2MM0   f 1
 pcmpgtw mm2, mm0                ;3 p01 MM2 MM0MM2   f 2
 pand mm2, mm7                   ;3 p01 MM2 MM7MM2   f  0
 paddsw mm0, mm2                 ;3 p01 MM0 MM2MM0   f  1
 movq [edi + ecx], mm0           ;4 p3      EDIECX   F 0
                                 ;0 p4      MM0      F 0
 psrlw mm0, 7                    ;4 p1  MM0 MM0      F 1
 por mm1, mm0                    ;3 p01 MM1 MM0MM1   F 2
 movq [esi + ecx], mm1           ;4 p3      ESIECX   F  0
                                 ;0 p4      MM1      F  0
 add ecx, byte 8                 ;3 p01 ECX ECX      f 0
  jnc Decomb2Loop                ;2 p1      FLAG     f 1

 ; 8 0 3 2 2 2 ->
 ; exe 5.5 
 ; RAT 5.6...
 ; Ret 6
 ; Deco 7 




 pop edi
 pop esi

 pop edx
 pop ecx

 pop ebx
 pop eax

ret


_VSmooth1RGB1555__Fiii:
 push eax
 push ebx

 push ecx
 push edx

 push esi
 push edi

 mov esi, [pBuf1]
 add esi, byte 8

 mov ecx, [nOp1]
 add esi, ecx

 xor ecx, byte -1
 mov ebx, [iVgaX21]

 inc ecx
 mov edi, ebx

 add ebx, esi
 add edi, edi

 add edi, esi

 movq mm6, [mask1]
 movq mm7, [mask2]

 movq mm0, [esi+ecx]
 movq mm1, [ebx+ecx]
 movq mm3, [esi+ecx-8]


 ; more opt possible (+2reg -1psrlw)
align16
MMXLoop1:
 movq mm5, mm0
 movq mm4, mm1

 movq [esi+ecx-8], mm3
 pand mm0, mm7

 movq mm2, [edi+ecx]
 pand mm1, mm6

 movq mm3, mm2
 pand mm2, mm6

 pand mm5, mm6
 pand mm4, mm7

 pand mm3, mm7
 paddw mm5, mm2

 paddw mm3, mm0
 psrlw mm5, 1

 psrlw mm3, 1
 paddw mm5, mm1

 paddw mm3, mm4
 psrlw mm5, 1

 psrlw mm3, 1
 pand mm5, mm6

 movq mm0, [esi+ecx+8]
 pand mm3, mm7

 movq mm1, [ebx+ecx+8]
 por mm3, mm5

 add ecx, byte 8
  jnc MMXLoop1

 pop edi
 pop esi

 pop edx
 pop ecx

 pop ebx
 pop eax

ret

_VSmooth2RGB1555__Fiii:
 push eax
 push ebx

 push ecx
 push edx

 push esi
 push edi

 push ebp

 mov esi, [pBuf2]

 add esi, byte 8
 mov ebp, [nOp2]

 xor ebp, byte -1
 mov ebx, [iVgaX22]

 add esi, ebx
 mov edx, ebx

 mov eax, ebx
 add edx, edx

 inc ebp
 mov edi, ebx

 add ebx, esi
 add edi, edi

 mov ecx, eax
 add edi, esi

 xor ecx, byte -1

 inc ecx

 movq mm6, [mask1]

 movq mm7, [mask2]

 ; lots of optimize possible 
align16
MMXLoop2:
 ;AGI
 movq mm0, [esi+ecx]

 movq mm1, [edi+ecx]
 movq mm4, mm0

 movq mm5, mm1
 pand mm0, mm7

 pand mm4, mm6
 pand mm5, mm6

 pand mm1, mm7
 paddw mm4, mm5

 paddw mm0, mm1
 psrlw mm4, 1

 psrlw mm0, 1
 pand mm4, mm6

 pand mm0, mm7

 por mm4, mm0

 movq [ebx+ecx], mm4

 add ecx, 8
  jnc MMXLoop2

 sub ecx, eax
 add ebx, edx

 add esi, edx
 add edi, edx

 add ebp, edx
  jnc MMXLoop2

 pop ebp

 pop edi
 pop esi

 pop edx
 pop ecx

 pop ebx
 pop eax

ret

xyz times 8 dd 0

align8
mask1 times 2 dd 0x7C1F03E0
mask2 times 2 dd 0x03E07C1F
decombInc   times 2 dd 0x01000100
decombYMask times 2 dd 0xFF00FF00

align16
