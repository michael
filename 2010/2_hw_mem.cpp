//2010 0.1 Copyright (C) Michael Niedermayer 1998

#include <stdlib.h>
#include <stdio.h>
#include <go32.h>
#include <dpmi.h>
#include <string.h>
#include <sys/nearptr.h>
#include <sys/farptr.h>
#include <math.h>
#include "2_all.h"
#include "2_hw_mem.h"
#include "2010.h"

 extern volatile VID2MEMBUF *vid2MemBuf, actVid2MemBufp;
 extern volatile int actVid2MemBuf, grabVid2MemBuf;
 extern volatile LockList  *locklist;
 extern volatile AllocList *alloclist;
 extern volatile VdsList *vdslist;
 extern volatile MapList *maplist;

bool checkAlign(void *p, int align){        
  if(int(p) & (align-1)) return false;
  else                   return true;
}

void *newAlign(int size, int align){
  int v= int(new char[size + align + 3]);
#ifdef debug
  printf("anew %d\n", v);
#endif
  int v2= ( (v+align-1+4) & ~(align-1) );
  ((int*)v2)[-1]= v;
  return (void*)v2;
}

void deleteAlign(void *v){        
#ifdef debug
  printf("adel %d\n", int(((char**)v)[-1]) );
#endif
  delete [] ((char**)v)[-1];
}

void locklin(void *start, void *end){
 volatile LockList *llp, *llp2;
 __dpmi_meminfo meminfo;

 llp=new LockList;
 llp2=locklist;
 while(llp2!=NULL && llp2->next!=NULL) llp2=llp2->next;

 meminfo.address=(unsigned long)(start);
 meminfo.size=(unsigned long)(int(end) - int(start));
 if(__dpmi_lock_linear_region(&meminfo)!=0) error(Lock);

 if(llp2!=NULL) llp2->next=llp;
 else locklist=llp2=llp;
 llp->start=start;
 llp->end=end;
 llp->next=NULL;
}

void unlocklin(void *start, void *end){
 volatile LockList *llp, *llp2;
 __dpmi_meminfo meminfo;

 llp=locklist;
 while(llp!=NULL && (llp->start!=start || llp->end!=end)) llp=llp->next;
 if(llp==NULL){
   while(llp!=NULL && ((llp->start!=start && llp->end!=end)
                    || llp->start>start  || llp->end<end)) llp=llp->next;
 }
 if(llp==NULL) error(error_code(-8));

 meminfo.address=(u_long)(start);
 meminfo.size=(u_long)(int(end) - int(start));
 __dpmi_unlock_linear_region(&meminfo);

 if(llp->start<start){
   llp->end=start;
 }else if(llp->end>end){
   llp->start=end;
 }else{
   llp2=locklist;
   while(llp2!=NULL && llp2->next!=llp) llp2=llp2->next;
   if(llp2!=NULL){
     llp2->next=llp->next;
   }else{
     locklist=llp->next;
   }
   delete llp;
 }
}

void lock(void *start, void *end){
 locklin((char*)start + __djgpp_base_address, (char*)end + __djgpp_base_address);
}

void unlock(void *start, void *end){
 unlocklin((char*)start + __djgpp_base_address, (char*)end + __djgpp_base_address);
}

void unlockall(void){
 volatile LockList *llp, *llp2;
 __dpmi_meminfo meminfo;

 llp=locklist;
 while(llp!=NULL){
   meminfo.address=(u_long)(llp->start);
   meminfo.size=(u_long)(int(llp->end) - int(llp->start));
   __dpmi_unlock_linear_region(&meminfo);
   llp2=llp;
   llp=llp->next;
   delete llp2;
 }
}

void dpmi_addlinlock(volatile AllocList *old, volatile AllocList **al){
 volatile AllocList *llp, *llp2;

 llp=new AllocList;
 llp2=*al;
 while(llp2!=NULL && llp2->next!=NULL && llp2->next->phys<old->phys) llp2=llp2->next;

 if(*al!=NULL && (*al)->phys>old->phys) llp2=NULL;

 *llp=*old;
 if(llp2==NULL){
   llp->next=*al;
   *al=llp;
 }else{
   llp->next=llp2->next;  
   llp2->next=llp;
 }
}

u_long dpmi_alloclinlock(volatile AllocList **al){
 volatile AllocList *llp, *llp2;
 __dpmi_meminfo meminfo;
 u_long phys;

 meminfo.size=4096;
 if(__dpmi_allocate_memory(&meminfo)==-1) return 0;
 if(__dpmi_lock_linear_region(&meminfo)==-1){
   __dpmi_free_memory(meminfo.handle);
   return 0;
 }

 if((meminfo.address & 4095) != 0) error(error_code(-90));

 phys=getphys(meminfo.address);

 llp=(AllocList *)(meminfo.address - __djgpp_base_address);
 llp2=*al;
 while(llp2!=NULL && llp2->next!=NULL && llp2->next->phys<phys) llp2=llp2->next;

 if(*al!=NULL && (*al)->phys>phys) llp2=NULL;

 if(llp2==NULL){
   llp->next=*al;
   *al=llp;
 }else{
   llp->next=llp2->next;
   llp2->next=llp;
 }
 llp->addr=meminfo.address;
 llp->hand=meminfo.handle;
 llp->phys=phys;

 return meminfo.address;
}

void dpmi_freelinlock(volatile AllocList *old, volatile AllocList **al){
 volatile AllocList *llp2;
 __dpmi_meminfo meminfo;


 meminfo.address=old->addr;
 meminfo.size=4096;
 meminfo.handle=old->hand;

 llp2=*al;
 while(llp2!=NULL && llp2->next!=old) llp2=llp2->next;
 if(llp2!=NULL){
   llp2->next=old->next;
 }else{
   *al=old->next;
 }

 if((u_long)old + __djgpp_base_address != meminfo.address) delete old;

 if(meminfo.handle!=0){
   __dpmi_unlock_linear_region(&meminfo);
   __dpmi_free_memory(meminfo.handle);
 }

}

void dpmi_freeall(volatile AllocList **al){
 volatile AllocList *llp, *llp2;
 __dpmi_meminfo meminfo;

 llp=*al;
 while(llp!=NULL){
   meminfo.address=llp->addr;
   meminfo.size=4096;
   meminfo.handle=llp->hand;
   llp2=llp;
   llp=llp->next;
   if((u_long)llp2 + __djgpp_base_address != meminfo.address) delete llp2;
   if(meminfo.handle!=0){
     __dpmi_unlock_linear_region(&meminfo);
     __dpmi_free_memory(meminfo.handle);
   }
 }
 *al=NULL;
}


void allocCont(VID2MEMBUF *v2mb, int size, int *num){
 u_long addr;

 size= (size + 4095) & (~4095);     //round up to next 4096 boundray

 checkVDS();

 
 addr=(u_long)map(2048*1024, 2048*1024 + 1024*1024);  //check if mapping of RAM is ok
 unmap((void*)addr);

 dds vds_info;
 int left=*num;
 for(int tryi=left; tryi>0; tryi=min(tryi-1, left) ){
   for(int slack=0; ; slack+=4096){
     vds_info= vds_alloc(size*tryi + slack);

     if(vds_info.phys==0xFFFFFFFF){
       break;
     }else{
       printf("AllocatedContVDS %d, at Address %lX\n", size*tryi + slack , vds_info.phys);
       fflush(stdout);

       const int j=~(1024*1024*4-1);

       int i;
       for(i=0; i<tryi; i++){
         if( ((vds_info.phys + slack + size*i         ) & j)
          != ((vds_info.phys + slack + size*i + size-1) & j) ) break;
       }
       if(i==tryi){
         printf("Success!\n");
         fflush(stdout);

         for(int k=0; k<tryi; k++){
           v2mb[*num-left+k].b=
                     (byte*)( map(  vds_info.phys + slack + size*k
                                  , vds_info.phys + slack + size*k + size))  
                                                     - __djgpp_base_address; // FIX ?-1

           v2mb[*num-left+k].phys= vds_info.phys + size*k + slack;
         }

         left-=tryi;
         tryi++;

         break;
       }

       printf("4MB Boundray violation!\n");
       fflush(stdout);

       vds_free(&vds_info);

     }
   }
 }

 *num-=left;
 if(*num==0) error(MemAlloc);

 v2mb[0].num= *num;
 for(int i=0; i<*num; i++){
   v2mb[0].state= Empty;
 }

}
                                   
void checkVDS(void){               

 if((_farpeekb(_dos_ds, 0x47B) & 32) != 32) error(NoVDS);

}

u_long getphys(u_long addr){
__dpmi_regs r;
dds info;

 info.selector=0;
 info.size=4096;
 info.offset=addr;

 dosmemput(&info, sizeof(dds), __tb); // store in buffer

 r.x.ax=0x8103;
 r.x.dx=4;
 r.x.es = __tb >> 4;
 r.x.di = 0;
 __dpmi_int(0x4B, &r);
 if((r.x.flags & carry_flag) == carry_flag) error(VDS);
 dosmemget(__tb, sizeof(dds), &info);

 r.x.ax=0x8104;
 r.x.dx=0;
 r.x.es = __tb >> 4;
 r.x.di = 0;
 __dpmi_int(0x4B, &r);
 if((r.x.flags & carry_flag) == carry_flag) error(VDS);

 return info.phys;

}

dds vds_alloc(int size){
 dds vds_info;
 __dpmi_regs r;
 volatile VdsList *vd;

 vds_info.size=size;

 dosmemput(&vds_info, sizeof(dds), __tb); // store in buffer

 r.x.ax=0x8107;
 r.x.dx=0;
 r.x.es=__tb >> 4;
 r.x.di=0;

 __dpmi_int(0x4B, &r);
 if((r.x.flags & carry_flag) == carry_flag){
   vds_info.phys=0xFFFFFFFF;
   return vds_info;
 };
 dosmemget(__tb, sizeof(dds), &vds_info);

 vd=vdslist;
 while(vd!=NULL && vd->next!=NULL) vd=vd->next;
 if(vd==NULL){
   vdslist=vd=new VdsList;
 }else{
   vd->next=new VdsList;
   vd=vd->next;
 }

 vd->vds_info=vds_info;
 vd->next=NULL;

 return vds_info;
}

void vds_free(dds *vds_info){
 __dpmi_regs r;
 volatile VdsList *vd, *vd2;

 vd=vdslist;
 while(vd!=NULL && vd->next!=NULL && vd->next->vds_info.phys!=vds_info->phys)
   vd=vd->next;

 if(vd==vdslist && vd->vds_info.phys==vds_info->phys ){
   vdslist= vdslist->next;
   delete vd;
 }else if(vd!=vdslist && vd->next!=NULL){
   vd2= vd->next;
   vd->next= vd->next->next;
   delete vd2;
 }

 dosmemput(vds_info, sizeof(dds), __tb); // store in buffer
 r.x.ax=0x8108;
 r.x.dx=0;
 r.x.es=__tb >> 4;
 r.x.di=0;

 __dpmi_int(0x4B, &r);
}

void vds_freeall(void){
 __dpmi_regs r;
 volatile VdsList *vd, *vd2;

 vd=vdslist;
 while(vd!=NULL){
   dosmemput((dds*)(&vd->vds_info), sizeof(dds), __tb); // store in buffer
   r.x.ax=0x8108;
   r.x.dx=0;
   r.x.es=__tb >> 4;
   r.x.di=0;

   __dpmi_int(0x4B, &r);
   vd2=vd;
   vd=vd->next;
   delete vd2;
 }

 vdslist=NULL;
}

void *map(u_long start, u_long end){
 volatile MapList *llp, *llp2;
 __dpmi_meminfo meminfo;

 llp=new MapList;
 llp2=maplist;
 while(llp2!=NULL && llp2->next!=NULL) llp2=llp2->next;

 meminfo.address=start;
 meminfo.size=end - start;
 if(__dpmi_physical_address_mapping(&meminfo)!=0) error(Map);

 if(llp2!=NULL) llp2->next=llp;
 else maplist=llp2=llp;
 llp->start=(void*)meminfo.address;
 llp->next=NULL;
 return (void*)meminfo.address;
}

void unmap(void *start){
 volatile MapList *llp, *llp2;
 __dpmi_meminfo meminfo;

 llp=maplist;
 while(llp!=NULL && llp->start!=start) llp=llp->next;
 if(llp==NULL) error(error_code(-19));

 meminfo.address=(u_long)(start);
 __dpmi_free_physical_address_mapping(&meminfo);

 llp2=maplist;
 while(llp2!=NULL && llp2->next!=llp) llp2=llp2->next;
 if(llp2!=NULL){
   llp2->next=llp->next;
 }else{
   maplist=llp->next;
 }
 delete llp;
}

void unmapall(void){
 volatile MapList *llp, *llp2;
 __dpmi_meminfo meminfo;

 llp=maplist;
 while(llp!=NULL){
   meminfo.address=(u_long)(llp->start);
   __dpmi_free_physical_address_mapping(&meminfo);
   llp2=llp;
   llp=llp->next;
   delete llp2;
 }
}

int getFreeMemory(void)
{
	int h[3000];
 	int x;
   	__dpmi_meminfo dmi;
	
   	for(x=0; ; x++)
	{
    	dmi.size= 1024*1024;
        int e=__dpmi_allocate_memory(&dmi);
        if(e==-1) break;
        h[x]=dmi.handle;
	}
	

	for(int i = x-1; i>=0; i--)
    {
		__dpmi_free_memory(h[i]);
    }
	
	return x * 1024 * 1024 ;
}
