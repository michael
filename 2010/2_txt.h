//2010 0.1 Copyright (C) Michael Niedermayer 1998
#ifndef n2_txt_h
#define n2_txt_h

#define LINES 25

struct CACHE2{
  int page;
  int sub;
  bool seen;
  char text[LINES*40];
  CACHE2 *next;
  int land;
};

struct CACHE1{
  char chan[28];
  CACHE2 **pt;
};

struct MAG{
  CACHE2 *pt;
  int order;
  int land;
};

struct GETTAB{
  int page;
  char name[15];
  FILE *f;
  int clockDelta;
  int nextClock;
};


void txt_decrypt(void);

#endif
