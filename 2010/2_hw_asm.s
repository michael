//2010 0.1 Copyright (C) Michael Niedermayer 1998

 _lock_start:

.globl _lock_start
.globl _meteor_handler
.globl _lock_end
.globl _save_selectors__Fv
.globl _in_int
.globl _page_flip
.globl _vid2MemBuf
.globl _actVid2MemBufp
.globl _actVid2MemBuf
.globl _grabVid2MemBuf
.globl _vidbuf
.globl _vgax
.globl _xresvb
.globl _vb
.globl _mc
.globl _xresc
.globl _vgay
.globl _yuvMode
.globl _grabYuvMode
.globl _locklist
.globl _alloclist
.globl _vdslist
.globl _maplist
.globl _meteors
.globl _old_intmet_vect
.globl _old_intmet_vect_offset
.globl _old_intmet_vect_selector
.globl _metmode
.globl _active_meteor
.globl _latency
.globl _addr_errors
.globl _corr_errors
.globl _frames
.globl _fields
.globl _saa7196_buf
.globl _TVStd
.globl _x_field
.globl _y_field
.globl _oneField
.globl _stride
.globl _single
.globl _cryptStd
.globl _wndx
.globl _wndy
.globl _outy
.globl _mmx
.globl _my_cs
.globl _my_ds

.globl _real_int__FsP11__dpmi_regs
.globl _real_regs

.align 4

_in_int: .long 0
_page_flip: .long 0
_actVid2MemBufp: .long 0
_actVid2MemBuf: .long 0
_grabVid2MemBuf: .long 0
_vid2MemBuf: .long 0
.space 64, 0
_vidbuf: .long 0
_vgax: .long 0
_vb: .long 0
_mc: .long 0
_xresvb: .long 0
_xresc: .long 0
_vgay: .long 0
_yuvMode: .long 0
_grabYuvMode: .long 0
_locklist: .long 0
_alloclist: .long 0
_vdslist: .long 0
_maplist: .long 0
_old_intmet_vect:
_old_intmet_vect_offset: .long 0
_old_intmet_vect_selector: .word 0
.align 4
_metmode: .long 0
_active_meteor: .long 0
_latency: .long 32
_addr_errors: .long 0
_corr_errors: .long 0
_frames: .long 0
_fields: .long 0
_saa7196_buf: .space 52, 0
_TVStd: .long 0
_x_field: .long 0
_y_field: .long 0
_oneField: .long 0
_stride: .long 0
_single: .long 0
_cryptStd: .long 0
_wndx: .long 0
_wndy: .long 0
_outy: .long 0
_mmx: .long 1
_my_cs: .word 0
_my_ds: .word 0

_meteors: .space 0x80, 0

_ds: .word 0
_es: .word 0
_fs: .word 0
_gs: .word 0


.align 4
_real_regs:
_r_di: .long 0
_r_si: .long 0
_r_bp: .long 0
_r_sp2: .long 0
_r_bx: .long 0
_r_dx: .long 0
_r_cx: .long 0
_r_ax: .long 0
_r_fl: .word 0
_r_es: .word 0
_r_ds: .word 0
_r_fs: .word 0
_r_gs: .word 0
_r_ip: .word 0
_r_cs: .word 0
_r_sp: .word 0
_r_ss: .word 0
       

.align 4
_save_selectors__Fv:
 movw %ds, _ds
 movw %es, _es
 movw %fs, _fs
 movw %gs, _gs
ret

   // int num
.align 4
_real_int__FsP11__dpmi_regs:

 pushal
 pushw %es

 movw 38(%esp), %bx
 movl 42(%esp), %edi

 movw %ds, %ax
 movw %ax, %es

 movw $0 , (_r_ss - _real_regs)(%edi)
 movw $0 , (_r_sp - _real_regs)(%edi)
 movl $0 , (_r_sp2 - _real_regs)(%edi)
 pushfw
 popw (_r_fl - _real_regs)(%edi)

 xorw %cx, %cx
 movw $0x0300, %ax
 int $0x31

 popw %es
 popal
ret

.align 4
_meteor_handler:
// int $3

 pushal
 pushw %ds
 pushw %es
 pushw %fs
 pushw %gs

// movw $0x00B7, %ax
// movw     %ax, %ds              //HACK FIX BUG GRRRR
// movw     %ax, %es              //HACK FIX BUG GRRRR
// movw     %ax, %fs              //HACK FIX BUG GRRRR
// movw     %ax, %gs              //HACK FIX BUG GRRRR
 movw %cs:_ds, %ds
 movw     _ds, %es
 movw     _ds, %fs
 movw     _ds, %gs     // all ds fix hack

 call _meteor_int_handler__Fv

 popw %gs
 popw %fs
 popw %es
 popw %ds

// int $3

 movb $0x20, %al
 outb %al, $0xA0
 outb %al, $0x20
 //   FIX  this QUICK HACK (? if irq<8)
 popal
sti
iret

_lock_end:
