; 2010 0.1 Copyright (C) Michael Niedermayer 1998

%define par_5 (esp + 04)
%define par_4 (esp + 08)
%define par_3 (esp + 12)
%define par_2 (esp + 16)
%define par_1 (esp + 20)
%define par0  (esp + 24)
%define par1  (esp + 28)
%define par2  (esp + 32)
%define par3  (esp + 36)
%define par4  (esp + 40)
%define par5  (esp + 44)
%define par6  (esp + 48)
%define par7  (esp + 52)
%define par8  (esp + 56)

;%define int3 db 0xCC

%define lt(a,b) ( ((a)-(b)) >>31)
%define gt(a,b) ( ((b)-(a)) >>31)
%define le(a,b) ( ((a)-(b)-1) >>31)
%define ge(a,b) ( ((b)-(a)-1) >>31)
%define eq(a,b) (ge(a,b) & le(a,b))


%define align2  times ($$-$) & 1  nop
;%define align4  times ($$-$) & 3  nop
;%define align8  times ($$-$) & 7  nop
;%define align16 times ($$-$) & 15 nop

%macro align4 0
 times ge(($$-$)&3, 3) cmp ebx, byte 0
 times ge(($$-$)&3, 2) cmp ebx, ebx
 times ge(($$-$)&3, 1) nop
%endmacro

; lea 4 byte and 7 byte possible

%macro align8 0
 times ge(($$-$)&7, 6) cmp ebx, 0
 times ge(($$-$)&7, 5) cmp eax, 0
 times ge(($$-$)&7, 3) cmp ebx, byte 0
 times ge(($$-$)&7, 2) cmp ebx, ebx
 times ge(($$-$)&7, 1) nop
%endmacro

%macro align16 0
 times eq(($$-$)&15, 10) cmp eax, 0
 times ge(($$-$)&15, 6) cmp ebx, 0
 times ge(($$-$)&15, 6) cmp ebx, 0
 times ge(($$-$)&15, 5) cmp eax, 0
 times ge(($$-$)&15, 3) cmp ebx, byte 0
 times ge(($$-$)&15, 2) cmp ebx, ebx
 times ge(($$-$)&15, 1) nop
%endmacro
