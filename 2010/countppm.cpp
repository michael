#include <stdio.h>
#include <stdlib.h>

#include <string.h>

int main(int argc, char **argv)
{
	if(argc<2) 
	{
		printf("Command line Error!\n");
		exit(1);
	}
	
	FILE * inFile= fopen(argv[1], "rb");
	if(inFile==NULL) 
	{
		printf("Cant open inFile!\n");
		exit(1);
	}
	
	const int bufferSize= 1024*1024;
	unsigned char *buffer= new unsigned char[bufferSize];
	
	int xBuffer= bufferSize;
	
	for(int i=0; ; i++)
	{
		memmove(buffer, &buffer[xBuffer], bufferSize - xBuffer);
		memset(&buffer[bufferSize - xBuffer], 0, xBuffer);
		
		fread(&buffer[bufferSize - xBuffer], xBuffer, 1, inFile);
		
		for(xBuffer=1; ; xBuffer++)
		{
			if(xBuffer+20>bufferSize) exit(1);									// can happen if jpeg too large or at end (HACK) 
			
			if( 	buffer[xBuffer     ] == 0xFF
				&&	buffer[xBuffer +  2] == 0xFF
				&&	buffer[xBuffer +  6] == 'J'
				&&	buffer[xBuffer +  7] == 'F'
				&&	buffer[xBuffer +  8] == 'I'
				&&	buffer[xBuffer +  9] == 'F'
				&&	buffer[xBuffer + 10] == 0)
			{
				break;
			}
		}
	
		printf("%d\r", i);
		fflush(stdout);
	}	
}		
	
