//2010 0.1 Copyright (C) Michael Niedermayer 1998
#ifndef n2_vc_h
#define n2_vc_h

void vc_decrypt(void);
void saveVCCache(void);

struct CutPCache{
    byte *pbCCutP;
    CutPCache *pNext;
    CutPCache *pPrev;
    byte score;
	byte isDeFinal;
	byte reserved1;
	byte reserved2;
    int nUsed;
    int nbCCutP;
};

struct CutPLut{
    CutPCache *pCutPCache;
    CutPLut *pNext;
};

#endif
