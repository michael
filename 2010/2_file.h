//2010 0.1 Copyright (C) Michael Niedermayer 1998
#ifndef n2_file_h
#define n2_file_h

struct bmp{
  word  magic    __attribute__ ((packed)) ;
  dword lenght   __attribute__ ((packed)) ;
  dword zero1    __attribute__ ((packed)) ;
  dword datstart __attribute__ ((packed)) ;
  dword palstart __attribute__ ((packed)) ;
  dword xres     __attribute__ ((packed)) ;
  dword yres     __attribute__ ((packed)) ;
  word  planes   __attribute__ ((packed)) ;
  word  bits     __attribute__ ((packed)) ;
  dword comp     __attribute__ ((packed)) ;
  dword size     __attribute__ ((packed)) ;
  dword xpel     __attribute__ ((packed)) ;
  dword ypel     __attribute__ ((packed)) ;
  dword clruse   __attribute__ ((packed)) ;
  dword clrimp   __attribute__ ((packed)) ;
};

void GrabBmp(void);
long fsize(FILE *f);
void LoadRaws(int, char **);
void GrabRaw(void);
void ShowRaw(void);
void RecordVideo(void);

#endif
