//2010 0.1 Copyright (C) Michael Niedermayer 1998

#include <stdio.h>
#include <time.h>
#include <string.h>
#include "2_all.h"
#include "2_grafix.h"
#include "2010.h"
#include "2_gfunc.h"
#include "2_hw.h"
#include "2_hw_mem.h"
#include "2_file.h"
#include "2_crypt.h"

#include <dpmi.h> // remove

 extern int xres, yres, xresc;
 extern int fysize;
 extern byte font[4096];
 extern int grabf;
 extern int yuvMode;
 extern volatile int addr_errors, corr_errors;
 extern int single;
 extern int bright, contr, satur;
 extern MENULEVEL menuLevel;
 extern int infoPosX;
 extern int infoPosY;
 extern VID2MEMBUF *vid2MemBuf;
 extern int some, some2;
 extern bool allowDrop;

 bool helpState=true;
 int iState=0;
 int showCont=0;

void showStuff(void){
 char textbuf[256];
 color c;

 if(helpState){
   char text[15][256]={ "F1/F2 Brightness     s TVStandart",
                        "F3/F4 Contrast       i debugInfo",
                        "F5/F6 Saturation     h Help", 
                        "g     ScreenShot     G SaveRaw",
                        "v     smoothIterlace p Pause",
                        "d     allowDrop      q Quit",
                        "S     ShowRaw        r Record", 
                        "            TELETXT:", 
                        "+     Page+      -     Page-",
                        "right subPage+   left  subPage-",
                        "up    Channel+   down  Channel-",
                        "c txt Channel    a num Page",
                        "g     savePage   G     saveAll",
                        "e     delUnSeen",
                        "f txt find       F     findNext"};

   for(int i=0; i<15; i++){
     c.init(255, 0, 0, false);
     gprint(infoPosX, infoPosY+=10, c.col, text[i]);
   }
   infoPosY+=5;
 }

 if(showCont!=0){
   showCont--;
   sprintf(textbuf,"%3.1f%% brightness", bright/2.55);
   c.init(255, 0, 0, false);

   gprint(infoPosX, infoPosY+=10, c.col, textbuf);
   sprintf(textbuf,"%3.1f%% contrast"  , contr/1.27);
   gprint(infoPosX, infoPosY+=10, c.col, textbuf);
   sprintf(textbuf,"%3.1f%% saturation", satur/1.27);
   gprint(infoPosX, infoPosY+=10, c.col, textbuf);
   infoPosY+=5;
 }

 if(menuLevel==mLTVStd){
#ifndef CRYPT
   char text[4][256]={ "1. PAL   4. PAL-TeleText",
                       "2. NTSC",
                       "3. SECAM",
                       ""};
#else
   char text[4][256]={ "1. PAL   4. PAL-TeleText",
                       "2. NTSC  5. VideoCrypt (PAL)",
                       "3. SECAM 6. NagraVision (PAL/secam)",
                       "7. "};
#endif
   for(int i=0; i<4; i++){
     c.init(255, 0, 0, false);
     gprint(infoPosX, infoPosY+=10, c.col, text[i]);
   }
   infoPosY+=5;
 }

 if(iState){
   c.init(255, 0, 0, false);


   static long T[10];
   for(int i=8; i>=0; i--)
   		{
		T[i+1] = T[i];
		}
   T[0] = uclock();

   sprintf(textbuf,"%2.2f Fps",1/((float)(T[0]-T[9])/(UCLOCKS_PER_SEC*9)));
   gprint(infoPosX, infoPosY+=10, c.col, textbuf);

   sprintf(textbuf,"%d Buf", vid2MemBuf[0].num);
   gprint(infoPosX+110, infoPosY   , c.col, textbuf);

   if(yuvMode==0) sprintf(textbuf,"RGB16");
   else           sprintf(textbuf,"YUV422");
   gprint(infoPosX+170, infoPosY   , c.col, textbuf);

   if(allowDrop){
     sprintf(textbuf,"allowDrop");
     gprint(infoPosX+230, infoPosY   , c.col, textbuf);
   }

   sprintf(textbuf,"%d Corrupted Fields", corr_errors);
   gprint(infoPosX, infoPosY+=10, c.col, textbuf);

   sprintf(textbuf,"%d Address error's", addr_errors);
   gprint(infoPosX, infoPosY+=10, c.col, textbuf);

   sprintf(textbuf,"%d Some", some);
   gprint(infoPosX, infoPosY+=10, c.col, textbuf);
   sprintf(textbuf,"%d Some2", some2);
   gprint(infoPosX, infoPosY+=10, c.col, textbuf);
   infoPosY+=5;
 }
 if(iState==2){
   c.init(255, 0, 0, false);
 
   int nMbLeft= getFreeMemory() / (1024 * 1024);

   sprintf(textbuf,"%dMb left", nMbLeft);
   gprint(infoPosX, infoPosY+=10, c.col, textbuf);
   infoPosY+=5;
 }

}

