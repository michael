//2010 0.1 Copyright (C) Michael Niedermayer 1998
#ifndef n2_71x6
#define n2_71x6

#define SAA7116_vendev 0x12238086

#define SAA7196_I2C_W 0x40
#define SAA7196_I2C_R 0x41

#define SAA7196_regs 49

enum TVSTD{
 PAL,
 TXTPAL,
 NTSC,
 SECAM,
};

enum CRYPTSTD{
 nix,
 vc,
 nag,
 vpaytv
};

/*
 pci_cmd;   //04-05
 pci_sts;   //06-07
 rev;       //08
 classc1;   //09
 classc2;   //0A-0B
 res0C;     //0C
 mlt;       //0D
 headt;     //0E
 bist;      //0F
*/

struct meteor{
 volatile struct saa7116 *saa7116;   
 byte   bus;                
 byte   dev;                
 byte   irq;                
 byte   intt;
 u_long maxRange;
};

struct eo {
  u_long even;
  u_long odd;
};

struct eo3 {
  u_long even[3];
  u_long odd[3];
};

struct eo4 {
  u_long even[4];
  u_long odd[4];
};

struct rt_mode {
  unsigned mode:8;		/* 0:7  Pixel format for even fields */
  unsigned route:24;		/* 8:31 Pixel router mode for even fields */
};

struct ff_trig {
  unsigned packed:7;            /* 0:6 Fifo trigger Packed Mode */
  unsigned :9;                  /* 7:15 Unused */
  unsigned planar:7;            /* 16:22 Fifo trigger Planar Mode */
  unsigned :9;			/* 23:31 Unused */
};

struct capt_c {
  unsigned cont_even:1;         /* 0  Capture Continous Even Fields */
  unsigned cont_odd :1;         /* 1  Capture Continous Odd  Fields */
  unsigned sing_even:1;         /* 2  Capture Single    Even Fields */
  unsigned sing_odd :1;         /* 3  Capture Single    Odd  Fields */
  unsigned done_even:1;         /* 4  Even Field Done */
  unsigned done_odd :1;         /* 5  Odd  Field Done */
  unsigned VRSTN    :1;         /* 6  ? */
  unsigned fifo_en  :1;         /* 7  FIFO Enable */
  unsigned corr_even:1;         /* 8  Even Field Corrupt */
  unsigned corr_odd :1;         /* 9  Odd  Field Corrupt */
  unsigned aerr_even:1;         /* 10 Even Field Address Error */
  unsigned aerr_odd :1;         /* 11 Odd  Field Address Error */
  unsigned res1:2;              /* 12:13 Unused */
  unsigned corr_dis :1;         /* 14 Corrupt Disable */
  unsigned range_en :1;         /* 15 Range Enabler */
  unsigned res2:16;             /* 16:31 Unused */
};

struct ints_c {
  unsigned stat_even :1;         /* 0  Interrupt Status Even     Fields */
  unsigned stat_odd  :1;         /* 1  Interrupt Status Odd      Fields */
  unsigned stat_start:1;         /* 2  Interrupt Status start of Fields */
  unsigned res1:5;               /* 3:7 Unused */
  unsigned mask_even :1;         /* 8  Interrupt Mask   Even     Fields */
  unsigned mask_odd  :1;         /* 9  Interrupt Mask   Odd      Fields */
  unsigned mask_start:1;         /* 10 Interrupt Mask   start of Fields */
  unsigned res2:21;              /* 11:31 Unused */
};

struct i2c_stat {
  unsigned AutoEnable:1;        /* 0:0 AutoEnable */
  unsigned bypass:1;            /* 1:1 bypass */
  unsigned SDAOut:1;            /* 2:2 SDA Output */
  unsigned SCLOut:1;            /* 3:3 SCL Output */
  unsigned :4;                  /* 4:7 ? */
  unsigned AutoAbort:1;         /* 8:8 Auto Abort */
  unsigned d_abort:1;           /* 9:9 Status of i2c write: 0==success  realy auto abort*/
  unsigned SDAIn:1;             /*10:10 SDA Input */
  unsigned SCLIn:1;             /*11:11 SCL Input */
  unsigned :4;                  /*12:15 ? */
  unsigned AutoAddr:8;          /*16:23 Auto Address */
  unsigned r_data:8;            /*24:31 Data returned by i2c read */
};

struct i2c_cmd {
  unsigned w_data:8;		/* 0:7 Data to be written out */
  unsigned reg_addr:8;		/* 8:15 Register address in device space */
  unsigned dev_addr:8;		/* 16:23 16: 0==write; 17:23: Device address */
  unsigned xfer:1;		/* 24: Set to start write. Clears when done */
  unsigned :7;			/* 25:31 Unused */
};

#define v_struct volatile struct
#define v_u_long volatile u_long
#define v_union  volatile union

				/* (pointer-addition-values)hex-offset(s) */
struct saa7116 {
  v_struct eo3 dma;               /* ( 0-5 )00-14: Base address, e/o fields */
  v_struct eo3 stride;            /* ( 6-11)18-2c: Address stride, e/o fields */
  v_struct rt_mode rt_mode_e;     /* (12)30: Route and Mode for even fields */
  v_struct rt_mode rt_mode_o;     /* (13)34: Route and Mode for odd fields */
  v_struct ff_trig fifo_trigger;  /* (14)38 Fifo triggers: Planar and Packed */
  v_u_long field_toggle;          /* (15)3c  bit 8=1 1=1 0=1 2=field_toggle */
  v_union{
    v_u_long capt_ctl_a;
    v_struct capt_c capt_ctl;
  };                            /* (16)40: Capture control, etc. */
  v_u_long retry_wait;            /* (17)44 */
  v_union{
    v_u_long ints_ctl_a;
    v_struct ints_c ints_ctl;
  };                            /* (18)48: Interrupt control and status */
  v_struct eo field_mask;         /* (19-20)4c-50 */
  v_u_long mask_lengths;          /* (21)54 */
  v_u_long fifo_limits;           /* (22)58 */
  v_u_long i2c_clocks;            /* (23)5c */
  v_union{
    v_u_long i2c_stat_a;
    v_struct i2c_stat i2c_stat;   
  };                            /* (24)60 i2c mode control and status */
  v_union{
    v_u_long i2c_cmd_a;
    v_struct i2c_cmd i2c_cmd;     
  };                            /* (25)64 Direct mode command and data */
  v_struct eo4 i2c_auto;          /* (26-33)68-84*/
  v_u_long i2c_regs_enable;       /* (34)88 */
  v_struct eo dma_end;            /* (35-36)8c-90 */
};

#define SAA7196_IDEL	0x00	/* Increment delay */
#define SAA7196_HSB5	0x01	/* H-sync begin; 50 hz */
#define SAA7196_HSS5	0x02	/* H-sync stop; 50 hz */
#define SAA7196_HCB5	0x03	/* H-clamp begin; 50 hz */
#define SAA7196_HCS5	0x04	/* H-clamp stop; 50 hz */
#define SAA7196_HSP5	0x05	/* H-sync after PHI1; 50 hz */
#define SAA7196_LUMC	0x06	/* Luminance control */
#define SAA7196_HUEC	0x07	/* Hue control */
#define SAA7196_CKTQ	0x08	/* Colour Killer Threshold QAM (PAL, NTSC) */
#define SAA7196_CKTS	0x09	/* Colour Killer Threshold SECAM */
#define SAA7196_PALS	0x0a	/* PAL switch sensitivity */
#define SAA7196_SECAMS	0x0b	/* SECAM switch sensitivity */
#define SAA7196_CGAINC	0x0c	/* Chroma gain control */
#define SAA7196_STDC	0x0d	/* Standard/Mode control */
#define SAA7196_IOCC	0x0e	/* I/O and Clock Control */
#define SAA7196_CTRL1	0x0f	/* Control #1 */
#define SAA7196_CTRL2	0x10	/* Control #2 */
#define SAA7196_CGAINR	0x11	/* Chroma Gain Reference */
#define SAA7196_CSAT	0x12	/* Chroma Saturation */
#define SAA7196_CONT	0x13	/* Luminance Contrast */
#define SAA7196_HSB6	0x14	/* H-sync begin; 60 hz */
#define SAA7196_HSS6	0x15	/* H-sync stop; 60 hz */
#define SAA7196_HCB6	0x16	/* H-clamp begin; 60 hz */
#define SAA7196_HCS6	0x17	/* H-clamp stop; 60 hz */
#define SAA7196_HSP6	0x18	/* H-sync after PHI1; 60 hz */
#define SAA7196_BRIG	0x19	/* Luminance Brightness */
#define SAA7196_FMTS	0x20	/* Formats and sequence */
#define SAA7196_OUTPIX	0x21	/* Output data pixel/line */
#define SAA7196_INPIX	0x22	/* Input data pixel/line */
#define SAA7196_HWS	0x23	/* Horiz. window start */
#define SAA7196_HFILT	0x24	/* Horiz. filter */
#define SAA7196_OUTLINE	0x25	/* Output data lines/field */
#define SAA7196_INLINE	0x26	/* Input data lines/field */
#define SAA7196_VWS	0x27	/* Vertical window start */
#define SAA7196_VYP	0x28	/* AFS/vertical Y processing */
#define SAA7196_VBS	0x29	/* Vertical Bypass start */
#define SAA7196_VBCNT	0x2a	/* Vertical Bypass count */
#define SAA7196_VBP	0x2b	/* veritcal Bypass Polarity */
#define SAA7196_VLOW	0x2c	/* Colour-keying lower V limit */
#define SAA7196_VHIGH	0x2d	/* Colour-keying upper V limit */
#define SAA7196_ULOW	0x2e	/* Colour-keying lower U limit */
#define SAA7196_UHIGH	0x2f	/* Colour-keying upper U limit */
#define SAA7196_DPATH	0x30	/* Data path setting  */


int scan_pci_meteors(void);
void init_meteor(void);
void close_meteor(void);
void meteor_int_handler(void);
void meteor_int_handler_end(void);
void contGrab(void);
void write_saa7196(u_char reg, u_char data);
u_char read_saa7196(u_char reg);
void setCont(void);
void setStdScale(void);
void grab_next(VID2MEMBUF v2mb);
void grab_nix();
void doTxtHack(int);
void vPayTvShiftHack(int);
void vPayTvBrightHack(bool, int);

#endif
