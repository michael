//2010 0.1 Copyright (C) Michael Niedermayer 1999

//FIX search multiply (use deltas)

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include "2_all.h"
#include "2_crypt.h"
#include "2_vc.h"
#include "2_nag.h"
#include "2_71x6.h"
#include "2_gfunc.h"
#include "2_mmx.h"
#include "2_nag_a.h"
#include "2_hw_mem.h"
#include "2010.h"

#define dwnSmp 3

#define SAVE_SEEDS

#define FREQ_CHROM     4433618.75
#define FREQ_HS        15625.0
#define PHASE_DRIFT_PER_LINE (-fmod(FREQ_CHROM/FREQ_HS*4.0, 1.0)/4.0*PI*2.0)

extern vgax, vgay, wndx, wndy, outy, y_field, x_field;
extern VID2MEMBUF *actVid2MemBufp;
extern volatile int scales_x, scalee_x;
extern volatile int scales_y, scalee_y;
extern u_long asm_temp, asm_temp4;
extern bool iState;
extern int yuvMode;
extern bool mmx;
extern int infoPosX;
extern int infoPosY;
extern int some;
extern bool drop;

static int *decoLine=NULL;
static int decoLines;
static int keyTable[256];
static int *best1=NULL;
static BEST2 *best2=NULL;           
static u_short *keysList=NULL;
static int outy1;
static int nagStart2;
static int nagEnd2;
static int nagStart1;
static int nagEnd1;
static int nagSize1;
static int nagSize2;
static int nagSizeAll;
static int rawWssThreshold=10000;

// fix inlines 

static int qsortHelper(const void *x0, const void *x1){
  SINFO *sInfo0 = (SINFO*)(x0);
  SINFO *sInfo1 = (SINFO*)(x1);
  int r;
  r=sInfo0->line0 - sInfo1->line0;
  if(r!=0) return r;
  r=sInfo0->line1 - sInfo1->line1;
  if(r!=0) return r;
  return sInfo0->key - sInfo1->key;
}

static void getPerm(const int key, int * const cle2enc){
  int keyNdx= (key >> 7) & 0xFF;
  const int keyInc= ((key & 0x7F) << 1) + 1;
  int buffer[32];

  for(int i=0; i<32; i++) buffer[i]= i;

  for(int i=0; i<255; i++){
    cle2enc[i]= buffer[ keyTable[keyNdx] ];
    buffer[ keyTable[keyNdx] ]= i + 32;
    keyNdx= (keyNdx + keyInc) & 0xFF;
  }

  for(int i=0; i<32; i++){
    cle2enc[i+255]= buffer[i];
  }
}

static inline void doDwnSmp(byte * const lowRes, byte * const highRes){

 if(mmx){
   asm(//"int $3 \n\t"
         "pushl %%ebp          \n\t"
         "cmpb 3f-1, %%cl      \n\t"
         " je 5f               \n\t"
         "movb %%cl, 3f-1      \n\t"
         "movl $1, %%eax       \n\t"
         "shll %%cl, %%eax     \n\t"
         "movl %%eax, 4f-4     \n\t"

         "5:                   \n\t"

         "1:                   \n\t"
         "movb (%%edi), %%al   \n\t"   // U        load 32 byte in L1-cache
         "xorl %%ebx, %%ebx    \n\t"   //  V 1
         "movl $0xFFFF, %%ebp  \n\t"   // U
         "4:                   \n\t"
         "xorl %%eax, %%eax    \n\t"   //  V 1
         "2:                   \n\t"
         "addl %%eax, %%ebx    \n\t"   // U
         "xorl %%eax, %%eax    \n\t"   //  V 1
         "movb (%%esi), %%al   \n\t"   // U
         "addl $2, %%esi       \n\t"   //  V 1
         "decl %%ebp           \n\t"   // U
         " jnz 2b              \n\t"   //  V 1
         "incl %%edi           \n\t"   // U
         "addl %%eax, %%ebx    \n\t"   //  V 1
         "shrl $50, %%ebx      \n\t"   // U  1
         "3:                   \n\t"
         "decl %%edx           \n\t"   //  V 1
         "movb %%bl, -1(%%edi) \n\t"   // U
         " jnz 1b              \n\t"   //  V 1
    
         "popl %%ebp            \n\t"
          : 
          : "c" (dwnSmp), 
            "S" (highRes + 1) ,
            "d" (wndx>>dwnSmp) ,
            "D" (lowRes) 
          : "%eax", "%ebx", "%ecx", "%edx", "%esi", "%edi");

 }
 else{
     asm(//"int $3 \n\t"
         "pushl %%ebp          \n\t"
         "cmpb 3f-1, %%cl      \n\t"
         " je 5f               \n\t"
         "movb %%cl, 3f-1      \n\t"
         "movl $1, %%eax       \n\t"
         "shll %%cl, %%eax     \n\t"
         "movl %%eax, 4f-4     \n\t"

         "5:                   \n\t"


         "1:                   \n\t"
         "movw (%%edi), %%ax   \n\t"   // U        load 32 byte in L1-cache
         "xorl %%ebx, %%ebx    \n\t"   //  V 1
         "movl $0xFFFF, %%ebp  \n\t"   // U
         "4:                   \n\t"
         "xorl %%eax, %%eax    \n\t"   //  V 1
         "2:                   \n\t"
         "addl %%eax, %%ebx    \n\t"   // U
         "xorl %%eax, %%eax    \n\t"   //  V 1
         "movb (%%esi), %%al   \n\t"   // U
         "addl $2, %%esi       \n\t"   //  V 1
         "decl %%ebp           \n\t"   // U
         " jnz 2b              \n\t"   //  V 1
         "addl $2, %%edi       \n\t"   // U
         "addl %%eax, %%ebx    \n\t"   //  V 1
         "shrl $50, %%ebx      \n\t"   // U  1
         "3:                   \n\t"
         "decl %%edx           \n\t"   //  V 1
         "movw %%bx, -2(%%edi) \n\t"   // U
         " jnz 1b              \n\t"   //  V 1
    
         "popl %%ebp            \n\t"
          : 
          : "c" (dwnSmp), 
            "S" (highRes + 1) ,
            "d" (wndx>>dwnSmp) ,
            "D" (lowRes) 
          : "%eax", "%ebx", "%ecx", "%edx", "%esi", "%edi");
 }




}

static inline int nagLineLogi2Phys(const int logi){
  int phys;
#ifdef CHECK
  if(logi>=287) error(-100);
#endif

  if(logi<32) phys= nagStart1 + ( logi    <<1);
  else        phys= nagStart2 + ((logi-32)<<1);
#ifdef CHECK
  if(phys>=576) error(-101);
#endif
  return phys;
}

static inline void corrV(byte * p1, byte * p2, const int type, int *vPhase){
  const bool xchg[4]={false, true, false, true};
  const int  neg [4]={    0,    0,    -1,   -1};

  if(xchg[type]){

    for(int x=0; x<(wndx<<1); x+=32){        // 32 FASTER until asm
      int v1= char(p1[x  ]);            
      int u1= char(p1[x+2]);

      const int u2= char(p2[x  ]);            
      const int v2= char(p2[x+2]);

      u1=(u1^neg[type]) - neg[type];
      v1=(v1^neg[type]) - neg[type];
   
      *vPhase+= mabs(u1-u2) + mabs(v1-v2);
      *vPhase-= mabs(u1+u2) + mabs(v1+v2);
    }
  }
  else{
    for(int x=0; x<(wndx<<1); x+=32){          // 32 FASTER until asm
      int u1= char(p1[x  ]);            
      int v1= char(p1[x+2]);

      const int u2= char(p2[x  ]);            
      const int v2= char(p2[x+2]);

      u1=(u1^neg[type]) - neg[type];
      v1=(v1^neg[type]) - neg[type];
   
      *vPhase+= mabs(u1-u2) + mabs(v1-v2);
      *vPhase-= mabs(u1+u2) + mabs(v1+v2);
    }
  }
}

void nag_decrypt(void){
 outy1= outy>>1;
 nagStart2= int(double(17+0   - scales_y)/y_field*outy1 + .5)*2+1;
 nagEnd2  = int(double(17+254 - scales_y)/y_field*outy1 + .5)*2+1;
 nagStart1= int(double(17+255 - scales_y)/y_field*outy1 + .5)*2+2;
 nagEnd1  = int(double(17+286 - scales_y)/y_field*outy1 + .5)*2+2;
 nagSize1= (nagEnd1 - nagStart1)/2 + 1;
 nagSize2= (nagEnd2 - nagStart2)/2 + 1;
 nagSizeAll= nagSize1 + nagSize2;
 char textbuf[10][128];
 color c;
 const int dwnSmpSize= mmx ? dwnSmp : dwnSmp-1;
 long T1, T2;
 T1=uclock();

 if(nagSizeAll!=NAG_LINES) error(Nagra);

#ifdef debug
 printf("%d %d\n", sizeof(u_short), sizeof(BEST2) );
#endif

 static bool first=true;

 static char *sinPerLineMulX=new char[(NAG_LINES+32)<<8];
 static char *cosPerLineMulX=new char[(NAG_LINES+32)<<8];
 static long long *sinPerLine=new long long[NAG_LINES+32];
 static long long *cosPerLine=new long long[NAG_LINES+32];

 static FILE *seedsFile=NULL;

 if(first){

#ifdef SAVE_SEEDS 	
 	seedsFile= fopen("nagSeeds.dat", "w");
	if(seedsFile==NULL) error(error_code(-987));
#endif 
 	
   decoLines=32;
   decoLine=new int[decoLines];
   decoLine[ 0]= 14;
   decoLine[ 1]= 33;
   decoLine[ 2]= 47;
   decoLine[ 3]= 64;
   decoLine[ 4]= 81;
   decoLine[ 5]= 84;
   decoLine[ 6]= 90;
   decoLine[ 7]= 95;
   decoLine[ 8]=103;
   decoLine[ 9]=106;
   decoLine[10]=110;
   decoLine[11]=115;
   decoLine[12]=121;
   decoLine[13]=125;
   decoLine[14]=127;
   decoLine[15]=136;
   decoLine[16]=142;
   decoLine[17]=145;
   decoLine[18]=149;
   decoLine[19]=153;
   decoLine[20]=161;
   decoLine[21]=165;
   decoLine[22]=172;
   decoLine[23]=176;
   decoLine[24]=180;
   decoLine[25]=185;
   decoLine[26]=189;
   decoLine[27]=195;
   decoLine[28]=211;
   decoLine[29]=227;
   decoLine[30]=243;
   decoLine[31]=259;

   FILE *f= fopen("key.txt", "r");
   if(f==NULL) error(KeyFile);
   for(int i=0; i<256; i++)
     if( fscanf(f, "%d", &keyTable[i])!=1 ) error(KeyFile);
   fclose(f);

   const float a=PHASE_DRIFT_PER_LINE;
   for(int l=0; l<NAG_LINES+32; l++){
     for(int v=0; v<256; v++){
       int r=v;
       if(v>127) r=v-256;
       sinPerLineMulX[v + (l<<8)]= char(sin(a*l)*r);
       cosPerLineMulX[v + (l<<8)]= char(cos(a*l)*r);
     }
     sinPerLine[l]= int(sin(a*l)*255) & 0xFFFF;
     cosPerLine[l]= int(cos(a*l)*255) & 0xFFFF;
   }


   f=fopen("nagra.dat", "rb");
   if(f!=NULL){
     int s=0;
     int decoLines2;
     fread(&decoLines2,   sizeof(int), 1, f);
     if(decoLines2!=decoLines) goto badFile;

     int decoLine2[NAG_LINES];
     fread(decoLine2,     sizeof(int), decoLines2, f);
     if( memcmp(decoLine, decoLine2, decoLines) ) goto badFile;

     int best2Size;
     int keysListSize;
     fread(&best2Size,          sizeof(int), 1, f);
     fread(&keysListSize,       sizeof(int), 1, f);

     best1=          new int[decoLines * NAG_LINES * 33];
     best2=          new BEST2[best2Size];
     keysList=       new u_short[keysListSize];

     fread(best1,          sizeof(int),      decoLines * NAG_LINES * 33, f);
     fread(best2,          sizeof(BEST2),    best2Size                 , f);
     fread(keysList,       sizeof(u_short),  keysListSize              , f);

     for(int i=0; i<decoLines * NAG_LINES * 33; i++) s+=best1[i];
     for(int i=0; i<best2Size                 ; i++) s+=best2[i].line;
     for(int i=0; i<keysListSize              ; i++) s+=keysList[i];

     badFile:;
       fclose(f);
       f=NULL;
   }

   if(best1==NULL){
     int best2Size=0;
     int keysListSize=0;
     int best2i=0;
     int keysListi=0;
     for(int phase=0; phase<2; phase++){
       if(phase==1){
         best1=    new int[NAG_LINES * decoLines * 33];
         for(int i=0; i<NAG_LINES * decoLines * 33; i++) best1[i]=-1;
         best2=    new BEST2[best2Size];
         keysList= new u_short[keysListSize];
       }
       for(int wssLine=0; wssLine<33; wssLine++){
         for(int dl=0; dl<decoLines; dl++){
           int sInfos=0;
           SINFO *sInfo= new SINFO[KEYS];
           int *cle2enc= new int[NAG_LINES];
           for(int key=0; key<KEYS; key++){
             int keyNdx= (key >> 7) & 0xFF;
//             const int keyInc= ((key & 0x7F) << 1) + 1;

             if(keyTable[keyNdx]!=wssLine && wssLine!=32) continue;
             getPerm(key, cle2enc);

             if(phase==0) keysListSize++;
             int clearLine;
             for(clearLine=0; clearLine<NAG_LINES; clearLine++){
               if(cle2enc[clearLine] == decoLine[dl]) break;
             }
             if(clearLine==NAG_LINES) asm("int $3\n\t");

             int line0= cle2enc[(clearLine-1<0)
                                       ? (clearLine+2) : (clearLine-1)];
             int line1= cle2enc[(clearLine+1>=NAG_LINES)
                                       ? (clearLine-2) : (clearLine+1)];

             if(line1<line0){
               const int t=line0;
               line0=line1;
               line1=t;
             }

             sInfo[sInfos]=(SINFO){line0: line0, line1: line1, key: key};
             sInfos++; 
           }
           delete [] sInfo;
           delete [] cle2enc;
           qsort(sInfo, sInfos, sizeof(SINFO), qsortHelper);

           if(phase==0){
             u_long last0=510;
             u_long last1=510;
             for(int i=0; i<sInfos; i++){
               if(last0!=sInfo[i].line0 || last1!=sInfo[i].line1){
                 last0=sInfo[i].line0;
                 last1=sInfo[i].line1;
                 best2Size++;
               }
             }
           }
           else{
             u_long last0=510;
             u_long last1=510;
             int best1i=-1;
             for(int sInfoi=0; sInfoi<sInfos; sInfoi++){
               if(sInfo[sInfoi].line0 != last0){
                 last0= sInfo[sInfoi].line0;
                 best1i= wssLine*NAG_LINES*decoLines + dl*NAG_LINES + last0;
                 best1[best1i]=best2i;                 
                 last1=510;
               }
               if(sInfo[sInfoi].line1 != last1){
                 last1= sInfo[sInfoi].line1;
                 best2[best2i]=(BEST2){line: last1, keyNdx: keysListi};
                 best2i++;
                 if(keysListi>0) keysList[keysListi-1]|=KEYS;
               }
               keysList[keysListi]=sInfo[sInfoi].key;
               keysListi++;
             }
             if(keysListi>0) keysList[keysListi-1]|=KEYS;
           }
         }
       }
     }
 
     for(int best1i=NAG_LINES*decoLines*33-1; best1i>=0; best1i--){
       if(best1[best1i]==-1) best1[best1i]= best2i;
       else best2i=best1[best1i];
     }




 /*



     bool *state= new bool[NAG_LINES * NAG_LINES * decoLines];
     int *keysTempStart= new int[NAG_LINES * NAG_LINES * decoLines];
     int *keysTemp= new int[KEYS * decoLines];
     int best2Size=0;

     for(int i=0; i<NAG_LINES * NAG_LINES * decoLines; i++){
       state[i]=false;
       keysTempStart[i]=-1;
     }


     for(int key=KEYS-1; key>=0; key--){
       int perm[NAG_LINES];
       getPerm(key, perm);
       for(int clearLine=0; clearLine<NAG_LINES; clearLine++){
         for(int dL=0; dL<decoLines; dL++){
           if(perm[clearLine]==decoLine[dL]){
             int goodLine[2]={
                clearLine>0           ? perm[clearLine-1] : perm[clearLine+2],
                clearLine<NAG_LINES-1 ? perm[clearLine+1] : perm[clearLine-2] };

             if(goodLine[1]<goodLine[0]){
               const int t= goodLine[0];
               goodLine[0]= goodLine[1];
               goodLine[1]= t;
             }

             const int ndx=
                   dL*NAG_LINES*NAG_LINES + goodLine[0]*NAG_LINES + goodLine[1];

             if(!state[ndx]) best2Size++;
             state[ndx]=true;
             keysTemp[key + dL*KEYS]= keysTempStart[ndx];
             keysTempStart[ndx]= key + dL*KEYS;

           }
         }
       }
     }

     // fix compression for keysList
     int keysListSize= KEYS*decoLines;

     best1= new int[NAG_LINES * decoLines * 33];
     best2= new BEST2[best2Size];
     keysList= new u_short[keysListSize];

     for(int i=0; i<NAG_LINES * decoLines * 33; i++){
       best1[i]=-1;
     }

     int best2Pos=0;
     int keysListPos=0;

     for(int dL=0; dL<decoLines; dL++){
       for(int line0=0; line0<NAG_LINES; line0++){
         const int ndx1= dL*NAG_LINES + line0;
         for(int line1=line0; line1<NAG_LINES; line1++){
           const int ndx2= dL*NAG_LINES*NAG_LINES + line0*NAG_LINES + line1;

           if(best1[ndx1]==-1) best1[ndx1]= best2Pos;

           if(keysTempStart[ndx2] == -1) continue;

           best2[best2Pos]= (BEST2){ line:line1, keyNdx:keysListPos };

//           printf("B2 %d %d\n", line1, keysListPos);


           int keyP=keysTempStart[ndx2];
           while(keyP!=-1){
             keysList[keysListPos]=keyP & (KEYS-1);
             keysListPos++;
//             printf("KL %d\n", keyP);
             keyP=keysTemp[keyP];
           }
           if(keysListPos>0) keysList[keysListPos-1]|=KEYS;
//           printf("KL END\n");

           best2Pos++;
         }
//         printf("B2 END\n");
       }
     }

     delete [] keysTemp;
     delete [] keysTempStart;
     delete [] state;

     */


     f=fopen("nagra.dat", "wb");
     if(f==NULL) error(DatWrite);

     fwrite(&decoLines,    sizeof(int), 1, f);

     fwrite(decoLine,      sizeof(int), decoLines, f);

     fwrite(&best2Size,          sizeof(int), 1, f);
     fwrite(&keysListSize,       sizeof(int), 1, f);

     fwrite(best1,          sizeof(int),      decoLines * NAG_LINES * 33, f);
     fwrite(best2,          sizeof(BEST2),    best2Size                 , f);
     fwrite(keysList,       sizeof(u_short),  keysListSize              , f);

     fclose(f);

   }
 }
 first=false;

 const int wssStartX=int(double(7   -scales_x+3)/x_field*wndx + .5);
 const int wssEndX=  int(double(164 -scales_x+3)/x_field*wndx + .5);
 const int wssLenX= wssEndX - wssStartX;

 static wssDat[max_x];

 static int lastVgaX=0;
 static wssThreshold=0;
 if(lastVgaX!=vgax){
   const byte wssRaw[53]= {1,1,1,1,1,  0,0,0,  1,1,1, 0,0,0,  1,1,1, 0,0,0,
                           1,1,1,  0,0,0,  1,1,1,  0,0,0,  1,1,1,1,  0,0,0,
                           1,1,1,1,  0,0,0,0,0,  1,1,1,1,1};
   const float delta= 53.0 / float(wssLenX);
   float pos=0;
   for(int i=wssStartX; i<wssEndX; i++){
     pos+= delta;
     wssDat[i]= wssRaw[int(pos)]*255;
   }

   wssThreshold= rawWssThreshold*wssLenX>>7;
   lastVgaX=vgax;
 }

 BESTCOEFFS *bestCoeffs= new BESTCOEFFS[decoLines];
 for(int i=0; i<decoLines; i++){
   bestCoeffs[i].coeff[0]=
   bestCoeffs[i].coeff[1]=0xFFFFF;
 }

 byte * const decoLowRes=(byte*) newAlign( (wndx>>dwnSmpSize) * decoLines, 8);
 for(int dL=0; dL<decoLines; dL++){
   byte * const linep= actVid2MemBufp->b + nagLineLogi2Phys(decoLine[dL])*(vgax<<1);
   byte * const lowResp= decoLowRes + dL*(wndx>>dwnSmpSize);
   doDwnSmp(lowResp ,linep);
 }

 if(iState){
   T2=uclock();
   sprintf(textbuf[0],"%f ReSample decoLines", (float)(T2-T1)/UCLOCKS_PER_SEC);
   T1=T2;
 }

 bool isWss;
 int wssLine=0;
 {
   int minSum=0x7FFFFFFF;
   for(int line=0; line<32; line++){
     int sum=0;
     byte * const linep= actVid2MemBufp->b + nagLineLogi2Phys(line)*(vgax<<1);
     for(int x=wssStartX; x<wssEndX; x++){
       sum+= mabs( int(linep[(x<<1) + 1]) - int(wssDat[x]) );
     }
     if(sum<minSum){
       minSum=sum;
       wssLine=line;
     }
   }
/*
   byte * const linep= actVid2MemBufp->b + nagLineLogi2Phys(wssLine)*(vgax<<1);
   for(int x=wssStartX; x<wssEndX; x++){
     linep[(x<<1) + 1]= (byte)(mabs( int(linep[(x<<1) + 1]) - int(wssDat[x]) ));
   }
 */
   
   if(minSum < wssThreshold){
     isWss=true;
     actVid2MemBufp->b[nagLineLogi2Phys(wssLine)*(vgax<<1) + (wssStartX<<1) + 3]=0;
     actVid2MemBufp->b[nagLineLogi2Phys(wssLine)*(vgax<<1) + (wssStartX<<1) + 5]=255;
   }
   else isWss=false;
 }
 if(!isWss) wssLine=32;

 byte * const lowRes=(byte*) newAlign( wndx>>dwnSmpSize, 8);
 for(int line=0; line<NAG_LINES; line++){       // vtune some btb misspredicts
   byte * const linep= actVid2MemBufp->b + nagLineLogi2Phys(line)*(vgax<<1);
//   printf("%d\n", line);
   doDwnSmp(lowRes, linep);
//   printf("xxX\n");
   byte *decoLowResp=decoLowRes;
   for(int dL=0; dL<decoLines; dL++){
     if(decoLine[dL]!=line)
       nagraCorr(int(lowRes), int(decoLowResp), wndx>>dwnSmpSize, int(&bestCoeffs[dL]), line);
     decoLowResp+= wndx>>dwnSmpSize;
   }
 }
 if(mmx) asm("emms\n\t");

 deleteAlign(decoLowRes);
 deleteAlign(lowRes);

 if(iState){
   T2=uclock();
   sprintf(textbuf[1],"%f Corr", (float)(T2-T1)/UCLOCKS_PER_SEC);
   T1=T2;
 }

 int *keysListPos= new int[decoLines];
 int keysListPoses= 0;
 for(int dL=0; dL<decoLines; dL++){
   if(bestCoeffs[dL].line[1] < bestCoeffs[dL].line[0]){
     const int t= bestCoeffs[dL].line[1];
     bestCoeffs[dL].line[1]= bestCoeffs[dL].line[0];
     bestCoeffs[dL].line[0]= t;
   }
#ifdef CHECK
   limit(bestCoeffs[dL].line[0], 0, 285);
#endif

   int l= best1[ bestCoeffs[dL].line[0]     + dL*NAG_LINES + wssLine*decoLines*NAG_LINES];
   int r= best1[ bestCoeffs[dL].line[0] + 1 + dL*NAG_LINES + wssLine*decoLines*NAG_LINES];
   if(l == r) continue;
   r--;

   while(l != r){
     if( int(best2[ (l+r)>>1 ].line) < bestCoeffs[dL].line[1] ) l= (l+r+2)>>1;
     else                                                       r= (l+r  )>>1;
   }

   if( int(best2[l].line) != bestCoeffs[dL].line[1] ) continue;

   keysListPos[keysListPoses]= best2[l].keyNdx;
   keysListPoses++;
 }

 if(iState){
   T2=uclock();
   sprintf(textbuf[2],"%f GenKeyPointers", (float)(T2-T1)/UCLOCKS_PER_SEC);
   T1=T2;
 }

 bool didSome;
 int lastLowKey=0xFFFF;
 int bestKey=0;
 int bestNum=0;
 int maxKeys=0;
 do{
   didSome=false;
   int lowKey=0xFFFF;
   int lowNum=0;
   for(int kLPi=0; kLPi<keysListPoses; kLPi++){

     if( (keysList[ keysListPos[kLPi] ] & (KEYS-1))==lastLowKey ){
       if( (keysList[ keysListPos[kLPi] ] & KEYS)==KEYS ){
         keysListPoses--;
         keysListPos[kLPi]= keysListPos[keysListPoses];
         continue;
       }
       keysListPos[kLPi]++;
       maxKeys++;
     }
                           
     if( (keysList[ keysListPos[kLPi] ] & (KEYS-1)) == lowKey ){
       lowNum++;
       if(lowNum>bestNum){
         bestNum=lowNum;
         bestKey=lowKey;
       }
     }
     else if( (keysList[ keysListPos[kLPi] ] & (KEYS-1)) < lowKey ){
       lowKey= keysList[ keysListPos[kLPi] ] & (KEYS-1);
       lowNum=0;
     }
     didSome= true;
   }
   lastLowKey=lowKey;
 }while(didSome);

 delete [] keysListPos;

 if(bestNum<2) drop=true;

 if(iState){
   T2=uclock();
   sprintf(textbuf[6],"%2d%6d%6s", bestNum, maxKeys, isWss ? "WSS" : "NoWSS");
   sprintf(textbuf[3],"%f FindKey", (float)(T2-T1)/UCLOCKS_PER_SEC);
   T1=T2;
 }

 int perm[NAG_LINES];
 getPerm(bestKey, perm);
#ifdef debug
 printf("%X\n", bestKey);
#endif

#ifdef SAVE_SEEDS
	if(drop)	fprintf(seedsFile, "FFFF\n"); 
	else 		fprintf(seedsFile, "%04X\n", bestKey);
#endif

 delete [] bestCoeffs;   // kill this


 byte *lastV[2]={NULL, NULL};
 int lastVType[2];
 int vPhase=0;
 for(int i=0; i<NAG_LINES; i++){
   if(perm[i]>=32 && i-perm[i]<=-16){          // fewer to test (FASTER)
     const cleMod4= (i      ) & 3;
     const encMod4= (perm[i]) & 3;
     const type= cleMod4 ^ encMod4;

     if(lastV[type&1^1] != NULL){
       byte *b=actVid2MemBufp->b + nagLineLogi2Phys( perm[i] ) * (vgax<<1);
       corrV( b, lastV[type&1^1], type ^ lastVType[type&1^1], &vPhase );
     }
     lastV[type&1]= actVid2MemBufp->b + nagLineLogi2Phys( perm[i] ) * (vgax<<1);
     lastVType[type&1]= type;
   }
 }
 if(vPhase>0) vPhase=1;
 else         vPhase=0;

 const bool xchg[4]={false, true, false, true};
 const int  neg [4]={    0,    0,    -1,   -1};

 char *pGood=(char*)(actVid2MemBufp->b);
 for(int line=NAG_LINES-1; line>0; line--){

   char *p= (char*)(actVid2MemBufp->b + nagLineLogi2Phys( perm[line] ) * (vgax<<1));

   if(perm[line]>=32){
     const cleMod4= (line       + vPhase ) & 3;
     const encMod4= (perm[line] + vPhase ) & 3;
     const type= cleMod4 ^ encMod4;

     const int l=(line-perm[line]+32)<<8;

     pGood= p;

     if(mmx){
       long long colorAsmDataTab[4];
       colorAsmDataTab[2]=0x00FF000000FF0000LL;
       colorAsmDataTab[3]=0xFF00FF00FF00FF00LL;

       if(xchg[type]){
         colorAsmDataTab[0]=
                        cosPerLine[l>>8]       + (sinPerLine[l>>8]<<16)
          +(            cosPerLine[l>>8] <<32) + (sinPerLine[l>>8]<<48);
         colorAsmDataTab[1]=
           ((0xFFFFLL - sinPerLine[l>>8])<<7 ) + (cosPerLine[l>>8]<<23)
          +((0xFFFFLL - sinPerLine[l>>8])<<39) + (cosPerLine[l>>8]<<55);
         if((perm[line] & 1) ^ vPhase){
           colorAsmDataTab[0]^=0xFFFF0000FFFF0000LL;
           colorAsmDataTab[1]^=0x0000FFFF0000FFFFLL;
         }
       }
       else{
         colorAsmDataTab[0]=
           ((0xFFFFLL - sinPerLine[l>>8])    ) + (cosPerLine[l>>8]<<16)
          +((0xFFFFLL - sinPerLine[l>>8])<<32) + (cosPerLine[l>>8]<<48);
         colorAsmDataTab[1]=
           (            cosPerLine[l>>8] <<7 ) + (sinPerLine[l>>8]<<23)
          +(            cosPerLine[l>>8] <<39) + (sinPerLine[l>>8]<<55);
         if((perm[line] & 1) ^ vPhase){
           colorAsmDataTab[0]^=0x0000FFFF0000FFFFLL;
           colorAsmDataTab[1]^=0xFFFF0000FFFF0000LL;
         }
       }


       if(neg[type]){
         colorAsmDataTab[0]^=0xFFFFFFFFFFFFFFFFLL;
         colorAsmDataTab[1]^=0xFFFFFFFFFFFFFFFFLL;
       }

       asm("movq    (%%eax), %%mm1       \n\t"
           "movq   8(%%eax), %%mm2       \n\t"
           "movq  16(%%eax), %%mm3       \n\t"
           "movq  24(%%eax), %%mm4       \n\t"

           "movq  (%%esi, %%ecx), %%mm0  \n\t"
           "movq %%mm1, %%mm5            \n\t"
           "movq %%mm0, %%mm6            \n\t"
           "psllw $8, %%mm0              \n\t"
           "pmaddwd %%mm0, %%mm5         \n\t"
           "pmaddwd %%mm2, %%mm0         \n\t"
           "1:                           \n\t"
             // Stall 1 Cyc
           ".byte 0x0f, 0xdb, 0xeb       \n\t" // no more bugs PLEEEEZE "pand %%mm3, %%mm5            \n\t"
           ".byte 0x0f, 0xdb, 0xf4       \n\t" // no more bugs PLEEEEZE "pand %%mm4, %%mm6            \n\t"

           "psrld $23, %%mm0             \n\t"
           "por %%mm6, %%mm5             \n\t"

           "por %%mm5, %%mm0             \n\t"

           "movq  %%mm0, (%%esi, %%ecx)  \n\t"

           "movq 8(%%esi, %%ecx), %%mm0  \n\t"
           "movq %%mm1, %%mm5            \n\t"

           "movq %%mm0, %%mm6            \n\t"
           "psllw $8, %%mm0              \n\t"

           "pmaddwd %%mm0, %%mm5         \n\t"
           "addl $8, %%ecx               \n\t"

           "pmaddwd %%mm2, %%mm0         \n\t"
           "  jnc 1b                     \n\t"
           "emms                         \n\t"
            :
            : "a" (int(colorAsmDataTab)),
              "S" (int(p) + (wndx<<1)),
              "c" (-wndx<<1) 
            : "%eax", "%ecx");
     }
     else{
       for(int x=0; x<wndx; x+=4){
         const int u= int(* p   );
         const int v= int(*(p+2));

         char u1;
         char v1;
         if((perm[line] & 1) ^ vPhase){
           u1=+ cosPerLineMulX[l+u] - sinPerLineMulX[l+v];
           v1=+ sinPerLineMulX[l+u] + cosPerLineMulX[l+v];
         }
         else{
           u1=+ cosPerLineMulX[l+u] + sinPerLineMulX[l+v];
           v1=- sinPerLineMulX[l+u] + cosPerLineMulX[l+v];
         }

         if(xchg[type]){
           * p   = *(p+4)= (v1^neg[type]) - neg[type];
           *(p+2)= *(p+6)= (u1^neg[type]) - neg[type];

           p +=8;

         }else{
           * p   = *(p+4)= (u1^neg[type]) - neg[type];
           *(p+2)= *(p+6)= (v1^neg[type]) - neg[type];

          p +=8;
       
         }
       }
     }
   }
   else{
     asm("1:                           \n\t"
         "movb  (%%esi, %%ecx), %%al   \n\t"
         "movb 2(%%esi, %%ecx), %%bl   \n\t"
         "movb %%al,  (%%edi, %%ecx)   \n\t"
         "movb %%bl, 2(%%edi, %%ecx)   \n\t"
         "movb 4(%%esi, %%ecx), %%al   \n\t"
         "movb 6(%%esi, %%ecx), %%bl   \n\t"
         "movb %%al, 4(%%edi, %%ecx)   \n\t"
         "movb %%bl, 6(%%edi, %%ecx)   \n\t"
         "movb 8(%%edi, %%ecx), %%al   \n\t"
         "addl $8, %%ecx               \n\t"
         "  jnc 1b                     \n\t"
          :
          : "D" (int(p    ) + (wndx<<1)),
            "S" (int(pGood) + (wndx<<1)) ,
            "c" (-wndx<<1) 
          : "%eax", "%ebx", "%ecx");
   }
 }


 if(iState){
   T2=uclock();
   sprintf(textbuf[4],"%f ColorDeco", (float)(T2-T1)/UCLOCKS_PER_SEC);
   T1=T2;
 }

/*
 for(int i=0; i<NAG_LINES; i++){
   const int p=nagLineLogi2Phys(i);
   actVid2MemBufp->b[p*(vgax<<1)+i*2+1]=255;
 }

 for(int i=0; i<decoLines; i++){
   const int p1=nagLineLogi2Phys(decoLine[i]);
   const int p2=nagLineLogi2Phys(bestCoeffs[i].line[0]);
   const int p3=nagLineLogi2Phys(bestCoeffs[i].line[1]);
   for(int j=0; j<20; j++){
     actVid2MemBufp->b[p1*(vgax<<1)+i*20+101+j]=255;
     actVid2MemBufp->b[p2*(vgax<<1)+i*20+101+j]=255;
     actVid2MemBufp->b[p3*(vgax<<1)+i*20+101+j]=255;
   }
 }
  */
 delete [] bestCoeffs;

 int enc2cleVec[max_x];
 int cle2encVec[max_x];

 for(int i=0; i<outy; i++){
   enc2cleVec[i]=
   cle2encVec[i]=-1;
 }

 for(int i=0; i<NAG_LINES; i++){
   const int encodLine= nagLineLogi2Phys(perm[i]);
   const int clearLine= i << ((outy==wndy) ? 1 : 0);

   enc2cleVec[ encodLine ]= clearLine;
   cle2encVec[ clearLine ]= encodLine;
 }

 byte *copyBuff= new byte[vgax<<1];

 for(int i=0; i<outy; i++){
   if(cle2encVec[i] == -1 || cle2encVec[i] == i) continue;
   int k;
   int j=i;
   do{
     k = j;
     j = enc2cleVec[j];
   } while(j != i && j != -1);
   const bool open= (j == -1);

   if(!open){
     memcpy(copyBuff, actVid2MemBufp->b + (i * vgax<<1), wndx << 1);
     j = i;
     do{
       k = j;
       j = cle2encVec[j];
       enc2cleVec[k] = k;
       cle2encVec[k] = k;
//       printf("closed %d %d\n", j, k );
       if(j != i)  memcpy(actVid2MemBufp->b + (k * vgax<<1),
                          actVid2MemBufp->b + (j * vgax<<1), wndx << 1);
     } while(j != i);
     memcpy(actVid2MemBufp->b + (k * vgax<<1), copyBuff, wndx << 1);
   }
   else{
     j = k;
     do{
       k = j;
       j = cle2encVec[j];
       enc2cleVec[k] = k;
       cle2encVec[k] = k;
//       printf("open %d %d\n", j, k );
       if(j != -1) memcpy(  actVid2MemBufp->b + (k * vgax<<1)
                          , actVid2MemBufp->b + (j * vgax<<1), wndx << 1);
     } while(j != -1);
   }

 }

/* if(outy == wndy){
   for(int i=0; i<outy; i+=2){
     memcpy(actVid2MemBufp->b + ((i+1) * vgax<<1),
            actVid2MemBufp->b + (i * vgax<<1)    , wndx << 1);
   } 
 }   */

 delete [] copyBuff;

 if(iState){
   T2=uclock();
   sprintf(textbuf[5],"%f ReOrder", (float)(T2-T1)/UCLOCKS_PER_SEC);
   T1=T2;
 }


 if(iState){
   T2=uclock();          
   c.init(255, 0, 0, false);
   gprint(infoPosX, infoPosY+=10,  c.col, textbuf[0]);
   gprint(infoPosX, infoPosY+=10,  c.col, textbuf[1]);
   gprint(infoPosX, infoPosY+=10, c.col, textbuf[2]);
   gprint(infoPosX, infoPosY+=10, c.col, textbuf[3]);
   gprint(infoPosX, infoPosY+=10, c.col, textbuf[4]);
   gprint(infoPosX, infoPosY+=10, c.col, textbuf[5]);
   gprint(infoPosX, infoPosY+=10, c.col, textbuf[6]);
/*   gprint(infoPosX, infoPosY+=10, c.col, textbuf[8]);
   gprint(infoPosX, infoPosY+=10, c.col, textbuf[7]);
   gprint(infoPosX, infoPosY+=10, c.col, textbuf[8]);
   gprint(infoPosX, infoPosY+=10, c.col, textbuf[9]);*/
   infoPosY+=5;
   T1=T2;
 }

}

