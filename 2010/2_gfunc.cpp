//2010 0.1 Copyright (C) Michael Niedermayer 1998

#include <stdlib.h>
#include <string.h>
#include "2_all.h"
#include "2_gfunc.h"

 extern int vgax, vgay, xresc, mc;
 extern int fysize;
 extern byte font[4096];
 extern int g_mode;
 extern int page_flip;
 extern VID2MEMBUF *actVid2MemBufp;
 extern byte *vidbuf;
 extern int yuvMode;

static inline void memsetw(void *pt, u_short c, int num);
static inline void memsetd(void *pt, u_long  c, int num);

static inline void memsetd(void *pt, u_long c, int num){

 asm(//"int $3 \n\t"
     "rep    \n\t"
     "stosl  \n\t"
     :
     : "a" (c), "D" (pt), "c" (num)
     : "%ecx", "%edi");


}

static inline void memsetw(void *pt, u_short c, int num){

 asm(//"int $3 \n\t"
     "rep    \n\t"
     "stosw  \n\t"
     :
     : "a" (c), "D" (pt), "c" (num)
     : "%ecx", "%edi");


}

void rect(int xs, int ys, int xe, int ye, const COL c){
  if(xs<0) xs=0;
  if(ys<0) ys=0;
  if(xe>vgax) xe=vgax;
  if(ye>vgay) ye=vgay;

  if(xe<0) xs=0;
  if(ye<0) ys=0;
  if(xs>vgax) xe=vgax;
  if(ys>vgay) ye=vgay;

  if(yuvMode){
    xs>>=1;
    xe>>=1;
    for(int y=ys; y<ye; y++){
      memsetd( &actVid2MemBufp->b[(y*vgax +xs+xs) << mc], c.yuv, xe-xs);
    }
  }
  else{
    for(int y=ys; y<ye; y++){
      memsetw( &actVid2MemBufp->b[(y*vgax +xs) << mc], c.rgb16, xe-xs);
    }
  }

}

void xcliped_gprint(int xp, int yp, const COL c, int text){
 int ix, iy;
 int t3;
 byte t1;

 if(yp>0) iy=0;
 else iy=-yp;
 for(; iy<fysize && (iy+yp<vgay); iy++){
   t1=font[(text<<4)+iy];
   t3=xp+(yp+iy)*vgax;
   if(xp>0) ix=0;
   else ix=-xp;
   for(; ix<8 && ix+xp<vgax; ix++){
     if(((t1>>(7-ix)) & 1) == 1){
       if(yuvMode!=0)
         ((long*) actVid2MemBufp->b)[(ix+t3)>>1]=c.yuv;      
       else                                   
         ((short*)actVid2MemBufp->b)[(ix+t3)   ]=c.rgb16;      
     }
   }
 }
}

void gprint(int xp, int yp, const COL c, char *text){
 static int clipyp, clipym;
 int i, t=0, tp=0;


 if(xp<0){
  if(xp+int(strlen(text))*9+1<0) return;
  i=(-xp+1)/9+1;
  if(i>int(strlen(text))) return;
  text+=i;
  xp+=i*9;
  xcliped_gprint(xp-9, yp, c, *(text-1));
 }
 if(yp<0){
  if(yp+fysize<=0) return;
  clipym=-yp;
 }
 else clipym=0;

 if(xp+int(strlen(text))*9+1>=vgax){
  if(xp>=vgax) return;
  i=(vgax-xp)/9;
  t=text[i];
  tp=i;
  text[i]=0;
 }

 if(yp+fysize>vgay){
  if(yp>=vgay) return;
  clipyp=vgay-yp;
 }
 else clipyp=fysize;

 //FIX ME TEST reg, imm is NP

 static int vgaxmc;
 vgaxmc= vgax<<mc;
 if(yuvMode!=0){
   asm(//"int $3\n\t"
        "pushl %%ebp               \n\t"
        "pushl %%eax               \n\t"
        "10:                       \n\t"
        "movl %4, %%esi            \n\t"
        "movl %%esi, %%edi         \n\t"
        "imull %1, %%esi           \n\t"
        "popl %%ebp                \n\t"
        "pushl %%ebp               \n\t"
        "movl %%ebp, %%eax         \n\t"
        "shrl $16, %%ebp           \n\t"
        "testl $2, %%edx           \n\t"
        " jz 0f                    \n\t"
        "xchgl %%eax, %%ebp        \n\t"
        "0:                        \n\t"
        "xorl %%ebx,%%ebx          \n\t"     
        "movb (%%ecx), %%bl        \n\t"
        "testb %%bl, %%bl          \n\t"
        " jz 9f                    \n\t"
        "shll $4, %%ebx            \n\t"
        "addl %%edi, %%ebx         \n\t"
        "addl %2, %%ebx            \n\t"
        "movb (%%ebx), %%bl        \n\t"
        "testb $128, %%ebx         \n\t"
        " jz 1f                    \n\t"
        "movw %%ax,  (%%edx,%%esi) \n\t"
        "1:                        \n\t"
        "testb $64, %%bl           \n\t"
        " jz 2f                    \n\t"
        "movw %%bp, 2(%%edx,%%esi) \n\t"
        "2:                        \n\t"
        "testb $32, %%bl           \n\t"
        " jz 3f                    \n\t"
        "movw %%ax, 4(%%edx,%%esi) \n\t"
        "3:                        \n\t"
        "testb $16, %%bl           \n\t"
        " jz 4f                    \n\t"
        "movw %%bp, 6(%%edx,%%esi) \n\t"
        "4:                        \n\t"
        "testb $8, %%bl            \n\t"
        " jz 5f                    \n\t"
        "movw %%ax, 8(%%edx,%%esi) \n\t"
        "5:                        \n\t"
        "testb $4, %%bl            \n\t"
        " jz 6f                    \n\t"
        "movw %%bp,10(%%edx,%%esi) \n\t"
        "6:                        \n\t"
        "testb $2, %%bl            \n\t"
        " jz 7f                    \n\t"
        "movw %%ax,12(%%edx,%%esi) \n\t"
        "7:                        \n\t"
        "testb $1, %%bl            \n\t"
        " jz 8f                    \n\t"
        "movw %%bp,14(%%edx,%%esi) \n\t"
        "8:                        \n\t"
        "incl %%edi                \n\t"
        "addl %1, %%esi            \n\t"
        "cmpl %3, %%edi            \n\t"
        " jb 0b                    \n\t"
        "xchgl %%eax, %%ebp        \n\t"
        "addl $18, %%edx           \n\t"
        "incl %%ecx                \n\t"
        "jmp 10b                   \n\t" 
        "9:                        \n\t" 
        "popl %%eax                \n\t"
        "popl %%ebp                \n\t"
        :
        : "m" (fysize), "m"(vgaxmc), "m" (&font), "m" (clipyp), "m" (clipym),
          "a" (c.yuv), "c" (text), "d" (((u_long)actVid2MemBufp->b)+((xp+yp*vgax)<<1))
        : "%ebx", "%ecx", "%edx", "%esi", "%edi");
 }else{
   asm(//"int $3\n\t"
        "10:                       \n\t"
        "movl %4, %%esi            \n\t"
        "movl %%esi, %%edi         \n\t"
        "imull %1, %%esi           \n\t"
        "0:                        \n\t"
        "xorl %%ebx,%%ebx          \n\t"
        "movb (%%ecx), %%bl        \n\t"
        "testb %%bl, %%bl          \n\t"
        " jz 9f                    \n\t"
        "shll $4, %%ebx            \n\t"
        "addl %%edi, %%ebx         \n\t"
        "addl %2, %%ebx            \n\t"
        "movb (%%ebx), %%bl        \n\t"
        "testb $128, %%ebx         \n\t"
        " jz 1f                    \n\t"
        "movw %%ax,  (%%edx,%%esi) \n\t"
        "1:                        \n\t"
        "testb $64, %%bl           \n\t"
        " jz 2f                    \n\t"
        "movw %%ax, 2(%%edx,%%esi)\n\t"
        "2:                        \n\t"
        "testb $32, %%bl           \n\t"
        " jz 3f                    \n\t"
        "movw %%ax, 4(%%edx,%%esi)\n\t"
        "3:                        \n\t"
        "testb $16, %%bl           \n\t"
        " jz 4f                    \n\t"
        "movw %%ax, 6(%%edx,%%esi)\n\t"
        "4:                        \n\t"
        "testb $8, %%bl            \n\t"
        " jz 5f                    \n\t"
        "movw %%ax, 8(%%edx,%%esi)\n\t"
        "5:                        \n\t"
        "testb $4, %%bl            \n\t"
        " jz 6f                    \n\t"
        "movw %%ax,10(%%edx,%%esi)\n\t"
        "6:                        \n\t"
        "testb $2, %%bl            \n\t"
        " jz 7f                    \n\t"
        "movw %%ax,12(%%edx,%%esi)\n\t"
        "7:                        \n\t"
        "testb $1, %%bl            \n\t"
        " jz 8f                    \n\t"
        "movw %%ax,14(%%edx,%%esi)\n\t"
        "8:                        \n\t"
        "incl %%edi                \n\t"
        "addl %1, %%esi            \n\t"
        "cmpl %3, %%edi            \n\t"
        " jb 0b                    \n\t"
        "addl $18, %%edx           \n\t"
        "incl %%ecx                \n\t"
        "jmp 10b                   \n\t" 
        "9:                        \n\t" 
        :
        : "m" (fysize), "m"(vgaxmc), "m" (&font), "m" (clipyp), "m" (clipym),
          "a" (c.rgb16), "c" (text), "d" (((u_long)actVid2MemBufp->b)+((xp+yp*vgax)<<1))
        : "%ebx", "%ecx", "%edx", "%esi", "%edi");
   }
 if(t!=0){
  xcliped_gprint(vgax - ((vgax-xp) % 9), yp, c, t);
  text[tp]=t;
 }
}

