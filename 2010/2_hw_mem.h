//2010 0.1 Copyright (C) Michael Niedermayer 1998
#ifndef n2_hw_mem_h
#define n2_hw_mem_h

#define carry_flag 1

struct dds{
 dword size;
 dword offset;
 word  selector;
 word  id;
 dword phys;
};

struct VdsList{
 dds vds_info;
 volatile VdsList *next;
};

struct LockList{
 void *start;
 void *end;
 volatile LockList *next;
};

struct MapList{
 void *start;
 volatile MapList *next;
};

struct AllocList{
 u_long addr;
 u_long phys;
 int hand;
 volatile AllocList *next;
};

bool checkAlign(void *p, int align);        
void *newAlign(int size, int align);
void deleteAlign(void *v);
void locklin(void *, void *);
void unlocklin(void *, void *);
void lock(void *, void *);
void unlock(void *, void *);
void unlockall(void);
void dpmi_addlinlock(volatile AllocList *old, volatile AllocList **al);
u_long dpmi_alloclinlock(volatile AllocList **al);
void dpmi_freelinlock(volatile AllocList *old, volatile AllocList **al);
void dpmi_freeall(volatile AllocList **al);
void allocCont(VID2MEMBUF *v2mb, int size, int *num);
void checkVDS(void);
u_long getphys(u_long);
dds vds_alloc(int size);
void vds_free(dds *vds_info);
void vds_freeall(void);
void *map(u_long start, u_long end);
void unmap(void *start);
void unmapall(void);
int getFreeMemory(void);

#endif
