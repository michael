#include <stdio.h>
#include <stdlib.h>

#include <string.h>

int main(int argc, char **argv)
{
	if(argc<3) 
	{
		printf("Command line Error!\n");
		exit(1);
	}
	
	FILE * inFile= fopen(argv[1], "rb");
	if(inFile==NULL) 
	{
		printf("Cant open inFile!\n");
		exit(1);
	}
	
				/* load the Pos at witch the Jpeg beginns */

	long filePos=0;
	
	FILE * datFile= fopen("extractppm.dat", "rb");
	if(datFile==NULL) 
	{
		filePos=0;
	}
	else
	{
		fscanf(datFile, "%D", &filePos);
	
		fclose(datFile);
	}
	
	if( stricmp("X000000.ppm", argv[2])==0 )	filePos=0;	
	
	fseek(inFile, filePos, SEEK_SET);
	
	const int bufferSize= 1024*100;
	unsigned char *buffer= new unsigned char[bufferSize];

	memset(buffer, 0, bufferSize);
	fread(buffer, bufferSize, 1, inFile);

	fclose(inFile);
	
	int xNextJpeg;
	for(xNextJpeg=1; xNextJpeg+20<bufferSize; xNextJpeg++)
	{
		if( 	buffer[xNextJpeg     ] == 0xFF
			&&	buffer[xNextJpeg +  2] == 0xFF
			&&	buffer[xNextJpeg +  6] == 'J'
			&&	buffer[xNextJpeg +  7] == 'F'
			&&	buffer[xNextJpeg +  8] == 'I'
			&&	buffer[xNextJpeg +  9] == 'F'
			&&	buffer[xNextJpeg + 10] == 0)
		{
			filePos+= xNextJpeg;
			break;
		}
	}
	
	if(xNextJpeg+20 >= bufferSize)											// End 
	{
		filePos=0;												
	}
	
				/* save Pos at witch the next Jpeg beginns */

	datFile= fopen("extractppm.dat", "wb");
	if(datFile==NULL) 
	{
		printf("Cant open datFile!\n");
		exit(1);
	}
	
	fprintf(datFile, "%D\n", filePos);
	
	fclose(datFile);
	
	
				/* extract the Jpeg */

	FILE * outFile= fopen("temp.jpg", "wb");
	if(outFile==NULL) 
	{
		printf("Cant open outFile!\n");
		exit(1);
	}
	
	fwrite(buffer, xNextJpeg, 1, outFile); 									// FIXME the last one will be too Large
		
	fclose(outFile);
	
				/* convert it to a PPM (Binary) */
	
	//char textBuffer[50];
	//sprintf(textBuffer, "djpeg -outfile %s temp.jpg", argv[2]);
	//system( textBuffer );
	system( "djpeg -outfile temp.ppm temp.jpg" );

	
				/* convert the Binary PPM to a Ascii (Bug in Dos or Mpeg_enc ?)*/

	FILE * ppmFile= fopen("temp.ppm", "rb");
	if(outFile==NULL) 
	{
		printf("Cant open ppmFile!\n");
		exit(1);
	}
	
	fread(buffer, 99999, 1, ppmFile); 				
	
	fprintf(stdout, "P3\n192 144\n255\n");
	
	for(int i=0; i<192*144*3; i++)
	{
		fprintf(stdout, "%d ", buffer[i]);
	}

}		
	
