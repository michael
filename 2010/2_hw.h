//2010 0.1 Copyright (C) Michael Niedermayer 1998
#ifndef n2_hw_h
#define n2_hw_h

#define confadd_port  0xCF8
#define confdata_port 0xCFC 
                            
#define PCI_IO_vendev  0x00    
#define PCI_IO_command 0x04   
#define PCI_IO_latency 0x0D 
#define PCI_IO_base    0x10 
#define PCI_IO_irq     0x3C  
                             
#define PCI_command_io     0x1
#define PCI_command_memory 0x2
#define PCI_command_master 0x4

union confadd{
 struct{
  unsigned res:2;
  unsigned reg:6;
  unsigned func:3;
  unsigned dev:5;
  unsigned bus:8;
  unsigned res2:7;
  unsigned enable:1;
 }f ;
 u_long a;
};

struct ModeInfoBlock {
  word  ModeAttrib  __attribute__ ((packed));
  byte  WinAAttrib  __attribute__ ((packed));
  byte  WinBAttrib  __attribute__ ((packed));
  word  WinGranul   __attribute__ ((packed));
  word  WinSize     __attribute__ ((packed));
  word  SegA        __attribute__ ((packed));
  word  SegB        __attribute__ ((packed));
  dword FarFunc     __attribute__ ((packed));
  word  BLine       __attribute__ ((packed));
  word  Xres        __attribute__ ((packed));
  word  Yres        __attribute__ ((packed));
  byte  Xchar       __attribute__ ((packed));
  byte  Ychar       __attribute__ ((packed));
  byte  planes      __attribute__ ((packed));
  byte  bpp         __attribute__ ((packed));
  byte  banks       __attribute__ ((packed));
  byte  model       __attribute__ ((packed));
  byte  BankSize    __attribute__ ((packed));
  byte  pages       __attribute__ ((packed));
  byte  bogus1      __attribute__ ((packed));
  byte  RSize       __attribute__ ((packed));
  byte  RPos        __attribute__ ((packed));
  byte  GSize       __attribute__ ((packed));
  byte  GPos        __attribute__ ((packed));
  byte  BSize       __attribute__ ((packed));
  byte  BPos        __attribute__ ((packed));
  byte  XSize       __attribute__ ((packed));
  byte  XPos        __attribute__ ((packed));
  byte  DirectInfo  __attribute__ ((packed));
  dword PhysBase    __attribute__ ((packed));
  byte  bogus2[228] __attribute__ ((packed));
};

struct VesaInfoBlock{
  byte   signature[4]    __attribute__ ((packed));
  word   ver             __attribute__ ((packed));
  byte  *OemSPointer     __attribute__ ((packed));
  dword  capab           __attribute__ ((packed));
  byte  *modes           __attribute__ ((packed));
  word   mem64k          __attribute__ ((packed));
  word   oem_ver         __attribute__ ((packed));
  byte  *oem_vendorname  __attribute__ ((packed));
  byte  *oem_productname __attribute__ ((packed));
  byte  *oem_revisionstr __attribute__ ((packed));
  byte   bogus[478]      __attribute__ ((packed));
};

void mouse_handler(void);
void set_start_disp(int x, int y);
void init_hw(void);
void setpal(void);
ModeInfoBlock *get_mode_info(int mode);
VesaInfoBlock *get_vesa_info(void);
void close_hw(void);
void copy_vidbuffer(void);
void disabled_call_mouse_handler(void);
void sig_handler(int i);
void end_func(void);
u_long read_pci(int,int,int,int,int);
void write_pci(int,int,int,int,int,u_long);
void mem2vid(byte *to, byte *from, int num, int bpp);
int check_mmx(void);

#endif
