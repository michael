//2010 0.1 Copyright (C) Michael Niedermayer 1998
#ifndef n2_nag_h
#define n2_nag_h

#define MIN_DWN_SMP 1
#define KEYS 32768
#define NAG_LINES 287

struct BEST2{
  unsigned line:9;
  unsigned keyNdx:23;
};

struct BESTCOEFFS{
  int line[2];
  int coeff[2];
};

struct SINFO{
  unsigned line0:9;
  unsigned line1:9;
  unsigned key:15;
};

void nag_decrypt(void);

#endif
