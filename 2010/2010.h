//2010 0.1 Copyright (C) Michael Niedermayer 1998

#ifndef n2010_h
#define n2010_h

enum error_code{
 CommLine,
 Help,
 NoVDS,
 VDS,
 NoVESA,
 VESA,
 VESAFlip,
 VESAVer,
 NoMeteor,
 DPMI,
 FileRead,
 FileWrite,
 Map,
 NoFont,
 Font,
 MemAlloc,
 FileMax,
 Lock,
 SAA7196,
 DatWrite,
 KeyFile,
 Align,
 Nagra,
 TxtFile,
};

enum MENULEVEL{
  mLNorm,
  mLTVStd,
  mLTXTPage,
  mLTXTChan,
};

enum TABTYPE{
  TAB_HEX,
  TAB_DEZ,
  TAB_STR,
};

int error(error_code e);
void exitt(void);

#endif
