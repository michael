//2010 0.1 Copyright (C) Michael Niedermayer 1998
#ifndef n2_hw_a_h
#define n2_hw_a_h

void CopyYuv2Rgb1(int, int, int, int);
void CopyYuv2Rgb2(int, int, int, int);

#endif
