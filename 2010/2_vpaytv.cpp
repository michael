//2010 0.1 Copyright (C) Michael Niedermayer 2000

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include "2_all.h"
#include "2_crypt.h"
#include "2_vc.h"
#include "2_nag.h"
#include "2_71x6.h"
#include "2_gfunc.h"
#include "2_mmx.h"
#include "2_nag_a.h"
#include "2_hw_mem.h"
#include "2_vpaytv.h"
#include "2_pal.h"
#include "2010.h"

/* SyncSupression Encoding Constants */
#define Q_2_HSYNC_DISTANCE (QCOL_SIZE + 2)						// only 400x300 
#define QCOL_SIZE 14 											// only 400x300 

/* VSync Detection */
#define VSYNC_SEARCH_COLUMN (-2)
#define MIN_VSYNC_DISTANCE 20

/* HSync Detection */
#define QCOL_CORREL_QCOL_SIZE 5 								// only 400x300 
#define QCOL_CORREL_BORDER_SIZE (QCOL_CORREL_QCOL_SIZE)		// only right, there is none at the left	
#define QCOL_CORREL_SIZE (QCOL_CORREL_QCOL_SIZE + QCOL_CORREL_BORDER_SIZE)	
#define QCOL_COEFFICIENT_THRESHOLD 0.20
#define HSYNC_SEARCH_START (Q_2_HSYNC_DISTANCE-5)
#define HSYNC_SEARCH_END (Q_2_HSYNC_DISTANCE+5)

#define REAL_HSYNC_LOC (wndx-10)
#define LEFT_HSYNC_BOUNDRY (wndx-10)
#define RIGHT_HSYNC_BOUNDRY (wndx-5)

#define TOP_VSYNC_BOUNDRY (wndy-50)
#define BOTTOM_VSYNC_BOUNDRY (wndy+50)

/* Brightness / Contrast Fix */
#define RELATIV_LEVEL_SHIFT 2.0 
#define BLANK_REF_LOC (-3)
#define SYNC_REF_LOC 3
//#define CLIP_REJECTION_THRESHOLD 1.5

/* Color-Phase Fix */
#define START_Q_PHASE (some2 /180.0*PI)
#define Q_PHASE_DELTA (-2*PI /(PAL_NUM_LINES))


extern vgax, vgay, wndx, wndy, outy, y_field, x_field;
extern VID2MEMBUF *actVid2MemBufp;
extern volatile int scales_x, scalee_x;
extern volatile int scales_y, scalee_y;
extern u_long asm_temp, asm_temp4;
extern bool iState;
extern int yuvMode;
extern bool mmx;
extern int infoPosX;
extern int infoPosY;
extern int some, some2;
extern bool drop;
extern bool isVCPhaseTrick;
extern int bright;
extern int contr;

static inline float atan3(const float f1, const float f2){
  float out;
                     // ?! atan2 crashes 
  asm(//"int $3\n\t"
      "fpatan \n\t"
      : "=t"(out)
      : "0"(f2), "u"(f1)
      : "st(1)"
                          );

  return out;
}

static int qPhase=0;
int findHSync(void)
{
	long T1=0, T2;
	color c;
    c.init(255, 0, 0, false);
	char textbuf[100];
    
	if(iState==2)
		T1=uclock();
	
	static float blankCorrelVector[4]= {0.5, 0.5, 0.5, 0.5};
	static float qColCorrelVector[4]= {1.0, 1.0, 0.0, 0.0};

	static bool isFirst= true;
	if(isFirst)
	{

		/* Normalize CorrelVectors */

		double mean= 0;
		for(int y=0; y<4; y++)
		{
			mean+= blankCorrelVector[y] * QCOL_CORREL_BORDER_SIZE;
			mean+= qColCorrelVector[y] * QCOL_CORREL_QCOL_SIZE;
		}
		mean/= 4.0 * QCOL_CORREL_SIZE;

		float variance= 0;
		for(int y=0; y<4; y++)
		{
			blankCorrelVector[y] -= mean;
			qColCorrelVector[y] -= mean;
			
			variance += blankCorrelVector[y] * blankCorrelVector[y] * QCOL_CORREL_BORDER_SIZE;
			variance += qColCorrelVector[y] * qColCorrelVector[y] * QCOL_CORREL_QCOL_SIZE;
		}
		variance/= 4.0 * QCOL_CORREL_SIZE;
		double stdDev= sqrt(variance);

		for(int y=0; y<4; y++)
		{
			blankCorrelVector[y] /= stdDev;
			qColCorrelVector[y] /= stdDev;
		}
		
	}
	
	byte const * const p= &actVid2MemBufp->b[0];

	if(iState==2)
	{
	    T2=uclock();
    	sprintf(textbuf,"%f X", (float)(T2-T1)/UCLOCKS_PER_SEC);
    	gprint(infoPosX, infoPosY+=10, c.col, textbuf);
		T1=T2;
	}
	
	int aSumsOfEquivalentPoints[4][1000];
	for(int i=0; i<4; i++)
		for(int x=0; x<wndx; x++)
			aSumsOfEquivalentPoints[i][x]= 0;

	int aSumsOfSqrs[1000];
	for(int x=0; x<wndx; x++)
		aSumsOfSqrs[x]=0;

	for(int line=0; line<wndy; line+=3)
		for(int x=0; x<wndx; x++)
		{
			const int index= (line*vgax + x)*2;
			const int a= 	  mabs(int((char)(p[index + 2])))
							+ mabs(int((char)(p[index    ])));
			aSumsOfEquivalentPoints[line & 3][x]+= a;
			aSumsOfSqrs[x]+= a*a;
		}
	
	for(int x=0; x<wndx; x++)
	{
		aSumsOfEquivalentPoints[0][x]*= 3;
		aSumsOfEquivalentPoints[1][x]*= 3;
		aSumsOfEquivalentPoints[2][x]*= 3;
		aSumsOfEquivalentPoints[3][x]*= 3;
		aSumsOfSqrs[x]*= 3;
	}
			
		

	if(iState==2)
	{
	    T2=uclock();
    	sprintf(textbuf,"%f EquivSum & SumSqr", (float)(T2-T1)/UCLOCKS_PER_SEC);
    	gprint(infoPosX, infoPosY+=10, c.col, textbuf);
		T1=T2;
	}	
	
			
	int bestQLine=-1;
	float bestCoefficient=-1;
	for(int xStart=0; xStart<wndx-QCOL_CORREL_SIZE; xStart++)
		{
		int sum=0;
		for(int line=0; line<4; line++)
			for(int x=xStart; x<xStart + QCOL_CORREL_SIZE; x++)
				sum+= aSumsOfEquivalentPoints[line][x];

		const double mean= double(sum) / (QCOL_CORREL_SIZE*wndy);

		int sumOfSqrs=0;
		for(int x=xStart; x<xStart + QCOL_CORREL_SIZE; x++)
			sumOfSqrs+= aSumsOfSqrs[x];
	
		float variance= (sumOfSqrs - sum*mean) / (QCOL_CORREL_SIZE*wndy); 
//		printf("%f = %d - %d * %f / ...\n", variance, sumOfSqrs, sum, mean);
		if(variance<0.0) variance=0;

		float coefficient[4]={0,0,0,0};
		for(int line=0; line<4; line++)
		{
			for(int x=xStart; x<xStart + QCOL_CORREL_QCOL_SIZE; x++)
			{
				coefficient[0]+= (aSumsOfEquivalentPoints[line][x]-mean*wndy/4) * qColCorrelVector[(line+0) &3];
				coefficient[1]+= (aSumsOfEquivalentPoints[line][x]-mean*wndy/4) * qColCorrelVector[(line+1) &3];
				coefficient[2]+= (aSumsOfEquivalentPoints[line][x]-mean*wndy/4) * qColCorrelVector[(line+2) &3];
				coefficient[3]+= (aSumsOfEquivalentPoints[line][x]-mean*wndy/4) * qColCorrelVector[(line+3) &3];				
			}

			for(int x=xStart + QCOL_CORREL_QCOL_SIZE; x<xStart + QCOL_CORREL_SIZE; x++)
			{
				coefficient[0]+= (aSumsOfEquivalentPoints[line][x]-mean*wndy/4) * blankCorrelVector[(line+0) &3];
				coefficient[1]+= (aSumsOfEquivalentPoints[line][x]-mean*wndy/4) * blankCorrelVector[(line+1) &3];
				coefficient[2]+= (aSumsOfEquivalentPoints[line][x]-mean*wndy/4) * blankCorrelVector[(line+2) &3];
				coefficient[3]+= (aSumsOfEquivalentPoints[line][x]-mean*wndy/4) * blankCorrelVector[(line+3) &3];				
			}
		}

		for(int i=0; i<4; i++)
		{
			coefficient[i]/= QCOL_CORREL_SIZE * wndy * sqrt(variance + 0.000001);

			if(coefficient[i] > bestCoefficient)
			{
				bestCoefficient= coefficient[i];
				bestQLine= xStart - QCOL_SIZE + QCOL_CORREL_QCOL_SIZE;		
				qPhase= (4-i) &3;
//				some= int(minmax(-10000, bestCoefficient*100, 10000));
			}
		}
	}

	if(iState==2)
	{
	    T2=uclock();
    	sprintf(textbuf,"%f coeffTime", (float)(T2-T1)/UCLOCKS_PER_SEC);
    	gprint(infoPosX, infoPosY+=10, c.col, textbuf);
		T1=T2;
    
		sprintf(textbuf,"%f coeff", bestCoefficient);
	   	gprint(infoPosX, infoPosY+=10, c.col, textbuf);
	}

	
	if(bestCoefficient < QCOL_COEFFICIENT_THRESHOLD) 	return -1;

	int bestHSyncScore= 0;
	int bestHSync= 0;
	int hSync;	
	for(hSync= bestQLine + HSYNC_SEARCH_START; hSync < bestQLine + HSYNC_SEARCH_END; hSync++)
	{
		if(hSync+1 >= wndx) return -1;
		
		int hSyncScore= 0;
		for(int line= 0; line<wndy; line+=3)
		{
			if(line&3 == qPhase) continue;
			hSyncScore+= (int)( (byte)( p[(line*vgax + hSync-1)*2 + 1] ) )
						-(int)( (byte)( p[(line*vgax + hSync+1)*2 + 1] ) );
		}
		
		if(hSyncScore > bestHSyncScore)
		{
			bestHSyncScore= hSyncScore;
			bestHSync= hSync;
		}		
	}
	
	return bestHSync; 
	
		
//	return minmax(0, bestQLine + Q_2_HSYNC_DISTANCE, wndx-1);	
}

int findVSync(int qCol)
{
	if(qCol==-1) return -1;
	if(qCol<2 || qCol+1>=wndx) return -1;

    byte const * const p= &actVid2MemBufp->b[0];

	int aRunningMeanQ[600];
	int aRunningMeanNotQ[600];

	int qSum=0;
	int notQSum=0;
	int runningQSum=0;
	int runningNotQSum=0;
	int nRunningQSum=0;
	int nRunningNotQSum=0;
	for(int vSyncStart= -PAL_VSYNC_SIZE; vSyncStart<wndy; vSyncStart++)
		{
		const vSyncEnd= vSyncStart + PAL_VSYNC_SIZE;
		int index= vSyncStart;
		if(index<0) index= vSyncStart + wndy + PAL_VSYNC_SIZE;

		if(vSyncStart>=0)
			{
			const int xP= (vSyncStart*vgax + qCol + VSYNC_SEARCH_COLUMN) * 2;
			const int scoreStart=      int(p[xP + 1]) 
								+ mabs(int((char)(p[xP + 2])))
								+ mabs(int((char)(p[xP])));

			if((vSyncStart&3) == qPhase)
				{
				runningQSum-= scoreStart;
				nRunningQSum--;
				}
			else
				{
				runningNotQSum-= scoreStart;
				nRunningNotQSum--;
				}
			}
		if(vSyncEnd<wndy)
			{
			const int xP= (vSyncEnd*vgax + qCol + VSYNC_SEARCH_COLUMN) * 2;
			const int scoreEnd=        int(p[xP + 1]) 
								+ mabs(int((char)(p[xP + 2])))
								+ mabs(int((char)(p[xP])));

			if((vSyncEnd&3) == qPhase)
				{
				qSum+=scoreEnd;
				runningQSum+= scoreEnd;
				nRunningQSum++;
				}
			else
				{
				notQSum+= scoreEnd;
				runningNotQSum+= scoreEnd;
				nRunningNotQSum++;
				}
			}

		if(nRunningQSum) 	aRunningMeanQ[index]= runningQSum / nRunningQSum;
		if(nRunningNotQSum) aRunningMeanNotQ[index]= runningNotQSum / nRunningNotQSum;
		}

	
	const int qMean= qSum * 4 / (wndy*3);
	const int notQMean= notQSum * 4 / wndy;

	int bestScore= -1000000;
	int bestVSync= -1;

	for(int vSync= 0; vSync<wndy+PAL_VSYNC_SIZE; vSync++)
		{
		if(vSync < wndy && vSync > wndy-MIN_VSYNC_DISTANCE) continue;   
		if(vSync >= wndy && vSync-wndy < MIN_VSYNC_DISTANCE) continue;

		int score= aRunningMeanNotQ[vSync] - aRunningMeanQ[vSync];

//		if(aRunningMeanNotQ[vSync] > VSYNC_OVERBRIGHT_THRESHOLD) score=-1000000;

		if(score>bestScore)
			{
			bestScore= score;
			bestVSync= vSync;
			}
		}
	return bestVSync;
}

// Should be writen as an Object...
//6015X 10960D 994498XX 3224315DD 1789858XD 55n

static int hSyncDelaySumX=  6015;
static int hSyncDelaySumD=  10960;
static int hSyncDelaySumXX= 994498;
static int hSyncDelaySumDD= 3224315;
static int hSyncDelaySumDX= 1789858;
static int hSyncDelays=55;
		
static int lastHSyncDelay=0;
static int lastHSync=0; //from last call to doHSyncDelay
void doHSyncDelay(int hDistance, int hSync)
{
	double sigmaXD=   double(hSyncDelaySumDX) * double(hSyncDelays) 
					- double(hSyncDelaySumX) * double(hSyncDelaySumD);
	sigmaXD/= double(hSyncDelays) * double(hSyncDelays);

	double sigmaX2=   double(hSyncDelaySumXX) * double(hSyncDelays) 
					- double(hSyncDelaySumX) * double(hSyncDelaySumX);
	sigmaX2/= double(hSyncDelays) * double(hSyncDelays);
	if(sigmaX2 <= 0.0) sigmaX2 = 1e-50;

	double diffFromMeanX= double(hDistance) - double(hSyncDelaySumX) / double(hSyncDelays);
	double diffFromMeanD= diffFromMeanX * sigmaXD / sigmaX2;

	/*
		printf("%d %d\n", hDistance, hSync);
	printf("%dX %dD %dXX %dDD %dXD %dn\n", 
		hSyncDelaySumX, hSyncDelaySumD,
		hSyncDelaySumXX, hSyncDelaySumDD,
		hSyncDelaySumDX, hSyncDelays);
	printf("%fSXD %fSX2\n",sigmaXD, sigmaX2);
	printf("%fdfmx %fdfmd\n", diffFromMeanX, diffFromMeanD);

	printf("%f\n", diffFromMeanD + double(hSyncDelaySumD) / double(hSyncDelays) + 0.5);
	*/
	
	
	int delay= int(diffFromMeanD + double(hSyncDelaySumD) / double(hSyncDelays) + 0.5);
	if(delay<0) delay=0;

//	delay= random() % 511;
//	printf("%d Delay\n", delay);

	lastHSyncDelay= delay;
	lastHSync= hSync;

	vPayTvShiftHack(delay);
}

static nStat=0;
static int *statX= new int[10000];
static int *statY= new int[10000];
void analyzeHSyncDelay(int newHSync)
{	
	int realHDistance= newHSync - lastHSync;
	if(newHSync!=-1 && realHDistance>0)
		{
		statX[nStat]= realHDistance;
		statY[nStat++]= lastHSyncDelay;

		hSyncDelaySumX+= realHDistance;
		hSyncDelaySumD+= lastHSyncDelay;
		hSyncDelaySumXX+= realHDistance * realHDistance;
		hSyncDelaySumDD+= lastHSyncDelay * lastHSyncDelay;
		hSyncDelaySumDX+= lastHSyncDelay * realHDistance;
		hSyncDelays++;

		if( mabs(hSyncDelaySumDX) + mabs(hSyncDelaySumDD) + mabs(hSyncDelaySumXX) > 100000000 
				&& hSyncDelays%2==0)
			{
			hSyncDelaySumX/=2;
			hSyncDelaySumD/=2;
			hSyncDelaySumXX/=2;
			hSyncDelaySumDD/=2;
			hSyncDelaySumDX/=2;
			hSyncDelays/=2;
			}
		}

	lastHSyncDelay=0;
	/*
	printf("%dX %dD %dXX %dDD %dXD %dn\n",
			hSyncDelaySumX,
			hSyncDelaySumD,
			hSyncDelaySumXX,
			hSyncDelaySumDD,
			hSyncDelaySumDX,
			hSyncDelays);
	*/

}

// Should be writen as an Object...
static int vSyncDelaySumY=  3;
static int vSyncDelaySumD=  3;
static int vSyncDelaySumYY= 10;
static int vSyncDelaySumDD= 10;
static int vSyncDelaySumDY= 10;
static int vSyncDelays=2;
static int lastVSyncDelay=0;
static int lastVSync=0; //from last call to doVSyncDelay
void doVSyncDelay(int vDistance, int vSync)
{
	double sigmaYD=   double(vSyncDelaySumDY) * double(vSyncDelays) 
					- double(vSyncDelaySumY) * double(vSyncDelaySumD);
	sigmaYD/= double(vSyncDelays) * double(vSyncDelays);

	double sigmaY2=   double(vSyncDelaySumYY) * double(vSyncDelays) 
					- double(vSyncDelaySumY) * double(vSyncDelaySumY);
	sigmaY2/= double(vSyncDelays) * double(vSyncDelays);
	if(sigmaY2 <= 0.0) sigmaY2 = 1e-50;

	double diffFromMeanY= double(vDistance) - double(vSyncDelaySumY) / double(vSyncDelays);
	double diffFromMeanD= diffFromMeanY * sigmaYD / sigmaY2;

	int delay= int(diffFromMeanD + double(vSyncDelaySumD) / double(vSyncDelays) + 0.5);
	if(delay<0) delay=0;

	delay/=2;
	if(delay>500) delay=500;
	delay= random() % 151;
	lastVSyncDelay= delay;
	lastVSync= vSync;

	vPayTvShiftHack(delay*10);
}

void analyzeVSyncDelay(int newVSync)
{
	int realVDistance= newVSync - lastVSync;
	if(newVSync!=-1 && realVDistance>0)
		{

//		statX[nStat]= lastVSyncDelay;
	//	statY[nStat++]= realVDistance;

		vSyncDelaySumY+= realVDistance;
		vSyncDelaySumD+= lastVSyncDelay;
		vSyncDelaySumYY+= realVDistance * realVDistance;
		vSyncDelaySumDD+= lastVSyncDelay * lastVSyncDelay;
		vSyncDelaySumDY+= lastVSyncDelay * realVDistance;
		vSyncDelays++;

		if( mabs(vSyncDelaySumDY) + mabs(vSyncDelaySumDD) + mabs(vSyncDelaySumYY) > 100000000 
				&& vSyncDelays%2==0)
			{
			vSyncDelaySumY/=2;
			vSyncDelaySumD/=2;
			vSyncDelaySumYY/=2;
			vSyncDelaySumDD/=2;
			vSyncDelaySumDY/=2;
			vSyncDelays/=2;
			}
		}

	lastVSyncDelay=0;
	/*
	printf("%dY %dD %dYY %dDD %dYD %dn\n",
			vSyncDelaySumY,
			vSyncDelaySumD,
			vSyncDelaySumYY,
			vSyncDelaySumDD,
			vSyncDelaySumDY,
			vSyncDelays);
	*/

}

void fixColor(int qCol)
{
	/*
	qCol&= ~2;
    byte * const p= &actVid2MemBufp->b[0];

	for(int qLine=qPhase+1; qLine<wndy-3; qLine+=4)
	{
		const float targetPhase= (START_Q_PHASE - Q_PHASE_DELTA*qLine)*0;
		
		int nSamples=0;
		int uSum=0, vSum=0;
		for(int x=qCol+2; x<qCol + QCOL_SIZE-1; x+=2)
		{
			if(x>=wndx) break;
			
			uSum+= char(p[(qLine*vgax + x)*2    ]);
			vSum+= char(p[(qLine*vgax + x)*2 + 2]);
			
			nSamples++;
		}
		
		const float u= ((float)uSum) / nSamples;
		const float v= ((float)vSum) / nSamples;
		
		const float qPhase= atan3(u, v);
		const float phaseDiff= targetPhase - qPhase;

		const int s= (int)(sin(phaseDiff)*127.0);
		const int c= (int)(cos(phaseDiff)*127.0);
		
		printf("%f %f %f %f \n", u, v, atan3(u,v)/PI*180, targetPhase/PI*180);
		for(int line= qLine; line< qLine+4; line++)
		{
			for(int x=0; x<wndx-1; x+=2)
			{
				int ou= (char)p[ (line*vgax + x)*2     ];
				int ov= (char)p[ (line*vgax + x)*2 + 2 ];
				
				int nu= (ou*c - ov*s)>>7;
				int nv= (ou*s + ov*c)>>7;
				
				p[ (line*vgax + x)*2     ]= (char)nu;
				p[ (line*vgax + x)*2 + 2 ]= (char)nv;
           
			}
		}

	}
	*/
}


void fixBrightness(int hSync)
{
	static int darkestVal=0;
	static int brightestVal=255;
	
	darkestVal++;
	brightestVal--;
	
	byte * const p= &actVid2MemBufp->b[0];

	int nClipedPixels= 0;
	
	infoPosY+=10;	
	
	for(int line=1; line<wndy; line++)
    {

        if(hSync + 3>=wndx) break;
		
		double encodedBlankLevel = 0;
		double encodedSyncLevel  = 0;
		
		int nLines= 0;
		
		for(int refLine= max(line-2, 0); refLine<min(wndy, line+2); refLine++)
		{
			if((refLine&3) == qPhase) continue;
			
			nLines++;
			encodedBlankLevel += p[(refLine*vgax + hSync + BLANK_REF_LOC)*2 + 1];
			encodedSyncLevel  += p[(refLine*vgax + hSync + SYNC_REF_LOC )*2 + 1];
		}
		
		encodedBlankLevel/= nLines;
		encodedSyncLevel /= nLines;
		
		const double shift= (RELATIV_LEVEL_SHIFT + (bright - 0x80) / 4.0)* (encodedBlankLevel - encodedSyncLevel);
		
		const double blankLevel = encodedBlankLevel - shift;
		const double syncLevel = encodedSyncLevel - shift;
		
		const int blackLevel = int(blankLevel) ; 
		
		const int scale =  
			int((PAL_BLANK_LEVEL - PAL_SYNC_LEVEL) / max(blankLevel - syncLevel, 1.0) * (256.0 * 255.0) * (contr-0x40+10)/10.0); 
		
		const int brightestPossible= ((255 - blackLevel) * scale) >> 8;
		const int darkestPossible= ((0 - blackLevel) * scale) >> 8;
		
		if(iState>=1)
		{
			for(int x=0; x<256; x++)
			{
				if(x<darkestPossible || x>brightestPossible) 
				{
				p[((infoPosY+5)*vgax + x)*2 + 1]= 255;					
				p[((infoPosY+6)*vgax + x)*2 + 1]= 0;					
					
				p[((infoPosY+5)*vgax + x)*2 ]= 0;					
				p[((infoPosY+6)*vgax + x)*2 ]= 0;					

				}
			}
		}
		

				
		

        for(int x=0; x<min(hSync + BLANK_REF_LOC - 1, wndx); x++)
        {/*
			if(p[(line*vgax + x)*2 + 1] >= brightestVal && (some&1))
			{
				p[(line*vgax + x)*2]= 127;
				
		//		nClipedPixels++;
				
				if(p[(line*vgax + x)*2 + 1] > brightestVal) brightestVal= p[(line*vgax + x)*2 + 1];
			}
			else if( p[(line*vgax + x)*2 + 1] <= darkestVal && (some&1))
			{
				p[(line*vgax + x)*2]= 128;
	
	//			nClipedPixels++;
				
				if(p[(line*vgax + x)*2 + 1] < darkestVal) darkestVal= p[(line*vgax + x)*2 + 1];
			}
			else*/
			{
				int v = p[(line*vgax + x)*2 + 1];
				
				v = ((v - blackLevel) * scale) >> 8;
				
				p[(line*vgax + x)*2 + 1]= minmax(0, v, 255);
				
			}
        }
    }
	

//	static int nClipedPixelSum=0;
	
//	nClipedPixelSum += nClipedPixels;
}

void showStat(void)
{
    byte * const p= &actVid2MemBufp->b[0];

	for(int i=0; i<200; i+=2)
		{
		p[(100*vgax + i)*2 + 1]=128;
		p[(i*vgax + 100)*2 + 1]=128;
		}

	for(int i=0; i<nStat; i++)
		{
		int x= statX[i] + 100;
		int y= statY[i]/10 + 100;
		if(y<0 || x<0) continue;
		if(y>=wndy || x>=wndx) continue;

		int xP= (y*vgax + x)*2;
		p[xP + 1]=255;
		}
}

void movePic(int hSync)
{
    int nPixMove= REAL_HSYNC_LOC - hSync;

    if(nPixMove>wndx) nPixMove=0;
    if(nPixMove<-wndx) nPixMove=0;

    byte * p= &actVid2MemBufp->b[0];
    for(int line=1; line<wndy; line++)
    {
        if(nPixMove>0)
        {
            memmove(&p[nPixMove*2], &p[0], (wndx-nPixMove)*2);
        }
        else if(nPixMove<0)
        {
            memmove(&p[0], &p[-nPixMove*2], (wndx+nPixMove)*2);
        }
        if(nPixMove & 1)
            for(int x=0; x<wndx-2; x++)
            {
                p[x*2]= p[x*2+2];
            }
		for(int x=0; x<wndx-2; x++)
		{
//			p[x*2]=0;
		}
        p+= vgax<<1;
    }
}


extern volatile bool isVPayTvBrightHack;
extern volatile int vPayTvQPosInLCC;
	
void vPayTvDecrypt(void)
{
	static int dropList[10]; //0 -> nothing, 1 -> Drop, 2 -> Drop & Return
	for(int i=0; i<9; i++)
		dropList[i]= dropList[i+1];
	dropList[9]= 0;

 //   if(isVPayTvBrightHack) dropList[1]|=2;
	
	//isVPayTvBrightHack= true;
	isVPayTvBrightHack= false;
	if(dropList[0])
	{
		drop=true;
        if(dropList[0]&2) return;
	}

	int hSync= findHSync();
	int vSync= -1;//findVSync(qCol);

	if(hSync!= -1)
	{
		vPayTvQPosInLCC= (hSync-wndx-12)*1888 / wndx; //FIXME #DEFINE IT
	}

	int qCol;
	if(hSync==-1) 	qCol= -1;
	else 			qCol= hSync - Q_2_HSYNC_DISTANCE;
	
    byte * p= &actVid2MemBufp->b[0];
    for(int line=0; line<wndy; line++)
    {
        if((line&3) == qPhase)
        {
            p[2*qCol+1]=0;
            p[2*qCol+3]=255;
            p[2*hSync+1]=0;
            p[2*hSync+3]=255;
        }
        p+= vgax<<1;
    }

    p= &actVid2MemBufp->b[0];
	for(int x=0; x<wndx; x++)
    {
        if((x&3)==0)
        {
//            p[(vSync*vgax + x)*2 + 1]=0;
 //           p[(vSync*vgax + x)*2 + 3]=255;
        }
    }


	if(lastHSyncDelay!=0)
	{
		analyzeHSyncDelay(hSync);
	}

	if(lastVSyncDelay!=0)
	{
		analyzeVSyncDelay(vSync);
	}

	static bool isVSyncBad= false;
	if(vSync!=-1 && (vSync<TOP_VSYNC_BOUNDRY || vSync>BOTTOM_VSYNC_BOUNDRY) && 0) 
	{
		if(isVSyncBad)
		{
			doVSyncDelay(BOTTOM_VSYNC_BOUNDRY - vSync, vSync);
            dropList[1]|=2;
			isVSyncBad= false;
		}
		else
		{
			isVSyncBad= true;
		}
	}
    else if(hSync!=-1 && hSync<LEFT_HSYNC_BOUNDRY)
	{
		doHSyncDelay((RIGHT_HSYNC_BOUNDRY + LEFT_HSYNC_BOUNDRY)/2 - hSync, hSync);
        dropList[1]|=2;
	}
	else if(hSync==-1 && 0)
	{
		doHSyncDelay(wndx/2, 1);
		lastHSyncDelay=0;
        dropList[1]|=2;
	}
	else if(isVCPhaseTrick)
	{
		isVCPhaseTrick= false;
        vPayTvShiftHack(1000);
        dropList[1]|=2;
	}
	else
	{
		isVSyncBad= false;
	}

    if(hSync==-1) dropList[1]|=2;


	fixBrightness(hSync);
	
	//fixColor(qCol);

//	showStat();
  
	movePic(hSync);

	lastHSync= hSync;        
}
