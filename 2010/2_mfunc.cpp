//2010 0.1 Copyright (C) Michael Niedermayer 1998

#include "2_all.h"
#include "2_mfunc.h"
#include "2_menu.h"
#include "2010.h"
#include "2_71x6.h"

 extern int vgax, y_field, x_field, outy;
 extern int buttons, xpos, ypos, lxpos, lypos;
 extern int me, mx, my;
 extern menu menu_main[mainend+1];
 extern menu menu_hdeci[end_hdeci+1];
 extern menu menu_vdeci[end_vdeci+1];
 extern menu menu_std[end_std+1];
 extern int grabf;
 extern int istate, cstate;
 extern int yuvmode;
 extern tv_form std;
 extern crypt_form crypt;
 extern int nagra_mode, one_field, v_smooth;

 extern volatile u_char saa7196_buf[SAA7196_regs];
 extern int v_smooth;

 int hdeci_state=menuhtapa;
 int vdeci_state=menuvfila;
 int   std_state=menupal;

void grabm(int){
 grabf=1;
}

void infoonoff(int){
 istate^=1;
}

void vsmoothonoff(int){
 v_smooth^=1;
}

void contonoff(int){
 cstate^=1;
}

void menuf_hdeci(int){

 for(int i=0; i<end_hdeci; i++){
   menu_hdeci[i].c.init(menudiscolor);
 }
 menu_hdeci[hdeci_state].c.init(menuencolor);
}

void menuf_vdeci(int){

 for(int i=0; i<end_vdeci; i++){
   menu_vdeci[i].c.init(menudiscolor);
 }
 menu_vdeci[vdeci_state].c.init(menuencolor);
}

void menuf_std(int){

 for(int i=0; i<end_std; i++){
   menu_std[i].c.init(menudiscolor);
 }
 menu_std[std_state].c.init(menuencolor);
}

void hdeci(int num){
 int i;
 
 i=hdeci_state;
 hdeci_state=num;
 if(i==menuhtapa && vdeci_state==menuvfila && num!=menuhtapa){
   double d = ( (one_field==0) ? double(outy>>1) : double(outy) ) / double(y_field);
        if( d <=  4.0/15.0) vdeci(menuvfil2);
   else if( d <= 13.0/15.0) vdeci(menuvfil1);
   else                     vdeci(menuvfilb);
 }

 if(num==menuhtapa) vdeci_state=menuvfila;


 if(num==menuhtapa) i=0x80;
 else               i=0   ;

 write_saa7196(0x28, (saa7196_buf[0x28] & 0x7F) | i );
                           /*   7  Adaptive filter switch
                              6:5  Vertical luminance data processing
                                4  [8] Vertical start of scaling window 
                              3:2  [9:8] Line number per input field
                              1:0  [9:8] Line number per output field      */

 write_saa7196(0x24, (saa7196_buf[0x24] & 0x1F) | (num<<5) );
                           /* 7:5  Horizontal decimation filter
                                4  [8] Horizontal start position of scaling win
                              3:2  [9:8] Pixel number per line on input
                              1:0  [9:8] Pixel number per line on output   */

}

void vdeci(int num){
 int i;
 
 i=vdeci_state;
 vdeci_state=num;
 if(i==menuvfila && hdeci_state==menuhtapa && num!=menuvfila){
   double d = double(vgax) / double(x_field);
        if( d <=  3.0/15.0) hdeci(menuhtap9);
   else if( d <=  7.0/15.0) hdeci(menuhtap5);
   else if( d <= 11.0/15.0) hdeci(menuhtap4);
   else if( d <= 14.0/15.0) hdeci(menuhtap2);
   else                     hdeci(menuhtapb);
 }

 if(num==menuvfila) hdeci_state=menuhtapa;

 if(num==menuvfila) i=0x80;
 else               i=num<<5;

 write_saa7196(0x28, (saa7196_buf[0x28] & 0x1F) | i );
                           /*   7  Adaptive filter switch
                              6:5  Vertical luminance data processing
                                4  [8] Vertical start of scaling window 
                              3:2  [9:8] Line number per input field
                              1:0  [9:8] Line number per output field      */
}

void mstd(int num){
 int y=0, f=0, n=0;

 std_state=num;

 if(num==menupal)        std=PAL,   crypt=nix, y=0, f=0, n=0;
 else if(num==menuntsc)  std=NTSC,  crypt=nix, y=0, f=0, n=0;
 else if(num==menusecam) std=SECAM, crypt=nix, y=0, f=0, n=0;
 else if(num==menuvc)    std=PAL,   crypt=vc , y=1, f=1, n=0;
 else if(num==menunag)   std=PAL,   crypt=nag, y=1, f=1, n=1;

 if(y==0 && yuvmode==1) yuvmode=0;
 if(y==1 && yuvmode==0) yuvmode=1;

 if(f==1){
   vdeci(menuvfilb);
   v_smooth=0;
 }

 if(n==1){
   if(one_field==1 && yuvmode==2) error(MemAlloc);
   nagra_mode=1;
 }
 else
   nagra_mode=0;

 for(int i=0; i<end_std; i++){
   if(num!=i) menu_std[i].c.init(menudiscolor);
   else       menu_std[i].c.init(menuencolor);
 }

 setstd_scale();

 cont_grab();

}
