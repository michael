//2010 0.1 Copyright (C) Michael Niedermayer 1998
#ifndef n2_vc_a_h
#define n2_vc_a_h

int vc_corr(int, int, int, int, int, int);
int vc_corr_mmx(int, int, int, int, int, int);

#endif
