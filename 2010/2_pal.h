//2010 0.1 Copyright (C) Michael Niedermayer 2000
#ifndef n2_pal_h
#define n2_pal_h

#define PAL_FREQ_CHROM     			4433618.75
#define PAL_FREQ_HS        			15625.0
#define PAL_NUM_LINES 				625
#define PAL_PHASE_DRIFT_PER_LINE 	(-fmod(PAL_FREQ_CHROM/PAL_FREQ_HS*4.0, 1.0)/4.0*PI*2.0)
#define PAL_VSYNC_SIZE 				26 //?

#define PAL_BLACK_LEVEL 0.0
#define PAL_BLANK_LEVEL 0.0
#define PAL_WHITE_LEVEL 1.0
#define PAL_SYNC_LEVEL (-43.0 / 100.0) //?
 
#endif
