//2010 0.1 Copyright (C) Michael Niedermayer 1998
#ifndef n2_mmx_h
#define n2_mmx_h

asm(".macro cpuid \n\t"
    ".word 0xA20F \n\t"
    ".endm        \n\t");

asm(".macro emms  \n\t"
    ".word 0x770F \n\t"
    ".endm        \n\t");

#endif
