//2010 0.1 Copyright (C) Michael Niedermayer 1998
#ifndef n2_crypt_a_h
#define n2_crypt_a_h

void VSmooth1RGB1555(int, int, int);
void VSmooth2RGB1555(int, int, int);

void Decomb1(int, int, int);
void Decomb2(int, int, int);

void SetDecombInc(int);

#endif
