//2010 0.1 Copyright (c) 1998 Michael Niedermayer

#include <stdlib.h>
#include <pc.h>
#include <stdio.h>
#include <dpmi.h>
#include <string.h>
#include <sys/nearptr.h>
#include <unistd.h>
#include <conio.h>
#include <keys.h>
#include <crt0.h>
#include <ctype.h>
#include <signal.h>
#include <time.h>
#include "2_all.h"
#include "2010.h"
#include "2_hw.h"
#include "2_hw_mem.h"
#include "2_file.h"
#include "2_grafix.h"
#include "2_crypt.h"
#include "2_71x6.h"
#include "2_nag.h"
#include "2_vc.h"
//#include "2_vpaytv.h"  //REMOVE FIXME (Debuging only)

 extern volatile bool oneField;
 extern int iVSmooth;
 extern int g_mode;
 extern VID2MEMBUF *vid2MemBuf, *actVid2MemBufp;
 extern byte *vidbuf;
 extern volatile int page_flip;
 extern int vgax, vgay, xresvb, xresc, vb, mc;
 extern int active_meteor;
 extern int yuvMode, grabYuvMode;
 extern bool mmx;
 extern int bright, contr, satur;
 extern int showCont;
 extern CRYPTSTD cryptStd;
 extern TVSTD TVStd;
 extern int iState;
 extern bool helpState;
 extern bool allowDrop;
 extern int reqPage, reqSubPage, reqChanNum;
 extern char reqChanStr[];
 extern char findTxt[];
 extern bool isVCPhaseTrick;
extern bool drop;

 int maxBuf=2;
 int some=0;
 int some2=0;

int _crt0_startup_flags = _CRT0_FLAG_NEARPTR | _CRT0_FLAG_NONMOVE_SBRK;
 bool fShowRaw=false;
 int grabf;
 int grabV=0;
 int fysize;
 byte font[4096];
 int vid=0;
 int bpp;
 unsigned short *yuv2RgbLut;
 int edge_lut[256];
 MENULEVEL menuLevel=mLNorm;
 int infoPosX;
 int infoPosY;
 bool strangeRgb16= true;   //  commandLineOpt FIX
 long uclockWaste;
 int vgaBytesPerScanLine;

static inline void getUclockWaste(void){
  long T1=uclock();
  for(int i=0; i<1000; i++) uclock();
  uclockWaste= (uclock()-T1)/1000;
  printf("uclockWaste %d\n", int(uclockWaste));
}

void printg_t(char *text){
 close_hw();
 printf(text);
}

int error(error_code e){
// asm("int $3\n\t");
 switch(e){
  case Lock        : printg_t("\nError (Un)Locking Linear Region!\n");
                     break;
  case NoVDS       : printg_t("\nVDS Not Detected!\n");
                     break;
  case VDS         : printg_t("\nVDS Error!\n");
                     break;
  case NoVESA      : printg_t("\nVESA Not Detected!\n");
                     break;
  case FileRead    : printg_t("\nFile-Read Error!\n");
                     break;
  case Map         : printg_t("\nCannot Map Physical Address!\n");
                     break;
  case VESA        : printg_t("\nError initalizing VESA Mode!\n");
                     break;
  case VESAFlip    : printg_t("\nPage Fliping Error!\n");
                     break;
  case VESAVer     : printg_t("\nError 2010 needs at least VESA 2.0!\n");
                     break;
  case NoMeteor    : printg_t("\nMeteor Not Detected!\n");
                     break;
  case DPMI        : printg_t("\nDPMI Error!\n");
                     break;
  case NoFont      : printg_t("\nNo 2010.Fnt?!\n");
                     break;
  case Font        : printg_t("\n2010.Fnt is damaged!\n");
                     break;
  case FileWrite   : printg_t("\nFile-Write Error!\n");
                     break;
  case MemAlloc    : printg_t("\nMemory allocation error!\n");
                     break;
  case SAA7196     : printg_t("\nPhilips SAA7196 error!\n");
                     break;
  case KeyFile     : printg_t("\nKeyFile error!\n");
                     break;
  case TxtFile     : printg_t("\nTxtFile error!\n");
                     break;
  case DatWrite    : printg_t("\nDatWrite error!\n");
                     break;
  case Align       : printg_t("\nAlign error!\n");
                     break;
  case Nagra       : printg_t("\nNagra error!\n");
                     break;
  case CommLine    : printg_t("\nCommand-Line error!\n");
  case Help        : printg_t("\n2010 [<optons>]\n"
                            "options: -v n (Video-mode)\n"
                            "         -m n (Meteor Card)\n"
                            "         -NoFlip (Disable Page-Fliping)\n"
                            "         -NoMMX  (Disable MMX-Support)\n"
                            "         -MaxBuf (num of Buffers)\n"
                            "         -s <file0.raw> ... <filen.raw> (show raw pics)\n"
                            "         -? / -h (This stuff)\n");
                     break;
  default          : printg_t("\nUnknown error!");
                     printf("error %d\n", int(e));
                     break;
 }

// asm("int $3\n\t");
 exit(e);
}

void exitt(void){
 close_hw();
 saveVCCache();   
}

int main(int argc, char **argv){
 int NoFlip=0;
 char buf[256];
 __dpmi_regs r;
 ModeInfoBlock *mib;
 VesaInfoBlock *vib;
 FILE *f;
 int mc2;

 printf("2010 0.10 Copyright (c) 1998 Michael Niedermayer\n");

 atexit(exitt);

 signal(SIGABRT, sig_handler);
 signal(SIGFPE , sig_handler);
 signal(SIGILL , sig_handler);
 signal(SIGINT , sig_handler);
 signal(SIGSEGV, sig_handler);
 signal(SIGTRAP, sig_handler);

 for(int i=1;i<argc;i++){
  if(stricmp(argv[i],"-v")==0){
    i++;
    sscanf(argv[i],"%d",&vid);
  }
  else if(stricmp(argv[i],"-m")==0){
    i++;
    sscanf(argv[i],"%d",&active_meteor);
  }
  else if(stricmp(argv[i],"-NoFlip")==0){
    NoFlip=1;
  }
  else if(stricmp(argv[i],"-NORGB")==0){
    yuvMode=
    grabYuvMode=2;
  }
  else if(stricmp(argv[i],"-maxBuf")==0){
    i++;
    sscanf(argv[i],"%d",&maxBuf);
  }
  else if(stricmp(argv[i],"-NoMMX")==0){
    mmx=0;
  }
  else if(stricmp(argv[i],"-?")==0 || stricmp(argv[i],"-H")==0){
    error(Help);
  }
/*  else if(stricmp(argv[i],"-v")==0){
   i++;
   if(stricmp(argv[i],"vid")==0) std=vid;
   else if(stricmp(argv[i],"nagra")==0) std=nagra;
   else error(CommLine);
  } */
  else if(stricmp(argv[i],"-s")==0){
   LoadRaws(argc-i-1, &argv[i+1]);
   i=argc;
  } 
  else error(CommLine);
 }

 if(check_mmx()==0) mmx=0;
 else{
  printf("MMX Detected!\n");
  if(mmx==0) printf(" but not used because -NoMMX\n");
 }

 printf("2010.Fnt...\n");
 if((f=fopen("2010.fnt", "rb"))==NULL) error(NoFont);
 fysize=fsize(f)>>8;
 if(fysize==0) error(Font);
 for(int i=0; i<256; i++) fread(font+(i<<4),1,fysize,f);
 fclose(f);
   
 if(scan_pci_meteors() == 0) error(NoMeteor);
// if(active_meteor>=i) active_meteor=0;
// printf("%d Meteor(s) Detected!\n", i);

 for(int i=0; i<256; i++){
   int j=i-2;
   if(j<0) j=0;

        if(j>5 ) j=(j>>1) + 3;
   else if(j>10) j=(j>>2) + 7;
   else if(j>20) j=13;

   edge_lut[i]=j;
 }

 printf("Building YUV to RGB LookUp Tables!\n");
 yuv2RgbLut=new unsigned short[256*128];
 for(int u=-16; u<=15; u++)
    {
    for(int v=-16; v<=15; v++)
        {
        for(int y=0; y<=31; y++)
            {
            int r= minmax(0, int( y + double(v) * 1.375                       +.5), 31); 
            int g= minmax(0, int((y - double(v) * 0.703 - double(u) * 0.343)*2+.5), 63); 
            int b= minmax(0, int( y                     + double(u) * 1.734   +.5), 31); 

            const int xLut= y + ((u&0x1F)<<5) + ((v&0x1F)<<10);
            yuv2RgbLut[ xLut ]= b + (g<<5) + (r<<11);

            }
        }
    }

 if((vib=get_vesa_info())==0) error(NoVESA);
 if(vib->ver < 0x200) error(VESAVer);
 if(vid==0){
  printf("VESA %d.%d detected!\n", (vib->ver>>8) & 255, vib->ver & 255);
 

  for(int i=256;i<512;i++){
   if((mib=get_mode_info(i))!=0){
    if((mib->ModeAttrib & (8+16+128))==8+16+128 && mib->bpp==16
                      && mib->model==6 && (mib->Xres & 0x3F8) == mib->Xres
                                       && (mib->Yres & 0x3FF) == mib->Yres){
                                       // FIX ME 3FF statt 3F8 OK?
     printf("%d = %dx%dx%d",i,mib->Xres,
                              mib->Yres,
                              mib->bpp);
     if(mib->pages==0 || NoFlip==1) printf(" No Page Flipping!");
     printf("\n");
    }
   }
  }
  printf("Input Video-Mode Num:\n");
  buf[0]=255;
  sscanf((char*)cgets(buf),"%d",&vid);
  printf("\n");
 }
 

 if((mib=get_mode_info(vid))==0) error(VESA);
 if(!((mib->ModeAttrib & (8+16+128))==8+16+128 && mib->bpp==16
                      && mib->model==6 && (mib->Xres & 0x3F8) == mib->Xres
                                       && (mib->Yres & 0x3FE) == mib->Yres))
                                                    error(VESA);


 vgax=mib->Xres;
 vgay=mib->Yres;
 mc=mc2=1;
 xresc=vgax<<mc;
 bpp=mib->bpp;
 vb=bpp>>3;
 xresvb=vgax*vb;
 if(mib->pages>0 && NoFlip==0) page_flip=1;
 else page_flip=0;
 vgaBytesPerScanLine= mib->BLine;

 printf("Allocating Physical Continous Buffers...\n");

 vid2MemBuf=(VID2MEMBUF*)(int(&vid2MemBuf)+4);

 if(vgay<370)                                               // +100 for out of array (optimizations...)
   allocCont(vid2MemBuf, (vgax * vgay << 2)+100, &maxBuf);
 else
   allocCont(vid2MemBuf, (vgax * vgay << 1)+100, &maxBuf);

 actVid2MemBufp= &vid2MemBuf[0];

 int i;
 if(page_flip==0) i= vgax * vgay * vb;
 else             i= vgax * vgay * vb << 1;

 vidbuf=(byte*)(map(mib->PhysBase, mib->PhysBase+i))  - __djgpp_base_address;
  
 r.x.ax=0x4F02;
 r.x.bx=vid | 16384;
 __dpmi_int(0x10,&r);
 if(r.x.ax!=0x4F) error(VESA);
 g_mode=1;
  
 init_hw();
 init_meteor();
 getUclockWaste();
 static int delay=0;

 bool quit=false;
 while(!quit){
	//vPayTvBrightHack(false, 0);
   infoPosY=0;
   infoPosX=10;

   if(grabf==2 && TVStd!=TXTPAL) GrabRaw(), grabf=0;
//   if(grabf==2) GrabRaw(), grabf=0; //REMOVE

   if(fShowRaw) ShowRaw();

   decrypt();
   showStuff();
	
	if(!drop)
	{
 		if(grabf==1 && TVStd!=TXTPAL) GrabBmp(), grabf=0;
   
   		if(grabV==1) RecordVideo();
	}

   for(int k=0; k<delay*10000000; k++);

   copy_vidbuffer();

   static char tab[256];
   static int tabNdx;
   static void *tabTo;
   static TABTYPE tabType;
   static bool tabActive=false;
   static int tabLen;

   while(kbhit()){
     int key= getkey();

     if(tabActive){
       tab[tabNdx]=key;
       tabNdx++;
       if(key==K_Escape){
         tabActive=false;
       }
       else if(key==K_Return || tabNdx>=tabLen){
         tab[tabNdx]=0;
         switch(tabType){
           case TAB_DEZ : sscanf(tab, "%d", (int*)tabTo);
                          break;
           case TAB_HEX : sscanf(tab, "%X", (int*)tabTo);
                          break;
           case TAB_STR : sscanf(tab, "%s", (char*)tabTo);
                          break;
         }

         tabActive=false;
       }
       continue;
     }

     switch(menuLevel){
     	case mLNorm : switch(key){
				case 'p'  :
				case 'P'  : getkey();
				break;
				
				case 'r'  : key=0, 
							grabV&=1;
							grabV^=1;
				break;			
                       
					   case 'g'  : key=0, grabf=1;
                       break;
                       case 'G'  : key=0, grabf=2;
                       break;
                       case 'q'  :
                       case 'Q'  : quit=true;
                       break;
                       case 'S'  : fShowRaw=!fShowRaw;
                       break;
                       case K_F1 : bright--;
                                   showCont=10;
                                   if(bright<0)   bright=0;
                                   setCont();
                       break;
                       case K_F2 : bright++;
                                   showCont=10;
                                   if(bright>255) bright=255;
                                   setCont();
                       break;
                       case K_F3 : contr--;
                                   showCont=10;
                                   if(contr<0)   contr=0;
                                   setCont();
                       break;
                       case K_F4 : contr++;
                                   showCont=10;
                                   if(contr>127) contr=127;
                                   setCont();
                       break;
                       case K_F5 : satur--;
                                   showCont=10;
                                   if(satur<0)   satur=0;
                                   setCont();
                       break;
                       case K_F6 : satur++;
                                   showCont=10;
                                   if(satur>127) satur=127;
                                   setCont();
                       break;
                       case 's'  : menuLevel=mLTVStd, helpState=false;
                       break;
                       case 't'  :
                       case 'T'  : isVCPhaseTrick=true;
                       break;
                       case 'i'  :
                       case 'I'  : iState++; iState%=ISTATES;
                       break;
                       case 'v'  :
                       case 'V'  : iVSmooth++; iVSmooth%=3;
                       break;
                       case 'h'  :
                       case 'H'  : helpState= !helpState;
                       break;
                       case 'd'  :
                       case 'D'  : allowDrop= !allowDrop;
                       break;
                       case '+'  : reqPage++;
                       break;
                       case '-'  : reqPage--;
                       break;
                       case K_Up : reqChanNum++;
                       break;
                       case K_Down           : reqChanNum--;
                       break;
                       case K_Right          : reqSubPage++;
                       break;
                       case K_Left           : reqSubPage--;
                       break;
                       case K_Control_KPPlus : reqPage+=10;
                       break;
                       case K_Control_KPDash : reqPage-=10;
                       break;
                       case K_Control_Up     : reqChanNum+=10;
                       break;
                       case K_Control_Down   : reqChanNum-=10;
                       break;
                       case K_Control_Right  : reqSubPage+=10;
                       break;
                       case K_Control_Left   : reqSubPage-=10;
                       break;
                       case '1'  : delay++;
                       break;
                       case '2'  : delay--; if(delay<0) delay=0;
                       break;
                       case '3'  : some++;
                       break;
                       case '4'  : some--; if(some<0) some=0;
                       break;
                       case '5'  : some2++;
                       break;
                       case '6'  : some2--; if(some2<0) some2=0;
                       break;
                       case 'a'  : 
                       case 'A'  : if(TVStd!=TXTPAL) break;
                                   tabActive=true;
                                   tabTo=&reqPage;
                                   tabLen=3;
                                   tabType=TAB_HEX;
                                   tabNdx=0;
                       break;
                       case 'f'  : if(TVStd!=TXTPAL) break;
                                   tabActive=true;
                                   tabTo=&findTxt[1];
                                   tabLen=120;
                                   tabType=TAB_STR;
                                   tabNdx=0;
                                   findTxt[1]=0;
                       case 'F'  : if(TVStd!=TXTPAL) break;
                                   findTxt[0]=1;
                       break;
                     }
       break;
       case mLTVStd : switch(key){
                       case '1'  : TVStd= PAL;
                                   cryptStd= nix;
                                   if(yuvMode==1) yuvMode=0;
                                   grabYuvMode=yuvMode;
                                   setStdScale();
                                   contGrab();
                       break;
                       case '2'  : TVStd= NTSC;
                                   cryptStd= nix;
                                   if(yuvMode==1) yuvMode=0;
                                   grabYuvMode=yuvMode;
                                   setStdScale();
                                   contGrab();
                       break;
                       case '3'  : TVStd= SECAM;
                                   cryptStd= nix;
                                   if(yuvMode==1) yuvMode=0;
                                   grabYuvMode=yuvMode;
                                   setStdScale();
                                   contGrab();
                       break;
                       case '4'  : TVStd= TXTPAL;
                                   cryptStd= nix;
                                   if(yuvMode!=2) yuvMode=0, grabYuvMode=1;
                                   else error(error_code(-988));
                                   iVSmooth=0;
                                   setStdScale();
                                   contGrab();
                       break;
#ifdef CRYPT
                       case '5'  : TVStd= PAL;
                                   cryptStd= vc;
                                   if(yuvMode==0) yuvMode=1;
                                   grabYuvMode=yuvMode;
//                                   iVSmooth=2;
                                   setStdScale();
                                   contGrab();
                       break;
                       case '6'  : TVStd= PAL;
                                   cryptStd= nag;
                                   if(yuvMode==0) yuvMode=1;
                                   grabYuvMode=yuvMode;
//                                   iVSmooth=2;
                                   setStdScale();
                                   contGrab();
                       break;
                       case '7'  : TVStd= PAL;
                                   cryptStd= vpaytv;
                                   if(yuvMode==0) yuvMode=1;
                                   grabYuvMode=yuvMode;
//                                   iVSmooth=2;
                                   setStdScale();
                                   contGrab();
                       break;
#endif
                     }
                     menuLevel=mLNorm;
       break;

     }
   }
 }

 exit(0);
}
