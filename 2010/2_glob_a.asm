;2010 0.1 Copyright (C) Michael Niedermayer 1998

%include "2_all_a.asm"

segment .data

global esp_save
global _asm_temp
global _asm_temp2
global _asm_temp3
global _asm_temp4

segment .bss

;align4

esp_save   resd 1
_asm_temp  resd 1
_asm_temp2 resd 1
_asm_temp3 resd 1
_asm_temp4 resd 1

