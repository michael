//2010 0.1 Copyright (C) Michael Niedermayer 1998

#include <string.h>
#include <stdio.h>
#include <time.h>
#include "2_all.h"
#include "2_crypt.h"
#include "2_hw_mem.h"
#include "2_cryp_a.h"
#include "2_vc.h"
#include "2_nag.h"
#include "2_txt.h"
#include "2_71x6.h"
#include "2_gfunc.h"
#include "2_vpaytv.h"

#define COMB_FIX 256

extern volatile TVSTD TVStd;
extern volatile CRYPTSTD cryptStd;
extern vgax, vgay, wndx, wndy, y_field, outy;
extern VID2MEMBUF *actVid2MemBufp;
extern volatile int scales_x, scalee_x;
extern volatile int scales_y, scalee_y;
extern int yuvMode;
extern volatile bool oneField;
extern int satur;
extern bool iState;
extern int infoPosX, infoPosY;
extern int some;

static void VSmooth(void);
static void Decomb(int q);

int iVSmooth=2;
bool showPoints=false;

 asm("__esp: .long 0\n\t");

void decrypt(void){

 long T1=0, T2;
 char textbuf[3][128];
 if(iState){
   T1=uclock();
 }

 if( cryptStd == nag && satur!=0 ) Decomb(0);
 if( cryptStd == vc  && satur!=0 ) Decomb(1);  

 if(iState){
   T2=uclock();
   sprintf(textbuf[0],"%f DeComb", (float)(T2-T1)/UCLOCKS_PER_SEC);
   T1=T2;
 }

#ifdef CRYPT
 if(cryptStd==vc)       vc_decrypt();
 else if(cryptStd==nag) nag_decrypt();
 else if(cryptStd==vpaytv) vPayTvDecrypt();
#endif

 if(TVStd==TXTPAL) txt_decrypt();

 if(iState){
   T2=uclock();
   sprintf(textbuf[1],"%f DeCrypt", (float)(T2-T1)/UCLOCKS_PER_SEC);
   T1=T2;
 }

 if(!oneField && iVSmooth) VSmooth();

 if(iState){
   T2=uclock();
   color c;
   c.init(255, 0, 0, false);
   sprintf(textbuf[2],"%f VSmooth", (float)(T2-T1)/UCLOCKS_PER_SEC);
   gprint(infoPosX, infoPosY+=10,  c.col, textbuf[0]);
   gprint(infoPosX, infoPosY+=10,  c.col, textbuf[1]);
   gprint(infoPosX, infoPosY+=10,  c.col, textbuf[2]);
   infoPosY+=5;
 }

}


static void Decomb(int q){
 int *temp= (int*)newAlign(max_x * sizeof(int), 8);
 const int outy1= outy>>1;

 const int stride=oneField ? (vgax<<1) : (vgax<<2);
 const int o=oneField ? 0 : 1;
 const int end=oneField ? outy : outy1;

// for(int i=0; i<wndx; i++) temp[i]=128<<8;
 for(int i=0; i<wndx; i++) temp[i]=0;

// SetDecombInc((some<<4) + (some<<20));
 SetDecombInc( 256 + (256<<16) );

 if(q==1){
  int linep= stride*256;
  if(o) linep+=vgax<<1;
  for(int line=256; line>=0; line--){
    Decomb1(int(actVid2MemBufp->b + linep), (wndx<<1), int(temp));
    linep-=stride;
  }
 }

 SetDecombInc( 128 + (128<<16) );

 int linep= 0;
 if(o) linep+=vgax<<1;
 for(int line=0; line<end; line++){
   Decomb2(int(actVid2MemBufp->b + linep), (wndx<<1), int(temp));
   linep+=stride;
 }

 asm("emms\n\t");

 deleteAlign(temp);
}

static void VSmooth(void){

 if(yuvMode==0)
    {
    if(iVSmooth==1)
        VSmooth1RGB1555(int(actVid2MemBufp->b), vgax<<1, vgax*(wndy-3)<<1);
    else
        VSmooth2RGB1555(int(actVid2MemBufp->b), vgax<<1, vgax*(wndy-3)<<1);
    }
 asm("emms\n\t");
}
