//2010 0.1 Copyright (C) Michael Niedermayer 1998
#ifndef n2_crypt_h
#define n2_crypt_h

#define max_x 900
#define max_y 700

#define FREQ_CHROM     4433618.75
#define FREQ_HS        15625.0
#define FREQ_PIX       (FREQ_HS * 1888.0 * 0.5)

void decrypt(void);

#endif
