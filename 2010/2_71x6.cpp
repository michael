//2010 0.1 Copyright (C) Michael Niedermayer 1998
#include <stdio.h>
#include <string.h>
#include <sys/nearptr.h>
#include <go32.h>
#include <dpmi.h>
#include <time.h>
#include <pc.h>
#include "2_all.h"
#include "2_71x6.h"
#include "2_hw.h"
#include "2_hw_mem.h"
#include "2_hw_asm.h"
#include "2010.h"       
                 
//#define wait_time 0.002
//#define wait_time 0.010
#define wait_time 0.020

//#define NoColorDeco

extern meteor meteors[8];
extern __Go32_Info_Block _go32_info_block;
extern u_short my_cs;
extern u_short my_ds;
extern dword old_intmet_vect_offset;
extern  word old_intmet_vect_selector;
extern  byte meteor_handler;
extern volatile int metmode;
extern volatile int active_meteor;
extern int latency;
extern volatile int addr_errors;
extern volatile int corr_errors;
extern volatile int frames, fields;
extern volatile u_char saa7196_buf[SAA7196_regs];
extern volatile TVSTD TVStd;
extern volatile CRYPTSTD cryptStd;
extern volatile int x_field, y_field;
extern volatile int xresc, yuvMode, mc, grabYuvMode;
extern volatile VID2MEMBUF *vid2MemBuf, *actVid2MemBufp;
extern volatile int grabVid2MemBuf, actVid2MemBuf;
extern volatile int stride;
extern volatile bool oneField;
extern int wndx, wndy, outy;
extern int single;
extern int some;

extern int vgax, vgay;

int v_smooth=1;

int bright=0x80, satur=0x40, contr=0x40;
int scales_x=0x03, scales_y= 0x11;           
int scalee_x=0x03, scalee_y= 0x11;               

int bypass_y=0x0, bypasc_y=0x0;

static inline int write_i2c(u_char dev, u_char reg, u_char data);
static inline void wait(double);
static inline void reset_7116(void);
static void start_grab(void);

int scan_pci_meteors(void){
 int i, dev, bus;
 u_long u;

 memset(meteors, -1, sizeof(meteor)*8);

 i=0;
 for(bus=0; bus<32; bus++){
   for(dev=0; dev<32; dev++){
     if(read_pci(bus, dev, 0, PCI_IO_vendev, 4)==SAA7116_vendev){
       meteors[i].bus=bus;
       meteors[i].dev=dev;
       meteors[i].irq=read_pci(bus, dev, 0, PCI_IO_irq, 1);
       if(meteors[i].irq<8) meteors[i].intt=meteors[i].irq
           +_go32_info_block.master_interrupt_controller_base;
       else                 meteors[i].intt=meteors[i].irq
           +_go32_info_block.slave_interrupt_controller_base-8;


       u=read_pci(bus, dev, 0, PCI_IO_command, 2);
       u|= PCI_command_master | PCI_command_memory;
       write_pci(bus, dev, 0, PCI_IO_command, 2, u);
       write_pci(bus, dev, 0, PCI_IO_latency, 1, latency);

       u=read_pci(bus, dev, 0, PCI_IO_base, 4) & (~0xFF);     //fix me 4kb boundray
       meteors[i].saa7116=(saa7116*)( (char*)map(u, u+sizeof(saa7116)) - __djgpp_base_address );

       printf("IRQ: %X INT: %X BUS: %X DEV: %X MemMap: %X\n",meteors[i].irq, meteors[i].intt, bus, dev, int(u));
//       printf("%X %X\n",_go32_info_block.master_interrupt_controller_base, _go32_info_block.slave_interrupt_controller_base);
       i++;
     }
   }
 }
 return i;
}

static inline void reset_7116(void){

 wait(wait_time);

 meteors[active_meteor].saa7116->ints_ctl=(ints_c){
       stat_even: 1, stat_odd: 1, stat_start: 1, res1: 0,
       mask_even: 0, mask_odd: 0, mask_start: 0, res2: 0};

 meteors[active_meteor].saa7116->capt_ctl=(capt_c){
       cont_even: 0, cont_odd: 0, sing_even: 0, sing_odd: 0,
       done_even: 0, done_odd: 0, VRSTN:     0, fifo_en:  0,
       corr_even: 0, corr_odd: 0, aerr_even: 0, aerr_odd: 0,
       res1:      0, corr_dis: 0, range_en:  0, res2:     0};

 wait(0.02);

 meteors[active_meteor].saa7116->capt_ctl=(capt_c){
       cont_even: 0, cont_odd: 0, sing_even: 0, sing_odd: 0,
       done_even: 1, done_odd: 1, VRSTN:     1, fifo_en:  1,
       corr_even: 1, corr_odd: 1, aerr_even: 1, aerr_odd: 1,
       res1:      0, corr_dis: 0, range_en:  1, res2:     0};
 meteors[active_meteor].saa7116->capt_ctl=(capt_c){
       cont_even: 0, cont_odd: 0, sing_even: 0, sing_odd: 0,
       done_even: 0, done_odd: 0, VRSTN:     1, fifo_en:  1,
       corr_even: 0, corr_odd: 0, aerr_even: 0, aerr_odd: 0,
       res1:      0, corr_dis: 0, range_en:  1, res2:     0};
 meteors[active_meteor].saa7116->capt_ctl=(capt_c){
       cont_even: 0, cont_odd: 0, sing_even: 0, sing_odd: 0,
       done_even: 0, done_odd: 0, VRSTN:     1, fifo_en:  0,
       corr_even: 0, corr_odd: 0, aerr_even: 0, aerr_odd: 0,
       res1:      0, corr_dis: 0, range_en:  1, res2:     0};

 wait(0.1);

 meteors[active_meteor].saa7116->capt_ctl=(capt_c){
       cont_even: 0, cont_odd: 0, sing_even: 0, sing_odd: 0,
       done_even: 0, done_odd: 0, VRSTN:     1, fifo_en:  1,
       corr_even: 0, corr_odd: 0, aerr_even: 0, aerr_odd: 0,
       res1:      0, corr_dis: 0, range_en:  1, res2:     0};

 wait(0.05);

// memset((void*)meteors[active_meteor].saa7116, 0, sizeof(saa7116));
}

static inline int write_i2c(u_char dev, u_char reg, u_char data){

 meteors[active_meteor].saa7116->i2c_cmd=
    (i2c_cmd){w_data: data, reg_addr: reg, dev_addr: dev, xfer: 1};
// printf("%X %X %X / %X %X %X\n", dev, reg, data,
//                   meteors[active_meteor].saa7116->i2c_cmd.dev_addr,
//                   meteors[active_meteor].saa7116->i2c_cmd.reg_addr,
//                   meteors[active_meteor].saa7116->i2c_cmd.w_data);
 int count=0x1FFFF;
 while(count > 0 && meteors[active_meteor].saa7116->i2c_cmd.xfer==1) count--;

 if(meteors[active_meteor].saa7116->i2c_stat.d_abort==1){
   meteors[active_meteor].saa7116->i2c_stat.d_abort=1;
   return -1;
 }

 if(count == 0) return -1;

 return 0;
}

void write_saa7196(u_char reg, u_char data){

 saa7196_buf[reg]=data;
 if(write_i2c(SAA7196_I2C_W, reg, data)==-1) error(SAA7196);
}

u_char read_saa7196(u_char reg){

 if(write_i2c(SAA7196_I2C_W, reg, saa7196_buf[reg])==-1) error(SAA7196);
 if(write_i2c(SAA7196_I2C_R, 0, 0)==-1) error(SAA7196);
 return meteors[active_meteor].saa7116->i2c_stat.r_data;
}

static inline void wait(double wt){
 long u;
 wt*=UCLOCKS_PER_SEC;

 u=uclock();

 while(uclock() <= u+wt);
}

volatile bool isVPayTvBrightHack= false;
volatile int vPayTvQPosInLCC= 100;

void meteor_int_handler(void){

 saa7116 *a_saa7116=(saa7116*)meteors[active_meteor].saa7116;
 int cap=a_saa7116->capt_ctl_a;

 if(a_saa7116->ints_ctl.stat_even  == 0 &
    a_saa7116->ints_ctl.stat_odd   == 0 &
    a_saa7116->ints_ctl.stat_start == 0 ) return; //asm("int $3\n\t"); // VERY BAD (?? WHO called that int)

 if((cap & 0x300) != 0){
   corr_errors++;
 }
 if((cap & 0xC00) != 0){   
   addr_errors++;
 }

 fields+= a_saa7116->ints_ctl.stat_even + a_saa7116->ints_ctl.stat_odd;
 frames+= a_saa7116->ints_ctl.stat_start;

// asm("int $3\n\t");

 a_saa7116->capt_ctl_a|=0x0F30;
 if(    a_saa7116->ints_ctl.stat_odd   == 1 && fields<11111
     && a_saa7116->ints_ctl.stat_even  == 0 && fields>=2
     && a_saa7116->ints_ctl.stat_start == 0                 ){
   fields=0;

   if( vid2MemBuf[ grabVid2MemBuf ].state == Grabbing )
     vid2MemBuf[ grabVid2MemBuf ].state= Grabbed;

   grabVid2MemBuf++;
   if( grabVid2MemBuf >= vid2MemBuf[0].num ) grabVid2MemBuf= 0;

   if( vid2MemBuf[ grabVid2MemBuf ].state == Empty ){
     vid2MemBuf[ grabVid2MemBuf ].state= Grabbing;
     grab_next( vid2MemBuf[ grabVid2MemBuf ] );
   }
   else{
     fields=11111;
     a_saa7116->capt_ctl=(capt_c){
         cont_even: 0, cont_odd: 0, sing_even: 0, sing_odd: 0,
         done_even: 1, done_odd: 1, VRSTN:     1, fifo_en:  1,
         corr_even: 1, corr_odd: 1, aerr_even: 1, aerr_odd: 1,
         res1:      0, corr_dis: 0, range_en:  1, res2:     0};
     grab_next( vid2MemBuf[ grabVid2MemBuf ] );
   }
			
 }
 else if(    a_saa7116->ints_ctl.stat_start==1 && fields>=11111
          && vid2MemBuf[ grabVid2MemBuf ].state == Empty        ){
   fields=0;
   a_saa7116->capt_ctl=(capt_c){
       cont_even: 1, cont_odd: 1, sing_even: 0, sing_odd: 0,
       done_even: 1, done_odd: 1, VRSTN:     1, fifo_en:  1,
       corr_even: 1, corr_odd: 1, aerr_even: 1, aerr_odd: 1,
       res1:      0, corr_dis: 0, range_en:  1, res2:     0};
   vid2MemBuf[ grabVid2MemBuf ].state= Grabbing;
 }
	
	
	if(cryptStd == vpaytv 
		&& (   a_saa7116->ints_ctl.stat_odd   == 1
			|| a_saa7116->ints_ctl.stat_even  == 1) )
	{
		isVPayTvBrightHack= !isVPayTvBrightHack;
		vPayTvBrightHack( isVPayTvBrightHack, vPayTvQPosInLCC );
	}
	

	
 a_saa7116->ints_ctl_a|=0x7;
}

void close_meteor(void){
 __dpmi_paddr address;

 meteors[active_meteor].saa7116->ints_ctl=(ints_c){
       stat_even: 1, stat_odd: 1, stat_start: 1, res1: 0,
       mask_even: 0, mask_odd: 0, mask_start: 0, res2: 0};
 meteors[active_meteor].saa7116->capt_ctl=(capt_c){
       cont_even: 0, cont_odd: 0, sing_even: 0, sing_odd: 0,
       done_even: 0, done_odd: 0, VRSTN:     0, fifo_en:  0,
       corr_even: 0, corr_odd: 0, aerr_even: 0, aerr_odd: 0,
       res1:      0, corr_dis: 0, range_en:  0, res2:     0};
 memset((void*)meteors[active_meteor].saa7116, 0, sizeof(saa7116));

 if(metmode==0) return;

   /* mask meteor's IRQ */
 asm("cli\n\t");
 if(meteors[active_meteor].irq<8)
      outportb(0x21, inportb(0x21) | (1 <<  meteors[active_meteor].irq     ));
 else outportb(0xA1, inportb(0xA1) | (1 << (meteors[active_meteor].irq - 8)));
 asm("sti\n\t");

 address.offset32=old_intmet_vect_offset;
 address.selector=old_intmet_vect_selector;
 __dpmi_set_protected_mode_interrupt_vector(
                                      meteors[active_meteor].intt, &address);

// unlock(meteor_int_handler, meteor_int_handler_end); FIX ME check if lock can handle multiple

  metmode=0;
}

void grab_next(VID2MEMBUF v2mb){
 saa7116 *a_saa7116=(saa7116*)meteors[active_meteor].saa7116;

 a_saa7116->dma_end.even=
 a_saa7116->dma_end.odd= 0;

 if(oneField){
   a_saa7116->dma.even[0]= a_saa7116->dma.odd [0]= 
   a_saa7116->dma.even[1]= a_saa7116->dma.odd [1]= 
   a_saa7116->dma.even[2]= a_saa7116->dma.odd [2]= v2mb.phys;
 }else{                                               
   a_saa7116->dma.even[0]= 
   a_saa7116->dma.even[1]= 
   a_saa7116->dma.even[2]= v2mb.phys;
   a_saa7116->dma.odd [0]=
   a_saa7116->dma.odd [1]=
   a_saa7116->dma.odd [2]= v2mb.phys + xresc;
 }

 a_saa7116->dma_end.even=
 a_saa7116->dma_end.odd= v2mb.phys + xresc*outy - 1;

}


void setCont(void){

 write_saa7196(0x12, satur);/*7:0  Chrominance saturation control for VRAM port (Signed (: Negativ Saturation) */
 write_saa7196(0x13, contr);/*7:0  Luminance contract control for VRAM port */
 write_saa7196(0x19,bright);/*7:0  Luminance brightness control for VRAM port */

}

void setStdScale(void){
 saa7116 *a_saa7116=(saa7116*)meteors[active_meteor].saa7116;
 int i;

 mc=1;

	if(cryptStd == vpaytv)	satur=0x18;
	else 					satur=0x40;
	setCont();
	
 if(TVStd==TXTPAL){
   scales_y=0x0 + 100, scalee_y=0x0 -288 + 20 + 100;
   xresc=vgax<<(mc+2);
   wndx=768;
 }
 else{ 
   scales_y=0x11, scalee_y=0x11;
   xresc=vgax<<mc;
   wndx=vgax;
 }

 if(TVStd==NTSC)
    y_field=240-scales_y+scalee_y, x_field=640-scales_x+scalee_x;
 else
    y_field=288-scales_y+scalee_y, x_field=768-scales_x+scalee_x;

 if(vgay-70 <= y_field && (cryptStd!=nag && TVStd!=TXTPAL)) oneField=true;
 else                                                       oneField=false;

 int yrest;
 if(TVStd==TXTPAL)                           yrest= 40;
 else if(cryptStd!=nag || vgay-70 > y_field) yrest= vgay;
 else                                        yrest= vgay << 1;

 if     (!oneField && yrest>y_field*2){
   outy= y_field*2;
   wndx= int( double(wndx) * double(outy) / double(yrest) );
 }
 else if(oneField && yrest>y_field  ){
   outy= y_field;
   wndx= int( double(wndx) * double(outy) / double(yrest) );
 }
 else{
   outy= yrest;
 }

 if(wndx > vgax && TVStd!=TXTPAL){
//   outy= int( double(outy) * double(x_field) / double(vgax) ); // ?! JOKE
   wndx= vgax;
 }

 if(wndx>x_field) wndx=x_field; // no scaling !?

 wndx =(wndx + 7) & 0x3F8;
 outy =(outy + 1) & 0x3FE;

 if(wndx > vgax ) wndx-=8;
 if(outy > yrest) outy-=2;

 if(TVStd==TXTPAL)  stride=((vgax<<3) - wndx)<<mc;
 else if(!oneField) stride=((vgax<<1) - wndx)<<mc;
 else               stride=( vgax     - wndx)<<mc;

 if((cryptStd!=nag && TVStd!=TXTPAL) || vgay-70 > y_field) wndy= outy;
 else                                                      wndy= outy >> 1;


 if(TVStd==TXTPAL) write_saa7196(0x06, 0xC6);            
 else              write_saa7196(0x06, 0x46);            
                           /*   7  Input mode =0 CVBS, =1 S-Video
                                6  Pre filter
                              5:4  Aperture Bandpass characteristics
                              3:2  Coring range for high freq
                              1:0  Aperture bandpass filter weights        */

 if(TVStd==SECAM) write_saa7196(0x0D, 0x85);
 else             write_saa7196(0x0D, 0x84); //0x84 
                           /*   7  VTR/TV mode bit = 1->VTR mode
                                3  Realtime output mode select bit
                                2  HREF position select
                                1  Status byte select
                                0  SECAM mode bit                          */

 if(TVStd==SECAM) write_saa7196(0x0F, 0xB0);
 else             write_saa7196(0x0F, 0x90); //90        
                           /*   7  Automatic Field detection
                                6  Field Select 0 = 50hz, 1=60hz
                                5  SECAM cross-colour reduction
                                4  Enable sync and clamping pulse
                              3:1  Luminance delay compensation            */
 if(cryptStd==vpaytv)
    write_saa7196(0x10, 0x02);
 else
    write_saa7196(0x10, 0x00); //0
                           /*   2  Select HREF Position
                              1:0  Vertical noise reduction                */

 write_saa7196(0x0C, 0xC0);  //0xC0
                           /*   7  Colour-on bit
                              6:5  AGC filter                              */

 if(cryptStd==vpaytv)
    write_saa7196(0x0E, 0xB8); //B8
 else
    write_saa7196(0x0E, 0x38); //38


                           /*   7  Horizontal clock PLL //38 / B8
                                5  Select interal/external clock source
                                4  Output enable of Horizontal/Vertical sync
                                3  Data output YUV enable
                                2  S-VHS bit
                                1  GPSW2
                                0  GPSW1                                   */


// if(some-0x60>=124 ) some=123+0x60;
 write_saa7196(0x01, 0x30);/* 7:0  Horizontal Sync Begin for 50hz          */
 write_saa7196(0x02, 0x00);/* 7:0  Horizontal Sync Stop for 50hz           */
 write_saa7196(0x03, 0xE8);/* 7:0  Horizontal Sync Clamp Start for 50hz    */
 write_saa7196(0x04, 0xB6);/* 7:0  Horizontal Sync Clamp Stop for 50hz     */
 write_saa7196(0x05, 0xF4);/* 7:0  Horizontal Sync Start after PH1 for 50hz */

 //TEST
// write_saa7196(0x11, some);/* 7:0  Chrominance gain conrtol for QAM        */


 if(grabYuvMode==0){
/*   if(oneField) write_saa7196(0x20, 0xF2);
   else         write_saa7196(0x20, 0x92);
   a_saa7116->rt_mode_e=a_saa7116->rt_mode_o=
      (rt_mode){mode: 0x00, route: 0x393939};*/
   if(oneField) write_saa7196(0x20, 0xF0);
   else         write_saa7196(0x20, 0x90);
   a_saa7116->rt_mode_e=a_saa7116->rt_mode_o=
      (rt_mode){mode: 0x01, route: 0xeeeeee};
 }
 else{
   if(oneField) write_saa7196(0x20, 0xF1);
   else         write_saa7196(0x20, 0x91);
   a_saa7116->rt_mode_e=a_saa7116->rt_mode_o=
      (rt_mode){mode: 0x41, route: 0xeeeeee};
 }
                           /*   7  ROM table bypass switch
                              6:5  Set output field mode
                                4  VRAM port outputs enable
                              3:2  First pixel position in VRO data
                              1:0  FIFO output register select             */
 write_saa7196(0x21, wndx & 0xFF);
                           /* 7:0  [7:0] Pixel number per line on output   */
 write_saa7196(0x22, x_field & 0xFF);
                           /* 7:0  [7:0] Pixel number per line on input    */
 write_saa7196(0x23, scales_x);/* 7:0  [7:0] Horizontal start position of scaling win*/
 i=saa7196_buf[0x24] & 0xE0;
 if(TVStd==TXTPAL) i=0x80;
 write_saa7196(0x24, i | ((x_field>>6) & 0x0C) | (wndx>>8));
                           /* 7:5  Horizontal decimation filter
                                4  [8] Horizontal start position of scaling win
                              3:2  [9:8] Pixel number per line on input
                              1:0  [9:8] Pixel number per line on output   */
 if(oneField) write_saa7196(0x25,  outy     & 0xFF);
 else         write_saa7196(0x25, (outy>>1) & 0xFF);
                           /* 7:0  [7:0] Line number per output field      */
 write_saa7196(0x26, y_field & 0xFF);
                           /* 7:0  [7:0] Line number per input field       */
 write_saa7196(0x27, scales_y);/* 7:0  [7:0] Vertical start of scaling window  */
 if(TVStd==TXTPAL) i=0x00;
 else              i=saa7196_buf[0x28] & 0xE0;
 if(oneField) write_saa7196(0x28, i | ((y_field>>6) & 0x0C) | (outy>>8));
 else         write_saa7196(0x28, i | ((y_field>>6) & 0x0C) | (outy>>9));
                           /*   7  Adaptive filter switch
                              6:5  Vertical luminance data processing
                                4  [8] Vertical start of scaling window 
                              3:2  [9:8] Line number per input field
                              1:0  [9:8] Line number per output field      */

 write_saa7196(0x29, bypass_y);/* 7:0  [7:0] Vertical bypass start             */
 write_saa7196(0x2A, bypasc_y);/* 7:0  [7:0] Vertical bypass count             */

 printf("%d %d %d %d %d %d %d %d\n",vgax, vgay , wndx, wndy, outy,
                     y_field , x_field, stride);

    // FIX (clear vga mem)
}

void init_meteor(void){
 __dpmi_paddr address;
 saa7116 *a_saa7116=(saa7116*)meteors[active_meteor].saa7116;

 if(__dpmi_get_protected_mode_interrupt_vector(
                      meteors[active_meteor].intt, &address)!=0) error(DPMI);
 old_intmet_vect_offset  =address.offset32;
 old_intmet_vect_selector=address.selector;

 metmode=1;

 address.offset32=(u_long)(&meteor_handler);
 address.selector=my_cs;  
 if(__dpmi_set_protected_mode_interrupt_vector(
                     meteors[active_meteor].intt, &address)!=0) error(DPMI);

   /* unmask meteor's IRQ */
 asm("cli\n\t");
 if(meteors[active_meteor].irq<8)
      outportb(0x21, inportb(0x21) & ~(1 <<  meteors[active_meteor].irq     ));
 else outportb(0xA1, inportb(0xA1) & ~(1 << (meteors[active_meteor].irq - 8)));
 asm("sti\n\t");

   /* init saa7116 */

 reset_7116();
 a_saa7116->capt_ctl=(capt_c){
       cont_even: 0, cont_odd: 0, sing_even: 0, sing_odd: 0,
       done_even: 0, done_odd: 0, VRSTN:     1, fifo_en:  0,
       corr_even: 0, corr_odd: 0, aerr_even: 0, aerr_odd: 0,
       res1:      0, corr_dis: 0, range_en:  0, res2:     0};  /* end reset / range contr */
 wait(wait_time);

 a_saa7116->dma.even[0]= a_saa7116->dma.odd[0]=
 a_saa7116->dma.even[1]= a_saa7116->dma.odd[1]= 
 a_saa7116->dma.even[2]= a_saa7116->dma.odd[2]= 0;
 a_saa7116->stride.even[0]= a_saa7116->stride.odd[0]= 
 a_saa7116->stride.even[1]= a_saa7116->stride.odd[1]= 
 a_saa7116->stride.even[2]= a_saa7116->stride.odd[2]= 0;

 a_saa7116->rt_mode_e= a_saa7116->rt_mode_o=
       (rt_mode){mode: 0x01, route: 0xEEEEEE};                   /* RGB 16 */
 a_saa7116->fifo_trigger= (ff_trig){packed: 0x20, planar: 0x20};  /* both 20h */
 a_saa7116->field_toggle= 0x107;                   /* enab field tog ,reserveds*/
 a_saa7116->capt_ctl=(capt_c){
       cont_even: 0, cont_odd: 0, sing_even: 0, sing_odd: 0,
       done_even: 0, done_odd: 0, VRSTN:     1, fifo_en:  1,
       corr_even: 0, corr_odd: 0, aerr_even: 0, aerr_odd: 0,
       res1:      0, corr_dis: 0, range_en:  0, res2:     0}; 
 a_saa7116->retry_wait= 0;
 a_saa7116->ints_ctl=(ints_c){
       stat_even: 1, stat_odd: 1, stat_start: 1, res1: 0,
       mask_even: 0, mask_odd: 0, mask_start: 0, res2: 0}; /* disable / clear all ints*/
 a_saa7116->field_mask=(eo){even: 0x1, odd: 0x1};              /* This and */
 a_saa7116->mask_lengths= 0;               /* that means capture all fields*/
 a_saa7116->fifo_limits=0x5007C;
 a_saa7116->i2c_clocks=0x461E1E0F;
 a_saa7116->i2c_stat_a=0x300;
 a_saa7116->i2c_cmd_a=0;
 a_saa7116->i2c_auto.even[0]=  a_saa7116->i2c_auto.odd[0]=
 a_saa7116->i2c_auto.even[1]=  a_saa7116->i2c_auto.odd[1]=
 a_saa7116->i2c_auto.even[2]=  a_saa7116->i2c_auto.odd[2]=
 a_saa7116->i2c_auto.even[3]=  a_saa7116->i2c_auto.odd[3]= 0;
 a_saa7116->i2c_regs_enable= 0;

 a_saa7116->dma_end=(eo){even: ~0, odd: ~0};
 meteors[active_meteor].maxRange=
     (a_saa7116->dma_end.even & a_saa7116->dma_end.odd) | 0xFF;
 a_saa7116->dma_end=(eo){even: 0, odd: 0};
 printf("maxRange = %X\n", int(meteors[active_meteor].maxRange));

 frames=corr_errors=addr_errors=0;

 /* init SAA7196 */

 write_saa7196(0x00, 0x50);/* 7:0  Increment Delay                         */

 write_saa7196(0x01, 0x30);/* 7:0  Horizontal Sync Begin for 50hz          */
 write_saa7196(0x02, 0x00);/* 7:0  Horizontal Sync Stop for 50hz           */
 write_saa7196(0x03, 0xE8);/* 7:0  Horizontal Sync Clamp Start for 50hz    */
 write_saa7196(0x04, 0xB6);/* 7:0  Horizontal Sync Clamp Stop for 50hz     */
 write_saa7196(0x05, 0xF4);/* 7:0  Horizontal Sync Start after PH1 for 50hz */

 write_saa7196(0x07, 0x00);/* 7:0  Hue                                     */
 write_saa7196(0x08, 0x7F);/* 7:3  Colour-killer threshold QAM (PAL, NTSC) */
 write_saa7196(0x09, 0x7F);/* 7:3  Colour-killer threshold SECAM           */
 write_saa7196(0x0A, 0x7F);/* 7:0  PAL switch sensitivity                  */
 write_saa7196(0x0B, 0x7F);/* 7:0  SECAM switch sensitivity                */
	
 write_saa7196(0x11, 0x2C);/* 7:0  Chrominance gain conrtol for QAM        */
 write_saa7196(0x14, 0x34);/* 7:0  Horizontal sync begin for 60hz          */
//#ifdef notdef
 write_saa7196(0x15, 0x0A);/* 7:0  Horizontal sync stop for 60hz           */
 write_saa7196(0x16, 0xF4);/* 7:0  Horizontal clamp begin for 60hz         */
 write_saa7196(0x17, 0xCE);/* 7:0  Horizontal clamp stop for 60hz          */
 write_saa7196(0x18, 0xF4);/* 7:0  Horizontal sync start after PH1 for 60hz */
//#else
//   0c fb d4 ec             0x0a, 0xf4, 0xce, 0xf4,
//#endif
 write_saa7196(0x1A, 0x00);
 write_saa7196(0x1B, 0x00);
 write_saa7196(0x1C, 0x00);
 write_saa7196(0x1D, 0x00);
 write_saa7196(0x1E, 0x00);
 write_saa7196(0x1F, 0x00);
 write_saa7196(0x2B, 0x00);/*   4  [8] Vertical bypass start
			     2  [8] Vertical bypass count
			     0	Polarity, internally detected odd even flag */
 write_saa7196(0x2C, 0x80);/* 7:0  Set lower limit V for colour-keying     */
 write_saa7196(0x2D, 0x7F);/* 7:0  Set upper limit V for colour-keying     */
 write_saa7196(0x2E, 0x80);/* 7:0  Set lower limit U for colour-keying     */
 write_saa7196(0x2F, 0x7F);/* 7:0  Set upper limit U for colour-keying     */
 write_saa7196(0x30, 0x9F);/*   7  VRAM bus output format
                                6  Adaptive geometrical filter
                                5  Luminance limiting value
                                4  Monochrome and two's complement output data sel
                                3  Line quailifier flag
                                2  Pixel qualifier flag
                                1  Transparent data transfer
                                0  Extended formats enable bit             */

 setCont();
 setStdScale();

// for(int c=0; c<0x30; c++){
//   printf("%X, %X, %X\n",c, read_saa7196(c), saa7196_buf[c]);
// }

 start_grab();
}

static void start_grab(void){
 saa7116 *a_saa7116=(saa7116*)meteors[active_meteor].saa7116;

 a_saa7116->ints_ctl=(ints_c){
       stat_even: 1, stat_odd: 1, stat_start: 1, res1: 0,
       mask_even: 0, mask_odd: 0, mask_start: 0, res2: 0};

 wait(wait_time);

 volatile VID2MEMBUF grabVid2MemBufp= vid2MemBuf[ grabVid2MemBuf ];
                             
 if(oneField){
   a_saa7116->dma.even[0]= a_saa7116->dma.odd [0]=
   a_saa7116->dma.even[1]= a_saa7116->dma.odd [1]=
   a_saa7116->dma.even[2]= a_saa7116->dma.odd [2]=
                                           grabVid2MemBufp.phys;     
   a_saa7116->stride.even[0]=a_saa7116->stride.odd[0]= 
   a_saa7116->stride.even[1]=a_saa7116->stride.odd[1]= 
   a_saa7116->stride.even[2]=a_saa7116->stride.odd[2]=
                                                      stride;
 }else{                                               
   a_saa7116->dma.even[0]= 
   a_saa7116->dma.even[1]= 
   a_saa7116->dma.even[2]=
                     grabVid2MemBufp.phys;
   a_saa7116->dma.odd [0]=
   a_saa7116->dma.odd [1]=
   a_saa7116->dma.odd [2]=
                     grabVid2MemBufp.phys + xresc;
   a_saa7116->stride.even[0]=a_saa7116->stride.odd[0]= 
   a_saa7116->stride.even[1]=a_saa7116->stride.odd[1]= 
   a_saa7116->stride.even[2]=a_saa7116->stride.odd[2]=
                                                      stride;
 }
 a_saa7116->dma_end.even=
 a_saa7116->dma_end.odd= grabVid2MemBufp.phys + xresc*outy - 1;

  

 wait(wait_time);

 a_saa7116->capt_ctl=(capt_c){
       cont_even: 1, cont_odd: 1, sing_even: 0, sing_odd: 0,
       done_even: 1, done_odd: 1, VRSTN:     1, fifo_en:  1,
       corr_even: 1, corr_odd: 1, aerr_even: 1, aerr_odd: 1,
       res1:      0, corr_dis: 0, range_en:  1, res2:     0};
 a_saa7116->ints_ctl=(ints_c){
       stat_even: 1, stat_odd: 1, stat_start: 1, res1: 0,
       mask_even: 1, mask_odd: 1, mask_start: 1, res2: 0};
 
}

void contGrab(void){
 saa7116 *a_saa7116=(saa7116*)meteors[active_meteor].saa7116;

 if(oneField){
   a_saa7116->stride.even[0]=a_saa7116->stride.odd[0]= 
   a_saa7116->stride.even[1]=a_saa7116->stride.odd[1]= 
   a_saa7116->stride.even[2]=a_saa7116->stride.odd[2]=
                                                      stride;
 }else{                                               
   a_saa7116->dma.odd [0]=
   a_saa7116->dma.odd [1]=
   a_saa7116->dma.odd [2]=
                     a_saa7116->dma.even[0] + xresc;
   a_saa7116->stride.even[0]=a_saa7116->stride.odd[0]= 
   a_saa7116->stride.even[1]=a_saa7116->stride.odd[1]= 
   a_saa7116->stride.even[2]=a_saa7116->stride.odd[2]=
                                                      stride;
 }
 a_saa7116->dma_end.even=
 a_saa7116->dma_end.odd= a_saa7116->dma.even[0] + xresc*outy - 1;

  

 wait(wait_time);

}

void doTxtHack(int txtHackState){
//  return; //REMOVE
  switch(txtHackState){
    case 8 ... 100:
                 write_saa7196(0x10, 0x00);
                           /*   2  Select HREF Position
                              1:0  Vertical noise reduction                */
                 write_saa7196(0x0F, 0x90);        
                           /*   7  Automatic Field detection
                                6  Field Select 0 = 50hz, 1=60hz
                                5  SECAM cross-colour reduction
                                4  Enable sync and clamping pulse
                              3:1  Luminance delay compensation            */
    break;
    case 7:
                 write_saa7196(0x10, 0x02);
                           /*   2  Select HREF Position
                              1:0  Vertical noise reduction                */
                 write_saa7196(0x0F, 0x50);        
                           /*   7  Automatic Field detection
                                6  Field Select 0 = 50hz, 1=60hz
                                5  SECAM cross-colour reduction
                                4  Enable sync and clamping pulse
                              3:1  Luminance delay compensation            */
    break;
    case 6:
                 write_saa7196(0x10, 0x02);
                           /*   2  Select HREF Position
                              1:0  Vertical noise reduction                */
                 write_saa7196(0x0F, 0x90);        
                           /*   7  Automatic Field detection
                                6  Field Select 0 = 50hz, 1=60hz
                                5  SECAM cross-colour reduction
                                4  Enable sync and clamping pulse
                              3:1  Luminance delay compensation            */

    break;
  }
}

void vPayTvShiftHack(int amount)
{
//	__dpmi_yield();

	write_saa7196(0x0F, 0x50);

//	for(int i=1000*amount; i>0; i--);
	for(int i=0; i<10000*amount; i++);

    write_saa7196(0x0F, 0x10);
}


void vPayTvBrightHack(bool b, int posInLCC)
    {
		

/*
	int hSyncBegin= 		- 96 +posInLCC;
	int hSyncStop= 			   0 +posInLCC;
	int hClampBegin=		  48 +posInLCC;
	int hClampStop=			 148 +posInLCC;
	int hSyncStartAfterPH1=	  96;//+posInLCC;
*/


	int hSyncBegin=         - 96 +posInLCC;
	int hSyncStop= 			- 70 +posInLCC;
	int hClampBegin=		- 60 +posInLCC;
    int hClampStop=         - 59 +posInLCC+some/2;
	int hSyncStartAfterPH1=	  96;//+posInLCC;

	if(some/2==0) hClampStop=         148 +posInLCC;
	//else 		hClampStop=         -59 +posInLCC + 10;

	hSyncBegin = minmax(-382, hSyncBegin,  126);
	hSyncStop  = minmax(-380, hSyncStop,   128);
	hClampBegin= minmax(-254, hClampBegin, 254);
	hClampStop = minmax(-252, hClampStop,  256);

	hSyncBegin/=2; 
	hSyncStop/=2; 	
	hClampBegin/=2;
	hClampStop/=2;
	hSyncStartAfterPH1/=8;

	if(hSyncStop <= hSyncBegin) hSyncStop= hSyncBegin+1;
	if(hClampBegin <= hSyncStop) hClampBegin= hSyncStop+1;
	if(hClampStop <= hClampBegin) hClampStop= hClampBegin+1;

    if(b ^ (some&1))
        {
		write_saa7196(0x01, (-hSyncBegin-3        )&0xFF);/* 7:0  Horizontal Sync Begin for 50hz          */
 		write_saa7196(0x02, (-hSyncBegin        )&0xFF);/* 7:0  Horizontal Sync Begin for 50hz          */
  //      write_saa7196(0x02, (-hSyncStop       )&0xFF);/* 7:0  Horizontal Sync Stop for 50hz           */
        write_saa7196(0x03, (-hClampBegin-3       )&0xFF);/* 7:0  Horizontal Sync Clamp Start for 50hz    */
        write_saa7196(0x04, (-hClampBegin       )&0xFF);/* 7:0  Horizontal Sync Clamp Start for 50hz    */
  //      write_saa7196(0x04, (-hClampStop        )&0xFF);/* 7:0  Horizontal Sync Clamp Stop for 50hz     */
        write_saa7196(0x05, (-hSyncStartAfterPH1)&0xFF);/* 7:0  Horizontal Sync Start after PH1 for 50hz */
        }		
    else
        {
        write_saa7196(0x01, (-hSyncBegin        )&0xFF);/* 7:0  Horizontal Sync Begin for 50hz          */
        write_saa7196(0x02, (-hSyncStop         )&0xFF);/* 7:0  Horizontal Sync Stop for 50hz           */
        write_saa7196(0x03, (-hClampBegin       )&0xFF);/* 7:0  Horizontal Sync Clamp Start for 50hz    */
        write_saa7196(0x04, (-hClampStop        )&0xFF);/* 7:0  Horizontal Sync Clamp Stop for 50hz     */
        write_saa7196(0x05, (-hSyncStartAfterPH1)&0xFF);/* 7:0  Horizontal Sync Start after PH1 for 50hz */
        }
 /*
	printf("T %d %d %d %d %d\n"
		,(-hSyncBegin        )&0xFF
		,(-hSyncStop         )&0xFF
		,(-hClampBegin       )&0xFF
		,(-hClampStop        )&0xFF
		,(-hSyncStartAfterPH1)&0xFF);
*/
	
    }

		
void meteor_int_handler_end(void){
}

