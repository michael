;2010 0.1 Copyright (C) Michael Niedermayer 1998

%include "2_all_a.asm"

segment .data

extern _mmx
extern esp_save
extern _asm_temp
global _vc_corr__Fiiiiii
global _vc_corr_mmx__Fiiiiii

%define line1a       par1
%define line1b       par2
%define count1       par3
%define line2a       par4
%define line2b       par5
%define count2       par6

%define mline1a       par_1
%define mline1b       par0 
%define mcount1       par1
%define mline2a       par2
%define mline2b       par3
%define mcount2       par4

_vc_corr__Fiiiiii:
 push ebx

 push ecx
 push edx

 push esi
 push edi

 push ebp
 mov esi, [line1a]

 mov edi, [line1b]
 mov [esp_save], esp

 xor ecx, ecx
 mov esp, [count1]

 xor ebp, ebp
 xor edx, edx

 cmp esp, byte 4
  jb skip1

 mov eax, [esp + esi]
 mov ebx, [esp + edi]
 or eax, 0x80008000        

NoMMXloop:
 sub edx, ecx                ; U
 sub eax, ebx                ;  V 1 
 and edx, 0x00FF00FF         ; U
 add ebp, edx                ;  V 1 
 mov ebx, eax                ; U
 and eax, 0xFF00FF00         ;  V 1
 shr eax, 8                  ; U
 mov ecx, [esp + esi + 4]    ;  V 1
 xor eax, 0x00800080         ; U
 or  ecx, 0x80008000         ;  V 1
 xor ebx, eax                ; U
 mov edx, [esp + edi + 4]    ;  V 1
 sub ebx, eax                ; U
 sub ecx, edx                ;  V 1
 and ebx, 0x00FF00FF         ; U
 mov edx, ecx                ;  V 1
 add ebp, ebx                ; U 
 and ecx, 0xFF00FF00         ;  V 1
 shr ecx, 8                  ; U
 mov eax, [esp + esi + 8]    ;  V 1
 xor ecx, 0x00800080         ; U
 or  eax, 0x80008000         ;  V 1
 xor edx, ecx                ; U
 mov ebx, [esp + edi + 8]    ;  V 1
 add esp, 8                  ; U
  jnc NoMMXloop              ;  V 1

skip1:

 mov esp, [esp_save]
 xor ecx, ecx

 mov esi, [line2a]           ; AGI
 mov edi, [line2b]

 mov esp, [count2]
 xor edx, edx

 cmp esp, byte 4
  jb skip2

 mov eax, [esp + esi]       
 mov ebx, [esp + edi]
 or eax, 0x80008000        

NoMMXloop2:
 sub edx, ecx                ; U
 sub eax, ebx                ;  V 1 
 and edx, 0x00FF00FF         ; U
 add ebp, edx                ;  V 1 
 mov ebx, eax                ; U
 and eax, 0xFF00FF00         ;  V 1
 shr eax, 8                  ; U
 mov ecx, [esp + esi + 4]    ;  V 1
 xor eax, 0x00800080         ; U
 or  ecx, 0x80008000         ;  V 1
 xor ebx, eax                ; U
 mov edx, [esp + edi + 4]    ;  V 1
 sub ebx, eax                ; U
 sub ecx, edx                ;  V 1
 and ebx, 0x00FF00FF         ; U
 mov edx, ecx                ;  V 1
 add ebp, ebx                ; U 
 and ecx, 0xFF00FF00         ;  V 1
 shr ecx, 8                  ; U
 mov eax, [esp + esi + 8]    ;  V 1
 xor ecx, 0x00800080         ; U
 or  eax, 0x80008000         ;  V 1
 xor edx, ecx                ; U
 mov ebx, [esp + edi + 8]    ;  V 1
 add esp, byte 8             ; U
  jnc NoMMXloop2             ;  V 1

skip2:

 mov esp, [esp_save]
 mov eax, ebp                ;  

 shr eax, 16                 ;
 and ebp, 0x0000FFFF         ;

 add eax, ebp                ;
 pop ebp

 pop edi
 pop esi

 pop edx
 pop ecx

 pop ebx

ret

_vc_corr_mmx__Fiiiiii:
; int3
 push ebx
 push esi

 push edi
 push ecx

 mov esi, [mline1a]
 mov edi, [mline1b]

 mov ebx, edi
 mov ecx, [mcount1]

 and ecx, byte -8
 and ebx, byte 7

 shl ebx, 3
 and edi, byte -8

 add esi, ecx

 movd mm6, ebx         
 pxor mm7, mm7

 xor ebx, byte -1
 add edi, ecx

 add ebx, byte 65           
 xor ecx, byte -1

 inc ecx

 movd mm5, ebx        
 pxor mm4,mm4

 cmp ecx, byte 8
  jb skipMMX1

 movq mm3, [ecx + edi    ]

 movq mm1, [ecx + edi + 8]
 psrlq mm3, mm6

 movq mm0, [ecx + esi    ]
 psllq mm1, mm5
 
 por mm1, mm3

align16
MMXLoop:
 movq mm2, [ecx + esi     ]  ;4p2 wMM2rECXESI FD0            FD0
; db 0x3E
; dd 0x31946F0F
; dd 0x00000000
; db 0x3E

 psubusb mm0, mm1            ;3p01wMM0rMM1MM0 FD1            FD1
 psubusb mm1, mm2            ;3p01wMM1rMM2MM1 FD2            FD2
 movq mm3, [ecx + edi + 8 ]  ;5p2 wMM3rECXEDI F D0            FD0
 por mm1, mm0                ;3p01wMM1rMM0MM1 fF D0           FD1
 movq mm0, [ecx + esi + 8 ]  ;5p2 wMM0rECXESI  F D1           FD2
 psrlq mm3, mm6              ;3p1 wMM3rMM6     F D2           F D0
 movq mm2, mm1               ;3p01wMM2rMM1     F  D0           F D0
 punpcklbw mm1, mm4          ;3p1 wMM1rMM4MM1  fF  D0          F D1
 punpckhbw mm2, mm4          ;3p1 wMM2rMM4MM2   F  D1          F D2
 paddusw mm7, mm1            ;3p01wMM7rMM1MM7r  F  D2          F  D0
 paddusw mm7, mm2            ;3p01wMM7rMM2MM7r  F   D0         F  D1
 movq mm1, [ecx + edi + 16]  ;5p2 wMM1rECXEDIr  fF   D0        fF  D0
 psllq mm1, mm5              ;3p1 wMM1rMM5       F   D1         F  D1
 por mm1, mm3                ;3p01wMM1rMM3MM1    F   D2         F  D2
 add ecx, byte 8             ;3p01wECXrECX       F    D0        F   D0
  jnc MMXLoop                ;2p1     rFLAG      F    D1        F   D1
; 6.5 cyc exe 8 0 5 4 0 0
; 6.7 cyc RAT
; 8->7 cyc Deco
; 6 cyc Ret

skipMMX1:

 mov esi, [mline2b]         
 mov edi, [mline2a]

 mov ebx, edi
 mov ecx, [mcount2]

 and ecx, byte -8
 and ebx, byte 7

 shl ebx, 3
 and edi, byte -8

 add esi, ecx

 movd mm6, ebx         

 xor ebx, byte -1
 add edi, ecx

 add ebx, byte 65           
 xor ecx, byte -1

 inc ecx

 movd mm5, ebx        

 cmp ecx, byte 8
  jb skipMMX2

 movq mm3, [ecx + edi    ]

 movq mm1, [ecx + edi + 8]
 psrlq mm3, mm6

 movq mm0, [ecx + esi    ]
 psllq mm1, mm5
 
 por mm1, mm3

align16
MMXLoop2:
 movq mm2, [ecx + esi     ]  ;4p2 wMM2rECXESI FD0            FD0
; db 0x3E
; dd 0x31946F0F
; dd 0x00000000
; db 0x3E

 psubusb mm0, mm1            ;3p01wMM0rMM1MM0 FD1            FD1
 psubusb mm1, mm2            ;3p01wMM1rMM2MM1 FD2            FD2
 movq mm3, [ecx + edi + 8 ]  ;5p2 wMM3rECXEDI F D0            FD0
 por mm1, mm0                ;3p01wMM1rMM0MM1 fF D0           FD1
 movq mm0, [ecx + esi + 8 ]  ;5p2 wMM0rECXESI  F D1           FD2
 psrlq mm3, mm6              ;3p1 wMM3rMM6     F D2           F D0
 movq mm2, mm1               ;3p01wMM2rMM1     F  D0           F D0
 punpcklbw mm1, mm4          ;3p1 wMM1rMM4MM1  fF  D0          F D1
 punpckhbw mm2, mm4          ;3p1 wMM2rMM4MM2   F  D1          F D2
 paddusw mm7, mm1            ;3p01wMM7rMM1MM7r  F  D2          F  D0
 paddusw mm7, mm2            ;3p01wMM7rMM2MM7r  F   D0         F  D1
 movq mm1, [ecx + edi + 16]  ;5p2 wMM1rECXEDIr  fF   D0        fF  D0
 psllq mm1, mm5              ;3p1 wMM1rMM5       F   D1         F  D1
 por mm1, mm3                ;3p01wMM1rMM3MM1    F   D2         F  D2
 add ecx, byte 8             ;3p01wECXrECX       F    D0        F   D0
  jnc MMXLoop2               ;2p1     rFLAG      F    D1        F   D1
; 6.5 cyc exe 8 0 5 4 0 0
; 6.7 cyc RAT
; 8->7 cyc Deco
; 6 cyc Ret

skipMMX2:

 movq mm0, mm7
 psrlq mm7, 32

 paddusw mm7, mm0

 movq mm0, mm7
 psrlq mm7, 16

 paddusw mm7, mm0

 movd eax, mm7

 and eax, 0xFFFF
 pop ecx

 pop edi
 pop esi

 pop ebx

ret



align16
