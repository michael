//TicTacToe 1.0 Copyright (c) 1999 Michael Niedermayer

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.net.*;
import java.util.*;

/**
 * @author Michael Niedermayer
 */
public class TicTacToe extends JApplet implements ActionListener, KeyListener
	{
	protected static final int COMP=0;
	protected static final int USER=1;
	private int mouseX=-1, mouseY=-1;
	protected JButton [][] aJButton= new JButton[3][];
	protected ImageIcon [] icon= new ImageIcon[6];
	protected int firstPlayer= 1;
	protected State state= new State( firstPlayer );

	private void initPicsFromState()
		{
		boolean [][] line= state.getLine();
		for(int x=0; x<3; x++)
			{
			for(int y=0; y<3; y++)
				{
				int s= state.get(x, y) + 1;

				aJButton[x][y].setRolloverIcon(icon[s+3]);
//				aJButton[x][y].setSelectedIcon(icon[s+3]);

				if(line[x][y])
					{
					aJButton[x][y].setIcon(icon[s+3]);
					}
				else
					{
					aJButton[x][y].setIcon(icon[s]);
					}
				}
			}
		}

	public void init() 
		{
		ClassLoader c= getClass().getClassLoader();
		icon[0]= new ImageIcon( c.getResource("empty.jpg" ) );
	 	icon[1]= new ImageIcon( c.getResource("x.jpg"     ) );
		icon[2]= new ImageIcon( c.getResource("o.jpg"     ) );
  		icon[3]= new ImageIcon( c.getResource("emptyL.jpg") );
		icon[4]= new ImageIcon( c.getResource("xL.jpg"    ) );
		icon[5]= new ImageIcon( c.getResource("oL.jpg"    ) );

		Container cp= getContentPane();

		cp.setLayout( new GridLayout(3, 3) );

		for(int x=0; x<3; x++)
			{
			aJButton[x]= new JButton[3];
			}

		for(int y=0; y<3; y++)
			{
			for(int x=0; x<3; x++)
				{
				JButton jb= new JButton(icon[0]);
				aJButton[x][y]= jb;
				jb.setBorder( BorderFactory.createRaisedBevelBorder() );
				jb.setRolloverEnabled(true);
				cp.add(jb);

				jb.putClientProperty("xPos", new Integer(x));
				jb.putClientProperty("yPos", new Integer(y));
				jb.addActionListener(this);
				jb.addKeyListener(this);
				}
			}

		aJButton[1][1].requestFocus();
		initPicsFromState();
		}

	public void actionPerformed(ActionEvent e)
		{
		JButton jButton= (JButton)e.getSource();

		int x= ((Integer)jButton.getClientProperty("xPos")).intValue();
		int y= ((Integer)jButton.getClientProperty("yPos")).intValue();

		if(state.isEnd())
			{
		 	state.makeEmpty();

			firstPlayer^= 1;
			state.setFirstPlayer(firstPlayer);

			if(firstPlayer==COMP)
				state.makeMove();
			}
		else
			{
			try
				{
				state.set(x, y, USER);
				}
			catch(IllegalMoveException e2)
				{
				return;
				}
			if(!state.isEnd())
				state.makeMove();
			}

		initPicsFromState();
		}
	public void keyTyped(KeyEvent e){}
	public void keyPressed(KeyEvent e)
		{
		JButton jButton= (JButton)e.getSource();

		int x= ((Integer)jButton.getClientProperty("xPos")).intValue();
		int y= ((Integer)jButton.getClientProperty("yPos")).intValue();

		if(e.getKeyCode() == e.VK_LEFT) x--;
		if(e.getKeyCode() == e.VK_RIGHT) x++;
		if(e.getKeyCode() == e.VK_DOWN) y++;
		if(e.getKeyCode() == e.VK_UP) y--;

		x+=3;
		y+=3;
		x%=3;
		y%=3;

		aJButton[x][y].requestFocus();
		}
	public void keyReleased(KeyEvent e){}
	public String getAppletInfo()
		{
		return "TicTacToe 1.0 Copyright (c) 1999 Michael Niedermayer\n";
		}
	}
