 ;VER 0.92 75 byte 8086 needed
 ;reg at start needed  AH=0,ahem si=0/bl=0
org 0x100
incr equ 8
bias equ 1024
   mov bh,0A0h      ;  DS=A000
   mov ds,bx        ;  "
   MOV AL,013h      ;V-mode 320x200x256
   INT 10h          ;    "
   MOV BP,bias
NEXT:
  ADD word [SEM1-bias+bp],incr
  MOV BH,-192*incr/256
N0:   
   PUSH si
   MOV CL,150
   XOR si,si
   XOR di,di
N3:    
    MOV AX,di               
    IMUL AX
    IDIV BP
    XCHG AX,si              
    XCHG AX,di              
    IMUL di
    IDIV BP                  
    ADD AX,AX
DB 05h              ;add AX,imm16
SEM1 DW -100*incr
    XCHG di,AX
    IMUL AX
    IDIV BP
    ADD si,AX       
    ADD AX,AX
    SUB AX,si       
    ADD AX,BX    
    XCHG AX,si      

    TEST AH,11110000b
  LOOPZ N3
   POP si
  CMP BX,BP
   JGE NEXT
  MOV [si],CL 
  ADD BX,incr
  INC si
  JNZ N0
 RET

           
