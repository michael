/*
 *  mndiff Copyright (C) 2005 Michael Niedermayer <michaelni@gmx.at>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
 
#include <assert.h>
#include <inttypes.h>
#include <stdio.h>
#include "vis.h"

//#define ABS(a) ((a) >= 0 ? (a) : (-(a)))
//#define MAX(a,b) ((a) > (b) ? (a) : (b))
#define MIN(a,b) ((a) > (b) ? (b) : (a))

static int get_next_diff(int i0, int i1, int *off, int length){
    int diff= i1 - i0;
    int first_diff;

    for(first_diff=i0; first_diff<length; first_diff++){
        if(off[first_diff] != first_diff + diff)
           break;
    }
    return first_diff;
}

static int get_next_equal(int i, int *off, int length){
    while(i<length && off[i] < 0)
        i++;
    return i;
}

static int get_context(uint8_t *data, int i, int length, int context, int backward){
    for(; i>=0 && i<length; i+= 1-2*backward){
        if(data[i] == '\n'){
            context--;
            if(context <= 0)
                break;
        }
    }
    if(context<=0 && backward && data[i]=='\n') i++;
    return i;
}

static int reorder(FILE *f, uint8_t *data_out, int *off_out[2], uint8_t *data[2], int length[2], int *off[2], char *seperator[6], int context){
    int i0, i1, i1out;
#if 0
    i0=i1=i1out=0;
    while(i1<length[1]){
        if(off[1][i1] == -1){
            fprintf(stderr, "-1 \n");
            while(i1<length[1] && off[1][i1] == -1)
                i1++;
        }else{
            fprintf(stderr, "%d \n", off[1][i1]);
            i1++;
            while(i1<length[1] && off[1][i1-1]+1 == off[1][i1])
                i1++;
        }
    }
#endif
    i0=i1=i1out=0;
    for(;;){
        while(i0<length[0] && off[0][i0] == -1){
            off_out[0][i0++]= -1;
        }
        while(i1<length[1] && off[1][i1] == -1){
            off_out[1][i1out]= -1;
            data_out[i1out++]= data[1][i1++];
        }
        if(i0 >= length[0])
            break;
        if(i1 >= length[1] || off[0][i0] != i1){
            int prev_context= get_context(data[1], i1, length[1], context+1, 1);
            int next_context= get_context(data[1], i1, length[1], context, 0);
            int new_next_context= get_context(data[1], off[0][i0], length[1], context, 0);

            fputs(seperator[0], f);
            fwrite(data[1] + prev_context, 1, i1 - prev_context, f);
            fputs(seperator[2], f);
            fwrite(data[1] + off[0][i0], 1, new_next_context - off[0][i0], f);
            fputs("\n", f);
            fputs(seperator[3], f);
            fputs(seperator[4], f);
            fwrite(data[1] + i1, 1, next_context - i1, f);
            fputs(seperator[5], f);
            fputs(seperator[1], f);

            i1= off[0][i0];
        }
        while(i1<length[1] && i0<length[0] && off[1][i1] == i0){ //FIXME i0len check may be unneedd
            off_out[0][i0   ]= i1out;
            off_out[1][i1out]= i0;
            data_out[i1out++]= data[1][i1++];
            i0++;
        }
    }
    assert(i0 == length[0]);
    assert(i1out == length[1]);
    
    return 0;
}

int vis(FILE *f, uint8_t *data[2], int length[2], int *off[2], char *seperator[6], int context){
    int i0, i1, prev_block_end, end;
    uint8_t data_out[length[1]]; //FIXME dynamic alloc? context?
    int off0[length[0]];
    int off1[length[1]];
    int *off_out[2]={off0, off1};

    for(i0=0; i0<length[0]; i0++){
        int i1= off[0][i0];
//         fprintf(stderr, "%d - %d\n", i0, i1);
        if(i1<0) continue;
        if(data[0][i0] != data[1][i1]){
            off[0][i0]=
            off[1][i1]= -1;
            assert(0);
        }
    }
    
    reorder(f, data_out, off_out, data, length, off, seperator, context);
    
    //FIXME ugly replace X by X_in
    data[1]= data_out;
    off[0]= off_out[0];
    off[1]= off_out[1];

    i0=i1=0;
    prev_block_end=-1;
    end=0;
    for(;;){
        int first_diff= get_next_diff (i0, i1, off[0], length[0]);
        int first_equal=get_next_equal(first_diff, off[0], length[0]);
        int next_diff= get_next_diff (first_equal, off[0][MIN(first_equal,length[0]-1)], off[0], length[0]);
        int prev_context= get_context(data[0], first_diff, length[0], context, 1);
        int next_context= get_context(data[0], first_equal, length[0], context, 0);
        
        assert(first_diff >= i0);
        assert(first_equal >= first_diff);
        assert(next_diff >= first_equal);
        assert(first_diff >= prev_context);
        assert(first_equal<= next_context);
//        fprintf(stderr, "(%d %d) %d %d %d (%d %d)\n", i0, i1, first_diff, first_equal, next_diff, prev_context, next_context);
        
        /* check if the files are identical */
        if(length[0] == length[1] && i0==0 && i1==0 && first_diff==length[0])
            break;
//        fprintf(stderr, "%d %d %d %d %d\n", length[0], length[1], i0, i1, first_diff);

        if(prev_block_end < prev_context || end){
            if(prev_block_end>=0){
                assert(prev_block_end >= i0);
                fwrite(data[0] + i0, 1, prev_block_end - i0, f);
                fputs(seperator[1], f);
                if(end) break;
            }
            fputs(seperator[0], f);
            fwrite(data[0] + prev_context, 1, first_diff - prev_context, f);
        }else{
//            fprintf(stderr, "%d %d %d\n", i0, first_diff, length[0]);
            fwrite(data[0] + i0, 1, first_diff - i0, f);
        }
        assert(first_diff >= 0);
        if(first_diff != i0){
            i0 = first_diff;
            i1 = off[0][first_diff-1]+1;
        }else
            end=1;
        if(i0 < length[0] && off[0][i0] < 0){
            fputs(seperator[2], f);
            for(; i0 < length[0] && off[0][i0]<0; i0++){
                fputc(data[0][i0], f);
            }
            fputs(seperator[3], f);
            end=0;
        }
        if(i1 < length[1] && off[1][i1] < 0){
            fputs(seperator[4], f);
            for(; i1 < length[1] && off[1][i1]<0; i1++){
                fputc(data[1][i1], f);
            }
            fputs(seperator[5], f);
            end=0;
        }
        assert(i0 == first_equal);

        prev_block_end= next_context;
        if(first_diff >= length[0] && off[0][first_diff-1] == length[1]-1)
            break;
    }
    return 0;
}
