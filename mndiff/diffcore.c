/*
 *  mndiff Copyright (C) 2005 Michael Niedermayer <michaelni@gmx.at>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <assert.h>
#include <inttypes.h>
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
 
#include "diffcore.h"

#if 0
static inline long long rdtsc(void)
{
        long long l;
        asm volatile(   "rdtsc\n\t"
                : "=A" (l)
        );
        return l;
}

#define START_TIMER \
uint64_t tend;\
uint64_t tstart= rdtsc();\

#define STOP_TIMER(id) \
tend= rdtsc();\
{\
  static uint64_t tsum=0;\
  static int tcount=0;\
  static int tskip_count=0;\
  if(tcount<2 || tend - tstart < 8*tsum/tcount){\
      tsum+= tend - tstart;\
      tcount++;\
  }else\
      tskip_count++;\
  if(256*256*256*64%(tcount+tskip_count)==0){\
      fprintf(stderr, "%Ld dezicycles in %s, %d runs, %d skips\n", tsum*10/tcount, id, tcount, tskip_count);\
  }\
}
#else
#define START_TIMER
#define STOP_TIMER(x)
#endif

//FIXME memcmp ?
//FIXME len insteda of end?
/**
 *
 * needs zero byte at the end
 */
static inline int cmp(uint8_t *a, uint8_t *b, uint8_t *enda, uint8_t *endb){
    for(;a<enda && b<endb; a++, b++){
        if(*a != *b)
            break;
    }
    return (*a) - (*b);
}

static inline int cmp_len(uint8_t *a, uint8_t *b, uint8_t *enda, uint8_t *endb){
    int i;
    for(i=0; i<enda-a && i<endb-b; i++){
        if(a[i] != b[i])
            break;
    }
    return i;
}

//we cant (easily) use libc qsort as we need to pass the end pointer into the thing
static void sort(uint8_t **suffix, int len, uint8_t *end){
    int mid= len>>1;
    uint8_t *pivot= suffix[mid];
    uint8_t *tmp;
    int i= 1;
    int j= len-1;
    
    suffix[mid]= suffix[0];
    suffix[0]= pivot;
    
    for(;;){
        while(i<=j && cmp(suffix[i], pivot, end, end) <= 0)
            i++;
        while(i<=j && cmp(suffix[j], pivot, end, end) >= 0)
            j--;
        if(i>=j)
            break;
        tmp= suffix[i]; 
        suffix[i++]= suffix[j];
        suffix[j--]= tmp;
    }
    assert(j+1 == i);
    suffix[0]= suffix[j];
    suffix[j]= pivot;

    if(j>1)
        sort(suffix, j, end);
    if(len-i>1)
        sort(suffix + i, len - i, end);
}

#define MSB2 ((UINT_MAX>>2)+1)

static uint8_t **find(uint8_t **suffix, int len, uint8_t *end, uint8_t *key, int key_i, uint8_t *key_end, int *clen){
    int cmp_v;

    key += key_i;
    clen += key_i;
    while(len>1){
        int mid= len>>1;
        uint8_t *src= suffix[mid];
        cmp_v= clen[end - src];
        if((signed)(cmp_v &(~MSB2)) <= key_i){
            cmp_v= cmp_len(src, key, end, key_end);
            if(cmp(src+cmp_v, key+cmp_v, end, key_end) > 0)
                cmp_v |= MSB2;
            clen[end - src] = cmp_v + key_i + 1;
        }
//         else
//             assert((cmp(suffix[mid], key, end, key_end) > 0) == !!(cmp_v & MSB2));

        if(cmp_v & MSB2){
            len= mid;
        }else{
            suffix += mid;
            len -= mid;
        }
    }
/*        if(cmp_v & MSB2)
            cmp_v= -(cmp_v - MSB2);*/
    return suffix;
}

//FIXME remove this
#define DEASSOCIATE(tt, t, i, j) \
    if(t[i][j] != -1){\
        tt[1-i][ t[i][j] ]= -1;\
        t [1-i][ t[i][j] ]= -1;\
    }

#define ASSOCIATE(tt, t, i, j)\
    DEASSOCIATE(tt, t, 0, i)\
    DEASSOCIATE(tt, t, 1, j)\
    tt[0][i]=\
    t[0][i] = j;\
    tt[1][j]=\
    t[1][j] = i;

static inline void associate_clean(DiffCore *c, int idx0, int i){
    int a= c->temp_off[0][idx0]= c->off[0][idx0];
    int b= c->temp_off[1][i   ]= c->off[1][i   ];
    if(a >= 0)
        c->temp_off[1][a]= c->off[1][a];
    if(b >= 0)
        c->temp_off[0][b]= c->off[0][b];
}

static inline int get_class(int c){
    if(   (c >= 'a' && c <= 'z')
       || (c >= 'A' && c <= 'Z')
       || (c >= '0' && c <= '9')
       || c=='_')
        return 1;
    else if(c==' ' || c=='\t')
        return 2;
    else if( c== '\n')
        return 3;
    else
        return 4;
}

static const int score_tab[5][5]={
 {  0,  0,  0,  0,  0},
 {  0,  0,-10,-20,-10},
 {  0,-10,  0,-20,-10},
 {  0,-20,-20,  0,-20},
 {  0,-10,-10,-20,  0},
};

//assert score(a,b) == score(b,a)
//assert score(a,0) == 0
//score(a,a) might be non 0, for example for '\n'

#ifndef NDEBUG
static int get_score(DiffCore *c, int *off[2], uint8_t *data, int len[2]){
    int i, j;
    int score= 0;
    int last_off= -1;
    int last_class= 0;
    
    for(i=0; i<len[0]; i++){
        int class= get_class(data[i]);
        if(off[0][i] != -1){
            score += c->match_score;
        }
        if(last_off != -1 && last_off + 1 == off[0][i]){
            score += score_tab[last_class][class];
        }else if(last_off + 1 != off[0][i] && last_off != off[0][i])
            score += c->boundary_score;
        last_off= off[0][i];
        last_class= class;
    }

    for(j=0; j<2; j++){
        int last_non_neg_off= -1;
        for(i=0; i<len[j]; i++){
            if(off[j][i] != -1){
                if(last_non_neg_off > off[j][i])
                    score += c->reorder_score;
                last_non_neg_off= off[j][i];
            }
        }
    }
    return score;
}
#endif

static inline int update(DiffCore *c, int value, int i, int off_index){
    int score= 0;
    int j;
    int right_off= -1;
    int left_off= -1;
    int *off= c->temp_off[off_index];
    uint8_t *data= c->data[off_index];
    int len= c->length[off_index];
    int *cache= c->update_cache[off_index];

    //calculate and count boundary and match score just once
    if(!off_index){
        //FIXME border
        int class= get_class(data[i]);
        //FIXME redundancy
        if(off[i] != -1)
            score -= c->match_score;
        if(value != -1)
            score += c->match_score;
    
        if(i){
            int last_off=   i ? off[i-1] : -1;
            int last_class= i ? get_class(data[i-1]) : 0;
            if(last_off + 1 == off[i]) score -= score_tab[last_class][class];
            else if(last_off != off[i]) score -= c->boundary_score;
            if(last_off + 1 == value ) score += score_tab[last_class][class];
            else if(last_off != value ) score += c->boundary_score;
        }
        if(i+1<len){
            int next_off=   i+1<len ? off[i+1] : -1;
            int next_class= i+1<len ? get_class(data[i+1]) : 0;
            if(next_off - 1 == off[i]) score -= score_tab[next_class][class];
            else if(next_off != off[i]) score -= c->boundary_score;
            if(next_off - 1 == value ) score += score_tab[next_class][class];
            else if(next_off != value ) score += c->boundary_score;
        }
    }
    
    if(i <= cache[0] || i >  cache[1])
        cache[0]= i-1;
    if(i <  cache[1] || i >= cache[2])
        cache[2]= i+1;
    cache[1]= i;
    
    assert(cache[0] <= cache[1]);
    assert(cache[1] <= cache[2]);

    for(j=cache[0]; j>=0; j--){
        if(off[j] != -1){
            left_off= off[j];
            break;
        }
    }
    cache[0]= j;

    for(j=cache[2]; j<len; j++){
        if(off[j] != -1){
            right_off= off[j];
            break;
        }
    }
    cache[2]= j;
    
    if(off[i]!=-1){
        if(left_off != -1 && left_off > off[i])
            score -= c->reorder_score;
        if(right_off != -1 && right_off < off[i])
            score -= c->reorder_score;
        if(right_off != -1 && left_off != -1 && right_off < left_off)
            score += c->reorder_score;
    }
    if(value!=-1){
        if(left_off != -1 && left_off > value)
            score += c->reorder_score;
        if(right_off != -1 && right_off < value)
            score += c->reorder_score;
        if(right_off != -1 && left_off != -1 && right_off < left_off)
            score -= c->reorder_score;
    }

    off[i]= value;

    return score;
}

static void build_collisiontree(int (**tree)[2], int cindex, int (*line)[4], int count, unsigned int bit){
    int (*next_line)[4]= malloc(sizeof(int)*4*count); //cant be on the stack as its to big
    int next_count[2]= {0,0};
    int i;

    for(i=0; i<count; i++){
        int index= !!(line[i][1] & bit);
        next_line[ next_count[index]   ][0+2*index]= line[i][0];
        next_line[ next_count[index]++ ][1+2*index]= line[i][1];
        tree[0][cindex+i][0] = next_count[0];
        tree[0][cindex+i][1] = next_count[1];
    }
    if(bit>1){
        build_collisiontree(tree+1, cindex                ,   next_line       , next_count[0], bit>>1);
        build_collisiontree(tree+1, cindex + next_count[0], &(next_line[0][2]), next_count[1], bit>>1);
    }
    free(next_line);
}

static int link_cmp(const void *a, const void *b){
    const int *da = (const int *) a;
    const int *db = (const int *) b;
    
    return da[2] - db[2];
}

//data must have a zero byte at the end
int mndiff(DiffCore c){
    int i,j,k, pass;
    int change=1;
    int length[2]= {c.length[0], c.length[1]};
    uint8_t *data[2]= {c.data[0], c.data[1]};
    uint8_t **suffix= malloc(sizeof(uint8_t *) * length[0]);
    int *clen= malloc(sizeof(int) * (length[0] + length[1]));
    uint8_t *end[2]= {data[0] + length[0],
                      data[1] + length[1]};

    if(data[0][length[0]] || data[1][length[1]])
        return -1;
        
    for(i=0; i<2; i++){
        c.temp_off[i]= malloc(sizeof(int) * length[i]);
        memcpy(c.temp_off[i], c.off[i], sizeof(int)*length[i]);
    }

    for(i=0; i<length[0]; i++)
        suffix[i]= data[0] + i;
    if(c.flags & MNDIFF_FLAG_VERBOSE)
        fprintf(stderr, "sorting suffix array ...\n");
{START_TIMER
    sort(suffix, length[0], end[0]);
STOP_TIMER("sort")}
    
    for(i=1; i<length[0]; i++)
        assert(cmp(suffix[i-1], suffix[i], end[0], end[0]) <= 0);
    
    if(1){
        int (*collision[32])[2];
        int (*link)[4];
        int link_count=0;
        int log2_len;
#define TRIES 4

        for(i=0; i<32; i++){
            collision[i]= malloc(sizeof(int) * length[1] * 2 * TRIES);
        }
        link= malloc(sizeof(int) * length[1] * 4 * TRIES);
        memset(link, -1, sizeof(int) * length[1] * 4 * TRIES);

        if(c.flags & MNDIFF_FLAG_VERBOSE)
            fprintf(stderr, "building likely link table ...\n");
        
        memset(clen, 0, sizeof(int)*(length[0] + length[1]));

        for(i=0; i<length[1]; i++){
            uint8_t **left;
            int best[TRIES][4]={{0}};

{START_TIMER
            left= find(suffix, length[0], end[0], data[1], i, end[1], clen);
STOP_TIMER("find")}

            left -= TRIES/2;
            while(left < suffix) left++;
            for(j=0; j<TRIES && left < suffix + length[0]; j++, left++){ //FIXME get rid of left or j
                int idx0= (*left) - data[0];
                int len= (clen[end[0] - (*left) + i] & (~MSB2)) - i - 1; //  cmp_len(*left, data[1] + i, end[0], end[1]);
                
                if(len<0)
                    len= cmp_len(*left, data[1] + i, end[0], end[1]);
                
                best[j][1]= idx0;
                best[j][2]= len;
            }
            qsort(best, TRIES, sizeof(int)*4, link_cmp);

            for(j=TRIES-1; j>=0; j--){
                if(best[j][2] > 4 && best[j][2] > best[TRIES-1][2] - 4){
                    int idx0= best[j][1];
                    int len= best[j][2];
                    for(k=0; k<len; k++){
                        link[link_count  ][0]= i + k;
                        link[link_count++][1]= idx0 + k;
                    }
                }
            }
            i+= best[TRIES-1][2];
        }
        
        if(c.flags & MNDIFF_FLAG_VERBOSE)
            fprintf(stderr, "finding collisions ...\n");
        log2_len= log(length[0]-1)/log(2);
        assert(1<<(log2_len + 1) >= length[0]);
        assert(1<<(log2_len    ) <  length[0]);
        build_collisiontree(collision, 0, link, link_count, 1<<log2_len);
        
        for(i=0; i<link_count; i++){
            int idx0= link[i][1];
            int cindex= i;
            int ccount= link_count;
            int cstart= 0;
            int collision_count= 0;

            for(j=0; j<=log2_len; j++){
                int bit= log2_len - j;
                int left_size= collision[j][cstart + ccount-1][0]; //FIXME bad var name and below too
                assert(bit >= 0);
//                fprintf(stderr, "%d %d %d\n", collision[j][cstart + ccount-1][0], collision[j][cstart + ccount-1][1],  ccount);
                assert(collision[j][cstart + ccount-1][0] + collision[j][cstart + ccount-1][1] == ccount);
                assert(j || (idx0 & (2<<bit))==0);
                if(idx0 & (1<<bit)){
                    collision_count += left_size - collision[j][cstart + cindex][0];
                    cindex= collision[j][cstart + cindex][1] - 1;
                    cstart+= left_size;
                    ccount= ccount - left_size;
                }else{
#if 1
                    if(cindex)
                        collision_count += collision[j][cstart + cindex - 1][1];
#else
                    collision_count += collision[j][cstart + cindex][1];
#endif
                    cindex= collision[j][cstart + cindex][0] - 1;
                    ccount= left_size;
                }
                assert(ccount > 0);
                assert(cstart + ccount <= link_count);
                assert(cindex >= 0);
                assert(cindex < ccount);
                assert(collision_count >= 0);
                assert(collision_count <= link_count);
            }
            link[i][2]= collision_count;
#if 0
            collision_count= 0;
            for(j=0; j<link_count; j++){
                int a= link[i][1] - link[j][1];
                int b= link[i][0] - link[j][0];
                if(a*(int64_t)b < 0)
                    collision_count++;
            }
            fprintf(stderr, "%d %d\n", collision_count, link[i][2]);
            assert(collision_count == link[i][2]);
#endif
        }

        if(c.flags & MNDIFF_FLAG_VERBOSE)
            fprintf(stderr, "sorting collisions ...\n");
        qsort(link, link_count, sizeof(int)*4, link_cmp);
        
        for(i=0; i<link_count-1; i++)
            assert(link[i][2] <= link[i+1][2]);

        memset(clen, 0, sizeof(int)*(length[0] + length[1]));
        
        if(c.flags & MNDIFF_FLAG_VERBOSE)
            fprintf(stderr, "linking least constraining ...\n");
        for(j=0; j<link_count; j++){
            int best_score= INT_MIN;
            int best_len= 0;
            int best_len2= 0;
            int idx0= link[j][1];
            int score= 0;
            int len;
            
            i= link[j][0];

            if(c.off[1][i] == idx0)
                continue;
                
            for(k=0; i+k < length[1] && idx0+k < length[0]; k++){
                if(c.temp_off[0][idx0+k] != i+k){
                    int o= c.off[1][i+k];
                    if(data[1][i+k] != data[0][idx0+k])
                        break;
                    if(o != -1 && c.temp_off[0][o] != -1 && (o<idx0 || o>idx0+k)){
                        score+= update(&c, -1, o, 0);
                    }
                    o= c.off[0][idx0+k];
                    if(o != -1 && c.temp_off[1][o] != -1 && (o<i || o>i+k)){
                        score+= update(&c, -1, o, 1);
                    }
                    score+= update(&c, i+k, idx0+k, 0);
                    score+= update(&c, idx0+k, i+k, 1);

                    if(score > best_score){
                        best_score= score;
                        best_len= k+1; //FIXME rename best_len -> best_right or end?
                        best_len2= 0;
                    }
                }
            }
            len= k;
            for(k=0; k<len; k++){
                associate_clean(&c, idx0+k, i+k);
            }
            memset(c.update_cache, 0, sizeof(c.update_cache));
            score=0;
            len=best_len;
            for(k=len-1; i+k>=0 && idx0+k>=0; k--){
                if(c.temp_off[0][idx0+k] != i+k){
                    int o= c.off[1][i+k];
                    if(data[1][i+k] != data[0][idx0+k])
                        break;
                    if(o != -1 && c.temp_off[0][o] != -1 && (o<idx0+k || o>idx0+len-1)){
                        score+= update(&c, -1, o, 0);
                    }
                    o= c.off[0][idx0+k];
                    if(o != -1 && c.temp_off[1][o] != -1 && (o<i+k || o>i+len-1)){
                        score+= update(&c, -1, o, 1);
                    }
                    score+= update(&c, i+k, idx0+k, 0);
                    score+= update(&c, idx0+k, i+k, 1);

                    if(score > best_score){
                        best_score= score;
                        best_len2= k;
                    }
                }
            }
            for(k++; k<len; k++){
                associate_clean(&c, idx0+k, i+k);
            }
            memset(c.update_cache, 0, sizeof(c.update_cache));
//                memcpy(temp_off, off[0], sizeof(int)*length[0]);

            if(best_score>0){
#ifndef NDEBUG
                int old_score0= get_score(&c, c.off, data[0], length);
                int old_score1= old_score0;
#endif
                assert(best_len2<=best_len);
                assert(idx0+best_len <= length[0]);
                assert(i   +best_len <= length[1]);
                assert(idx0+best_len2>= 0);
                assert(i   +best_len2>= 0);
                for(k=best_len2; k<best_len; k++){
                    ASSOCIATE(c.temp_off, c.off, idx0+k, i+k);
                }
#ifndef NDEBUG
                old_score0 += best_score;
                if(c.flags & MNDIFF_FLAG_DEBUG)
                    fprintf(stderr, "MATCH %d-%d (%d %d) %d (+%d %d=%d) collisions:%d\n", 
                        idx0, i, best_len2, best_len, best_score, old_score1, old_score0, get_score(&c, c.off, data[0], length), link[j][2]);
                assert(get_score(&c, c.off, data[0], length) == old_score0);
#endif
                change=1;
            }
        }
        
        free(link);
        for(i=0; i<32; i++){
            free(collision[i]);
        }
    }

    if(c.flags & MNDIFF_FLAG_VERBOSE)
        fprintf(stderr, "iterative refinement ");
    for(pass=0; change; pass++){
        change=0;
        if(c.flags & MNDIFF_FLAG_PROGRESS)
            fprintf(stderr, ".");

        memset(clen, 0, sizeof(int)*(length[0] + length[1]));

        for(i=0; i<length[1]; i++){
            int best_score= INT_MIN;
            uint8_t **best=NULL;
            int best_len= 0;
            int best_len2= 0;
//            int tries= i==0 || c.off[1][i-1]+1 != c.off[1][i] ? 10 : 2;
            int tries= c.off[1][i] == -1 ? 10 : 2; //FIXME user specify
            uint8_t **left;

            if(i && i+1<length[1] && c.off[1][i-1]+1 == c.off[1][i] && c.off[1][i] == c.off[1][i+1]-1)
                continue;

{START_TIMER
            left= find(suffix, length[0], end[0], data[1], i, end[1], clen);
STOP_TIMER("find")}

{START_TIMER

            left -= tries;
            while(left < suffix) left++;
            for(j=0; j<2*tries && left < suffix + length[0]; j++, left++){ //FIXME get rid of left or j
                int idx0= (*left) - data[0];
                int len= (clen[end[0] - (*left) + i] & (~MSB2)) - i - 1; //  cmp_len(*left, data[1] + i, end[0], end[1]);
                //FIXME remove len calc, as just stoping at the first differing byte is easier
                int score= 0;
                
                //this will practically never be executed
                if(len<0)
                    len= cmp_len(*left, data[1] + i, end[0], end[1]);
                    
                if(c.temp_off[0][idx0] == i && i+1<length[1] && i && c.temp_off[0][idx0 + 1] == i + 1 && c.temp_off[0][idx0 - 1] == i - 1)
                    continue;
                    //FIXME this is usless if no change
                
                for(k=0; k<len; k++){
                    if(c.temp_off[0][idx0+k] != i+k){
                        int o= c.off[1][i+k];
START_TIMER
                        if(o != -1 && c.temp_off[0][o] != -1 && (o<idx0 || o>idx0+k)){
                            score+= update(&c, -1, o, 0);
                        }
                        o= c.off[0][idx0+k];
                        if(o != -1 && c.temp_off[1][o] != -1 && (o<i || o>i+k)){
                            score+= update(&c, -1, o, 1);
                        }
                        score+= update(&c, i+k, idx0+k, 0);
                        score+= update(&c, idx0+k, i+k, 1);
    
                        if(score > best_score){
                            best_score= score;
                            best_len= k+1; //FIXME rename best_len -> best_right or end?
                            best_len2= 0;
                            best= left;
                        }
STOP_TIMER("INNER")
                    }
                }
                for(k=0; k<len; k++){
                    associate_clean(&c, idx0+k, i+k);
                }
                memset(c.update_cache, 0, sizeof(c.update_cache));
                score=0;
                if(best == left) len=best_len;
                else             len=0;
                for(k=len-1; i+k>=0 && idx0+k>=0; k--){
                    if(c.temp_off[0][idx0+k] != i+k){
                        int o= c.off[1][i+k];
START_TIMER
                        if(data[1][i+k] != data[0][idx0+k])
                            break;
                        if(o != -1 && c.temp_off[0][o] != -1 && (o<idx0+k || o>idx0+len-1)){
                            score+= update(&c, -1, o, 0);
                        }
                        o= c.off[0][idx0+k];
                        if(o != -1 && c.temp_off[1][o] != -1 && (o<i+k || o>i+len-1)){
                            score+= update(&c, -1, o, 1);
                        }
                        score+= update(&c, i+k, idx0+k, 0);
                        score+= update(&c, idx0+k, i+k, 1);
    
                        if(score > best_score){
                            best_score= score;
                            best_len2= k;
                        }
STOP_TIMER("INNER2")
                    }
                }
                for(k++; k<len; k++){
                    associate_clean(&c, idx0+k, i+k);
                }
                memset(c.update_cache, 0, sizeof(c.update_cache));
//                memcpy(temp_off, off[0], sizeof(int)*length[0]);
            }
STOP_TIMER("loop1")}

{START_TIMER            
            if(best_score>0){
                int idx0= (*best) - data[0];
#ifndef NDEBUG
                int old_score0= get_score(&c, c.off, data[0], length);
                int old_score1= old_score0;
#endif
                assert(best_len2<=best_len);
                assert(idx0+best_len <= length[0]);
                assert(i   +best_len <= length[1]);
                assert(idx0+best_len2>= 0);
                assert(i   +best_len2>= 0);
                for(j=best_len2; j<best_len; j++){
                    ASSOCIATE(c.temp_off, c.off, idx0+j, i+j);
                }
#ifndef NDEBUG
                old_score0 += best_score;
                if(c.flags & MNDIFF_FLAG_DEBUG)
                    fprintf(stderr, "MATCH %d-%d (%d %d) %d (+%d %d=%d)\n", 
                        idx0, i, best_len2, best_len, best_score, old_score1, old_score0, get_score(&c, c.off, data[0], length));
                assert(get_score(&c, c.off, data[0], length) == old_score0);
#endif
                change=1;
            }
STOP_TIMER("best")}
        }
    }

    if(c.flags & MNDIFF_FLAG_VERBOSE)
        fprintf(stderr, "\n");
    free(suffix);
    free(c.temp_off[0]);
    free(c.temp_off[1]);
    free(clen);
    return 0;
}
