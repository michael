/*
 *  mndiff Copyright (C) 2005 Michael Niedermayer <michaelni@gmx.at>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <assert.h>

#include "diffcore.h"
#include "vis.h"

static uint64_t fsize(FILE *f){
    uint64_t cur= ftell(f);
    uint64_t size;
    
    fseek(f, 0, SEEK_END); //FIXME 64bit stuff?
    size= ftell(f);
    fseek(f, cur, SEEK_SET);
    
    return size;
}

static void do_prepass(DiffCore c){
    int i, j, k;
    DiffCore c2= c;
    int *remap[2];
    
    for(i=0; i<2; i++){
        c2.data[i]= malloc(c.length[i]+1);
        c2.data[i][c.length[i]]= 0;
        c2.off[i]= malloc(c.length[i]* sizeof(int));
        memset(c2.off[i], -1, c.length[i]* sizeof(int));

        remap[i]= malloc(c.length[i]* sizeof(int));
        
        for(k=j=0; j<c.length[i]; j++){
            int v= c.data[i][j];
            if(v == ' ' || v == '\t' || v == '\n')
                continue;
            c2.data[i][k]= v;
            remap[i][k++]= j;
        }
        c2.length[i]= k;
        c2.data[i][k]= 0;
    }
    if(c.flags & MNDIFF_FLAG_VERBOSE)
        fprintf(stderr, "prepass...\n");
    if(mndiff(c2) < 0){
        fprintf(stderr, "internal error during prepass\n");
        exit(1);
    }

    for(k=0; k<c2.length[0]; k++){
        int i0, i1;
        
        if(c2.off[0][k] == -1)
            continue;
            
        i0= remap[0][k];
        i1= remap[1][ c2.off[0][k] ];

        assert(c.off[0][i0] == -1);
        assert(c.off[1][i1] == -1);
        c.off[0][i0]= i1;
        c.off[1][i1]= i0;
    }
}

int main(int argc,char* argv[]){
    int i;
    int file_count=0;
    FILE *file[2];
    int threshold=256*256*256;
    char *seperator[6]={
    "@@++\n", "\n@@--\n",
    "<+","+>",
    "<-","->"};
    int context=3;
    int prepass= 0;
    DiffCore c;
    
    memset(&c, 0, sizeof(DiffCore));
    c.match_score= 20;
    c.boundary_score= -50;
    c.reorder_score= -2000;
    
    for(i=1; i<argc; i++){
        if(!strcmp(argv[i], "-h")){
            printf(
                "mndiff 0.1 Copyright (C) 2005 Michael Niedermayer\n"
                "usage: mndiff [options] <file1> <file2>\n"
                "   -h        this help text\n"
                "   -ms <num> matching score                    default=20\n"
                "   -bs <num> boundary score                    default=-50\n"
                "   -rs <num> reorder score                     default=-2000\n"
                "   -wp       whitespace ignoring prepass\n"
                "   -v        verbose\n"
                "   -v -v     debug\n"
                "   -p        \n"
                "   -c        cute ANSI/VT100 colors\n"
                "   -t        ugly HTML colors\n"
                "   -C <num>  number of context lines shown     default=3\n");
            exit(1);
        }else if(!strcmp(argv[i], "-C")){
            context= atoi(argv[++i]);
        }else if(!strcmp(argv[i], "-ms")){
            c.match_score= atoi(argv[++i]);
        }else if(!strcmp(argv[i], "-bs")){
            c.boundary_score= atoi(argv[++i]);
        }else if(!strcmp(argv[i], "-rs")){
            c.reorder_score= atoi(argv[++i]);
        }else if(!strcmp(argv[i], "-wp")){
            prepass= 1;
        }else if(!strcmp(argv[i], "-p")){
            c.flags |= MNDIFF_FLAG_PROGRESS;
        }else if(!strcmp(argv[i], "-v")){
            if(c.flags & MNDIFF_FLAG_VERBOSE)
                c.flags |= MNDIFF_FLAG_DEBUG;
            c.flags |= MNDIFF_FLAG_VERBOSE;
        }else if(!strcmp(argv[i], "-c")){
            seperator[2] = "\e[32;4m";
            seperator[3] = "\e[0m";
            seperator[4] = "\e[31;4m";
            seperator[5] = "\e[0m";
        }else if(!strcmp(argv[i], "-t")){
            seperator[0] = "<pre>";
            seperator[1] = "</pre><hr>";
            seperator[2] = "<u><b><font color=\"#800000\">";
            seperator[3] = "</font></b></u>";
            seperator[4] = "<u><b><font color=\"#008000\">";
            seperator[5] = "</font></b></u>";
            printf("<html><head><title>Diff</title></head><body>\n"); //FIXME closing tags too
        }else{
            if(file_count >= 2){
                fprintf(stderr, "too many files specified\n");
                exit(1);
            }
            file[file_count]= fopen(argv[i], "r");
            if(!file[file_count]){
                fprintf(stderr, "cant open \"%s\"\n", argv[i]);
                exit(1);
            }
            file_count++;
        }
    }
    if(file_count < 2){
        fprintf(stderr, "too few files specified\n"); //FIXME stdin
        exit(1);
    }

    for(i=0; i<2; i++){
        c.length[i]= fsize(file[i]);
        c.data[i]= malloc(c.length[i]+1);
        c.data[i][c.length[i]]= 0;
        c.off[i]= malloc(c.length[i]* sizeof(int));
        memset(c.off[i], -1, c.length[i]* sizeof(int));
        fread(c.data[i], 1, c.length[i], file[i]);
        fclose(file[i]);
    }
    
    if(prepass)
        do_prepass(c);
    
    if(c.flags & MNDIFF_FLAG_VERBOSE)
        fprintf(stderr, "diffing ...\n");
    if(mndiff(c) < 0){
        fprintf(stderr, "internal error during diffing\n");
        exit(1);
    }
    
    if(c.flags & MNDIFF_FLAG_VERBOSE)
        fprintf(stderr, "visualizing ...\n");
    if(vis(stdout, c.data, c.length, c.off, seperator, context) < 0){
        fprintf(stderr, "internal error during visualization\n");
        exit(1);
    }

    return 0;
}
