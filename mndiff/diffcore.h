/*
 *  mndiff Copyright (C) 2005 Michael Niedermayer <michaelni@gmx.at>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef DIFFCORE_H
#define DIFFCORE_H

//javadoc FIXME

#define MNDIFF_FLAG_PROGRESS 1
#define MNDIFF_FLAG_VERBOSE  2
#define MNDIFF_FLAG_DEBUG    4

typedef struct DiffCore{
    int match_score;
    int boundary_score;
    int reorder_score;
    int *off[2];
    int *temp_off[2];
    uint8_t *data[2];
    int length[2];
    int update_cache[2][4];
    int flags;
}DiffCore;

int mndiff(DiffCore c);

#endif /* DIFFCORE_H */

